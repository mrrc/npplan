# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 21.02.13
@summary: 
'''

from twisted.spread import pb
from twisted.internet import reactor

#funcName = 'runner'
funcName = 'getRunnerResults'

def main():
    factory = pb.PBClientFactory()
    reactor.connectTCP("localhost", 8800, factory)
    def1 = factory.getRootObject()
    def1.addCallbacks(got_obj1, err_obj1)
    reactor.run()


def err_obj1(reason):
    #print "error getting first object", reason
    reactor.stop()

def got_obj1(obj1):
    def2 = obj1.callRemote(funcName, 'tst05q')
    def2.addCallbacks(got_obj2)

def got_obj2(obj2):
    #print "got second object:", obj2
    print 'Function: %s' %funcName
    print len(obj2)
    j = 0
    for i in obj2:
        print 'obj%d' %j
        j += 1
        print i
    #print "telling it to do three(12)"
    #obj2.callRemote("three", 12)
    reactor.stop()

main()