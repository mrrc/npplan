# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 04.03.14
@summary: 
'''

from NPPlan.model.calc.geant.isodoseBuilder import isodoseBuilderFromNumpy
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import griddata
import matplotlib

filename = r'C:\Temp\ebt\cubes_out\cube_1_out\n82_001.txt'

allX = []
allY = []
splX = []
splY = []
data = []
dataDict = {}

for line in file(filename):
    if 'mm' in line:
        continue
    if '---' in line:
        continue
    #print line
    spl = line.split(' ')
    spl = [i for i in spl if i != '']
    x = float(spl[0])
    y = float(spl[1])
    val = float(spl[2].replace('\n', ''))
    #print x, y, val
    #allX.append(x)
    #allY.append(y)
    if x not in splX:
        splX.append(x)
    if y not in splY:
        splY.append(y)
    #data.append(val)
    try:
        dataDict[x][y] = val
    except KeyError:
        dataDict[x] = {}
        dataDict[x][y] = val

for x in dataDict:
    for y in dataDict[x]:
        allX.append(x)
        allY.append(y)
        data.append(dataDict[x][y])

allX = np.array(allX)
allY = np.array(allY)
data = np.array(data)
splX = np.array(splX)
splY = np.array(splY)
data2 = data.reshape((319, 289))
print allX.shape, allY.shape, data.shape
print splX.shape, splY.shape, data2.shape

#data = data.reshape((len(allX), len(allY)))
xi = np.linspace(0, 32, 300)
yi = np.linspace(0, 29, 330)
gridData = griddata(allX, allY, data, xi, yi)
print 'aaa'
#CS = plt.contour(splX, splY, data2, linewidths=1.0)
CS = plt.contour(xi, yi, gridData, linewidths=1.0)

plt.show()
