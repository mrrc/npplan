# -*- coding: utf8 -*-
'''
#@todo: REFACTOR ACCORDING TO DATABASE ENTRIES
@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 2
@date: 26.02.13
@summary: 
'''

import numpy

_DEBUG = True
if _DEBUG:
    import NPPlan.controller.logger as log
    global moduleName
    moduleName = 'model.calc.base.materials'
import operator

NPPLAN_MAT_NORMALIZE          = 0b00001
NPPLAN_MAT_NORMALIZE_NEGATIVE = 0b00010
NPPLAN_MAT_NOT_NORMALIZE      = 0b00100

NPPLAN_MAT_GET_TUPLE =    0b000001
NPPLAN_MAT_GET_MAT =      0b000010
NPPLAN_MAT_GET_NAME =     0b000100
NPPLAN_MAT_GET_ID =       0b001000
NPPLAN_MAT_GET_TRIPLE =   0b010000
NPPLAN_MAT_GET_DENSITY =  0b100000

def generateNames():
    """
    Создаёт массив соответствий названий элементов (ключ массива) и их атомных номеров (значение массива)
    в соответвие с периодической таблицей
    @return: массив
    @rtype: dict
    """
    lzd = {}

    lzd["BE"] = 04
    lzd["BA"] = 56
    lzd["BH"] = 107
    lzd["BI"] = 83
    lzd["BK"] = 97
    lzd["BR"] = 35
    lzd["RU"] = 44
    lzd["RE"] = 75
    lzd["RF"] = 104
    lzd["RG"] = 111
    lzd["RA"] = 88
    lzd["RB"] = 37
    lzd["RN"] = 86
    lzd["RH"] = 45
    lzd["TM"] = 69
    lzd["H"] = 01
    lzd["P"] = 15
    lzd["GE"] = 32
    lzd["GD"] = 64
    lzd["GA"] = 31
    lzd["OS"] = 76
    lzd["HS"] = 108
    lzd["ZN"] = 30
    lzd["HO"] = 67
    lzd["HF"] = 72
    lzd["HG"] = 80
    lzd["HE"] = 02
    lzd["PR"] = 59
    lzd["PT"] = 78
    lzd["PU"] = 94
    lzd["PB"] = 82
    lzd["PA"] = 91
    lzd["PD"] = 46
    lzd["PO"] = 84
    lzd["PM"] = 61
    lzd["C"] = 6
    lzd["K"] = 19
    lzd["O"] = 8
    lzd["S"] = 16
    lzd["W"] = 74
    lzd["EU"] = 63
    lzd["ES"] = 99
    lzd["ER"] = 68
    lzd["MD"] = 101
    lzd["MG"] = 12
    lzd["MO"] = 42
    lzd["MN"] = 25
    lzd["MT"] = 109
    lzd["U"] = 92
    lzd["FR"] = 87
    lzd["FE"] = 26
    lzd["FM"] = 100
    lzd["NI"] = 28
    lzd["NO"] = 102
    lzd["NA"] = 11
    lzd["NB"] = 41
    lzd["ND"] = 60
    lzd["NE"] = 10
    lzd["ZR"] = 40
    lzd["NP"] = 93
    lzd["B"] = 05
    lzd["CO"] = 27
    lzd["CM"] = 96
    lzd["F"] = 9
    lzd["CA"] = 20
    lzd["CF"] = 98
    lzd["CE"] = 58
    lzd["CD"] = 48
    lzd["V"] = 23
    lzd["CS"] = 55
    lzd["CR"] = 24
    lzd["CU"] = 29
    lzd["SR"] = 38
    lzd["KR"] = 36
    lzd["SI"] = 14
    lzd["SN"] = 50
    lzd["SM"] = 62
    lzd["SC"] = 21
    lzd["SB"] = 51
    lzd["SG"] = 106
    lzd["SE"] = 34
    lzd["YB"] = 70
    lzd["DB"] = 105
    lzd["DY"] = 66
    lzd["DS"] = 110
    lzd["LA"] = 57
    lzd["CL"] = 17
    lzd["LI"] = 03
    lzd["TL"] = 81
    lzd["LU"] = 71
    lzd["LR"] = 103
    lzd["TH"] = 90
    lzd["TI"] = 22
    lzd["TE"] = 52
    lzd["TB"] = 65
    lzd["TC"] = 43
    lzd["TA"] = 73
    lzd["AC"] = 89
    lzd["AG"] = 47
    lzd["I"] = 53
    lzd["IR"] = 77
    lzd["AM"] = 95
    lzd["AL"] = 13
    lzd["AS"] = 33
    lzd["AR"] = 18
    lzd["AU"] = 79
    lzd["AT"] = 85
    lzd["IN"] = 49
    lzd["Y"] = 39
    lzd["N"] = 07
    lzd["XE"] = 54
    lzd["CN"] = 112
    lzd["FL"] = 114
    lzd["LV"] = 116

    return lzd

def mendeleevName2num(name):
    """
    Возвращает атомный номер элемента в периодической таблице от имени элемента
    @param name: имя
    @return: int
    """
    global names
    return names[name]

def num2mendeleevName(num):
    """
    Возвращает имя элемента периодической таблица от атомного номера
    @param num: номер элемента
    @return: string
    """
    global names
    for i in names:
        if names[i] == int(num):
            return i

global names
names = generateNames()

class materialContainer(object):
    def __init__(self, outerCell=-70, target='mcnp'):
        self._mats = {}
        self._mat2names = {}
        self._names2mat = {}
        self._mat2density = {}
        self._target = target

        self._defaults = {
            'outerCell' : outerCell,
        }

    def add(self, cMap={}, id=-1, name='', density='', univ='', cell='', outerCell=''):
        #if -1 == id:
        #    id = self.genNextId()
        id = self.genNextId()
        if self._mats.has_key(id):
            # already has this key
            # @todo: throw exception (?)
            return -1
        id = int(id)
        self._mat2names[id] = name
        if 0 != len(name):
            self._names2mat[name] = id

        if '' == outerCell:
            outerCell = self._defaults['outerCell']

        self._mats[id] = material(cMap, id, density, univ, cell, outerCell, container=self)
        self._mat2density[id] = density
        pass

    def genNextId(self):
        if 0 != len(self._mats):
            return sorted(self._mats.keys())[-1]+1
        else:
            if 'mcnp' == self._target:
                return 1
            elif 'geant' == self._target:
                return 0

    def get(self, param, flag=NPPLAN_MAT_GET_MAT):
        try:
            if isinstance(param, int):
                # передан id материала
                id = param
            else:
                # передано имя материала
                id = self._names2mat[param]
            if flag & NPPLAN_MAT_GET_MAT:
                return self._mats[id]
            elif flag & NPPLAN_MAT_GET_NAME:
                return self._mat2names[id]
            elif flag & NPPLAN_MAT_GET_TUPLE:
                return self._mat2names[id], self._mats[id]
            elif flag & NPPLAN_MAT_GET_ID:
                return id
            elif flag & NPPLAN_MAT_GET_TRIPLE:
                return id, self._mat2names[id], self._mats[id]
            elif flag & NPPLAN_MAT_GET_DENSITY:
                return self._mat2density[param]
        except IndexError:
            return -1

    def list(self, flag=NPPLAN_MAT_GET_MAT):
        # @todo: implement flag
        for mat in self._mats:
            yield self._mats[mat]


    def printAsString(self):
        print self.getAsString()

    def getAsString(self):
        ret = ''
        for i in self._mats:
            ret = "%s %s:\n"  %(ret, self._mat2names[i],)
            ret = "%s %s\n" %(ret, self._mats[i].getAsString())
        return ret

    def __str__(self):
        return self.getAsString()

    def getClosestLowerByDensity(self, targetDensity, flag=NPPLAN_MAT_GET_MAT):
        #print self._mat2density
        #print sorted(self._mat2density)
        #print sorted(self._mat2density.iteritems(), key=operator.itemgetter(1))
        sortedItems = sorted(self._mat2density.iteritems(), key=operator.itemgetter(1))
        ii = sortedItems[0][0]
        for (num, dens) in sortedItems:
            #print num, dens
            if targetDensity >= dens:
                ii = num
        return self.get(ii, flag)

    def getIdNameList(self):
        return [(elem, self._mat2names[elem]) for elem in sorted(self._mat2names)]
        pass

    def iterateIdNameList(self):
        for elem in sorted(self._mat2names):
            yield (elem, self._mat2names[elem])

    def getMaterialsQuantity(self):
        return len(self._mats)

    def dcmListIterator(self, flag=NPPLAN_MAT_GET_MAT):
        if flag & NPPLAN_MAT_GET_MAT:
            for item in sorted(self._mat2names):
                yield self._mats[item]
        elif flag & NPPLAN_MAT_GET_ID:
            for item in sorted(self._mat2names):
                yield item

    def write2G4InputFormat(self, _stream):
        '''
        @param _stream: поток в который надо писать
        @return: file
        '''
        for el in self.iterateIdNameList():
            _stream.write('%d %s\n' % el)


class material(object):
    def __init__(self, cMap={}, id='', density=0, univ='', cell='', outerCell=-70, container=None):
        self.cm = compositeMap(cMap, NPPLAN_MAT_NORMALIZE_NEGATIVE)
        self._id = int(id)
        self._density = float(density)
        self._outerCell = outerCell
        if container is not None:
            self._container = container
        if not univ:
            self._univ = self._id
        if not cell:
            self._cell = self._id

    def getId(self):
        return self._id
    id = property(getId)

    def toMCNPMatCardString(self, newlines=False):
        retStr = "m%d " %self._id
        #retStr += self.cm.toMCNPString(True)
        retStr += self.cm.toMCNPString(newlines)
        return retStr

    def toMCNPDensityString(self):
        #  1      1  -1.060000     -70  u=    1
        return "%d      %d  %1.6f     %d  u=    %d" %(self._cell, self._id, self._density, self._outerCell, self._univ)

    def printAsString(self):
        print self.getAsString()

    def getAsString(self):
        ret = " Density: %f\n  ID: %d" %(self._density, self._id)
        #print '  Density: ', self._density
        #print '  ID: ', self._id
        ret = "%s\n%s" % (ret, self.cm.getAsString())
        return ret

    def __str__(self):
        return self.getAsString()
        #self.cm.printAsString()        #

    def toG4cc(self):
        '''
         @example:
          G4Material* lunginhale = new G4Material( "LungInhale",
                               density = 0.217*g/cm3,
                               numberofElements = 9);
          lunginhale->AddElement(elH,0.103);
          lunginhale->AddElement(elC,0.105);
          lunginhale->AddElement(elN,0.031);
          lunginhale->AddElement(elO,0.749);
          lunginhale->AddElement(elNa,0.002);
          lunginhale->AddElement(elP,0.002);
          lunginhale->AddElement(elS,0.003);
          lunginhale->AddElement(elCl,0.002);
          lunginhale->AddElement(elK,0.003);

        @return: string
        '''
        rStr = ''
        name = self._container.get(self._id, NPPLAN_MAT_GET_NAME)

        print name
        print self.cm.quantity
        rStr += 'G4Material* %s = new G4Material( "%s",\n' %(name.lower(), name)
        rStr += '                               density = %0.3f*g/cm3,\n' %(self._density)
        rStr += '                               numberofElements = %d);\n' %(self.cm.quantity)

        for elem in self.cm.list():
            rStr += '%s->AddElement(el%s,%0.3f);\n' %(name.lower(), elem['element'].name, -1.0*elem['normedWeight'])
        return rStr
        pass

    def toG4dcmList(self):
        rStr = ''
        for item in sorted(self._container._mat2names):
            rStr += '%d %s\n' % (item, self._container._mat2names[item])
            #print item
        return rStr
        pass

    def __contains__(self, item):
        return item in self.cm

    def __repr__(self):
        return 'Material %s with density %0.4f and cmap of %d elements: %s' % (
            self._id,
            self._density,
            self.cm.quantity,
            self.cm,
        )

class compositeMap(object):
    def __init__(self, elements={}, normalizationFlag=NPPLAN_MAT_NORMALIZE_NEGATIVE):
        self._map = {}
        #self._density = density
        self._normalization = normalizationFlag
        self._elemFormat = "%s %0.06f"

        if isinstance(elements, str):
            if 'm' == elements[0]:
                self._materialId = int(elements.split(' ')[0][1:])
                elements = elements.split(' ')[1:].join(' ')
            else:
                #self._materialId = int(id)
                self.mapFromString(elements)
            pass
        elif isinstance(elements, compositeMap):
            self._map = elements.getMap()
        else:
            #self._materialId = int(id)
            self.mapFromDict(elements)
        self.normalize()
        pass

    def getQuantity(self):
        return len(self._map)
    quantity = property(getQuantity)

    def normalize(self):
        # @todo: implement normalize positive with factor
        if self._normalization & NPPLAN_MAT_NOT_NORMALIZE:
            for i in self._map:
                self._map['normedWeight'] = self._map['weight']
        elif self._normalization & NPPLAN_MAT_NORMALIZE_NEGATIVE:
            weights = 0.0
            for i in self._map:
                weights += self._map[i]['weight']
            for i in self._map:
                self._map[i]['normedWeight'] = -1.0 * self._map[i]['weight'] / weights
            pass

    def mapFromString(self, elements):
        # @todo: implement
        elements = elements.replace("\n", ' ')

    def getMap(self):
        if 0 != len(self._map):
            return self._map

    def mapFromDict(self, elements):
        for elem in elements:
            elemObj = element(elem)
            self._map[elemObj.id] = {
                'element' : elemObj,
                #'id' : self._materialId,
                #'density' : self._density,
                'weight' : float(elements[elem]),
            }
            pass
        pass

    def printAsString(self):
        print self.getAsString()

    def getAsString(self):
        ret = ""
        for e in self._map:
            #print elem
            elem = self._map[e]
            #print '   ', num2mendeleevName(elem['element'].atomicNumber), elem['normedWeight']
            ret = "%s   %2s  %f\n" %(ret, num2mendeleevName(elem['element'].atomicNumber), elem['normedWeight'])
        return ret


    def out(self):
        print self._map

    def list(self):
        for i in self._map:
            yield self._map[i]

    def toMCNPString(self, newlines=False):
        #retStr = "m%d " %self._materialId
        retStr = ''
        if newlines:
            delim = " &\n"
        else:
            delim = ' '
        for elem in self._map:
            _cstr = self._elemFormat %(self._map[elem]['element'].toMCNPString(), self._map[elem]['normedWeight'])
            retStr = delim.join((retStr, _cstr))
        return retStr[len(delim):]

    def __contains__(self, item):
        if isinstance(item, str):
            item = element(item)
        for i in self._map:
            if self._map[i]['element'] == item:
                return True
        return False

    def __repr__(self):
        s = ''
        for i in self._map:
            if 0 == self._map[i]['element'].getIsotopeNumber():
                # @todo: wtf????
                continue
            s = '%s %s:%s' % (s, self._map[i]['element'].getIsotopeNumber(), self._map[i]['normedWeight'])
        return s

class element(object):
    def __init__(self, atomicNumber=1, isotopeNumber=1, library=''):
        global names
        self._namesTable = names
        self._numberFormat = '%d%03d'

        if isinstance(atomicNumber, str):
            self.fromString(atomicNumber)
        else:
            self._atomicNumber = atomicNumber
            self._isotopeNumber = isotopeNumber
            self._library = library

    def fromString(self, elementString):
        if '-' in elementString or not elementString[0].isdigit():
            try:
                self._atomicNumber = self._namesTable[elementString.split('-')[0]]
                self._isotopeNumber = int(elementString.split('-')[1])
                self._name = str(elementString.split('-')[0])
                self._library = ''
            except IndexError:
                self._atomicNumber = self._namesTable[elementString]
                self._name = str(elementString)
                self._isotopeNumber = 0
                self._library = ''
        else:
            if '.' not in elementString:
                # элемент без библиотеки
                elementInt = int(elementString)
                self._isotopeNumber = int(elementInt % 1000)
                #self._name = self._namesTable.keys()[self._namesTable.values()[self._isotopeNumber]]
                self._name = num2mendeleevName(self._isotopeNumber)
                self._atomicNumber = int(elementInt / 1000)
                self._library = ''
            else:
                self.fromString(elementString.split('.')[0])
                #self._name = self._namesTable.keys()[self._namesTable.values()[elementString.split('.')[0]]]
                self._library = elementString.split('.')[1]

    def getAtomicNumber(self):
        return self._atomicNumber
    atomicNumber = property(getAtomicNumber)

    def getIsotopeNumber(self):
        return self._isotopeNumber
    isotopeNumber = property(getIsotopeNumber)

    def getLibrary(self):
        return self._library
    library = property(getLibrary)

    def getName(self):
        return self._name
    name = property(getName)

    def toMCNPString(self):
        if '' == self._library:
            return self._numberFormat % (self.atomicNumber, self.isotopeNumber)
        else:
            return '%s.%s' % ((self._numberFormat % (self.atomicNumber, self.isotopeNumber)), self._library)

    def toString(self):
        return self.toMCNPString()

    def createId(self):
        return self._numberFormat % (self.atomicNumber, self.isotopeNumber)
    id = property(createId)

    def __repr__(self):
        return self.toString()

    def __eq__(self, other):
        return other.atomicNumber == self.atomicNumber and other.isotopeNumber == self.isotopeNumber

def setStandartMats(mats):
    """
    @param mats: материалы
    @type mats: materialContainer
    @return: класс
    @rtype: materialContainer
    """
    mats.add(
        name='Air',
        id=1,
        cMap={
            'O' : 20,
            'N' : 80,
            #'O-16' : 2317,
            #'N-14' : 7552,
            #'C' : 0001,
            #'AR' : 128,
        },
        density=0.001,
    )

    mats.add(
        name='AdiposeTissue',
        id=2,
        density=0.950,
        cMap = {
            'H' : 11.4,
            'C' : 58.9,
            'N' : 0.7,
            'O' : 28.7,
            'NA' : 0.1,
            'S' : 0.1,
            'CL' : 0.1,
        },
    )

    mats.add(
        name='Water',
        id=4,
        density=1.043,
        cMap = {
            'H' : 2,
            'O' : 1,
        },
    )

    mats.add(
        name='NormalTissue',
        id=3,
        density=1.06,
        cMap = {
            '1001' : '0.102000',
            '6000' : '0.143000',
            '7014' : '0.034000',
            '8016' : '0.708000',
           '11023' : '0.002000',
           '15031' : '0.003000',
           '16000' : '0.003000',
           '17000' : '0.002000',
           '19000' : '0.003000',
        }
    )

    mats.add(
        name='Bone',
        id=6,
        density=1.930,
        cMap = {
            'H' : 3.6,
            'C' : 15.9,
            'N' : 4.2,
            'O' : 44.8,
            'NA' : 0.3,
            'MG' : 0.2,
            'P' : 9.4,
            'S' : 0.3,
            'CA' : 21.3,
        },
    )

    return mats

if __name__ == '__main__':
    mats = materialContainer(target='geant')
    mats = setStandartMats(mats)

    #mats.printAsString()
    #id, name, mat = mats.getClosestLowerByDensity(1, NPPLAN_MAT_GET_TRIPLE)
    #print id, name, mat.getAsString()
    #print mat.toMCNPMatCardString(True)
    #for i in mats.list():
        #print i.toMCNPMatCardString(True)
        #print i.toG4cc()
    #    print i.toG4dcmList()
    for i in mats.dcmListIterator(NPPLAN_MAT_GET_ID):
        print i
    #print mats.getClosestLowerByDensity(1, NPPLAN_MAT_GET_DENSITY)