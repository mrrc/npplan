# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 12.02.13
@summary: 
'''

from time import strptime
import numpy as np


class mcnpReaderBase(object):
    """
    Базовый класс для чтения выходных данных mcnp-файла
    """
    def __init__(self, filename='', multiplier=1.0):
        """
        @param filename: путь к файлу
        @type filename: string
        @param multiplier: коэффициент
        @type multiplier: float
        """
        self._multiplier = multiplier
        self._filename = filename
        self._filestream = open(self._filename, 'r')

        self._runnerInfo = {}
        self.readInfo()

        self._params = {}
        self._data = {}
        self.readValues()

    def makeOriginalFileName(self):
        """
        Обрезает последнюю букву файла
        @return: имя файла без m
        @rtype: string
        """
        return self._filename[:-1]

    def readInfo(self):
        """
        Читает информацию о запуске: версию расчётной программы, время запуска и имя программы
        """
        line = self._filestream.readline()
        #program, version, date, time = line.split(' ')
        splitted = [i for i in line.split(' ') if i != '']
        self._runnerInfo['version'] = '%s %s' %(splitted[0], splitted[1])
        self._runnerInfo['start'] = strptime("%s %s" %(splitted[2], splitted[3]), '%m/%d/%y %H:%M:%S')
        self._runnerInfo['dumpsQuantity'] = int(splitted[4])
        self._runnerInfo['nps'] = int(splitted[5])
        self._runnerInfo['rnQuantity'] = int(splitted[6])
        self._runnerInfo['name'] = self._filestream.readline()
        # print self._runnerInfo
        #print program, version, date, time

    def readValues(self):
        self.readTalliesParams()
        self.readTallyData()
        #for line in self._filestream.readlines():
        #    print line

    def _skinUntil(self):
        pass

    def getTallies(self):
        return self._data.keys()

    def readTalliesParams(self):
        """
        Читает количество и список tally (строка ntal и следующая)
        """
        line = self._filestream.readline()
        while 'ntal' not in line:
            # skipping lines before ntal
            line = self._filestream.readline()

        self._params['talliesQuantity'] = int(line.split(' ')[-1])
        line = self._filestream.readline()
        self._params['talliesList'] = [int(i) for i in line.split(' ') if i != '']

        #print self._params

    def readTallyData(self):
        """
        Читает в цикле данные для каждого tally.
        Записывает параметры в массив self._data
        Ключ для записи - номер tally
        Записываемые параметры:
          info - данные от tally до следующей строки
          quantity - количество
          fDataCells - np.array(номера ячеек, в которых считается tally)
          fValues - np.array(значения по порядку)
          fErrors - np.array(ошибки по порядку)
        """
        for i in self._params['talliesList']:
            self._data[i] = {}
            curData = self._data[i]
            while 1:
                line = self._filestream.readline()
                if 'tally' in line:
                    break
            type = [int(i) for i in line.split(' ')[1:] if i != '']
            # print type
            curData['info'] = type
            #print self._data
            while 1:
                line = self._filestream.readline()
                if 'f' == line[0]:
                    break
                try:
                    curData['info'].extend([int(i) for i in line.split(' ')[1:] if i != ''])
                except ValueError:
                    curData['name'] = line
            #a = np.array()
            curData['quantity'] = int(line.split(' ')[-1])
            #print self._data
            fDataCells = []
            while 1:
                line = self._filestream.readline()
                if line[0] == 'd':
                    break
                fDataCells.extend([int(i) for i in line.split(' ')[1:] if i != ''])
            curData['fDataCells'] = np.array(fDataCells, dtype=np.int16)
            curData['others'] = {}
            while 1:
                spl = line.split(' ')
                curData['others'][spl[0]] = int(spl[-1])
                line = self._filestream.readline()
                if 'vals' in line:
                    break
            #print self._data
            vals = []
            errs = []
            for i in range(curData['quantity']):
                line = self._filestream.readline()
                if 'tfc' in line:
                    break
                parsedLine = self.parseLine(line)
                vals.extend(parsedLine[0])
                errs.extend(parsedLine[1])
                pass
            curData['fValues'] = np.array(vals, dtype=np.float64)
            if -1.0 != self._multiplier:
                curData['fValues'] *= self._multiplier
            curData['fErrors'] = np.array(errs, dtype=np.float64)
            #print self._data
            pass

        pass

    def parseLine(self, line):
        """
        Парсит строку в два разных массива (значения и ошибки)
        @param line: строка
        @type line: string
        @return: ([val1, val2,...], [err1, err2,...])
        @rtype: tuple
        """
        q = [float(i) for i in line.split(' ')[1:] if i != '']
        return [q[i] for i in range(len(q)) if i % 2 == 0], [q[i] for i in range(len(q)) if i % 2 == 1]

    def getFirstTally(self, sort=False):
        if sort:
            return sorted(self._data.keys())[0]
        return self._data.keys()[0]


class mcnpReaderSimple(mcnpReaderBase):
    def __init__(self, filename=''):
        mcnpReaderBase.__init__(self, filename)

    def getValueInCell(self, cell, tally=-1):
        """
        Возвращает значение в переданной ячейке cell для заданного tally
        @param cell: ячейка
        @type cell: int
        @param tally: tally
        @type tally: int
        @return: значение
        @rtype: float
        """
        if -1 == tally:
            tally = self.getFirstTally()
        #ind = self._data[tally]['fDataCells'].index(cell)
        #ind = self._data[tally]['fDataCells'].where(item==cell)
        ind = np.where(self._data[tally]['fDataCells']==cell)
        return float(self._data[tally]['fValues'][ind])

    def getErrorInCell(self, cell, tally=-1):
        """
        Возвращает ошибку в переданной ячейке cell для заданного tally
        @param cell: ячейка
        @type cell: int
        @param tally: tally
        @type tally: int
        @return: ошибка
        @rtype: float
        """
        if -1 == tally:
            tally = self.getFirstTally()
        ind = np.where(self._data[tally]['fDataCells']==cell)
        return float(self._data[tally]['fErrors'][ind])

    def getValueTupleInCell(self, tally=-1, cell=-1):
        """
        Возвращает tuple(значение, ошибка) в переданной ячейке cell для заданного tally
        @param cell: ячейка
        @type cell: int
        @param tally: tally
        @type tally: int
        @return: (значение, ошибка)
        @rtype: tuple
        """
        if -1 == tally:
            tally = self.getFirstTally()
        ind = np.where(self._data[tally]['fDataCells']==cell)
        return float(self._data[tally]['fValues'][ind]), float(self._data[tally]['fErrors'][ind])


class mcnpReaderGrid(mcnpReaderBase):
    def __init__(self, filename='', shape=(1, 0, 0), multiplier=1.0, sim=None, noshape=False):
        self._shape = shape
        self._noshape = noshape
        if sim is not None:
            self._simulation = sim
        mcnpReaderBase.__init__(self, filename, multiplier)
        self.reshape()

    def acquireSimulation(self, sim):
        """
        Привязывает ридер к своему классу моделирования
        @param sim: объект класса моделирования
        @type sim: NPPlan.model.calc.base.simulation.simulationBase
        @return:
        """
        self._simulation = sim

    def getShape(self):
        return self._shape

    def _reshape(self, arr):
        # distance = [[[0 for k in xrange(n)] for j in xrange(n)] for i in xrange(n)]
        newArr = [[[0.0 for k in xrange(self._shape[2])] for j in xrange(self._shape[1])] for i in xrange(self._shape[0])]
        #newArr = np.array([]*self._shape[0])
        #for i in range(self._shape[0]):
        #    newArr[i] = np.array([]*self._shape[1])
        #    for j in range(self._shape[1]):
        #        newArr[i][j] = np.array([]*self._shape[2])

        for i in range(self._shape[0]):
            for j in range(self._shape[1]):
                for k in range(self._shape[2]):
                    #ix + iy*noX + iz*noX*noY;
                    ind = i + j*self._shape[0] + k*self._shape[0]*self._shape[1]
                    newArr[i][j][k] = arr[ind]
        return np.array(newArr)
        pass

    def reshape(self):
        for i in self._data:
            vals = self._data[i]['fValues']
            errs = self._data[i]['fErrors']
            #self._data[i]['shapedValues'] = vals.reshape(self._shape)
            #self._data[i]['shapedErrors'] = errs.reshape(self._shape)
            if not self._noshape:
                self._data[i]['shapedValues'] = self._reshape(vals)
                self._data[i]['shapedErrors'] = self._reshape(errs)

            # @todo: move to another function
            if hasattr(self, '_simulation') and self._simulation is not None:
                self._data[i]['massesValues'] = []
                for val in range(len(vals)):
                    self._data[i]['massesValues'].append(self._simulation._inputObject.getter(val)['mass'] * vals[val]
                    )
                self._data[i]['massesValues'] = np.array(self._data[i]['massesValues'])
            self._data[i]['max'] = np.max(vals)
            self._data[i]['meanError'] = np.mean(errs[errs>0.0])
            #self._data[i]['meanErrorsPerSlice'] = #np.mean(np.mean(self._data[i]['shapedErrors'], axis=1), axis=2)
            #self._data[i]['maxValuesPerSlice'] = #np.max(np.max(self._data[i]['shapedErrors'], axis=1), axis=2)
            if not self._noshape:
                self._data[i]['meanErrorsPerSlice'] = np.mean(self._data[i]['shapedErrors'], axis=2)
                self._data[i]['maxValuesPerSlice'] = np.max(self._data[i]['shapedErrors'], axis=2)
        #print 'test masses values', self._data

    def getValueInCell(self, tally=-1, x=-1, y=-1, z=-1):
        """
        Возвращает значение в ячейке с tally по адресy x (если число), либо (x, y, z) если type(x) == tuple или 3 координаты
        @param tally: tally
        @type tally: int
        @param x: координата, одно число, tuple или первая координата трёхмерного массива (номер среза)
        @type x: int or tuple
        @param y: вторая координата трёхмерного массива
        @type y: int
        @param z: третья координата трёхмерного массива
        @type z: int
        """
        if tally == -1:
            tally = self.getFirstTally()
        if isinstance(x, int) and -1 == y and -1 == z:
            return self._data[tally]['fValues'][x]
        elif isinstance(x, tuple):
            x, y, z = x
        return self._data[tally]['shapedValues'][x, y, z]

    def getErrorInCell(self, tally=-1, x=-1, y=-1, z=-1):
        """
        Возвращает ошибку в ячейке с tally по адресy x (если число), либо (x, y, z) если type(x) == tuple или 3 координаты
        @param tally: tally
        @type tally: int
        @param x: координата, одно число, tuple или первая координата трёхмерного массива (номер среза)
        @type x: int or tuple
        @param y: вторая координата трёхмерного массива
        @type y: int
        @param z: третья координата трёхмерного массива
        @type z: int
        """
        if tally == -1:
            tally = self.getFirstTally()
        if isinstance(x, int) and -1 == y and -1 == z:
            return self._data[tally]['fErrors'][x]
        elif isinstance(x, tuple):
            x, y, z = x
        return self._data[tally]['shapedErrors'][x, y, z]

    def getValueTupleInCell(self, tally=-1, x=-1, y=-1, z=-1):
        """
        Возвращает ошибку в ячейке с tally по адресy x (если число), либо (x, y, z) если type(x) == tuple или 3 координаты
        @param tally: tally
        @type tally: int or tuple
        @param x: координата, одно число, tuple или первая координата трёхмерного массива (номер среза)
        @type x: int or tuple
        @param y: вторая координата трёхмерного массива
        @type y: int
        @param z: третья координата трёхмерного массива
        @type z: int
        """
        if isinstance(tally, tuple):
            # сумма tally
            acc = 0
            for t in tally:
                try:
                    acc += self._data[t]['fValues'][x]
                except TypeError:
                    acc += self._data[t]['shapedValues'][x]
            return acc, None
        if tally == -1:
            tally = self.getFirstTally()
        if isinstance(x, int) and -1 == y and -1 == z:
            return self._data[tally]['fValues'][x], self._data[tally]['fErrors'][x]
        elif isinstance(x, tuple):
            x, y, z = x
        return self._data[tally]['shapedValues'][x, y, z], self._data[tally]['shapedErrors'][x, y, z]

    def printValuesForTally(self, tally=-1, stream=None):
        if stream is None:
            from sys import stdout as oStream
            stream = oStream
        if -1 == tally:
            try:
                tally = 6
                self._data_[tally]
            except KeyError:
                tally = self.getFirstTally()
        for i in self._data[tally]['fValues']:
            print >> stream, i

    def printErrorsForTally(self, tally=-1, stream=None):
        if stream is None:
            from sys import stdout as oStream
            stream = oStream
        if -1 == tally:
            try:
                tally = 6
                self._data_[tally]
            except KeyError:
                tally = self.getFirstTally()
        for i in self._data[tally]['fErrors']:
            print >> stream, i

if __name__ == '__main__':
    fName = r'c:\mcnpx\executable\ng1voxm'
    obj = mcnpReaderGrid(fName, shape=(30, 30, 10))
    fName = r'c:\mcnpx\executable\ng1vox2m'
    obj2 = mcnpReaderGrid(fName, shape=(10, 30, 30))
    print 'aaa'
    #obj.printValuesForTally(16)
    print obj.getValueInCell(4, 900)
    #1520 20 20 1
    print obj.getValueInCell(4, 0, 0, 1)
    #print obj.getValueInCell(16, 1, 20, 20)
    #print obj.getValueInCell(16, 9, 12, 4)
    #print 'aaa'
    #obj2.printValuesForTally(16)
    #print 'ccc'
    #for i in range(10*30*30):
    #    print i
        #print obj.getValueInCell(6, i), obj.getValueInCell(36, i), 100 * obj.getValueInCell(36, i) / obj.getValueInCell(6, i), \
        #    obj2.getValueInCell(6, i), obj2.getValueInCell(36, i), 100 * obj2.getValueInCell(36, i) / obj2.getValueInCell(6, i)
    #    print '%1.8f   %1.8f' % (obj.getValueInCell(6, i), obj2.getValueInCell(6, i))



    #print obj
    #print obj.getTallies()
    print '\n'
    obj.printValuesForTally(16)
    obj.printErrorsForTally(6)
    '''
    t = {}
    for i in range(len(obj._data[6]['fValues'])):
        t[i] = {
            6: obj._data[6]['fValues'][i],
            16: obj._data[16]['fValues'][i],
            26: obj._data[26]['fValues'][i],
            36: obj._data[36]['fValues'][i],
            's': obj._data[16]['fValues'][i] + obj._data[26]['fValues'][i] + obj._data[36]['fValues'][i],
            's2': obj.getValueTupleInCell((16, 26, 36), i)
        }
    print t
    import pprint
    pprint.pprint(t) '''
