# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 11.07.13
@summary: 
'''

import NPPlan
import numpy as np
from NPPlan.model.calc.base.aggregate import aggregateMethodBase


class baseMatsMethod(aggregateMethodBase):
    def __init__(self, planParent, sliceIterator):
        aggregateMethodBase.__init__(self, planParent, sliceIterator)
        self._plan = planParent
        self._sliceIterator = sliceIterator
        self._compression = 16
        self._sliceData = {}
        self._rowsColumns = self._plan.planGetRowsColumns()
        self._matsApproximationValue = 0.2
        self._matStartNum = 500
        self._registeredMats = {
            'list': [],
            'data': {},
            'base': {},
            'densities': {},
        }

        self.air = NPPlan.mongoConnection.material.find_one({'name': 'Air'})
        self.bone = NPPlan.mongoConnection.material.find_one({'name': 'BoneGeneral'})
        self.tissue = NPPlan.mongoConnection.material.find_one({'name': 'TissueGeneral'})
        #tumour = NPPlan.mongoConnection.material.find_one({'name': 'TissueGeneral'})

    def work(self):
        import pprint
        for i, elem in self._sliceIterator():
            npString = np.copy(elem.getNumpyString())
            slope, intercept = self._plan.planGetRescaleParams(1)
            npString *= slope
            npString += intercept
            #print npString
            print 'slice', i, 'started!'
            self._sliceData[i] = self.npToMatGrid(npString)
            print 'slice', i, 'done!'
            #print 'rMat'
            #pprint.pprint(self._registeredMats)
            #print np.max(elem.getNumpyString()), np.min(elem.getNumpyString())
        #print self._registeredMats
        #print self._
        pprint.pprint(self._registeredMats)
        pprint.pprint(self._sliceData)
        pass

    def pixelValue(self, value):
        if value < -300:
            return 'air'
        elif -300 < value < 300:
            return 'tissue'
        else:
            return 'bone'

    def tripleToMaterial(self, triple):
        #print 'tripleToMaterial'
        trange = np.arange(0, 1 + self._matsApproximationValue, self._matsApproximationValue)
        #print triple, trange
        res = {}
        for ppi in range(len(triple)):
            i = triple[ppi]
            for vi in range(len(trange) - 1):
                vp = trange[vi]
                vn = trange[vi + 1]
                #print 'vp vn - i', vp - i, vn - i, vp * vn, (vp - i) * (vn - i) < 0, np.fabs(vp - i), np.fabs(vn - i)
                if (vp - i) * (vn - i) < 0 and np.fabs(vp - i) > self._matsApproximationValue / 2:
                    #print 'i -> vp, case 1', i, vi + 1, vp
                    res[ppi] = trange[vi + 1]
                if (vp - i) * (vn - i) < 0 and np.fabs(vn - i) > self._matsApproximationValue / 2:
                    #print 'i -> vp, case 2', i, vi, vp
                    res[ppi] = trange[vi]
                if (vp - i) * (vn - i) == 0 and i != 0.0:
                    res[ppi] = trange[-1]
            if ppi not in res:
                res[ppi] = 0.0

        # @todo: 4 строчки ниже - хак, переделать
        while np.sum(res.values()) < 1.0:
            res[np.where(res.values() == np.max(res.values()))[0][0]] += self._matsApproximationValue
        while np.sum(res.values()) > 1.0:
            res[np.where(res.values() == np.max(res.values()))[0][0]] -= self._matsApproximationValue

        for i in res:
            res[i] = float(int(res[i] * 100)) / 100
        #print 'mat for triple', triple, res
        return self.materialsSummator(res[0], res[1], res[2])
        pass

    def materialsSummator(self, ai, ti, bi):
        if self.checkMatExists((ai, ti, bi)):
            return self.getMaterialByMaterial((ai, ti, bi), 3)

        #if (ai, ti, bi) in self._registeredMats['list']:
        #    return self._matStartNum + self._registeredMats['list'].index((ai, ti, bi)) + 1
        #self._registeredMats['list'].append((ai, ti, bi))
        #mIndex = self._matStartNum + len(self._registeredMats['list'])
        #self._registeredMats['base'][mIndex] = {
        #    'air': ai,
        #    'tissue': ti,
        #    'bone': bi,
        #}
        #print ai, ti, bi
        #print 'NON EQUAL 1', np.round(ai + ti + bi) != 1.0
        aD = self.air.density if 0 == self.air.densityUnits else 0.001 * self.air.density
        tD = self.tissue.density if 0 == self.tissue.densityUnits else 0.001 * self.tissue.density
        bD = self.bone.density if 0 == self.bone.densityUnits else 0.001 * self.bone.density
        #print 'airdensity', aD, tD, bD
        #print 'resultDensity', aD * ai + tD * ti + bD * bi
        #self._registeredMats['densities'][mIndex] = aD * ai + tD * ti + bD * bi
        acmap = self.air.cmap
        rcMap = {}
        #print acmap
        #print 'cmap handler'
        # @todo: handle cmap correctly (!)
        for (c, m) in [(self.air.cmap, ai), (self.tissue.cmap, ti), (self.bone.cmap, bi)]:
            #print 'im', i, m
            for i in c:
                #print i[0], i[1], i[0][0], i[0][1]
                try:
                    rcMap[i[0]['symbol']] += i[1] * m
                except KeyError:
                    rcMap[i[0]['symbol']] = i[1] * m
        rcSum = 0
        for (k, v) in rcMap.iteritems():
            rcSum += v
        for (k, v) in rcMap.iteritems():
            rcMap[k] /= rcSum
        #self._registeredMats['data'][mIndex] = rcMap

        mIndex = self.registerMaterial((ai, ti, bi), metaInfo={
            'rcMap': rcMap,
            'density': aD * ai + tD * ti + bD * bi
        })
        return mIndex
        pass

    def npToMatGrid(self, npString):
        ret = []
        for i in range(0, self._rowsColumns[0], self._compression):
            for j in range(0, self._rowsColumns[1], self._compression):
                a = 0
                t = 0
                b = 0
                for k in range(i, i+self._compression):
                    for p in range(j, j+self._compression):
                        v = self.pixelValue(npString[k, p])
                        if v == 'air':
                            a += 1
                        elif v == 'tissue':
                            t += 1
                        else:
                            b += 1
                        pass
                pass
                ap = float(a) / self._compression ** 2
                tp = float(t) / self._compression ** 2
                bp = float(b) / self._compression ** 2
                ret.append(self.tripleToMaterial((ap, tp, bp)))
                #print ap, tp, bp, self.tripleToMaterial((ap, tp, bp))
        pass
        return np.array(ret, dtype=np.float16)
        #print airs, tissues, bones