# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 22.01.13
@summary: 
'''

NPPLAN_MAT_NORMALIZE          = 0b00001
NPPLAN_MAT_NORMALIZE_NEGATIVE = 0b00010
NPPLAN_MAT_NOT_NORMALIZE      = 0b00100

def generateNames():
    lzd = {}

    lzd["BE"] = 04
    lzd["BA"] = 56
    lzd["BH"] = 107
    lzd["BI"] = 83
    lzd["BK"] = 97
    lzd["BR"] = 35
    lzd["RU"] = 44
    lzd["RE"] = 75
    lzd["RF"] = 104
    lzd["RG"] = 111
    lzd["RA"] = 88
    lzd["RB"] = 37
    lzd["RN"] = 86
    lzd["RH"] = 45
    lzd["TM"] = 69
    lzd["H"] = 01
    lzd["P"] = 15
    lzd["GE"] = 32
    lzd["GD"] = 64
    lzd["GA"] = 31
    lzd["OS"] = 76
    lzd["HS"] = 108
    lzd["ZN"] = 30
    lzd["HO"] = 67
    lzd["HF"] = 72
    lzd["HG"] = 80
    lzd["HE"] = 02
    lzd["PR"] = 59
    lzd["PT"] = 78
    lzd["PU"] = 94
    lzd["PB"] = 82
    lzd["PA"] = 91
    lzd["PD"] = 46
    lzd["PO"] = 84
    lzd["PM"] = 61
    lzd["C"] = 6
    lzd["K"] = 19
    lzd["O"] = 8
    lzd["S"] = 16
    lzd["W"] = 74
    lzd["EU"] = 63
    lzd["ES"] = 99
    lzd["ER"] = 68
    lzd["MD"] = 101
    lzd["MG"] = 12
    lzd["MO"] = 42
    lzd["MN"] = 25
    lzd["MT"] = 109
    lzd["U"] = 92
    lzd["FR"] = 87
    lzd["FE"] = 26
    lzd["FM"] = 100
    lzd["NI"] = 28
    lzd["NO"] = 102
    lzd["NA"] = 11
    lzd["NB"] = 41
    lzd["ND"] = 60
    lzd["NE"] = 10
    lzd["ZR"] = 40
    lzd["NP"] = 93
    lzd["B"] = 05
    lzd["CO"] = 27
    lzd["CM"] = 96
    lzd["F"] = 9
    lzd["CA"] = 20
    lzd["CF"] = 98
    lzd["CE"] = 58
    lzd["CD"] = 48
    lzd["V"] = 23
    lzd["CS"] = 55
    lzd["CR"] = 24
    lzd["CU"] = 29
    lzd["SR"] = 38
    lzd["KR"] = 36
    lzd["SI"] = 14
    lzd["SN"] = 50
    lzd["SM"] = 62
    lzd["SC"] = 21
    lzd["SB"] = 51
    lzd["SG"] = 106
    lzd["SE"] = 34
    lzd["YB"] = 70
    lzd["DB"] = 105
    lzd["DY"] = 66
    lzd["DS"] = 110
    lzd["LA"] = 57
    lzd["CL"] = 17
    lzd["LI"] = 03
    lzd["TL"] = 81
    lzd["LU"] = 71
    lzd["LR"] = 103
    lzd["TH"] = 90
    lzd["TI"] = 22
    lzd["TE"] = 52
    lzd["TB"] = 65
    lzd["TC"] = 43
    lzd["TA"] = 73
    lzd["AC"] = 89
    lzd["AG"] = 47
    lzd["I"] = 53
    lzd["IR"] = 77
    lzd["AM"] = 95
    lzd["AL"] = 13
    lzd["AS"] = 33
    lzd["AR"] = 18
    lzd["AU"] = 79
    lzd["AT"] = 85
    lzd["IN"] = 49
    lzd["Y"] = 39
    lzd["N"] = 07
    lzd["XE"] = 54
    lzd["CN"] = 112
    lzd["FL"] = 114
    lzd["LV"] = 116

    return lzd

global names
names = generateNames()


class material(object):
    def __init__(self, cMap={}, id='', density=0, univ='', cell='', outerCell=-70):
        self.cm = compositeMap(cMap, NPPLAN_MAT_NORMALIZE_NEGATIVE)
        self._id = int(id)
        self._density = float(density)
        self._outerCell = outerCell
        if not univ:
            self._univ = self._id
        if not cell:
            self._cell = self._id


    def toMCNPMatCardString(self):
        retStr = "m%d " %self._id
        #retStr += self.cm.toMCNPString(True)
        retStr += self.cm.toMCNPString()
        return retStr

    def toMCNPDensityString(self):
        #  1      1  -1.060000     -70  u=    1
        return "%d      %d  %1.6f     %d  u=    %d" %(self._cell, self._id, self._density, self._outerCell, self._univ)

    def __contains__(self, item):
        return item in self.cm

class compositeMap(object):
    def __init__(self, elements={}, normalizationFlag=NPPLAN_MAT_NORMALIZE_NEGATIVE):
        self._map = {}
        #self._density = density
        self._normalization = normalizationFlag
        self._elemFormat = "%s %0.06f"

        if isinstance(elements, str):
            if 'm' == elements[0]:
                self._materialId = int(elements.split(' ')[0][1:])
                elements = elements.split(' ')[1:].join(' ')
            else:
                #self._materialId = int(id)
                self.mapFromString(elements)
            pass
        elif isinstance(elements, compositeMap):
            self._map = elements.getMap()
        else:
            #self._materialId = int(id)
            self.mapFromDict(elements)
        self.normalize()
        pass

    def normalize(self):
        # @todo: implement normalize positive with factor
        if self._normalization & NPPLAN_MAT_NOT_NORMALIZE:
            for i in self._map:
                self._map['normedWeight'] = self._map['weight']
        elif self._normalization & NPPLAN_MAT_NORMALIZE_NEGATIVE:
            weights = 0.0
            for i in self._map:
                weights += self._map[i]['weight']
            for i in self._map:
                self._map[i]['normedWeight'] = -1.0 * self._map[i]['weight'] / weights
            pass

    def mapFromString(self, elements):
        # @todo: implement
        elements = elements.replace("\n", ' ')

    def getMap(self):
        if 0 != len(self._map):
            return self._map

    def mapFromDict(self, elements):
        for elem in elements:
            elemObj = element(elem)
            self._map[elemObj.id] = {
                'element' : elemObj,
                #'id' : self._materialId,
                #'density' : self._density,
                'weight' : float(elements[elem]),
            }
            pass
        pass

    def out(self):
        print self._map

    def toMCNPString(self, newlines=False):
        #retStr = "m%d " %self._materialId
        retStr = ''
        if newlines:
            delim = "\n"
        else:
            delim = ' '
        for elem in self._map:
            _cstr = self._elemFormat %(self._map[elem]['element'].toMCNPString(), self._map[elem]['normedWeight'])
            retStr = delim.join((retStr, _cstr))
        return retStr[1:]

    def __contains__(self, item):
        if isinstance(item, str):
            item = element(item)
        for i in self._map:
            if self._map[i]['element'] == item:
                return True
        return False

class element(object):
    def __init__(self, atomicNumber=1, isotopeNumber=1, library=''):
        global names
        self._namesTable = names
        self._numberFormat = '%d%03d'

        if isinstance(atomicNumber, str):
            self.fromString(atomicNumber)
        else:
            self._atomicNumber = atomicNumber
            self._isotopeNumber = isotopeNumber
            self._library = library

    def fromString(self, elementString):
        if '-' in elementString:
            self._atomicNumber = self._namesTable[elementString.split('-')[0]]
            self._isotopeNumber = int(elementString.split('-')[1])
            self._library = ''
        else:
            if '.' not in elementString:
                # элемент без библиотеки
                elementInt = int(elementString)
                self._isotopeNumber = int(elementInt % 1000)
                self._atomicNumber = int(elementInt / 1000)
                self._library = ''
            else:
                self.fromString(elementString.split('.')[0])
                self._library = elementString.split('.')[1]

    def getAtomicNumber(self):
        return self._atomicNumber
    atomicNumber = property(getAtomicNumber)

    def getIsotopeNumber(self):
        return self._isotopeNumber
    isotopeNumber = property(getIsotopeNumber)

    def getLibrary(self):
        return self._library
    library = property(getLibrary)

    def toMCNPString(self):
        if '' == self._library:
            return self._numberFormat % (self.atomicNumber, self.isotopeNumber)
        else:
            return '%s.%s' % ((self._numberFormat % (self.atomicNumber, self.isotopeNumber)), self._library)

    def toString(self):
        return self.toMCNPString()

    def createId(self):
        return self._numberFormat % (self.atomicNumber, self.isotopeNumber)
    id = property(createId)

    def __repr__(self):
        return self.toString()

    def __eq__(self, other):
        return other.atomicNumber == self.atomicNumber and other.isotopeNumber == self.isotopeNumber



if __name__ == '__main__':
    e = element(1, 1, '20c')
    print e.toString()
    print e.toString() == '1001'
    e2 = element('1001.20c')
    print e2.toString()

    #map = compositeMap({'1001' : 20, '2005' : 30, '5008' : 40})
    mat = material({'1001' : 20, '2005' : 30, '5008' : 40}, '1', '200')
    print mat.toMCNPMatCardString()
    print mat.toMCNPDensityString()
    #map.out()
    #print map.toMCNPString(True)
    #print map.toMCNPString()
    #e3 = element(1, 1)
    #print e3
    #print e3 in map
    #print e in map
