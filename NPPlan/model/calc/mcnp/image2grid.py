# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 23.01.13
@summary: 
'''

import numpy as np
from PIL import Image
from joblib import Parallel, delayed
import time

def readImageToNumpy(filename):
    pilIm = Image.open(filename)
    pix = np.array(pilIm)
    return pix

def binCount(pix):
    return np.bincount(pix)

def makeSlice(pix, leftCorner, rightCorner, upperCorner, lowerCorner):
    #return np.transpose(np.transpose(pix[upperCorner:lowerCorner])[leftCorner:rightCorner])
    return pix[upperCorner:lowerCorner, leftCorner:rightCorner]

def nextCornersStageIterator(stepI=-1, stepJ=-1):
    stepI = 20 if stepI == -1 else stepI
    stepJ = 20 if stepJ == -1 else stepJ
    for i in range(0, 512, stepI):
        for j in range(0, 512, stepJ):
            lec = j
            upc = i
            loc = i + stepI if i + stepI < 512 else 512
            ric = j + stepJ if j + stepJ < 512 else 512
            yield (lec, ric, upc, loc)

def binCountInSmallGrid(grid):
    bins = np.bincount(grid)
    #print bins
    #print np.nonzero(bins)
    ret = {'sum' : np.sum(bins)}
    for i in np.nonzero(bins)[0]:
        #print i
        ret[i] = bins[i]
    return ret

def make2DGrid(pix):
    pass

def makeCellFromBin(cellsList, currentCell):
    sum = currentCell['sum']
    currentCell.pop('sum')
    emptyCell = {0:0, 128:0, 256:0}
    emptyCell.update(currentCell)
    return normalizeCell(emptyCell, sum)
    pass

def normalizeCell(cell, factor, multiplier=1):
    for i in cell:
        cell[i] = (float(cell[i]) / factor)*multiplier
    return cell

def makeCellsFromArray(arr):
    #print type(arr), arr.shape
    singleGrid = arr.reshape(arr.shape[0]*arr.shape[1])
    bins = binCountInSmallGrid(singleGrid)
    return makeCellFromBin({}, bins)

def makeCellsFromImage(*params):
    s = makeSlice(*params)
    c = makeCellsFromArray(s)
    return c

def singleImageToGrid(filename):
    im = readImageToNumpy(filename)
    o = []
    for i in nextCornersStageIterator(16, 16):
        o.append(makeCellsFromImage(im, *i))
    return o

def singleImageToGridCalc(filename):
    im = readImageToNumpy(filename)
    ro = []
    for i in nextCornersStageIterator(16, 16):
        o = makeSlice(im, *i)
        ro.append(makeCellsFromArray(o))
    return ro

def singleImageToGridParallel(filename, jobs=8):
    im = readImageToNumpy(filename)
    o = Parallel(n_jobs=jobs, verbose=0, pre_dispatch='1.5*n_jobs')\
        (delayed(makeCellsFromImage)(im, *i) for i in nextCornersStageIterator(16, 16))
    return o

def singleImageToGridParallelCalcOnly(filename, jobs=8):
    im = readImageToNumpy(filename)
    o = Parallel(n_jobs=jobs, verbose=0, pre_dispatch='1.5*n_jobs')\
        (delayed(makeSlice)(im, *i) for i in nextCornersStageIterator(16, 16))
    ro = []
    for i in o:
        ro.append(makeCellsFromArray(i))
    return ro

if __name__ == '__main__':
    filename = r'C:\NPPlan2\Data\4e1ad18d5157510c64000000\Segmented\slice.1'
    start = time.time()
    im = readImageToNumpy(filename)
    print 'Image.Read time: %f' % (time.time() - start)
    del im

    start = time.time()
    o = singleImageToGridParallel(filename)
    print 'time, default parallel: %f' % (time.time() - start)

    start = time.time()
    o = singleImageToGridParallel(filename, jobs=2)
    print 'time, jobs=2: %f' % (time.time() - start)

    start = time.time()
    o = singleImageToGridParallel(filename, jobs=1)
    print 'time, jobs=1: %f' % (time.time() - start)

    start = time.time()
    o = singleImageToGridParallelCalcOnly(filename)
    print 'time, only calc default parallel: %f' % (time.time() - start)

    start = time.time()
    o = singleImageToGridParallelCalcOnly(filename, jobs=2)
    print 'time, only calc jobs=2: %f' % (time.time() - start)

    start = time.time()
    o = singleImageToGridParallelCalcOnly(filename, jobs=1)
    print 'time, only calc jobs=1: %f' % (time.time() - start)

    start = time.time()
    o = singleImageToGrid(filename)
    print 'time, simple: %f' % (time.time() - start)

    start = time.time()
    o = singleImageToGridCalc(filename)
    print 'time, only calc simple: %f' % (time.time() - start)

    im = readImageToNumpy(filename)
    start = time.time()
    o = Parallel(n_jobs=2, verbose=0, pre_dispatch='1.5*n_jobs')\
        (delayed(makeSlice)(im, *i) for i in nextCornersStageIterator(16, 16))
    print 'time, in main: %f' % (time.time() - start)
    ro = []
    for i in o:
        ro.append(makeCellsFromArray(i))
    print 'time, slice parallel, in main: %f' % (time.time() - start)
    del im

    im = readImageToNumpy(filename)
    start = time.time()
    ao = []
    for i in nextCornersStageIterator(16, 16):
        ao.append(makeSlice(im, *i))
    o = Parallel(n_jobs=2, verbose=0, pre_dispatch='1.5*n_jobs')\
        (delayed(makeCellsFromArray)(i) for i in ao)
    print 'time, cell parallel, in main: %f' % (time.time() - start)
    del im

    im = readImageToNumpy(filename)
    start = time.time()
    ao = []
    for i in nextCornersStageIterator(16, 16):
        ao.append(makeSlice(im, *i))
    print 'time slice making single: %f' % (time.time() - start)
    del im

    im = readImageToNumpy(filename)
    start = time.time()
    o = Parallel(n_jobs=2, verbose=0, pre_dispatch='1.5*n_jobs')\
        (delayed(makeSlice)(im, *i) for i in nextCornersStageIterator(16, 16))
    print 'time slice making parallel: %f' % (time.time() - start)
    del im

    '''
        LAST RESULTS:
            C:\Python27\python.exe C:/NPPlan3/NPPlan/model/calc/mcnp/image2grid.py
            Image.Read time: 0.008000
            time, default parallel: 1.099000
            time, jobs=2: 1.022000
            time, jobs=1: 0.124000
            time, only calc default parallel: 1.427000
            time, only calc jobs=2: 1.383000
            time, only calc jobs=1: 0.090000
            time, simple: 0.069000
            time, only calc simple: 0.072000
            time, in main: 1.317000
            time, slice parallel, in main: 1.385000
            time, cell parallel, in main: 0.719000
            time slice making single: 0.004000
            time slice making parallel: 0.915000

            Process finished with exit code 0
    '''