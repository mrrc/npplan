# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 22.01.13
@summary: 
'''

__all__ = [
    'element',
    'image2grid',
    'mParser',
]