__author__ = 'mrxak'

import unittest
import NPPlan
import NPPlan.model.calc.base.systemOfUnits as systemOfUnits


class MyTestCase(unittest.TestCase):
    def test_Simple(self):
        #print systemOfUnits.unitBase("120 MeV", "Energy")
        #self.assertEqual(str(systemOfUnits.unitBase("120 MeV", "Energy")), "Energy: 120.000000 MeV")
        r = systemOfUnits.unitBase("120 MeV", "Energy")
        self.assertEqual(r.getAs("MeV", 1), 120)
        self.assertEqual(r.getAs("eV", 1), 120000000)
        self.assertEqual(r.getAs("GeV", 1), 0.12)
        #self.assertEqual(True, False)

    def test_Comparison(self):
        u = systemOfUnits.unitBase("120 GeV", "Energy")
        u2 = systemOfUnits.unitBase("120 GeV", "Energy")
        u3 = systemOfUnits.unitBase("100 MeV", "Energy")
        u4 = systemOfUnits.unitBase("20 eV", "Energy")

        self.assertEqual(u, u2)
        self.assertEqual(u==u2, True)
        self.assertEqual(u3 < u, True)
        self.assertEqual(u4 < u3, True)
        self.assertEqual(u2 > u4, True)
        self.assertEqual(u4 > u, False)
        pass

    def test_Sum(self):
        u1 = systemOfUnits.unitBase("120 GeV", "Energy")
        u2 = systemOfUnits.unitBase("50 GeV", "Energy")
        u3 = systemOfUnits.unitBase("150 MeV", "Energy")

        us1 = u1 + u2
        us2 = u1 + u3
        us3 = u1 + "50 GeV"

        ur1 = systemOfUnits.unitBase("170 GeV", "Energy")

        #print us1.getAs("MeV", 1)
        #print ur1.getAs("MeV", 1)

        self.assertEqual(us1==ur1, True)
        self.assertEqual(us3==ur1, True)
        self.assertEqual(us2.getAs("MeV", 1), 120150)


if __name__ == '__main__':
    unittest.main()
