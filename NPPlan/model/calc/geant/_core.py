# -*- coding: utf8 -*-
'''
DICOM2Geant преобразователь
@uses: pydicom, numpy
@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 2
@date: 25.01.13
@summary: DICOM2Geant преобразователь
@todo: getCalibration, getMaterialIndicies reader from file
'''

import numpy as np
import dicom

global compression
compression = 8
global dicomFile
global dicomNumpyFile

def getCalibration():
    # from geant calibration table
    return {
        -5000:0.0,
        -1000:0.0,
        -400:0.602,
        -150:0.924,
        100:1.075,
        300:1.145,
        2000:1.856,
        4927:3.379,
    }

def getMaterialIndices():
    # from geant DATA file
    '''return {
        'Air'            :0.207,
        'LungInhale'     :0.481,
        'LungExhale'     :0.919,
        'AdiposeTissue'  :0.979,
        'Breast'         :1.004,
        'Water'          :1.043,
        'Muscle'         :1.109,
        'Liver'          :1.113,
        'TrabecularBone' :1.496,
        'DenseBone'      :1.654,
    }'''
    return (
        ['Air', 'LungInhale', 'LungExhale', 'AdiposeTissue', 'Breast', 'Water', 'Muscle', 'Liver', 'TrabecularBone', 'DenseBone'],
        [0.207, 0.481, 0.919, 0.979, 1.004, 1.043, 1.109, 1.113, 1.496, 1.654]
        )

def getMaterialIndex(density):
    ind = getMaterialIndices()[1]
    #print ind, density
    ii = 0
    for i in range(len(ind)):
        if density >= ind[i]:
            ii = i
    return ii

def getMaterialIndiciesSize():
    return len(getMaterialIndices()[0])

def pixel2density(pixelValue):
    calibration = getCalibration()
    pixelRanges = sorted(calibration.keys())
    densityRanges = sorted(calibration.values())

    for j in range(1, len(pixelRanges)):
        if pixelValue >= pixelRanges[j-1] and pixelValue < pixelRanges[j]:
            deltaCT = pixelRanges[j] - pixelRanges[j-1]
            deltaDensity = densityRanges[j] - densityRanges[j-1]

            return densityRanges[j] - (( float(pixelRanges[j] - pixelValue)*deltaDensity/deltaCT ))

def meanInArea(pix, leftCorner, rightCorner, upperCorner, lowerCorner):
    return np.mean(pix[upperCorner:lowerCorner, leftCorner:rightCorner])

def readDicom2Numpy(filename):
    global dicomFile, dicomNumpyFile
    dicomFile = dicom.ReadFile(filename)
    d = np.fromstring(dicomFile.PixelData,dtype=np.int16)
    d = d.reshape((1,dicomFile.Columns,dicomFile.Rows))
    dicomNumpyFile = d
    return d

def getPixelSpacings():
    global dicomFile
    print dicomFile.PixelSpacing
    return (float(dicomFile.PixelSpacing[0]), float(dicomFile.PixelSpacing[1]))

def writeFile(outFileName):
    global dicomFile, compression, dicomNumpyFile

    f = open(outFileName, 'w')
    # materials quantity
    f.write('%d\n' %getMaterialIndiciesSize())

    # materials list
    for i in range(len(getMaterialIndices()[0])):
        f.write('%d %s \n' %(i, getMaterialIndices()[0][i]))

    # write number of voxels
    f.write('%d %d 1\n' %(int(dicomFile.Columns)/compression, int(dicomFile.Rows)/compression))

    # write pixel spacing (?)
    pixelSpacingX, pixelSpacingY = getPixelSpacings()
    f.write('%d %d\n' % (-1*int(dicomFile.Columns)*pixelSpacingX/2., int(dicomFile.Columns)*pixelSpacingX/2.))
    f.write('%d %d\n' % (-1*int(dicomFile.Rows)*pixelSpacingY/2., int(dicomFile.Rows)*pixelSpacingY/2.))

    # location/thickness
    try:
        sliceLocation = int(dicomFile.SliceLocation)
    except AttributeError:
        sliceLocation = 0
    thick = np.round(float(dicomFile.SliceThickness)/2.0)
    f.write('%d %d\n' %(sliceLocation-thick, sliceLocation+thick))

    # rescaling
    rescaleSlope = int(dicomFile[0x0028, 0x1053].value)
    #print rescaleSlope
    rescaleIntercept = int(dicomFile[0x0028, 0x1052].value)
    #print rescaleIntercept
    dicomNumpyFile *= rescaleSlope
    dicomNumpyFile += rescaleIntercept
    #for j in range(int(dicomFile.Rows)):
    #    for i in range(int(dicomFile.Columns)):
    #        print 'OLD', dicomNumpyFile[0, i, j], i, j
    #        dicomNumpyFile[0, i, j] = dicomNumpyFile[0, i, j]*rescaleSlope + rescaleIntercept
    #        print 'NEW', dicomNumpyFile[0, i, j], i, j
    #        pass

    # print avg densities
    matQ = 0
    for ranges in nextCornersStageIterator(compression, compression):
        if matQ > 40:
            f.write('\n')
            matQ = 0
        print '1', ranges
        avgInSquare = np.mean(dicomNumpyFile[0, ranges[0]:ranges[1], ranges[2]:ranges[3]])
        densityInSquare = pixel2density(avgInSquare)
        material = getMaterialIndex(densityInSquare)
        f.write('%d ' %material)
        print avgInSquare, densityInSquare, material
        matQ += 1
        pass

    f.write('\n')

    matQ = 0
    for ranges in nextCornersStageIterator(compression, compression):
        if matQ > 8:
            f.write('\n')
            matQ = 0
        print '2', ranges
        avgInSquare = np.mean(dicomNumpyFile[0, ranges[0]:ranges[1], ranges[2]:ranges[3]])
        densityInSquare = pixel2density(avgInSquare)
        f.write('%f ' %float(densityInSquare))
        matQ += 1

    f.close()

def nextCornersStageIterator(stepI=-1, stepJ=-1):
    stepI = 20 if stepI == -1 else stepI
    stepJ = 20 if stepJ == -1 else stepJ
    for i in range(0, 512, stepI):
        for j in range(0, 512, stepJ):
            lec = j
            upc = i
            loc = i + stepI if i + stepI < 512 else 512
            ric = j + stepJ if j + stepJ < 512 else 512
            yield (lec, ric, upc, loc)

if __name__ == '__main__':
    import sys
    #print len(sys.argv[:])
    if 1 == len(sys.argv[:]):
        print 'Usage: python dicom2geant.py [input filename] [output filename]'
        sys.exit(0)

    #filename = r'C:\Users\Chernukha\Pictures\DICOM\01111140\84269326'
    filename = sys.argv[:][1]
    print readDicom2Numpy(filename)

    getPixelSpacings()

    #filename2 = r'C:\Users\Chernukha\G4dcm\testr.g4dcm'
    filename2 = sys.argv[:][2]
    writeFile(filename2)



