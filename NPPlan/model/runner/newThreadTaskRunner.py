# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 17.05.13
@summary: 
'''

from multiprocessing import Process
from subprocess import check_call
import os
from threading import Timer
import time


p = None


def startTask(exeName, arg):
    check_call([exeName, arg])


def startThreadedTask(exeName, arg):
    global p
    p = Process(target=startTask, args=(exeName, arg))
    p.start()
    #p.join()


def checkAlive():
    global p
    if p.is_alive():
        print 'Still running'
    else:
        print 'Finished'


def startCheckerTimer(pr=None):
    while True:
        t = Timer(10.0, checkAlive)
        t.start()
        time.sleep(10)
        if not pr.is_alive():
            break


def startChecker():
    print 'checker started'
    global p
    #p2 = Process(target=startCheckerTimer, args=())
    #p2.start()
    #p2.join()
    startCheckerTimer(p)
    #t = Timer(10.0, checkAlive)
    #t.start()


def checkAndRemove(fname):
    if os.path.exists(fname):
        os.remove(fname)


def startMcnpTask(simName=None):
    #exeName = r'C:\mcnpx\executable\mcnpx.exe'
    exeName = r'C:\MCNP5\mcnp5.exe'
    if simName is None:
        simName = 'tstmm'

    ddir = os.path.dirname(exeName)
    os.chdir(ddir)
    checkAndRemove(simName + 'o')
    checkAndRemove(simName + 'm')
    checkAndRemove(simName + 'r')

    startThreadedTask(exeName, 'n=' + simName)
    startChecker()

    print 'task closed'

# exeName C:\mcnpx\executable\mcnpx.exe C:\MCNP5\mcnp5.exe
# n=tstmm

if __name__ == '__main__':
    startMcnpTask()