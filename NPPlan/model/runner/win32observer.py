# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 23.09.13
@summary: 
'''

import win32serviceutil
import win32service
import win32event

 # Twisted imports
from twisted.internet.protocol import Protocol, Factory
from twisted.internet import reactor

from spawnProtocol import ProcessProtocol


class WindowsService(win32serviceutil.ServiceFramework):
    _svc_name_ = "mcnpqueueservice"
    _svc_display_name_ = "mcnpqueueservice"

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)

    def SvcStop(self):
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)

    def observer(self):
        targetDir = 'C:\\mcnpx\\executable\\'
        taskDir = 'C:\\Temp\\mcnpqueue\\'
        resultsDir = 'C:\\Temp\\mcnpresults\\'
        import os
        import shutil
        import time
        oldPath = os.getcwd()
        while 1:
            time.sleep(1)
            lD = os.listdir(taskDir)
            if 0 == len(lD):
                continue
            if os.path.exists(os.path.join(targetDir, lD[0])):
                continue
            shutil.move(os.path.join(taskDir, lD[0]), targetDir)
            os.chdir(targetDir)
            reactor.spawnProcess(ProcessProtocol(filename=lD[0], path=targetDir), os.path.join(targetDir, 'mcnpx.exe'),
                                 args=['mcnpx.exe', 'n='+lD[0],])
            os.chdir(oldPath)
        pass

    def SvcDoRun(self):
        import servicemanager
        self.CheckForQuit()
         #factory = Factory()
        #factory.protocol = QOTD
         #reactor.listenTCP(8007, factory)
        #reactor.callFromThread(self.observer)
        #reactor.run(installSignalHandlers=0)
        self.observer()
        reactor.run()



    def CheckForQuit(self):
        retval = win32event.WaitForSingleObject(self.hWaitStop, 10)
        if not retval == win32event.WAIT_TIMEOUT:
            # Received Quit from Win32
            reactor.stop()

        reactor.callLater(1.0, self.CheckForQuit)


if __name__=='__main__':
    win32serviceutil.HandleCommandLine(WindowsService)