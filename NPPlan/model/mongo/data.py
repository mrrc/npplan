# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 23.04.13
@summary: 
'''

import NPPlan
import connector


class database(object):
    def __init__(self, host='', port='', dbname='', **kwargs):
        self._db = connector.connection(host, port, dbname)
        #print self._db

NPPlan.database = database('127.0.0.1', 27017, 'npplan3')

if __name__ == '__main__':
    NPPlan.init(programPath="c:/NPPlan3/", test=True, log=True)
    pass
    #NPPlan.database = database()