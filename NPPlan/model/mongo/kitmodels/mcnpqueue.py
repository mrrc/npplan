# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 19.09.13
@summary: 
'''

import NPPlan
from mongokit import Document
import datetime

@NPPlan.mongoConnection.register
class mcnpqueue(Document):
    use_dot_notation = True
    __collection__ = 'mcnptasks'
    __database__ = 'npplan3'
    structure = {
        'name': unicode,
        'description': unicode,
        'assignedPlan': str,
        'machineId': str,
        'machineStr': basestring,
        'status': {
            'created': bool,
            'running': bool,
            'paused': bool,
            'completed': bool,
            'moved': bool,
        },
        'paths': {
            'source': unicode,
            'task': unicode,
            'output': unicode,
            'mfile': unicode,
        },
        'version': basestring,
        'useMpi': bool,
        'mpiProc': int,
        'simTime': {
            'real': int,
            'original': int,
        },
        'times': {
            'added': datetime.datetime,
            'created': datetime.datetime,
            'started': datetime.datetime,
            'completed': datetime.datetime,
        },
        'errorCode': int,
    }
