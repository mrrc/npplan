# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 17.06.13
@summary: 
'''

import NPPlan
from mongokit import Document, Connection
import datetime

@NPPlan.mongoConnection.register
class isotope(Document):
    use_dot_notation = True
    __collection__ = 'isotopes'
    __database__ = 'npplan3'
    structure = {
        'name': basestring,
        'rusName': unicode,
        'symbol': basestring,
        'description': unicode,
        'pid': basestring,
        'atomicNum': int,
        'atomicMass': float,
        'geantSimData': dict,
        'mcnpSimData': dict,
    }
    default_values = {
        'description': u'',
        'rusName': u'',
        'pid': '0',
    }
