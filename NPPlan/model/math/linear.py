# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 05.03.13
@summary: 
'''

import numpy


def oldnewPointByCoordsAndDistance(x1, y1, z1, x2, y2, z2, dist):
    x1 = numpy.float(x1)
    y1 = numpy.float(y1)
    z1 = numpy.float(z1)
    x2 = numpy.float(x2)
    y2 = numpy.float(y2)
    z2 = numpy.float(z2)
    dist = numpy.float(dist)

    msX = numpy.sqrt(1 + ((y1 - y2) ** 2) / ((x1 - x2) ** 2) + ((z1 - z2) ** 2) / ((x1 - x2) ** 2))
    msY = numpy.sqrt(1 + ((x1 - x2) ** 2) / ((y1 - y2) ** 2) + ((z1 - z2) ** 2) / ((y1 - y2) ** 2))
    msZ = numpy.sqrt(1 + ((x1 - x2) ** 2) / ((z1 - z2) ** 2) + ((y1 - y2) ** 2) / ((z1 - z2) ** 2))

    xm = dist / msX + x2
    ym = dist / msY + y2
    zm = dist / msZ + z2

    return xm, ym, zm


def newPointByCoords(x1, y1, z1, x2, y2, z2, dist):
    r0 = numpy.array([x1, y1, z1])
    r2 = numpy.array([x2, y2, z2])

    p = r2 - r0

    return r0 + dist * p

def newPoint(v1, v2, dist):
    x1 = numpy.float(v1[0])
    y1 = numpy.float(v1[1])
    z1 = numpy.float(v1[2])
    x2 = numpy.float(v2[0])
    y2 = numpy.float(v2[1])
    z2 = numpy.float(v2[2])

    v1 = numpy.array(v1)
    v2 = numpy.array(v2)

    print v1-v2

    a = v2 - v1

    return v1 + dist*a

def distanceBetweenTwoPoints(x1, y1, z1, x2, y2, z2):
    return numpy.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2 + (z1 - z2) ** 2)

if __name__ == '__main__':
    c = newPoint((0, 0, 0), (2, 1, 0), 2)
    print c

