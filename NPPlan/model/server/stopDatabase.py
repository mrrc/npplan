# -*- coding: utf8 -*-
'''
Вызывает скрипт остановки БД.

См. NPPlan/scripts/stopMongod.js
@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 16.07.12
@summary: Вызывает скрипт остановки БД
'''

import NPPlan
from NPPlan.model.runner.local import runTask

def stopDatabase():
    print NPPlan.getProgramData()['appConfig']['server']['dbrun']

    str = '/'.join(NPPlan.getProgramData()['appConfig']['server']['dbrun'].split('/')[:-1])+'/mongo.exe' + ' ' + NPPlan.getProgramData()['appPath'] + '/scripts/stopMongod.js'

    runTask(str, daemon=False)