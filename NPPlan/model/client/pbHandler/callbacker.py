# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 30.11.12
@summary: 
'''

import NPPlan

from twisted.spread import pb
import time
import NPPlan.controller.logger as log
from random import random

class internalException(pb.Error): pass

class callbacker:
    # @todo: implement поиск по времени последнего запроса, повтор запроса по времени
    def handle(self, object):
        self.__dict__['timeFormat'] = ''
        self.__dict__['object'] = object
        self.__dict__['_data'] = {}

    def checkInternalException(self, failure):
        '''
        @todo: use NPPlan.log
        '''
        which = failure.trap(internalException)
        if which == internalException:
            print " remote raised a MyException"

    def rawDoCall(self, method, args):
        log.log('pbHandler.rawDoCall', 'Calling %s from remote' %(method, ))
        try:
            realArgs = args.copy()
            realArgs.pop('callback')
            deferred = self.__dict__['object'].callRemote(method, realArgs)
            deferred.addCallback(self.rawWorked)
            deferred.addErrback(self.checkInternalException)
        except pb.DeadReferenceError:
            # @todo: use NPPlan.log
            print 'pb: dc'

    def rawWorked(self, data):
        log.log('pbHandler.rawWorked', 'Data retrieved (callId %s)' %(data['callId']))
        self.updateData(data)
        self.doCallCallback(data['callId'])
        #print data
        #print self.__class__
        #return data

    def makeCallId(self):
        return str(random())

    def updateData(self, data):
        callId = data['callId']
        self.__dict__['_data'][callId]['result'] = data['result']
        self.__dict__['_data'][callId]['responseTime'] = self.now()
        self.__dict__['_data'][callId]['status'] = 2

    def doCall(self, method, args, callback, callId=None, expiration=60):
        #if callId is None:
        # @todo: make callId
        #    callId = callback.__class__
        if callId is None:
            callId = self.makeCallId()
        log.log('pbHandler.doCall', 'Calling method %s (callId %s)' %(method, str(callId)))
        # проверяем, есть ли callId в вызовах
        if self.__dict__['_data'].has_key(callId):
            # проверяем, истекло ли установленное время с последнего запроса
            obj = self.__dict__['_data'][callId]
            if time.time() - obj['responseTime']['raw'] < expiration:
                # если не истекло, то вызываем callback со старыми данными
                log.log(
                    'pbHandler.doCall',
                    'Not expired since last call on %s (expiration %s, callId %s)' %(
                        obj['responseTime']['raw'],
                        expiration,
                        str(callId),
                        )
                )
                self.doCallCallback(callId)
                return
        self.__dict__['_data'][callId] = {
            'method' : method,
            'args' : args,
            'callId' : callId,
            'callback' : callback,
            'requestTime' : self.now(),
            'status' : 0
        }
        self.rawDoCall('callBackend', self.__dict__['_data'][callId])
        pass

    def now(self):
        t = time.time()
        return {
            'raw' : t,
            'text' : self.formatTime(t)
        }


    def formatTime(self, record):
        # @todo: accurate msecs
        ct = time.localtime(int(record))
        t = time.strftime("%Y-%m-%d %H:%M:%S", ct)
        s = "%s,%03d" % (t, int(1000*(record-int(record))))
        return s

    def doCallCallback(self, callId):
        if self.__dict__['_data'][callId]['callback'] is not None:
            self.__dict__['_data'][callId]['callback'](self.__dict__['_data'][callId]['result'])
        pass

    def fetchByCallId(self, callId):
        return self.__dict__['_data'][callId]

if '__main__' == __name__:
    from twisted.internet import reactor
    factory = pb.PBClientFactory()
    reactor.connectTCP("localhost", 8800, factory)
    deferred = factory.getRootObject()
    deferred.addCallback(callbacker().handle)
    reactor.run()