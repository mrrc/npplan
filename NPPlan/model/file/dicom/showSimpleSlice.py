# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 21.05.13
@summary: 
'''

import vtk

window = 1200
level = 800
#inputFile = r'C:\Users\Chernukha\Pictures\DICOM\11070516\12390000\04848083'
inputFile = r'C:\Users\Chernukha\Pictures\DICOM\11070516\12390000\04831644'

reader = vtk.vtkDICOMImageReader()
im = vtk.vtkImageMapToWindowLevelColors()
im.SetWindow(window)
im.SetLevel(level)
reader.SetFileName(inputFile)
im.SetInput(reader.GetOutput())

atext = vtk.vtkTexture()
atext.SetInputConnection(im.GetOutputPort())
plane = vtk.vtkPlaneSource()
planeMapper = vtk.vtkPolyDataMapper()
planeMapper.SetInputConnection(plane.GetOutputPort())
planeActor = vtk.vtkActor()
planeActor.SetMapper(planeMapper)
planeActor.SetTexture(atext)

aRenderer = vtk.vtkRenderer()
aRenderer.AddActor(planeActor)
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(aRenderer)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)
aCamera = vtk.vtkCamera()
#aRenderer.SetActiveCamera(aCamera)
aRenderer.ResetCamera()
iren.Initialize()
renWin.Render()
iren.Start()