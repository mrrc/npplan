# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 15.10.12
@summary: 
'''

import NPPlan
import NPPlan.controller.logger as log
import dicom

def readRawInfo(fileName):
    ds = dicom.ReadFile(fileName)
    log.log('model.file.dicom.rawReader', 'File %s opened' %(fileName))
    return ds

if __name__ == '__main__':
    filename = r'C:\Users\Chernukha\G4dcm\1.dcm'
    print readRawInfo(filename)

    filename = r'C:\Users\Chernukha\G4dcm\m000-1.dcm'
    print readRawInfo(filename)