# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 21.05.13
@summary: 
'''

import NPPlan
import NPPlan.controller.logger as log
import os
import dicom
import shutil
from dicom.filereader import InvalidDicomError
import time
import __builtin__


class seriesEvaluator(object):
    def __init__(self, dirName='', globalFields=[]):
        self._dirName = dirName
        self._series = {}
        self._globalData = {
            '_len': 0,
        }

        #self._allDescr = 0008, 1030
        log.log('model.file.dicom.seriesEvaluator', 'Reading dicom data from %s' % dirName)

        for i in os.listdir(dirName):
            fPath = os.path.join(dirName, i)
            try:
                d = dicom.read_file(fPath)
            except InvalidDicomError:
                log.log('model.file.dicom.seriesEvaluator', 'Failed to read %s' % i)
                continue

            sv = str(d[(0x0008, 0x103e)].value)
            if 0 == self._globalData['_len']:
                for f in globalFields:
                    setattr(self, f[0], getattr(__builtin__, f[2])(d[f[1]].value))
                #self._globalData['description'] = str(d[(0x0008, 0x1030)].value)
                #self._globalData['name'] = str(d[(0x0010, 0x0010)].value)
                #self._globalData['study'] = str(d[(0x0008, 0x1030)].value)
                #self._globalData['facility'] = str(d[(0x0008, 0x0070)].value) + ' ' + str(d[(0x0008, 0x1010)].value) + \
                #                               ' ' + str(d[(0x0018, 0x1000)].value)
                #self._globalData['date'] = str(d[(0x0008, 0x0020)].value)
                #self._globalData['time'] = str(d[(0x0008, 0x0030)].value)

            if sv not in self._series:
                self._series[sv] = {
                    '_len': 0,
                    '_list': [],
                    'sid': int(d[(0x0020, 0x0011)].value),
                    'description': sv,
                    'suid': str(d[(0x0008, 0x0018)].value),
                    }

            self._series[sv]['_list'].append(i)
            self._series[sv]['_len'] += 1
            self._globalData['_len'] += 1

    def getSeriesNames(self):
        return self._series.keys()

    def getSeriesData(self, key=None):
        if key is None:
            return self._series
        else:
            return self._series[key]

    def moveSeriesTo(self, key, outFolder):
        if not os.path.exists(outFolder):
            log.log('model.file.dicom.seriesEvaluator', 'Creating folder: %s' % outFolder)
            os.mkdir(outFolder)
        log.log('model.file.dicom.seriesEvaluator', 'Copying dataset %s to %s' % (key, outFolder))
        for i in self._series[key]['_list']:
            shutil.copy(os.path.join(self._dirName, i),
                        os.path.join(outFolder, i)
                        )

    def getSeriesLength(self):
        return self._globalData['_len']

    allLen = property(getSeriesLength)