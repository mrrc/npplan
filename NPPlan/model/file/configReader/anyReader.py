# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 13.10.13
@summary:
'''

import NPPlan
import os
import xml.etree.ElementTree as ET


def autoRelease(fn):
    def wrapper(*args, **kwargs):
        print args
        res = fn(*args, **kwargs)
        args[0].releaseFile()
        return res

    return wrapper


def autoReadAndRelease(filename):
    # @todo: check if we have another open file
    def readReleaseDecorator(func):
        def wrapped(*args, **kwargs):
            args[0].loadFile(filename)
            res = func(*args, **kwargs)
            args[0].releaseFile()
            return res
        return wrapped
    return readReleaseDecorator


class xmlReaderBase(object):
    def __init__(self, *args, **kwargs):
        #self._appPath = kwargs.get('appPath', NPPlan.config.appPath)
        # '%s/view/client/config/%s.xml'  %(NPPlan.config.appPath, file)
        self._blocker = False
        #self._findConfigDir()
        self._loadedCache = {}
        #self._configDir = os.path.join(
        # @todo: check appPath var ???
        #    NPPlan.config.appPath,
        #    'view',
        #    'client',
        #    'config'
        #)
        # @todo: check if dir exists
        #if not os.path.
        pass

    def _findConfigDir(self):
        self._configDir = os.path.join(
            NPPlan.config.appPath,
            'view',
            'client',
            'config',
        )

    def getConfigDir(self):
        if hasattr(self, '_configDir'):
            return self._configDir
        else:
            self._findConfigDir()
            return self._configDir

    configDir = property(getConfigDir)

    def loadFile2Cache(self, name):
        fS = os.path.join(self.configDir, name+'.xml')
        if not os.path.exists(fS):
            return False
        if name in self._loadedCache:
            return True
        tree = ET.parse(fS)
        root = tree.getroot()
        print root
        self._loadedCache[name] = {
            'root': root
        }
        return True

    def loadFile(self, name):
        # @todo: refactor
        if self._blocker:
            return -2
        fS = os.path.join(self.configDir, name+'.xml')
        if not os.path.exists(fS):
            return -1
        self._blocker = True
        tree = ET.parse(fS)
        root = tree.getroot()
        self._cRoot = root
        self._cInfo = (name, fS, tree)
        return 0

    def releaseFile(self):
        del self._cRoot
        del self._cInfo
        self._blocker = False
        #print 'File released'
        return 0

    def _getBlockerFlag(self):
        return self._blocker

    state = property(_getBlockerFlag)

    def getCurrentOpenedInfo(self):
        return self._cInfo

    def _getTagText(self, tag, parent=None, ind=0):
        if parent is None:
            parent = self._cRoot
        return parent.findall('./'+tag)[ind].text


class xmlReaderMain(xmlReaderBase):
    #@autoRelease
    @autoReadAndRelease('stages')
    def readStagesList(self):
        #self.loadFile('stages')
        stages = self._cRoot.findall("*")
        ret = []
        for i in stages:
            ret.append(i.attrib['name'])
        #self.releaseFile()
        return ret
    pass

#print NPPlan.config.appPath
NPPlan.xmlReader = xmlReaderMain(appPath=NPPlan.config.appPath)
print NPPlan.config.appPath