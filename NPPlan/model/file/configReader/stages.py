# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 13.09.12
@summary: 
'''

import NPPlan
import xml.etree.ElementTree as ET
from NPPlan.model.file.configReader import readConfig as _readConfig

_root = None

def readConfig():
    global _root
    if _root is not None:
        return
    _root = _readConfig('stages')

def getStagesForType(type):
    global _root
    stages = _root.findall(".//*[@name='"+type+"']/stage")
    ret = []

    for i in stages:
        tmp = i.attrib
        for j in i:
            tmp[j.tag] = j.text
            if ('icon' == j.tag and j.attrib.has_key('type')):
                tmp['iconType'] = j.attrib['type']
        ret.append(tmp)
    return ret

def getBackend(type, order, **kwargs):
    """
    Возвращает содержимое тэга backend для заданного <type name=...><stage order=...>
    """
    global _root
    backend = _root.findall(".//*[@name='"+type+"']/stage[@order='"+order+"']/backend")
    backend = backend[0]
    if kwargs.get('noBackend'):
        return backend.text
    return type, backend.text

if _root is None:
    readConfig()

def getStagesList():
    global _root
    stages = _root.findall("*")
    ret = []
    for i in stages:
        ret.append(i.attrib['name'])
    return ret