# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 01.11.12
@summary: 
'''

from NPPlan.model.file.configReader import readConfig as _readConfig

_root = ''
def readConfig():
    global _root
    _root = _readConfig('ribbon')

def getItems(_from, itemList):
    ret = {}
    for i in itemList:
        ret[i] = _from.get(i, None)
    return ret

def getXmlItem(item, key):
    if len(item.findall(key)) > 0:
        return item.findall(key)[0].text
    else:
        return None

def parse():
    ret = {}

    pagesXML = _root.findall('page')
    pagesRet = {}
    panelsRet = {}
    itemsRet = {}
    for page in pagesXML:
        pagesRet[page.attrib['name']] = getItems(
            page.attrib,
            [
                'name',
                'rusname',
                'onstage',
                'onsubstage'
            ]
        )
        for panel in page.findall('panel'):
            panelsRet[panel.attrib['name']] = getItems(
                panel.attrib,
                [
                    'name',
                    'rusname',
                    'type',
                    'order',
                ]
            )
            if panel.attrib.has_key('rows'):
                panelsRet[panel.attrib['name']]['rows'] = sizeParse(panel.attrib['rows'])
            panelsRet[panel.attrib['name']]['parent'] = page.attrib['name']
            for item in panel.findall('item'):
                # @todo: list binding
                _item = {}
                _item.update(item.attrib)
                if 'order' not in _item:
                    _item['order'] = 0
                _item['id'] = item.findall('id')[0].text
                _item['name'] = item.findall('name')[0].text
                _item['rusname'] = item.findall('rusname')[0].text
                _item['click'] = getXmlItem(item, 'click')
                _item['rightClick'] = getXmlItem(item, 'rightClick')
                _item['ddClick'] = getXmlItem(item, 'ddClick')
                _item['type'] = getXmlItem(item, 'type')
                _item['icon'] = item.findall('icon')[0].text
                _item['iconSmall'] = getXmlItem(item, 'iconSmall')
                _item['iconDisabled'] = getXmlItem(item, 'iconDisabled')
                _item['iconType'] = item.findall('icon')[0].attrib.get('type', 'image')
                _item['iconSize'] = sizeParse(item.findall('icon')[0].attrib.get('size', '48,48'))
                _item['parent'] = panel.attrib['name']
                if len(item.findall('disabled')) > 0:
                    _item['disabled'] = True
                if len(item.findall('isLast')) > 0:
                    _item['isLast'] = True
                itemsRet[_item['name']] = _item

    ret['pages'] = pagesRet
    ret['panels'] = panelsRet
    ret['items'] = itemsRet
    return ret

def sizeParse(str):
    return (int(str.split(',')[0]), int(str.split(',')[1]),)