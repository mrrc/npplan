# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 26.09.12
@summary: 
'''

from NPPlan.model.file.configReader import readConfig as _readConfig

# @todo: abstract button reader

_root = {}
def readConfig(name):
    global _root
    if _root.has_key(name):
        return
    _root[name] = _readConfig(name)

def getButtons(name):
    global _root
    _ret = []
    if isinstance(name, int):
        buttons = _root[_root.keys()[name]].findall("./button")
    else:
        buttons = _root[name].findall("./button")
    for i in buttons:
        tmp = {}
        for btn in i:
            tmp[btn.tag] = btn.text
        _ret.append(tmp)
    return _ret