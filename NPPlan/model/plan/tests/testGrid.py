import NPPlan
import unittest


class MyTestCase(unittest.TestCase):
    def setUp(self):
        NPPlan.init(programPath="c:/NPPlan3/", test=True, log=False, utest=True)
        NPPlan.currentPlan = NPPlan.planAbstract()
        NPPlan.currentPlan.planLoadDicomSeriesFromFile("C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\")

    def test_GridBounds(self):
        grid = NPPlan.currentPlan.simCreateGridLocal()
        print grid.getSliceXYBounds(1, 1)
        self.assertEqual(grid.getSliceXYBounds(1, 0), (-92.5, 92.5, -92.5, 92.5))
        self.assertEqual(grid.getSliceXYBounds(2, 0), (-9.25, 9.25, -9.25, 9.25))
        self.assertEqual(grid.getSliceXYBounds(1, 1), (0, 185.0, 0, 185.0))

    def test_CellBounds(self):
        grid = NPPlan.currentPlan.simCreateGridLocal()
        grid.compression = 8
        self.assertEqual(grid.getCellXYBounds(2, 2), (-9.25, -8.9609375, -9.25, -8.9609375))
        grid.compression = 8
        self.assertEqual(grid.getCellXYBounds(2, 0), (-0.14453125, 0.14453125, -0.14453125, 0.14453125))
        grid.compression = 16
        self.assertEqual(grid.getCellXYBounds(2, 0), (-0.2890625, 0.2890625, -0.2890625, 0.2890625))
        self.assertEqual(grid.getCellXYBounds(2, 2), (-9.25, -9.25 + 0.2890625 * 2, -9.25, -9.25 + 0.2890625 * 2))


if __name__ == '__main__':
    unittest.main()
