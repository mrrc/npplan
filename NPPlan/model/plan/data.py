# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 2
@date: 16.05.13
@summary: 
'''

import NPPlan
# @todo: эта херня вызывается при старте, если модуля dicom нет то пиздец
try:
    from NPPlan.model.file.dicom import shortStructure
except ImportError:
    pass
from NPPlan.model.plan.images.imageList import image
from NPPlan.model.plan.images.container import sliceContainerBase
from NPPlan.model.plan.images.core import dicomImageTop, dicomImageElement
import os
try:
    import vtk
except ImportError:
    pass
# @todo: эта херня вызывается при старте, если модуля dicom нет то пиздец
try:
    from NPPlan.model.file.dicom import analyzer as DicomAnalyzer
except ImportError:
    pass
import datetime
from NPPlan.lib import parseObjectWithDotName as pd
#from NPPlan.model.mongo.kitmodels.plan import sliceDbElement


class pData(object):
    def __init__(self, parent):
        """
        @param parent: инстанс класса плана
        @type parent: NPPlan.model.plan.core.planAbstract
        """
        self._plan = parent
        self.__dict__['hasActive'] = False
        self.__dict__['errors'] = -1
        self.__dict__['slices'] = {}
        self.__dict__['creator'] = None
        self.__dict__['container'] = sliceContainerBase(self._plan, pData=self)
        self.__dict__['sliceNameToUid'] = {}
        self.__dict__['description'] = ''
        pass

    def loadDicomSeriesFromFile(self, seriesPath='', skipAnalyzer=False):
        #print seriesPath
        # @todo: рассматривать ориентацию сканирования (голова->ноги)
        self.__dict__['creator'] = 'dicomlocalseries'
        if os.path.isdir(seriesPath):
            self.__dict__['dir'] = seriesPath
        else:
            self.__dict__['dir'] = os.path.dirname(seriesPath)
        lf = sorted(os.listdir(self.__dict__['dir']))
        self.__dict__['filesList'] = lf
        self.__dict__['filesPaths'] = [os.path.join(self.__dict__['dir'], i) for i in lf]
        #print self.__dict__['filesList'], self.__dict__['filesPaths']
        #self.__dict__['imageInstance'] = image(seriesPath, thumbFlag=False)
        # 1. read main dicom data for all files (i.e. series data, patient data)
        topL = dicomImageTop(self.__dict__['filesPaths'][0])

        for i in range(len(self.__dict__['filesPaths'])):
            p = self.__dict__['filesPaths'][i]
            # 2. read dicom data for each file
            cF = dicomImageElement(p, parentData=topL, name=self.__dict__['filesList'][i])
            self.__dict__['container'].appendDicom(cF)
            self.__dict__['sliceNameToUid'][self.__dict__['filesList'][i]] = cF.getInstanceUID()

            #print cF
            # 3. vtkDicomReader for each file
            #cFvtkReader = vtk.vtkDICOMImageReader()
            #cFvtkReader.SetInputFile(cF)
            #self.__dict__['container'].appendVtk(cFvtkReader)

        # 4. vtkDicomReader for volume
        vtkVolume = vtk.vtkDICOMImageReader()
        vtkVolume.SetDirectoryName(self.__dict__['dir'])
        self.__dict__['vtkVolume'] = vtkVolume
        # 5. store 1-4 in __dict__
        # 6. getters for 5
        self.__dict__['hasActive'] = True
        if not skipAnalyzer:
            self._analyzeSlices()
        pass

    def getDicomLocalFolder(self):
        return self.__dict__['dir']

    def getFirstSlice(self, reverse=False):

        pass

    def getSlice(self, num=-1, location=None, uid=''):
        if -1 != num:
            return self.__dict__['container'].getSlice(
                uid=self.__dict__['container'].__dict__['numberToUid'][num]
            )

        pass

    def getRowsColumns(self):
        return int(self.__dict__['container'].getAnySlice().__dict__['data'][(0x0028, 0x0010)].value), \
               int(self.__dict__['container'].getAnySlice().__dict__['data'][(0x0028, 0x0011)].value)

    def iterateSlices(self):
        for i in self.__dict__['container'].iterateSlices():
            yield i

    def getFirstDicomImage(self):
        return self.__dict__['filesPaths'][0]

    def setCurrentSelectedByFileName(self, fname):
        for i in range(len(self.__dict__['filesList'])):
            if self.__dict__['filesList'][i] == fname:
                self._currentSelected = i

    def setRawCurrentSelected(self, pl):
        self._currentSelected = pl

    def getCurrentDicomImagePath(self):
        return self.__dict__['filesPaths'][self._currentSelected]

    def getSliceSpacing(self):
        # 0018, 0088 - spacing between
        # 0018, 0050 - slice thickness
        return self.__dict__['container'].getTagValue((0x0018, 0x0088), 'float')

    def getSliceThickness(self):
        return self.__dict__['container'].getTagValue((0x0018, 0x0050), 'float')
        pass

    def _analyzeSlices(self):
        # @todo: parallel analyzer
        self.__dict__['locationBounds'] = DicomAnalyzer.getInitialLocationBounds(self.__dict__['container'])
        self.__dict__['rescale'] = DicomAnalyzer.getRescaleParams(self.__dict__['container'].getAnySlice())
        #self.__dict__['sliceBounds'] =
        pass

    def getRescaleParams(self, flag=0):
        if 0 == flag:
            return self.__dict__['rescale']
        else:
            return self.__dict__['rescale']['slope'], self.__dict__['rescale']['intercept']

    def hasActivePlan(self):
        return self.__dict__['hasActive']

    def hasErrors(self):
        return self.__dict__['errors'] > 0

    def getLocalFolderPath(self):
        return self.__dict__['dir']

    def getSeriesFilenames(self):
        return self.__dict__['filesList']

    def hasSliceByFilename(self, fname):
        return fname in self.__dict__['filesList']

    def getSliceNameToUid(self):
        return self.__dict__['sliceNameToUid']

    def getSeriesLength(self):
        if not self.hasActivePlan():
            return -1
        return self.__dict__['container'].getSlicesQuantity()

    def setDescription(self, d):
        self.__dict__['description'] = d

    def getDescription(self):
        return self.__dict__['description']

    def getOriginalLocationBounds(self):
        return self.__dict__['locationBounds']

    def loadDicomSeriesFromNetwork(self, ):
        pass

    def getVtkVolumeReader(self):
        print self.__dict__['dir']
        return self.__dict__['vtkVolume']
        # return vtkDicomReader for volume
        pass

    def _getVtkImage(self, *args, **kwargs):
        # return vtkImage
        pass

    def setThumbsDir(self, dir):
        self._thumbDir = dir

    def getThumbsDir(self):
        return self._thumbDir or None

    def _getThumbnailButtons(self):
        # 'q': {'order': 0, 'text': 'test1', 'key': 'mode', 'default': 0},
        btn1 = {'order': 0, 'text': _('Name'), 'key': 'name', 'default': 1}
        btn2 = {'order': 1, 'text': _('Slice location'), 'key': 'loc', 'default': 0}
        btn3 = {'order': 2, 'text': _('Slice UID'), 'key': 'uid', 'default': 0}
        btn4 = {'order': 3, 'text': _('Instance data'), 'key': 'instance', 'default': 0}
        return {'btn1': btn1, 'btn2': btn2, 'btn3': btn3, 'btn4': btn4}
        pass

    def _getThumbnailItems(self):
        print '_getThumbnailItems'
        _ret = {}
        # @todo: implement thumbnail path !!!
        for val in self.__dict__['container'].iterateSlices():
            key = val[0]
            elem = val[1]
            print key, elem
            _ret[key] = {
                'name': elem.__dict__['name'],
                'loc': float(elem['SliceLocation']),
                'uid': key,
                'instance': int(elem['InstanceNumber']),
                'thumbnailPath': os.path.join(self.getThumbsDir(), elem.__dict__['name'] + '.png')
            }
        return _ret
        pass

    def getPixelSpacing(self, key=''):
        return self.__dict__['container'].getPixelSpacing(key)

    def toThumbnailData(self, ):
        return self._getThumbnailButtons(), self._getThumbnailItems()
        pass

    def getBitData(self, mode=-1):
        if -1 == mode:
            return {
                'bitsAllocated': self.__dict__['container'].getTagValue((0x0028, 0x0100), 'int'),
                'bitsStored': self.__dict__['container'].getTagValue((0x0028, 0x0101), 'int'),
                'highBit': self.__dict__['container'].getTagValue((0x0028, 0x0102), 'int'),
            }
        if 1 == mode:
            return (
                self.__dict__['container'].getTagValue((0x0028, 0x0100), 'int'),
                self.__dict__['container'].getTagValue((0x0028, 0x0101), 'int'),
                self.__dict__['container'].getTagValue((0x0028, 0x0102), 'int'),
            )

    def getDataModel(self, ):
        #self._dataModel = NPPlan.mongoConnection.plan
        #s = self.__dict__['container'].getAnySlice().__dict__['data']
        s = self.__dict__['container']
        _ret = {
            'seriesUID': s.getTagValue((0x0020, 0x000e)),
            'description': {
                'study': s.getTagValue((0x0008, 0x0030)),
                'series': s.getTagValue((0x0008, 0x0031)),
            },
            'source': self.getDataCreator(),
        }
        return _ret

    def getSlicesInfo(self):
        loc2id = self.__dict__['container'].getLocationToUid()
        num2id = self.__dict__['container'].getNumberToUid()
        sl2id = self.__dict__['sliceNameToUid']
        info = {}
        for k in [
            (loc2id, 'location'),
            (num2id, 'number'),
            (sl2id, 'filename'),
        ]:
            for i in k[0]:
                uid = k[0][i].replace('.', '_')
                self._addToSliceInfo(info, uid, k[1], i)
        return info
        pass

    def _addToSliceInfo(self, info, uid, key, value):
        if uid in info:
            info[uid][key] = value
        else:
            info[uid] = {key: value}

    def callSave(self, o, isNew=True):
        """
        @param o:
        @type o: NPPlan.model.mongo.kitmodels.systemPlan
        @param isNew:
        @type isNew: bool
        @return:
        """
        if self.__dict__['creator'] is not None \
            and self.__dict__['creator'] in ['dicomlocalseries', 'dicomnetworkseries']:
            # store loaded dicom values
            s = self.__dict__['container']
            for i in [
                ('seriesUID', (0x0020, 0x000e, 'str')),
                ('bodyPart', (0x0018, 0x0015, 'str')),
                ('sliceThickness', (0x0018, 0x0050, 'float')),
                ('source', (), 'getDataCreator', ()),
                ('pixelSpacing.x', (), 'getPixelSpacing', ('x', )),
                ('pixelSpacing.y', (), 'getPixelSpacing', ('y', )),
                ('rescale.intercept', (0x0028, 0x1052, 'int')),
                ('rescale.slope', (0x0028, 0x1053, 'int')),
                ('bitsData', (), 'getBitData', (-1, )),
                #('locationToUid', (), 'getLocationToUid', ()),
                #('numberToUid', (), 'getNumberToUid', ()),
                ('seriesLength', (), 'getSeriesLength', ()),
                ('image.rows', (0x0028, 0x0010, 'int')),
                ('image.columns', (0x0028, 0x0011, 'int')),
                ('samplesPerPixel', (0x0028, 0x0002, 'int')),
                ('creator.institution', (0x0008, 0x0080, 'str')),
                ('creator.institutionInfo', (0x0008, 0x0081, 'str')),
                ('creator.manufacturer', (0x0008, 0x0070, 'str')),
                ('creator.station', (0x0008, 0x1010, 'str')),
                ('creator.rphysician', (0x0008, 0x0090, 'str')),
                ('creator.pphysician', (0x0008, 0x1050, 'str')),
                ('creator.operator', (0x0008, 0x1070, 'str')),
                ('creator.device', (0x0018, 0x1000, 'str')),
                ('creator.private', (0x0019, 0x0010, 'str')),
                ('path.useLocal', (True, ),),
                ('path.local', (), 'getLocalFolderPath', ()),
                ('path.plan', ('npplan://station/planUID/', ), ),
                #('fileNameToUid', (), 'getSliceNameToUid', ()),
                ('description.study', (0x0008, 0x1030, 'str'), ),
                ('description.series', (0x0008, 0x103e, 'str'), ),
                ('description.plan', (), 'getDescription', ()),
                ('slices', (), 'getSlicesInfo', ()),
            ]:
                self.storeOneField(o, *i)
            #o['seriesUID'] = s.getTagValue((0x0020, 0x000e))
            o['edited'] = datetime.datetime.now()
        pass

    def callLoad(self, dbObject, **kwargs):
        # local loader
        if dbObject.path.useLocal:
            self.loadDicomSeriesFromFile(dbObject.path.local, skipAnalyzer=True)
            self.setDescription(dbObject.description.plan)
            pass
        # other loader
        pass

    def getLocationToUid(self):
        # proxy for container
        return self.__dict__['container'].getLocationToUid()

    def getNumberToUid(self):
        # proxy for container
        return self.__dict__['container'].getNumberToUid()

    def getPatientName(self):
        return self.__dict__['container'].getTagValue((0x0010, 0x0010), 'str')

    def storeOneField(self, dbObject=None, name='', tag=(), method='', margs=()):
        pass
        oVal = dbObject[name] if '.' not in name else pd(dbObject, name, 1)
        if 1 == len(tag):
            cVal = tag[0]
        elif '' == method:
            cVal = self.__dict__['container'].getTagValue((tag[0], tag[1]), tag[2])
        else:
            cVal = getattr(self, method)(*margs)
        print 'ov', name, method, oVal, cVal
        if oVal is None or oVal != cVal:
            if '.' not in name:
                dbObject[name] = cVal
            else:
                # @todo: refactor this ??
                vals = name.split('.')
                d = dbObject
                for i in range(len(vals) - 1):
                    d = getattr(d, vals[i])
                setattr(d, vals[-1], cVal)

        #if dbObject[name] is None or dbObject[name]

    def getDataCreator(self, *args, **kwargs):
        return self.__dict__['creator']


class simulationData(object):
    def __init__(self, parent):
        # used kitmodels: plan, systemPlan (planPositionData, planSimulationData), isotope,
        self._plan = parent
        # facilityParams - определяет тип шаблона расчёта
        self.__dict__['facilityParams'] = None
        # positionData - определяет конкретные параметры позиционирования
        # может быть несколько
        self.__dict__['positionData'] = None
        # contourData - информация о контурах
        self.__dict__['contourData'] = None
        # simulationData - информация о конкретном расчёте в выбранной среде моделирования
        self.__dict__['simulationData'] = None

    def setFacilityParams(self):
        pass

    def setPositionData(self, data):
        if self.__dict__['positionData'] is None:
            self.__dict__['positionData'] = data
        else:
            self.__dict__['positionData'].update(data)

    def setSimulationData(self, data):
        pass

    def callSave(self, o, isNew=True):
        """
        @param o:
        @type o: NPPlan.model.mongo.kitmodels.systemPlan
        @param isNew:
        @type isNew: bool
        @return:
        """
        o['beams'] = self.__dict__['positionData']
        o['edited'] = datetime.datetime.now()

    def callLoad(self, dbObject, **kwargs):
        self.__dict__['positionData'] = dbObject['beams']
        pass

    def createGridLocal(self, returnGrid=True):
        #self._plan.planGetVtkVolumeReader()
        print 'Folder', self._plan.planGetDicomLocalFolder()
        import NPPlan.model.calc.base.grid as simGrid
        from NPPlan.model.calc.mcnp.aggregate import baseMatsMethod
        from NPPlan.model.calc.mcnp.aggregate import lowerDensityMethod
        grid = simGrid.simulationGrid(self._plan)
        grid.start(self._plan.planIterateSlices).addContours(None).addFacility(None)

        for i, elem in self._plan.planIterateSlices():
            print i, elem.getNumber()
            print elem.getNumpyString()
        print self._plan.planGetRescaleParams()
        #print grid.setGridMethod(baseMatsMethod).toMcnp(simMethod=2, fullOutput=1)
        print 'grid get bounds', grid.getSliceXYBounds()
        if returnGrid:
            return grid
        pass
