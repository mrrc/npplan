# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 2
@date: 16.05.13
@summary: 
'''

import NPPlan
from NPPlan.model.abstracts.pathBinder import path

try:
    from NPPlan.model.file.dicom.shortStructure import readConcreteData, readCommonData
except ImportError:
    pass
try:
    from NPPlan.model.file.dicom.thumbnail import parseDir, simpleFile
except ImportError:
    pass

import os
# @todo: и это пофиксить при старте
try:
    import dicom
except ImportError:
    pass
import numpy as np


class dicomImageBase(object):
    def __getitem__(self, item):
        return self.__dict__['data'].get(item, None)

    def getNumber(self):
        print 'gettingNumber'
        return int(self.__dict__['data'][(0x0020, 0x0013)].value)
    number = property(getNumber)

    def getInstanceUID(self):
        return str(self.__dict__['data'][(0x0008, 0x0018)].value)
    pass


class dicomImageElement(dicomImageBase):
    def __init__(self, path, parentData=None, name='', skipNumpy=False):
        self.__dict__['data'] = dicom.read_file(path)
        self.__dict__['parent'] = parentData
        self.__dict__['path'] = path
        self.__dict__['name'] = name
        if not skipNumpy:
            self.__dict__['npString'] = np.fromstring(self.__dict__['data'].PixelData, dtype=np.int16)
            self.__dict__['npString2D'] = self.__dict__['npString'].reshape((self.__dict__['data'].Columns, self.__dict__['data'].Rows))
        pass

    def getNumber(self):
        return self.__dict__['data'][(0x0020, 0x0013)].value

    def getNumpyString(self):
        return self.__dict__['npString2D']

    pass


class dicomImageTop(dicomImageBase):
    def __init__(self, path):
        pass
        self.__dict__['data'] = dicom.read_file(path)
    pass


class imageBase(object):
    _path = ''
    def setDirName(self, name):
        self._path = path(localPath=name, type='image')
    def getDirName(self):
        return self._path
    dirName = property(getDirName, setDirName)

    def assertImageInstance(self, image):
        self._image = image
    def getImageInstance(self):
        return self._image
    image = property(getImageInstance)

    def mapNames(self):
        self._nameMapper = {}
        for i in self.images:
            self._nameMapper[self.images[i].name] = i

    def getByUID(self, uid):
        return self.images[uid]

    def getByName(self, name):
        return self.images[self._nameMapper[name]]


class imageDir(imageBase):
    def __init__(self, dir):
        """
        @param dir: директория
        @type dir: NPPlan.model.abstracts.pathBinder.path
        """
        self.images = {}
        self.dirName = dir
        for i in self.dirName.listDir():
            img = planImage(self, i)
            self.images[img.getUniqueSliceKey()] = img
        self.mapNames()

    def retrieveImages(self):
        return self.images

    def getAny(self):
        item = self.images.keys()[0]
        return self.images[item]

    def getNamesList(self):
        ret = []
        print self.images
        for i in sorted(self.images, key=lambda img: self.images[img]._dicomParams.SliceLocation):
            #print "id %s, name: %s, path: %s" %(i, self.images[i].name, self.images[i].pathString)
            ret.append([i, self.images[i].name, self.images[i].pathString])
        return ret

    def __repr__(self):
        return self.dirName.path


class thumbnailDir(imageBase):
    # @todo: implement load without parsing
    def __init__(self, reparse=False):
        self.reparseFlag = reparse
        self.images = {}
        self._nameMapper = {}

    def parse(self):
        if not self.reparseFlag:
            return
        output = self.dirName
        input = self._image.imageDir
        print output
        #print input
        #print input.imageDir.dirName.path
        #print input.getNamesList()
        namesList = input.getNamesList()
        parseDir(input.dirName.path+'/', output.path+'/')
        for i in namesList:
            self.images[i[0]] = thumbnailImage(self, i[0], i[1], os.path.join(output.path, '%s.png' %(i[1])))
        print namesList
        print self.images
        self.mapNames()

class planImageBase(object):
    def __init__(self, dir):
        """
        @param dir: директория со снимками
        @type dir: imageDir
        """
        self._dir = dir

    def load(self, name):
        self._filePath = path(localPath=os.path.join(self._dir.dirName.path, name))
        self._dicomParams = readConcreteData(self._filePath.path)

    def getPathAsString(self):
        return self._filePath.path
    pathString = property(getPathAsString)

    def getUniqueSliceKey(self):
        return self._dicomParams.SOPInstanceUID

class planImage(planImageBase):
    def __init__(self, dir, name):
        """
        @param dir: директория со снимками
        @type dir: imageDir
        """
        planImageBase.__init__(self, dir)
        self._dir = dir
        self.name = name
        self.load(name)

    def load(self, name):
        planImageBase.load(self, name)

    def getAssociatedThumbnail(self):
        return self._dir.image.thumbnailsDir.getByName(self.name)
    thumbnail = property(getAssociatedThumbnail)
        #print self._dicomParams.__dict__

class thumbnailImage(object):
    def __init__(self, dir, sopuid, name, path):
        self._dir = dir
        self.uid = sopuid
        self.name = name
        self.path = path
        pass

    def getPathAsString(self):
        return self.path
    pathString = property(getPathAsString)

    def getAssociatedImage(self):
        return self._dir.image.imageDir.getByUID(self.uid)
    image = property(getAssociatedImage)

    def recreateWithParams(self, window, level):
        f = simpleFile(self.image.pathString, self.path, window, level)


if __name__ == '__main__':
    p = imageDir('C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140')