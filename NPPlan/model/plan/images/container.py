# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 2
@date: 16.05.13
@summary: 
'''

import NPPlan
import __builtin__


class sliceContainerBase(object):
    def __init__(self, plan, pData=None):
        self.__dict__['parent'] = plan
        self.__dict__['parentPData'] = pData
        self.__dict__['slices'] = {}
        self.__dict__['data'] = {}
        self.__dict__['locationToUid'] = {}
        self.__dict__['numberToUid'] = {}

    def appendDicom(self, data):
        """
        @param data:
        @type data: NPPlan.model.plan.images.core.dicomImageElement
        @return:
        """
        uid = data.__dict__['data'][(0x0008, 0x0018)]
        self.__dict__['slices'][str(uid.value)] = data

        location = float(data.__dict__['data'][(0x0020, 0x1041)].value)
        number = int(data.__dict__['data'][(0x0020, 0x0013)].value)
        self.__dict__['locationToUid'][location] = str(uid.value)
        self.__dict__['numberToUid'][number] = str(uid.value)
        pass

    def getLocationToUid(self):
        return self.__dict__['locationToUid']

    def getNumberToUid(self):
        return self.__dict__['numberToUid']

    def appendVtk(self, vtkData):
        pass

    def iterateSlices(self, key=-1):
        for i in sorted(self.__dict__['slices']):
            yield (i, self.__dict__['slices'][i])

    def getSlicesQuantity(self):
        return len(self.__dict__['slices'])

    def getFirstSlice(self, reverse=False):
        return self.__dict__['parentPData'].getFirstSlice(reverse)
        pass

    def getAnySlice(self):
        return self.__dict__['slices'][self.__dict__['slices'].keys()[0]]

    def getSlice(self, uid):
        if not isinstance(uid, str):
            uid = str(uid.value)
        return self.__dict__['slices'][uid]

    def getSliceThickness(self):
        return float(self.getAnySlice().__dict__['data'][(0x0018, 0x0050)])

    def getTagValue(self, tag, t='str'):
        try:
            return getattr(__builtin__, t)(self.getAnySlice().__dict__['data'].get(tag).value)
        except AttributeError:
            return None

    def getPixelSpacing(self, key=''):
        xy = self.getAnySlice().__dict__['data'][(0x0028, 0x0030)]
        if '' == key:
            return (float(xy[0]), float(xy[1]))
        elif 'x' == key:
            return float(xy[0])
        elif 'y' == key:
            return float(xy[1])


