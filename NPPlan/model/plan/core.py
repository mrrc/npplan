# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 4
@date: 16.05.13
@summary: 
'''

import NPPlan
import data as pData
import inspect
import uuid
import datetime


def saver(funcToDecorate):
    def wrapper(*args, **kwargs):
        args[0].state = 's'
        funcToDecorate(*args, **kwargs)
        args[0].state = 'w'
    return wrapper


def loader(funcToDecorate):
    def wrapper(*args, **kwargs):
        args[0].state = 'l'
        funcToDecorate(*args, **kwargs)
        args[0].state = 'w'
    return wrapper


class planAbstract(object):
    def __init__(self, *args, **kwargs):
        self.__dict__['_invoked'] = []
        self.__dict__['data'] = pData.pData(self)
        self.__dict__['simData'] = pData.simulationData(self)
        self.__dict__['dbObject'] = None
        self.__dict__['state'] = 'w'
        self.invokeMethods(self.__dict__['data'], pData.pData, 'plan')
        self.invokeMethods(self.__dict__['simData'], pData.simulationData, 'sim')

    def invokeMethods(self, instance, obj, prependString):
        """
        Метод, исследующий открытые методы класса obj (методы, не имеющие _ первым символов) и устанавливающий
         у своего класса аттрибуты от созданных объектов instance с префиксом prependString.
        Таким образом, создаваемый объект данного класса позволяет обращаться к объектам порождаемых классов
        напрямую, минуя внутренние переменные данного класса
        @param instance: созданный класс объекта
        @type instance: instance
        @param obj: класс
        @type obj: classobj
        @param prependString: префикс имён после перегрузки
        @type prependString: str
        """
        self.__dict__['_invoked'].append((instance, obj, prependString))
        for i in inspect.getmembers(obj, predicate=inspect.ismethod):
            name = i[0]
            if name[0] == '_':
                continue
            else:
                name = name[0].upper() + name[1:]
            self.__setattr__(prependString + name, getattr(instance, i[0]))

    def hasDbObject(self):
        return self.__dict__['dbObject'] is None

    def getData(self):
        return self.__dict__['data']

    # inheritance
    def assertPatient(self, patient):
        """
        Привязывает план к конкретному пациенту
        @param patient: указатель на класс пациента
        @type patient: NPPlan.model.mongo.patient.patientObject.patient
        """
        self.__dict__['_patient'] = patient

    def assertImages(self, images):
        """
        Привязывает медицинские изображения к плану
        @param images: указатель на класс изображений
        @type images: NPPlan.model.plan.images.image
        """
        self.__dict__['_images'] = images
        pass

    def assertParentPlan(self, plan):
        pass
    def assertSiblingPlan(self, plan):
        pass
    def assertChildPlan(self, plan):
        pass

    # implements
    def setContours(self, contours):
        pass

    def setFacility(self, facility):
        self.__dict__['simData'].setFacilityParams(facility)
        pass

    def setPosition(self, position):
        self.__dict__['simData'].setPositionData(position)

    def setCalc(self, calc):
        pass

    def setResults(self, results):
        pass

    def setWorkState(self, s):
        print 'Current state %s, new state: %s' % (self.__dict__['state'], s)
        if s in ['s', 'w', 'l']:
            self.__dict__['state'] = s

    def getWorkState(self):
        return self.__dict__['state']

    @saver
    def save(self):
        f = False
        if self.__dict__['dbObject'] is None:
            # new plan
            self.__dict__['dbObject'] = NPPlan.mongoConnection.systemPlan()
            # generate plan UID
            self.__dict__['dbObject']['planUID'] = uuid.uuid1()
            self.__dict__['dbObject']['created'] = datetime.datetime.now()
            f = True
        # send dbObject to all child classes
        # if dbObject field is none or field value changed since last call - set it to actual value
        self.__dict__['data'].callSave(self.__dict__['dbObject'], isNew=f)
        self.__dict__['simData'].callSave(self.__dict__['dbObject'], isNew=f)
        print 'saving'
        print self.__dict__['dbObject']
        import pprint
        pprint.pprint(self.__dict__['dbObject'])
        self.__dict__['dbObject'].save()

    @loader
    def load(self, planUID=None, dbId=None):
        if planUID is not None:
            if isinstance(planUID, uuid.UUID):
                self.__dict__['dbObject'] = NPPlan.mongoConnection.systemPlan.find_one({'planUID': planUID})
            else:
                print planUID, type(planUID)
                planUID = uuid.UUID(planUID)
                print planUID, type(planUID)
                self.__dict__['dbObject'] = NPPlan.mongoConnection.systemPlan.find_one({'planUID': planUID})
                #print self.__dict__['dbObject'].count()
                #for i in NPPlan.mongoConnection.systemPlan.find():
                #    print i['planUID']
        if dbId is not None:
            self.__dict__['dbObject'] = NPPlan.mongoConnection.systemPlan.find_one({'_id': dbId})
        print self.__dict__['dbObject']

        self.__dict__['data'].callLoad(self.__dict__['dbObject'])
        # load data to child classes

    @saver
    def delete(self):
        if self.__dict__['dbObject'] is None:
            return
        self.__dict__['dbObject'].delete()
        self.__dict__['dbObject'] = None

    def getDbId(self):
        if self.__dict__['dbObject'] is None:
            return None
        return self.__dict__['dbObject']['_id']

    def getPlanId(self):
        if self.__dict__['dbObject'] is None:
            return None
        return self.__dict__['dbObject']['planUID']

    data = property(getData)
    state = property(getWorkState, setWorkState)
    dbId = property(getDbId)
    planId = property(getPlanId)


NPPlan.planAbstract = planAbstract
