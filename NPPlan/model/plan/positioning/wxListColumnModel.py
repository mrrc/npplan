# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 05.02.14
@summary: 
'''


class columnContainer(object):
    def __init__(self):
        self._data = {}
        self._fI = 0
        pass

    def addColumn(self, name='', category='', order=0, text='', width=0, image=''):
        self._data[self._fI] = {
            'name': name,
            'type': category,
            'order': order,
            'text': text,
            'width': width,
            'image': image,
        }
        self._fI += 1
        pass


class dvColumn(object):
    def __init__(self, title='', order=-1, size=30, dataType='string', reorderable=True, sortable=True,
                 resizeable=True):
        self.title = title
        self.order = order
        self.size = size
        self.dataType = dataType
        self.reorderable = reorderable
        self.sortable = sortable
        self.resizeable = resizeable
        pass