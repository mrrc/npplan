# -*- coding: utf8 -*-
"""
Модуль базовой инициализации приложения
@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 2
@date: 27.06.2012
@summary: _core
"""

import NPPlan
from NPPlan.model.abstracts.config import config as configManager
from NPPlan.model.errors import *
import os
import imp
import sys

NPPlan.config = configManager()
NPPlan.panelContainer = None


def init(**kwargs):
    """
    Инициализация базового уровня
    @summary: инициализация базового уровня
    @keyword programPath: директория запуска
    @type programPath: string || unicode
    @keyword test: если установлено в True, то главный контроллер вызываться не будет
    @type test: bool
    @keyword noPlan: не грузить plan если юниттест
    @keyword skipAfterPlan: завершить инициализацию сразу после подгрузки plan
    @todo: скипать дерево импортов
    """
    import NPPlan
    import socket
    NPPlan.config.programPath = kwargs.get('programPath')
    NPPlan.config.runningConfig = {'exiting': False, 'runningOs': getOs(),
                                   'usePlugins': kwargs.get('usePlugins', True),
                                   'hostname': socket.gethostname(),
                                   'fqdn': socket.getfqdn()}
    NPPlan.config.appPath = NPPlan.config.programPath+NPPlan.delimiter+'NPPlan'
    NPPlan.config.uuid = genAppHandle()
    NPPlan.config.pid = os.getpid()
    #NPPlan._programData['programPath'] = kwargs.get('programPath')
    #NPPlan._programData['runningConfig'] = {'exiting' : False, 'runningOs' : getOs()}
    #NPPlan._programData['appPath'] = NPPlan._programData['programPath']+'/NPPlan'
    #NPPlan._programData['uuid'] = genAppHandle()
    #NPPlan._programData['pid'] = os.getpid()
    if not kwargs.get('utest', False):
        import NPPlan.controller.console
    import NPPlan.model.file.config as configParser
    configParser.readConfig(NPPlan.config.programPath+NPPlan.delimiter+'NPPlan.ini')
    hasLog = kwargs.get('log', True)
    NPPlan.config.hasLog = hasLog
    if hasLog:
        import NPPlan.controller.logger as log
        log.init(int(NPPlan.config.appConfig.logger.verbose))
        log.log('core', 'Application started')
        #log.log('core', 'UUID generated: %s' %NPPlan._programData['uuid'])
        log.log('core', 'Program data: ')
        #log.log('core', NPPlan.getProgramData(), mode='con')
        log.log('core', NPPlan.config, mode='con')
        NPPlan.log = log.log
    else:
        NPPlan.log = lambda *args, **kwargs: None

    # register db models if connection exists
    try:
        import NPPlan.model.mongo.connector
        if NPPlan.mongoConnection is not None:
            import NPPlan.model.mongo.kitmodels
            NPPlan.config.runningConfig.hasDb = True
        else:
            NPPlan.config.runningConfig.hasDb = False
    except ImportError:
        NPPlan.config.runningConfig.hasDb = False

    if NPPlan.config.runningConfig.usePlugins:
        from NPPlan.model.pluginManager import pluginManager
        NPPlan.pluginManager = pluginManager()

    if kwargs.get('utest', False):
        if not kwargs.get('noPlan', False):
            import NPPlan.model.plan
        if kwargs.get('skipAfterPlan', True):
            return

    if 'client' == NPPlan.config.runningConfig.args.mode:
        from NPPlan.controller.client.mainC.abstracts.panelContainer import panelContainer
        NPPlan.panelContainer = panelContainer

    if not kwargs.get('test', False):
        try:
            import NPPlan.controller.main as starter
            starter.start()
        except NPPlanStartError:
            NPPlan.log('_core', 'Start failed with NPPlanStartError')
            sys.exit(-1)


def getRunningConfig(item=None):
    getattr(NPPlan.config.runningConfig, item)


def setRunningConfig(key, value=None):
    """
    Если key является dict, то объединяет конфиг с key, иначе устанавливает значения key конфига в value
    @todo: use setter as getRunningConfig
    @param key: ключ
    @type key: dict or string
    @param value: значение
    @type value: object
    """
    setattr(NPPlan.config.runningConfig, key, value)


def dumpRunningConfig():
    """
    Записывает текущее состояние конфига запуска в лог
    """
    try:
        NPPlan.log('core', 'Running config')
    except:
        import NPPlan.controller.logger as log
        log.log('core', 'Running config')
    # @todo: re implement
    #log.log('core', NPPlan.getRunningConfig())
    pass


def getProgramData():
    """
    Возвращает массив NPPlan._programData
    @summary: возвращает словарь с параметрами программы
    @return: NPPlan._programData
    @rtype: dict
    """
    return NPPlan._programData


def genAppHandle():
    """
    Возвращает UUID приложения U{http://tools.ietf.org/html/rfc4122.html} в вид строки
    @summary: возвращает UUID приложения
    @return: str(UUID)
    @rtype: string
    """
    import uuid
    return str(uuid.uuid1())


def getIconPath():
    #return r'C:\NPPlan3\NPPlan\images'
    #return NPPlan.config.appPath + '/images/'
    return os.path.join(NPPlan.config.appPath, 'images') + '/'

def getIcon(name, retVal=None):
    p = os.path.join(NPPlan.config.appPath, 'images', name)
    if os.path.exists(p):
        return p
    return retVal


def getOs():
    if sys.platform.startswith('win'):
        return 1
    elif sys.platform.startswith('linux'):
        return 2
    elif sys.platform.startswith('freebsd'):
        return 3


def pathJoin(*args):
    # @todo: reimplement os.path.join with win32 hack \\ -> /
    return os.path.join(*args)
    pass


def t():
    """
    timestamp
    @return: [hh:mm:ss]
    @rtype: str
    """
    import time
    return "[" + time.strftime("%H:%M:%S") + "] "

NPPlan.timestamp = t


if 1 == getOs():
    NPPlan.delimiter = delimiter = "\\"
else:
    NPPlan.delimiter = delimiter = '/'