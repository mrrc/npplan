# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 18.03.13
@summary: 
'''

try:
    from agw import foldpanelbar as fpb
except ImportError:
    import wx.lib.agw.foldpanelbar as fpb
import wx
import wx.lib.inspection
import NPPlan


class testPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        sizer = wx.BoxSizer(wx.VERTICAL)
        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        b1 = wx.Button(self, 10, "ttb", (20, 20))
        b2 = wx.Button(self, 10, "tttt2", (20, 20))
        b3 = wx.Button(self, 10, "tttds", (20, 20))
        btnSizer.AddMany([b1, b2, b3])
        #self.SetSizer(btnSizer)
        sizer.Add((1, 1), 1, wx.EXPAND)
        sizer.Add(btnSizer, 1, wx.EXPAND)
        sizer.Add((1, 1), 1, wx.EXPAND)
        self.SetSizer(sizer)
        self.SetSize((700, 400))


class Test(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, None, 1, 'FPB TEST', size=(520, 611))
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetBackgroundColour(wx.Color(255, 255, 255))

        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        b1 = wx.Button(self, 10, "init fpb", (20, 20))
        b2 = wx.Button(self, 10, "init fpb2", (20, 20))
        b3 = wx.Button(self, 10, "hide all fpb", (20, 20))
        btnSizer.AddMany([b1, b2, b3])
        #b4 = wx.Button(self, 10, "init fpb", (20, 20))

        self._container = NPPlan.panelContainer()

        self._panel = wx.Panel(self)
        #self._container.add('i', self._panel)
        self._container.i = self._panel
        self.renderPanel(self._panel, 'initial')

        sizer.Add(btnSizer, 0, wx.TOP | wx.LEFT | wx.EXPAND, 1)
        sizer.Add(self._panel, 0, wx.ALL | wx.EXPAND)
        #sizer.Add(self.ctrl, 0, wx.TOP|wx.LEFT, 8)
        #sizer.Add(self.ctrl2, 0, wx.TOP|wx.LEFT, 8)
        self.SetSizer(sizer)
        #sizer.Replace()
        #self.Bind(wx.EVT_BUTTON, self.initFpbOne, b1)
        b1.Bind(wx.EVT_BUTTON, self.initFpbOne)
        b2.Bind(wx.EVT_BUTTON, self.initFpbTwo)
        #self.Bind(wx.EVT_BUTTON, self.initFpbTwo, b2)
        b3.Bind(wx.EVT_BUTTON, self.hideFpbs)
        #self.Bind(wx.EVT_BUTTON, self.hideFpbs, b3)

    def renderPanel(self, p, t):
        #self._panel
        #self._panel.DestroyChildren()
        print t
        sizer = wx.BoxSizer(wx.VERTICAL)
        #wx.StaticText(self._panel, -1, t)
        sizer.Add(wx.StaticText(p, -1, t))
        sizer.Add(wx.StaticText(p, -1, t))
        sizer.Add(wx.StaticText(p, -1, t))
        sizer.Add(wx.StaticText(p, -1, t))
        sizer.Add(wx.StaticText(p, -1, t))
        sizer.Add(wx.StaticText(p, -1, t))
        sizer.Add(wx.StaticText(p, -1, t))
        sizer.Add(wx.StaticText(p, -1, t))
        p.SetSizer(sizer)

    def createFirstFPB(self, pnl):
        pnl.DestroyChildren()
        self._pnl = fpb.FoldPanelBar(pnl, -1, wx.DefaultPosition,
                                     wx.Size(-1,-1))
        item = self._pnl.AddFoldPanel("Caption Colours", collapsed=False,)
        self._pnl.AddFoldPanelWindow(item, wx.StaticText(item, -1, "Adjust The First Colour"),
                                     fpb.FPB_ALIGN_WIDTH, 5, 20)
        self.t2 = wx.StaticText(item, -1, 'ttt')
        self._pnl.AddFoldPanelWindow(item, self.t2,
                                     fpb.FPB_ALIGN_WIDTH, 5, 20)
        self._rslider2 = wx.Slider(item, -1, 0, 0, 255)
        self._pnl.AddFoldPanelWindow(item, self._rslider2, fpb.FPB_ALIGN_WIDTH, 2, 20)

        item = self._pnl.AddFoldPanel("Test Whole Panel", collapsed=True)
        _pnl = testPanel(item)
        self._pnl.AddFoldPanelWindow(item, _pnl, fpb.FPB_ALIGN_WIDTH | fpb.FPB_VERTICAL)

        item = self._pnl.AddFoldPanel("Misc Stuff", collapsed=True)

        button2 = wx.Button(item, wx.NewId(), "Collapse All")
        self._pnl.AddFoldPanelWindow(item, button2)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self._pnl, 0, wx.ALL | wx.EXPAND, 0)
        pnl.SetSizer(sizer)
        pnl.Layout()
        self.GetSizer().Layout()
        self._pnl.SetMinSize(pnl.GetSize())
        self.Layout()
        pass

    def initFpbOne(self, event):
        NPPlan.log('test', 'initFpbOne clicked')
        if not self._container.hasPanel('one'):
            #p = wx.Panel(self)
            #self._container.add('one', p)
            self._container.new(self, 'one')
            self._container.one.SetMinSize((700, 600))
            self._container.one.SetSize((700, 600))
            #self.renderPanel(self._container.one, 'one')
            self.createFirstFPB(self._container.one)
        self._container.replace(self.GetSizer(), 'i', 'one')
        #self._container.one.SetMinSize((700, 500))
        self.Layout()
        pass

    def initFpbTwo(self, event):
        print 'initFpbTwo clicked'
        self.renderPanel('2')
        pass

    def hideFpbs(self, event):
        print 'hideFpbs clicked'
        self._container.replace(self.GetSizer(), 'one', 'i')
        #self._panel.Hide()
        #self.GetSizer().Replace(self._panel, self._initial)
        #self._initial.Show()
        self.Layout()
        pass

if __name__ == '__main__':
    NPPlan.init(programPath='C:\\NPPlan3\\', test=True)
    app = wx.PySimpleApp()
    rtw = Test(app)
    rtw.Show()
    wx.lib.inspection.InspectionTool().Show()
    app.MainLoop()
    pass