#ifndef DetectorSD_h
#define DetectorSD_h 1

#include "G4VSensitiveDetector.hh"
class G4Step;
class RunAction;

//std::multimap<std::string, int> mParticlesMap;
//std::vector<std::string> mParticlesList;
//int jCounter;
#include <map>
#include <string>

class DetectorSD: public G4VSensitiveDetector 
{
  public:
    DetectorSD(G4String);
    ~DetectorSD();

    void Initialize(G4HCofThisEvent*);
    G4bool ProcessHits(G4Step*, G4TouchableHistory*);
    void EndOfEvent(G4HCofThisEvent*);
	void ParticlesAppender(std::string, unsigned long);
	void ParticlesAppenderLayer(std::string, unsigned long, unsigned long);
	void ParticlesAppenderEnergyLayer(std::string, G4double, unsigned long);
	void ParticlesAppenderKineticLayer(std::string, G4double, unsigned long);
	void ParticlesAppenderAnyDouble(std::multimap<std::string, G4double> *, std::string, G4double, unsigned long);
    
  private:
	G4String sName;
    RunAction* runAction;
    G4int nLayers;
    G4double* detEnergy;
	std::multimap<std::string, unsigned long> mParticlesMap;
	std::multimap<std::string, unsigned long> *mParticlesMapLayers;
	std::multimap<std::string, G4double> *mParticlesEnergiesLayers;
	std::multimap<std::string, G4double> *mParticlesKineticLayers;
	std::multimap<std::string, G4double> *mParticlesSourceLayers;
	std::multimap<std::string, G4double> *mParticlesSourceNumLayers;
};

#endif
