#ifndef RunAction_h
#define RunAction_h 1

class Hist1i;

#include "G4UserRunAction.hh"
#include "globals.hh"
#include <string>
#include <map>
#include <vector>
#include <ctime>
class G4Run;

class RunAction: public G4UserRunAction
{
  public:
    RunAction();
   ~RunAction();

  public:
    void BeginOfRunAction(const G4Run*);
    void EndOfRunAction(const G4Run*);
    
    void DisplayProgress(G4int);
    void FillEmitedHist(G4int);
    void FillEnergyHist(G4double);
    void FillEnergyProfile(G4double*);
    void LayerOut(G4double*);
	void ParticlesMapAppender(std::string, unsigned long);
	void ParticlesMapAppenderLayers(std::string, unsigned long, unsigned long);
	void ParticlesEnergiesAppenderLayers(std::string, G4double, unsigned long);
	void ParticlesKineticAppenderLayers(std::string, G4double, unsigned long);
	void ParticlesAppenderSource(std::string, G4double, unsigned long);
	void ParticlesAppenderNumSource(std::string, G4double, unsigned long);
	void ParticlesAppenderRAnyDouble(std::multimap<std::string, G4double> *, std::string, G4double, unsigned long);
	std::multimap<std::string, G4double> ParticlesSingleAppenderAnyDouble(std::multimap<std::string, G4double>, std::string, G4double);
	void GlassDataAppender(std::string, G4double, G4double, G4double);
	G4double GetValueDouble(std::multimap<std::string, G4double> obj, std::string pName);

  private:
    G4int eventsNumber;
    G4int printModulo;
    Hist1i* histEmited;
    Hist1i* histEnergy;
    Hist1i** histProfile;
    G4int nLayers;
    G4double* layen; //Selderey: переменная энергии в определённом слое
    std::multimap<std::string, unsigned long> mParticlesMapR;
    std::multimap<std::string, unsigned long> *mParticlesMapRLayers;
	std::multimap<std::string, G4double> *mParticlesREnergiesLayers;
	std::multimap<std::string, G4double> *mParticlesRKineticLayers;
	std::multimap<std::string, G4double> *mParticlesRSourceLayers;
	std::multimap<std::string, G4double> *mParticlesRSourceNumLayers;

	std::vector<std::string> glassRParticlesList;
	std::multimap<std::string, G4double> glassRParticlesNums;
	std::multimap<std::string, G4double> glassRParticlesEnergies;
	std::multimap<std::string, G4double> glassRParticlesKinetic;

	clock_t beginAt;
	clock_t finishedAt;
};

#endif
