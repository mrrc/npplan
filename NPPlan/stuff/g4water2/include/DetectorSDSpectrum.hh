#ifndef DetectorSDSpectrum_h
#define DetectorSDSpectrum_h 1
#include "G4VSensitiveDetector.hh"
#include <map>

class G4Step;
class RunAction;

class DetectorSDSpectrum: public G4VSensitiveDetector 
{
	public:
		DetectorSDSpectrum(G4String);
		DetectorSDSpectrum(G4String, G4double, G4double, G4double);
		DetectorSDSpectrum(G4String, G4double, G4double, G4double, G4int);
		~DetectorSDSpectrum();
		void Initialize(G4HCofThisEvent*);
		G4bool ProcessHits(G4Step*, G4TouchableHistory*);
		void EndOfEvent(G4HCofThisEvent*);
	private:
		RunAction* runAction;
		G4double lowLimit;
		G4double highLimit;
		G4double spStep;
		G4String detName;
		G4int useSide;

		std::ofstream fileoutPart;
		G4String fnamePart;
		G4bool flushNow;

		std::multimap<G4String, G4double> *mParticle2Energy;

		void _createStorage(void);
};
#endif