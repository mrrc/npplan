#include "PrimaryGeneratorAction.hh"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleDefinition.hh"
#include "Randomize.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction()
{
  particleGun = new G4ParticleGun(1);

  particleGun->SetParticleDefinition(G4Electron::ElectronDefinition());
  particleGun->SetParticleEnergy(5.4*GeV);
  //sigmaMomentumY = 0.0262;   // 1.5 градуса
  //sigmaMomentumX = 0.0262;   // 1.5 градуса
  sigmaMomentumY = 0.0;
  sigmaMomentumX = 0.0;
}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete particleGun;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* event)
{
  // определяем пучок частиц:
  // это частицы которые стартуют из плоскости Z = 0
  // при этом равномерно распределенные в направлениях X и Y
  // таким образом пучек имеет квадратное сечение
  //particleGun->SetParticleMomentumDirection(G4ThreeVector(0, 0, 1));
  G4double x = 4*cm*(1 - 2*G4UniformRand());
  G4double y = 4*cm*(1 - 2*G4UniformRand());
  particleGun->SetParticlePosition(G4ThreeVector(x, y, -6.2*m));


  // Гауссово распределение для поворота
  G4double momentumX = 0.0;
  G4double momentumY = 0.0;
  G4double momentumZ = 1.0;

  if ( sigmaMomentumX  > 0.0 )
    {
      momentumX += G4RandGauss::shoot( 0., sigmaMomentumX );
    }
  if ( sigmaMomentumY  > 0.0 )
    {
      momentumY += G4RandGauss::shoot( 0., sigmaMomentumY );
    }
 
  particleGun -> SetParticleMomentumDirection( G4ThreeVector(momentumX,momentumY,momentumZ) );

  // источник испускает одну частицу
  particleGun->GeneratePrimaryVertex(event);
}

