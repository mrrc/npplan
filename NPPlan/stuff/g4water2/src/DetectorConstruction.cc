#include "DetectorConstruction.hh"
#include "DetectorSD.hh"
#include "DetectorSDGlass.hh"

#include "DetectorSDSpectrum.hh"

#include "DetectorSDVox.hh"


#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4RotationMatrix.hh"
#include "G4NistManager.hh"
#include "globals.hh"
#include "G4VisAttributes.hh" 
#include "G4SDManager.hh"
#include "G4Region.hh"
#include "G4ProductionCuts.hh"
#include <fstream> 
#include <stdio.h>
#include <stdlib.h>
#include "ChamberParameterisation.hh"
#include "G4PVParameterised.hh"
#include "DetectorSDSimple.hh"

DetectorConstruction::DetectorConstruction() {}

DetectorConstruction::~DetectorConstruction() {}

// Р_Р°РєС_Р_С_ Р_Р>С_ Р_РїС_РчР_РчР>РчР_РёС_ Р_Р>РёР_С< Р_Р°С_С_РёР_Р°
#define SIZE(x) sizeof(x)/sizeof(*x)

G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // --- materials ---
  nistMan = G4NistManager::Instance();
  G4Material* air = nistMan->FindOrBuildMaterial("G4_AIR");
  G4Material* plex = nistMan->FindOrBuildMaterial("G4_PLEXIGLASS");
  G4Material* polystyrene = nistMan->FindOrBuildMaterial("G4_POLYSTYRENE");
  //G4Material* cells = nistMan->FindOrBuildMaterial("G4_MUSCLE_SKELETAL_ICRP");
  G4Material* water = nistMan->FindOrBuildMaterial("G4_WATER");
  G4Material* glass = nistMan->FindOrBuildMaterial("G4_GLASS_PLATE");
  G4Element* elH = new G4Element("Hydrogen", "H" , 1., 1.01*g/mole);
  G4Element* elC = new G4Element("Carbon", "C" , 6., 12.01*g/mole);
  G4Element* elN = new G4Element("Nitrogen", "N" , 7., 14.01*g/mole);
  G4Element* elO = new G4Element("Oxygen", "O" , 8., 16.00*g/mole);
  G4Element* elLi = new G4Element("Lithium", "Li" , 3., 6.94*g/mole);
  G4Element* elCl = new G4Element("Chlorine", "Cl" , 17., 35.45*g/mole);
  G4Element* elK = new G4Element("Potassium", "K", 19., 39.10*g/mole);
  G4Element* elBr = new G4Element("Bromine", "Br", 35., 79.90*g/mole);

  G4Material* Photo = new G4Material("Photo", 1.31*g/cm3, 8);  
  Photo->AddElement(elH, 0.050376414); 
  Photo->AddElement(elLi, 0.00084737);
  Photo->AddElement(elC, 0.621319937);
  Photo->AddElement(elN, 0.000171061);
  Photo->AddElement(elO,0.324100862);
  Photo->AddElement(elCl, 0.001731371);
  Photo->AddElement(elK, 0.000477409);
  Photo->AddElement(elBr, 0.000975575);

  getBigBoxData();
  getExpContainerData();
  getExpSeriesLength();
  getExpType();



  G4bool checkOverlaps = true;

   //     
  // World
  //
  G4double world_sizeXYZ = 12.2*m;
  
  G4Box* solidWorld =    
    new G4Box("World",                       //its name
       0.5*world_sizeXYZ, 0.5*world_sizeXYZ, 0.5*world_sizeXYZ);     //its size
      
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        air,           //its material
                        "World");            //its name
                                   
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "World",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      checkOverlaps);        //overlaps checking
  
  
	//plexLayer
	     G4Box* solidPlexLayer =    
    new G4Box("PlexLayer",                         //its name
        16.4*cm, 11.25*cm, 2.75*mm); //its size
      
    G4LogicalVolume* logicPlexLayer =                         
    new G4LogicalVolume(solidPlexLayer,            //its solid
                        plex,              //its material
                        "PlexLayer");              //its name
          
    new G4PVPlacement(0,                     //no rotation
                    G4ThreeVector(0, 0, -21.275*cm),  //at (0,0,0)
                    logicPlexLayer,                //its logical volume
                    "PlexLayer",                   //its name
                    logicWorld,              //its mother  volume
                    false,                   //no boolean operation
                     0,                     //copy number
                     checkOverlaps);        //overlaps checking
               
    // Glass box      
	     G4Box* solidGlassLayer =    
    new G4Box("GlassLayer",                         //its name
        16.4*cm, 11.25*cm, 5*mm); //its size
      
    G4LogicalVolume* logicGlassLayer =                         
    new G4LogicalVolume(solidGlassLayer,            //its solid
                        plex, //glass,              //its material
                        "GlassLayer");              //its name
          
    new G4PVPlacement(0,                     //no rotation
                    G4ThreeVector(0, 0, -22.275*cm),  //at (0,0,0)
                    logicGlassLayer,                //its logical volume
                    "GlassLayer",                   //its name
                    logicWorld,              //its mother  volume
                    false,                   //no boolean operation
                     0,                     //copy number
                     checkOverlaps);        //overlaps checking                     

                   

  // Big Water Bor


	//G4cout<<"Big water box has (x,y,z) "<<bigBoxSzX<<" "<<bigBoxSzY<<" "<<bigBoxSzZ<<" Center at "<<
	//	bigBoxSzX / 2.0<<" "<<bigBoxSzY / 2.0<<" "<<bigBoxSzZ / 2.0<<G4endl;

  G4Box* outterWBox =    
    new G4Box("outterWBox",                         //its name
	0.5*bigBoxSzX, 0.5*bigBoxSzY, 0.5*(bigBoxSzZ)); //its size

  G4Material *innerMaterial;
  if (0 == outterMaterialType) {
	innerMaterial = water;
  } else if (1 == outterMaterialType) {
	innerMaterial = air;
  }

  G4LogicalVolume* logicOutterWBox =                         
    new G4LogicalVolume(outterWBox,            //its solid
						air,              //its material
                        "outterWBoxVol");              //its name

    new G4PVPlacement(0,                     //no rotation
                    G4ThreeVector(0, 0, 0*cm),  //at (0,0,0)
					logicOutterWBox,                //its logical volume
                    "outterWBoxPlace",                   //its name
                    logicWorld,              //its mother  volume
                    false,                   //no boolean operation
                     0,                     //copy number
                     checkOverlaps);        //overlaps checking

	G4Box *innerWBox = 
		new G4Box("innerWBox",                         //its name
		0.5*expBigBoxX, 0.5*expBigBoxY, 0.5*(expBigBoxZ*expLen)); //its size

	G4LogicalVolume* logicInnerWBox =                         
		new G4LogicalVolume(innerWBox,            //its solid
							innerMaterial,              //its material
							"innerWBoxVol");              //its name
	G4double inpPlace = -0.5*(bigBoxSzZ)+0.5*(expBigBoxZ*expLen);
    new G4PVPlacement(0,                     //no rotation
                    G4ThreeVector(0, 0, inpPlace),  //at (0,0,0)
					logicInnerWBox,                //its logical volume
                    "innerWBoxPlace",                   //its name
                    logicOutterWBox,              //its mother  volume
                    false,                   //no boolean operation
                     0,                     //copy number
                     checkOverlaps);        //overlaps checking
  	
  // --- sensitive detectors ---
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  G4LogicalVolume *lastLayer;

  if (3 != expType) {
	  G4double lastPos;
	  if (0 != expLen) {
		addExpSeriesContainers(logicInnerWBox, sdMan, 0, (expBigBoxZ*expLen), "_box");
		lastPos = -0.5*bigBoxSzZ + 0.5*expBigBoxZ + expBigBoxZ*expLen;
		G4cout<<"Last container ends at "<< lastPos <<G4endl;
		G4cout<<"Every end "<< lastPos + expBigBoxZ <<G4endl;
		if (lastPos + expBigBoxZ < bigBoxSzZ / 2) {
			if (1 == expType) {
				lastLayer = addExpSeriesContainers(logicOutterWBox, sdMan, 1, lastPos, "_last");
				G4LogicalVolume *detLog = addZVoxelsLayer(lastLayer, G4ThreeVector(0, 0, 0), "w1vox1_", water, expBigBoxX / 2, expBigBoxY / 2, expBigBoxInnerZ / 2, nLayers, sdMan);

				DetectorSD* sens = new DetectorSD("box1");
				sdMan->AddNewDetector(sens);
				detLog->SetSensitiveDetector(sens);

			} else if (2 == expType) {
				lastLayer = addExpSeriesContainers(logicOutterWBox, sdMan, 1, lastPos, "_last1");
				addZVoxelsLayer(lastLayer, G4ThreeVector(0, 0, 0), "w1vox1_", water, expBigBoxX / 2, expBigBoxY / 2, expBigBoxInnerZ / 2, nLayers, sdMan);

				addVoxelsLayer(logicOutterWBox, G4ThreeVector(0, 0, lastPos+expBigBoxZ-photoWidth), "photo", Photo, 2.5*cm / 2, 5*cm/2, photoWidth / 2, voxelsNumber, sdMan, false);

				G4LogicalVolume *lastLayer2 = addExpSeriesContainers(logicOutterWBox, sdMan, 1, lastPos+expBigBoxZ+photoWidth+0.1*mm, "last2");
				addZVoxelsLayer(lastLayer2, G4ThreeVector(0, 0, 0), "w2vox2_", water, expBigBoxX / 2, expBigBoxY / 2, expBigBoxInnerZ / 2, nLayers, sdMan);
			}
		}
	  } else {
		  lastPos = -0.5*bigBoxSzZ + expBigBoxZ / 2;
		  lastLayer = addExpSeriesContainers(logicOutterWBox, sdMan, 1, lastPos, "_first");
		  addZVoxelsLayer(lastLayer, G4ThreeVector(0, 0, 0), "w1vox1_", water, expBigBoxX / 2, expBigBoxY / 2, expBigBoxInnerZ / 2, nLayers, sdMan);
	  }
  } else {
	  G4cout<<"expType: epi"<<G4endl;
G4double stPoz = -0.5*(expBigBoxZ*expLen);
G4LogicalVolume *t0;
DetectorSDSimple *st0 = new DetectorSDSimple("apt0", 8, 0);

t0 = addEpisContainers(logicInnerWBox, sdMan, "tt0", stPoz+2*cm);
		// test parametrise
		G4VPVParameterisation* chamberParam;
		chamberParam = new ChamberParameterisation(expLen, -1, expBigBoxInnerZ, expBigBoxX, expBigBoxY, expBigBoxZ, 3);

		G4VPhysicalVolume* physiChamber;

		physiChamber = new G4PVParameterised(
								"ChamberEpi",       // their name
								t0,    // their logical volume
								logicInnerWBox,    // Mother logical volume
								kZAxis,          // Are placed along this axis 
								8,    // Number of chambers
								chamberParam);   // The parametrisation
		t0->SetSensitiveDetector(st0);
		sdMan->AddNewDetector(st0);
/*
t0 = addEpisContainers(logicInnerWBox, sdMan, "tt0", stPoz+2*cm);
t0->SetSensitiveDetector(st0);
sdMan->AddNewDetector(st0);
t1 = addEpisContainers(logicInnerWBox, sdMan, "tt1", stPoz+8*cm);
t1->SetSensitiveDetector(st1);
sdMan->AddNewDetector(st1);
t2 = addEpisContainers(logicInnerWBox, sdMan, "tt2", stPoz+15.5*cm);
t2->SetSensitiveDetector(st2);
sdMan->AddNewDetector(st2);
int i;
i = 0;
t3 = addEpisContainers(logicInnerWBox, sdMan, "tt3", stPoz+(15.5+7.5+1.5*i)*cm);
t3->SetSensitiveDetector(st3);
sdMan->AddNewDetector(st3);
i = 1;
t4 = addEpisContainers(logicInnerWBox, sdMan, "tt4", stPoz+(15.5+7.5+1.5*i)*cm);
t4->SetSensitiveDetector(st4);
sdMan->AddNewDetector(st4);
i = 2;
t5 = addEpisContainers(logicInnerWBox, sdMan, "tt5", stPoz+(15.5+7.5+1.5*i)*cm);
t5->SetSensitiveDetector(st5);
sdMan->AddNewDetector(st5);
i = 3;
t6 = addEpisContainers(logicInnerWBox, sdMan, "tt6", stPoz+(15.5+7.5+1.5*i)*cm);
t6->SetSensitiveDetector(st6);
sdMan->AddNewDetector(st6);
i = 4;
t7 = addEpisContainers(logicInnerWBox, sdMan, "tt7", stPoz+(15.5+7.5+1.5*i)*cm);
t7->SetSensitiveDetector(st7);
sdMan->AddNewDetector(st7);
/*for(int i = 0; i < 5; i++) {
  addEpisContainers(logicInnerWBox, sdMan, "tt7", stPoz+(15.5+7.5+1.5*i)*cm);
}*/
//t7->SetSensitiveDetector(st7);


//sdMan->AddNewDetector(st3);
//sdMan->AddNewDetector(st4);
//sdMan->AddNewDetector(st5);
//sdMan->AddNewDetector(st6);
//sdMan->AddNewDetector(st7);
  }

  
  return physWorld;
}

void  DetectorConstruction::getBigBoxData(void) {
	std::ifstream expLData;
	expLData.open("bigContainerData.in", std::ifstream::in);
	expLData>>bigBoxSzX;
	expLData>>bigBoxSzY;
	expLData>>bigBoxSzZ;
	bigBoxSzX *= cm;
	bigBoxSzY *= cm;
	bigBoxSzZ *= cm;
	G4cout<<"-------- BIG WATER BOX DATA ----------"<<G4endl;
	G4cout<<"Box size (mm): "<<bigBoxSzX<<" x"
		<<bigBoxSzY<<" x"
		<<bigBoxSzZ<<G4endl;
	G4cout<<"-------- END OF BIG WATER BOX DATA ----------"<<G4endl;
	expLData.close();
}

void DetectorConstruction::getExpContainerData(void) {
	std::ifstream expLData;
	expLData.open("expContainerData.in", std::ifstream::in);
	expLData>>expBigBoxX;
	expLData>>expBigBoxY;
	expLData>>expBigBoxZ;
	expLData>>expFrontForL;
	expLData>>expFrontBacL;
	expLData>>airGap;
	expBigBoxX *= mm;
	expBigBoxY *= mm;
	expBigBoxZ *= mm;
	expFrontForL *= mm;
	expFrontBacL *= mm;
	airGap *= mm;
	expBigBoxInnerZ = expBigBoxZ - (expFrontForL+expFrontBacL+airGap);
	G4double pos;
	G4int nC;
	G4cout<<"-------- EXPERIMENT CONTAINER DATA ----------"<<G4endl;
	G4cout<<"Experiment containers data read:"<<G4endl;
	G4cout<<"Box size (mm): "<<expBigBoxX<<" x"
		<<expBigBoxY<<" x"
		<<expBigBoxZ<<G4endl;
	G4cout<<"Sides (mm): Front: "<<expFrontForL<<" Back: "<<expFrontBacL<<G4endl;
	G4cout<<"Air gap (mm): "<<airGap<<G4endl;
	G4cout<<"-------- END OF EXPERIMENT CONTAINER DATA ----------"<<G4endl;
	expLData.close();
}

G4int DetectorConstruction::getExpSeriesLength(void) {
	std::ifstream expLData;
	expLData.open("expData.in", std::ifstream::in);
	expLData>>expLen;
	expLData>>outterMaterialType;
	G4cout<<"Reading experimental boxes length: "<<expLen<<G4endl;
	if (0 == outterMaterialType) {
		G4cout<<"Outter material: water"<<G4endl;
	} else if (1 == outterMaterialType) {
		G4cout<<"Outter material: air"<<G4endl;
	}
	expLData.close();
	return expLen;
}

void DetectorConstruction::getExpType(void) {
	std::ifstream expLData;
	expLData.open("expType.in", std::ifstream::in);
	expLData>>expType;
	expLData>>nLayers;
	if (2 == expType) {
		expLData>>voxelsNumber;
		expLData>>photoWidth;
		photoWidth *= mm;
	}
	G4cout<<"Experiment type: "<<expType<<G4endl;
	G4cout<<"Number of layers in last box: "<<nLayers<<G4endl;
	if (2 == expType) {
		G4cout<<"Number of voxels in film: "<<voxelsNumber<<G4endl;
		G4cout<<"Film width (mm): "<<photoWidth<<G4endl;
	}
	expLData.close();
}

G4LogicalVolume* DetectorConstruction::addVoxelsLayer(G4LogicalVolume* parentVolume, 
										  G4ThreeVector position, 
										  G4String lName, 
										  G4Material* material,
										  G4double blX,
										  G4double blY,
										  G4double blZ,
										  G4int qLayers,
										  G4SDManager* sdMan) 
{
	//G4Material* water = nistMan->FindOrBuildMaterial("G4_WATER");

	// Voxels container
	G4Box* solidBox =    
    new G4Box(lName+"BoxS",                         //its name
        blX, blY, blZ); //its size
      
	G4LogicalVolume* logicBox =                         
		new G4LogicalVolume(solidBox,            //its solid
							material,              //its material
							lName+"BoxL");              //its name
          
    new G4PVPlacement(0,                     //no rotation
                    position,  //at (0,0,0)
                    logicBox,                //its logical volume
                    lName+"BoxP",                   //its name
                    parentVolume,              //its mother  volume
                    false,                   //no boolean operation
                     0,                     //copy number
                     true);        //overlaps checking

	// Voxels replica
	G4VSolid* layerX =
		new G4Box(lName+"layerX", blX, blY / qLayers, blZ);
    
	G4LogicalVolume* logicLayer =
		new G4LogicalVolume(layerX, material, lName+"layerXL");
   
    new G4PVReplica(lName+"layerXR", logicLayer, 
                 logicBox, kYAxis, qLayers, 2 * blY / qLayers);  


	G4VSolid* layersY =
		new G4Box(lName+"layerY", blX / qLayers, blY / qLayers, blZ);
    G4LogicalVolume* logicLayerY =
		new G4LogicalVolume(layerX, material, lName+"layerYL");
   
    new G4PVReplica(lName+"layerYR", logicLayerY, 
                 logicLayer, kXAxis, qLayers, 2 * blX / qLayers);  

	G4LogicalVolume* det_log;
    G4Box* det_box = new G4Box(lName+"detector", blX / qLayers, blY / qLayers, blZ);
    det_log = new G4LogicalVolume(det_box, material, lName+"detectorL");
    G4VPhysicalVolume* det_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), det_log, lName+"detectorP", logicLayerY, false, 0);

	DetectorSDVox* vox1 = new DetectorSDVox(lName+"vox", qLayers);
	sdMan->AddNewDetector(vox1);
	det_log->SetSensitiveDetector(vox1);

	return logicBox;
}

G4LogicalVolume* DetectorConstruction::addZVoxelsLayer(G4LogicalVolume* parentVolume, 
										  G4ThreeVector position, 
										  G4String lName, 
										  G4Material* material,
										  G4double blX,
										  G4double blY,
										  G4double blZ,
										  G4int qLayers,
										  G4SDManager* sdMan) 
{
	//G4Material* water = nistMan->FindOrBuildMaterial("G4_WATER");

	// Voxels container
	G4Box* solidBox =    
    new G4Box(lName+"BoxS",                         //its name
        blX, blY, blZ); //its size
      
	G4LogicalVolume* logicBox =                         
		new G4LogicalVolume(solidBox,            //its solid
							material,              //its material
							lName+"BoxL");              //its name
          
    new G4PVPlacement(0,                     //no rotation
                    position,  //at (0,0,0)
                    logicBox,                //its logical volume
                    lName+"BoxP",                   //its name
                    parentVolume,              //its mother  volume
                    false,                   //no boolean operation
                     0,                     //copy number
                     true);        //overlaps checking

	// Voxels replica
	G4VSolid* layerX =
		new G4Box(lName+"layerZ", blX, blY, blZ / qLayers);
    
	G4LogicalVolume* logicLayer =
		new G4LogicalVolume(layerX, material, lName+"layerZL");
   
    new G4PVReplica(lName+"layerZR", logicLayer, 
                 logicBox, kZAxis, qLayers, 2 * blZ / qLayers);  

	G4LogicalVolume* det_log;
	G4Box* det_box = new G4Box(lName+"detector", blX, blY, blZ / qLayers);
    det_log = new G4LogicalVolume(det_box, material, lName+"detectorL");
    G4VPhysicalVolume* det_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), det_log, lName+"detectorP", logicLayer, false, 0);

	//DetectorSDVox* vox1 = new DetectorSDVox(lName+"vox", qLayers);
	//sdMan->AddNewDetector(vox1);
	//det_log->SetSensitiveDetector(vox1);

	return det_log;
}

G4LogicalVolume* DetectorConstruction::addVoxelsLayer(G4LogicalVolume* parentVolume, 
										  G4ThreeVector position, 
										  G4String lName, 
										  G4Material* material,
										  G4double blX,
										  G4double blY,
										  G4double blZ,
										  G4int qLayers,
										  G4SDManager* sdMan,
										  G4bool halfSize) 
{
	if (true == halfSize) {
		return addVoxelsLayer(parentVolume, position, lName, material, blX / 2, blY / 2, blZ / 2, qLayers, sdMan);
	} else {
		return addVoxelsLayer(parentVolume, position, lName, material, blX, blY, blZ, qLayers, sdMan);
	}
}


G4LogicalVolume* DetectorConstruction::addExpContainer(G4LogicalVolume* parentVolume, G4double zposition, G4String eName, G4SDManager* sdMan, G4bool needCalc) {
	G4Material* plex = nistMan->FindOrBuildMaterial("G4_PLEXIGLASS");
	G4Material* water = nistMan->FindOrBuildMaterial("G4_WATER");
	// Experimental container
	G4double expBigBoxX = 5*cm;
	G4double expBigBoxY = 5*cm;
	G4double expBigBoxZ = 15.2*mm;
	G4double expFrontForL = 1.4*mm;
	G4double expFrontBacL = 1.2*mm;
	G4double airGap = 1.2*mm;

	G4ThreeVector position = G4ThreeVector(0, 0, plexLastLayerPosition+0.5*expBigBoxZ);//+1.5*airGap);

	G4bool checkOverlaps = true;

	G4Box* solidExpLayer =    
		new G4Box(eName+"ExpLayerS",                         //its name
		expBigBoxX / 2, expBigBoxY / 2, expBigBoxZ / 2); //its size
    G4LogicalVolume* logicExpLayer = 
    new G4LogicalVolume(solidExpLayer,            //its solid
                        plex, //glass,              //its material
                        eName+"ExpLayerL");              //its name

    new G4PVPlacement(0,                     //no rotation
                    position,
                    logicExpLayer,                //its logical volume
                    eName+"ExpLayerP",                   //its name
                    parentVolume,              //its mother  volume
                    false,                   //no boolean operation
                     0,                     //copy number
                     checkOverlaps);        //overlaps checking

	G4Box* liquidExpLayer =    
		new G4Box(eName+"ExpLayerLS",                         //its name
		expBigBoxX / 2, expBigBoxY / 2, (expBigBoxZ - expFrontBacL - expFrontForL) / 2); //its size
    G4LogicalVolume* logicLiquidExpLayer = 
    new G4LogicalVolume(liquidExpLayer,            //its solid
                        water,               //its material
                        eName+"ExpLayerLL");              //its name

    new G4PVPlacement(0,                     //no rotation
		G4ThreeVector(0, 0, (expFrontForL - expFrontBacL) / 2),  //at (0,0,0)
                    logicLiquidExpLayer,                //its logical volume
                    eName+"ExpLayerLP",                   //its name
                    logicExpLayer,              //its mother  volume
                    false,                   //no boolean operation
                     0,                     //copy number
                     checkOverlaps);        //overlaps checking

	//DetectorSDSpectrum* spectrum = new DetectorSDSpectrum(eName+"wSpectrum", 0, 6000, 100);
	//sdMan->AddNewDetector(spectrum);
	//logicExpLayer->SetSensitiveDetector(spectrum);

	return logicLiquidExpLayer;
}

void DetectorConstruction::addExpBigContainer(G4LogicalVolume* parentVolume, G4SDManager* sdMan) {
	G4Material* plex = nistMan->FindOrBuildMaterial("G4_PLEXIGLASS");
	G4Material* water = nistMan->FindOrBuildMaterial("G4_WATER");
	G4Material* air = nistMan->FindOrBuildMaterial("G4_AIR");

	std::ifstream expLData;
	expLData.open("expdata.in", std::ifstream::in);
	G4int expLen;
	expLData>>expLen;
	G4double pos;
	G4int nC;
	G4cout<<"Reading experimental position, lines "<<expLen<<G4endl;
	
	G4double expBigBoxX = 5*cm;
	G4double expBigBoxY = 5*cm;
	G4double expBigBoxZ = 15.2*mm;
	G4double expFrontForL = 1.4*mm;
	G4double expFrontBacL = 1.2*mm;
	G4double airGap = 1.2*mm;
	G4double waterThickness = 12.6 * mm;
	G4double waterGap = 1.2 * mm;

	G4bool checkOverlaps = true;
	
	G4LogicalVolume** bigBoxes = new G4LogicalVolume*[expLen];
	G4LogicalVolume** waterBoxes = new G4LogicalVolume*[expLen];
	G4LogicalVolume** airGapes = new G4LogicalVolume*[expLen];

	//G4VSolid* calorSolid = new G4Box("CalorBox", expBigBoxX / 2, expBigBoxY / 2, (expBigBoxZ+airGap)*expLen / 2);
	G4Box* calorSolid = new G4Box("CalorBox", expBigBoxX / 2, expBigBoxY / 2, (expBigBoxZ) / 2);
	G4int i;

	char n1[255];
	
	for (i = 0; i < expLen; i++) {
		sprintf(n1, "CalorLogical%d", i);
		bigBoxes[i] = new G4LogicalVolume(calorSolid, plex, (G4String)n1);
		new G4PVPlacement(
			0,
			G4ThreeVector(0.,0., -164*mm+(expBigBoxZ+airGap)*i),
			bigBoxes[i], 
			(G4String)n1, 
			parentVolume,
			false,
			i, 
			checkOverlaps
		);
	}

	G4Box* waterSolid = new G4Box("WaterCalorBox", expBigBoxX / 2, expBigBoxY / 2, (expBigBoxZ - (expFrontBacL + expFrontForL)) / 2);

	for (i = 0; i < expLen; i++) {
		sprintf(n1, "CalorWaterLogical%d", i);
		waterBoxes[i] = new G4LogicalVolume(waterSolid, plex, (G4String)n1);
		new G4PVPlacement(
			0,
			G4ThreeVector(0.,0., (expFrontForL - expFrontBacL) / 2),
			waterBoxes[i], 
			(G4String)n1, 
			bigBoxes[i],
			false,
			i, 
			checkOverlaps
		);
	}
	/*
	G4Box* airGapSolid = new G4Box("AirGapBox", expBigBoxX / 2, expBigBoxY / 2, airGap / 2);
	for (i = 0; i < expLen; i++) {
		sprintf(n1, "CalorAirGapLogical%d", i);
		airGapes[i] = new G4LogicalVolume(airGapSolid, plex, (G4String)n1);
		new G4PVPlacement(
			0,
			G4ThreeVector(0.,0., -164*mm+(expBigBoxZ+airGap)*i+((expBigBoxZ+airGap) / 2)),
			airGapes[i], 
			(G4String)n1, 
			parentVolume,
			false,
			i, 
			checkOverlaps
		);
	}
	*/

  //
  // Regions
  //
	for(i=0;i<expLen;i++)
	{
		sprintf(n1, "CalorLogical%d", i);
		G4Region* aRegion = new G4Region(n1);
		bigBoxes[i]->SetRegion(aRegion);
		aRegion->AddRootLogicalVolume(bigBoxes[i]);
	}
	return ;
}

void DetectorConstruction::addExpBigContainerNormed(G4LogicalVolume* parentVolume, G4SDManager* sdMan, G4double maxSizeZ) {
	G4Material* plex = nistMan->FindOrBuildMaterial("G4_PLEXIGLASS");
	G4Material* water = nistMan->FindOrBuildMaterial("G4_WATER");
	G4Material* air = nistMan->FindOrBuildMaterial("G4_AIR");

	std::ifstream expLData;
	expLData.open("expdata.in", std::ifstream::in);
	G4int expLen;
	expLData>>expLen;
	G4double pos;
	G4int nC;
	G4cout<<"Reading experimental position, lines "<<expLen<<G4endl;

	//G4double bigBoxSzZ = 41*cm;
	//G4double bigBoxSzZ = 31*cm;
	G4double expBigBoxX = 5*cm;
	G4double expBigBoxY = 5*cm;
	G4double expBigBoxZ = 15.2*mm;
	G4double expFrontForL = 1.4*mm;
	G4double expFrontBacL = 1.2*mm;
	G4double expBigBoxInnerZ = expBigBoxZ - (expFrontBacL + expFrontForL);
	G4double airGap = 1.2*mm;
	G4double bigBoxSzZ = (expLen+1)*(expBigBoxZ+airGap);
	G4double waterThickness = 12.6 * mm;
	G4double waterGap = 1.2 * mm;

	G4bool checkOverlaps = true;

	G4double plexZCenter = -1.0 * (maxSizeZ - bigBoxSzZ) / 2;
	G4cout<<"plexZCenter "<<plexZCenter<<G4endl;

	plexLastLayerPosition = plexZCenter + bigBoxSzZ / 2;


  G4Box* innerPlexBox =    
    new G4Box("innerPlexBox",                         //its name
	0.5*expBigBoxX, 0.5*expBigBoxY, 0.5*bigBoxSzZ); //its size

  G4LogicalVolume* innerPlexLogicBox =                         
    new G4LogicalVolume(innerPlexBox,            //its solid
                        plex,              //its material
                        "innerPlexBoxVol");              //its name

    new G4PVPlacement(0,                     //no rotation
                    G4ThreeVector(0, 0, plexZCenter),  //at (0,0,0)
					innerPlexLogicBox,                //its logical volume
                    "innerPlexBoxPlace",                   //its name
					parentVolume,              //its mother  volume
                    false,                   //no boolean operation
                     0,                     //copy number
                     checkOverlaps);        //overlaps checking

	G4int i;
	//G4LogicalVolume* *logicContainer = new G4LogicalVolume*[expLen];
	//G4VPhysicalVolume* *physicContainer = new G4VPhysicalVolume*[expLen];

	G4Box* waterSolid = new G4Box("innerWaterBox", expBigBoxX / 2, expBigBoxY / 2, expBigBoxInnerZ / 2);

	//for (i = 0; i < expLen; i++) {
//		logicContainer[i] = (G4LogicalVolume*)0;
		//physicContainer[i] = (G4VPhysicalVolume*)0;
	//}

	G4LogicalVolume* vol = new G4LogicalVolume(waterSolid,            //its solid
                        water,              //its material
                        "innerWaterVol");

	//addVoxelsLayer(vol, G4ThreeVector(0, 0, -6*mm), "vox1", water, expBigBoxX / 2, expBigBoxY / 2, 0.1*mm, 20, sdMan);
	
	G4Box* airSolid = new G4Box("innerAirBox", expBigBoxX / 2, expBigBoxY / 2, airGap / 2);
	G4LogicalVolume* airVol = new G4LogicalVolume(airSolid,            //its solid
                        air,              //its material
                        "innerAirVol");
	new G4PVPlacement(0,                     //no rotation
						G4ThreeVector(0, 0, -5.7*mm),  //at (0,0,0)
						airVol,                //its logical volume
						"innerAirBoxPlace",                   //its name
						vol,              //its mother  volume
						true,                   //no boolean operation
						 0,                     //copy number
						 checkOverlaps);        //overlaps checking

	/*
	G4double _pos;
	for (i = 0; i < expLen; i++) {
		logicContainer[i] = new G4LogicalVolume(waterSolid,            //its solid
                        water,              //its material
                        "innerWaterVol");              //its name
	}
	for (i = 0; i < expLen; i++) {
		_pos = -1.0 * (bigBoxSzZ / 2) + expBigBoxInnerZ*i + expBigBoxInnerZ / 2;
		G4cout<<"Position for "<<i<<": "<<_pos<<G4endl;
		physicContainer[i] = new G4PVPlacement(0,                     //no rotation
						G4ThreeVector(0, 0, _pos),  //at (0,0,0)
						logicContainer[i],                //its logical volume
						"innerWaterBoxPlace",                   //its name
						innerPlexLogicBox,              //its mother  volume
						true,                   //no boolean operation
						 i,                     //copy number
						 checkOverlaps);        //overlaps checking
	}*/
	G4VPVParameterisation* chamberParam;
	/*chamberParam = new ChamberParameterisation(  
                           expLen,          // NoChambers 
                           -1.0 * (bigBoxSzZ / 2) + expBigBoxInnerZ / 2,         // Z of center of first 
						   expBigBoxZ,        // Z spacing of centers
						   expBigBoxInnerZ,          // Width Chamber 
                           (expLen+1)*expBigBoxZ/expLen,           // lengthInitial 
                           (expLen+1)*expBigBoxZ);           // lengthFinal*/
	chamberParam = new ChamberParameterisation(expLen-1, bigBoxSzZ, expBigBoxInnerZ, expBigBoxX, expBigBoxY, expBigBoxZ, 1);

	G4VPhysicalVolume* physiChamber;

    physiChamber = new G4PVParameterised(
                            "Chamber",       // their name
                            vol,    // their logical volume
                            innerPlexLogicBox,    // Mother logical volume
                            kZAxis,          // Are placed along this axis 
                            expLen-1,    // Number of chambers
                            chamberParam);   // The parametrisation

	/*
	G4Box* airSolid = new G4Box("innerAirBox", expBigBoxX / 2, expBigBoxY / 2, airGap / 2);
	G4LogicalVolume* airVol = new G4LogicalVolume(airSolid,            //its solid
                        air,              //its material
                        "innerAirVol");

	G4VPVParameterisation* chamberParamAir;

	chamberParamAir = new ChamberParameterisation(expLen, bigBoxSzZ, expBigBoxInnerZ, expBigBoxX, expBigBoxY, expBigBoxZ, 2);
    physiChamber = new G4PVParameterised(
                            "ChamberAir",       // their name
                            airVol,    // their logical volume
                            innerPlexLogicBox,    // Mother logical volume
                            kZAxis,          // Are placed along this axis 
                            expLen,    // Number of chambers
                            chamberParam);   // The parametrisation
	*/
    DetectorSDSimple* detector = new DetectorSDSimple("DetectorSDSimple", expLen);
    sdMan->AddNewDetector(detector);
    vol->SetSensitiveDetector(detector);

	
}

G4LogicalVolume* DetectorConstruction::addExpSeriesContainers(G4LogicalVolume* parentVolume, G4SDManager* sdMan, G4int _tType, G4double zPos, G4String wName) {
	G4Material* plex = nistMan->FindOrBuildMaterial("G4_PLEXIGLASS");
	G4Material* polystyrene = nistMan->FindOrBuildMaterial("G4_POLYSTYRENE");
	G4Material* water = nistMan->FindOrBuildMaterial("G4_WATER");
	G4Material* air = nistMan->FindOrBuildMaterial("G4_AIR");

	G4String aName;
	if (0 == _tType) {
		aName = "_rep";
	} else {
		aName = "_target";
	}

	aName += wName;

	G4Box* bigPlexBoxWAir = 
		new G4Box("bigPlexBoxWAir"+aName,
		0.5*expBigBoxX, 
		0.5*expBigBoxY, 
		0.5*expBigBoxZ);

	G4LogicalVolume* innerPlexLogicBox = 
		new G4LogicalVolume(bigPlexBoxWAir, 
		polystyrene,              //its material
		"bigPlexBoxWAirVol"+aName);

	G4Box* airGapBox =
		new G4Box("airGapBox"+aName,
		0.5*expBigBoxX, 
		0.5*expBigBoxY, 
		0.5*airGap);

	G4LogicalVolume* airGapLogicBox = 
		new G4LogicalVolume(airGapBox, 
		air,              //its material
		"airGapBoxVol"+aName);

	G4double airGapPlace = -1 * expBigBoxZ / 2 + airGap / 2;

	new G4PVPlacement(0,                     //no rotation
		G4ThreeVector(0, 0, airGapPlace),  //at (0,0,0)
		airGapLogicBox,                //its logical volume
		"airGapBoxPlace"+aName,                   //its name
		innerPlexLogicBox,              //its mother  volume
		true,                   //no boolean operation
		0,                     //copy number
		true);        //overlaps checking

	G4Box* waterBox =
		new G4Box("waterBox"+aName,
		0.5*expBigBoxX, 
		0.5*expBigBoxY, 
		0.5*expBigBoxInnerZ);

	G4LogicalVolume* waterLogicBox = 
		new G4LogicalVolume(waterBox, 
		water,              //its material
		"waterBoxVol"+aName);

	G4double waterCenterZ = -1.0*expBigBoxZ/2+airGap+expFrontForL+expBigBoxInnerZ/2;
	G4cout<<"Water Center: "<<waterCenterZ<<G4endl;
	G4cout<<"Water Size: "<<expBigBoxInnerZ<<G4endl;

	new G4PVPlacement(0,                     //no rotation
		G4ThreeVector(0, 0, waterCenterZ),  //at (0,0,0)
		waterLogicBox,                //its logical volume
		"waterBoxPlace"+aName,                   //its name
		innerPlexLogicBox,              //its mother  volume
		true,                   //no boolean operation
		0,                     //copy number
		true);        //overlaps checking

	if (0 == _tType) {
		DetectorSDSimple* detector = new DetectorSDSimple("containersWater", expLen, 1);
		sdMan->AddNewDetector(detector);
		waterLogicBox->SetSensitiveDetector(detector);

		// test parametrise
		G4VPVParameterisation* chamberParam;
		chamberParam = new ChamberParameterisation(expLen, zPos, expBigBoxInnerZ, expBigBoxX, expBigBoxY, expBigBoxZ, 2);

		G4VPhysicalVolume* physiChamber;

		physiChamber = new G4PVParameterised(
								"Chamber"+aName,       // their name
								innerPlexLogicBox,    // their logical volume
								parentVolume,    // Mother logical volume
								kZAxis,          // Are placed along this axis 
								expLen,    // Number of chambers
								chamberParam);   // The parametrisation
	} else {
			// test placement
		DetectorSDSpectrum* spectrum = new DetectorSDSpectrum("lastLayer"+aName+"Spectrum", 0, 6000, 100);
		sdMan->AddNewDetector(spectrum);
		innerPlexLogicBox->SetSensitiveDetector(spectrum);

		new G4PVPlacement(0,                     //no rotation
			G4ThreeVector(0, 0, zPos),  //at (0,0,0)
			innerPlexLogicBox,                //its logical volume
			"testPlexPos"+aName,                   //its name
			parentVolume,              //its mother  volume
			true,                   //no boolean operation
			0,                     //copy number
			true);        //overlaps checking

	}
	if (0 == _tType) {
		return innerPlexLogicBox;
	} else {
		return waterLogicBox;
	}
}

G4LogicalVolume* DetectorConstruction::addEpisContainers(G4LogicalVolume* parent, G4SDManager* sdMan, G4String name, G4double pos) {
G4RotationMatrix *rotZ = new G4RotationMatrix;
rotZ->rotateX(90*degree);
	G4Material* plex = nistMan->FindOrBuildMaterial("G4_PLEXIGLASS");
	G4Material* water = nistMan->FindOrBuildMaterial("G4_WATER");
	G4Material* polystyrene = nistMan->FindOrBuildMaterial("G4_POLYSTYRENE");
	G4Tubs *t0 = new G4Tubs("t0",
		4*mm,
		5*mm,
		3*mm,
		0*degree,
		360*degree
		);

	G4Tubs *t1 = new G4Tubs("t1",
		0*mm,
		4*mm,
		3*mm,
		0*degree,
		360*degree
		);

	G4LogicalVolume* outterEpi = 
		new G4LogicalVolume(t0, 
		polystyrene,              //its material
		"epi0");

	G4LogicalVolume* innerEpi = 
		new G4LogicalVolume(t1, 
		water,              //its material
		"epi1");

	new G4PVPlacement(rotZ,                     //no rotation
		G4ThreeVector(0, 0, 0),  //at (0,0,0)
		innerEpi,                //its logical volume
		"epi1P",                   //its name
		outterEpi,              //its mother  volume
		true,                   //no boolean operation
		0,                     //copy number
		true);        //overlaps checking

	/*new G4PVPlacement(rotZ,                     //no rotation
		G4ThreeVector(0, 0, pos),  //at (0,0,0)
		outterEpi,                //its logical volume
		"epi1P",                   //its name
		parent,              //its mother  volume
		true,                   //no boolean operation
		0,                     //copy number
		true);        //overlaps checking*/

return outterEpi;


}
