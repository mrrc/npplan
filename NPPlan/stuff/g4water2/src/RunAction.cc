#include "RunAction.hh"
#include "Hist1i.h"
#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4Run.hh"
#include "Randomize.hh"
#include <string>
#include <map>
#include <vector>
#include <ctime>

RunAction::RunAction() {}

RunAction::~RunAction() {}

void RunAction::BeginOfRunAction(const G4Run* run)
{
  eventsNumber = run->GetNumberOfEventToBeProcessed();
  printModulo = eventsNumber/100;
  if (printModulo == 0)
    printModulo = 1;

  beginAt = clock();

  // узнаем количество слоев в детекторе
  nLayers = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->nLayers;
  
  // создаем гистограммы
  /*histEmited = new Hist1i(0, 15, 50);
  histEnergy = new Hist1i(0, 15, 50);
  histProfile = new Hist1i*[nLayers];
  for (int i = 0; i < nLayers; ++i)
    histProfile[i] = new Hist1i(0, 15, 50);*/
    
  layen = new G4double[nLayers];
  mParticlesMapRLayers = new std::multimap<std::string, unsigned long>[nLayers];
  mParticlesREnergiesLayers = new std::multimap<std::string, G4double>[nLayers];
  mParticlesRKineticLayers = new std::multimap<std::string, G4double>[nLayers];
  mParticlesRSourceLayers = new std::multimap<std::string, G4double>[nLayers];
  mParticlesRSourceNumLayers = new std::multimap<std::string, G4double>[nLayers];

  glassRParticlesEnergies.clear();
  glassRParticlesNums.clear();
  glassRParticlesKinetic.clear();
  glassRParticlesList.clear();
}

void RunAction::GlassDataAppender(std::string pName, G4double kin, G4double dep, G4double sum) {
  if(std::find(glassRParticlesList.begin(), glassRParticlesList.end(), pName) != glassRParticlesList.end()) {
    
  } else {
	  glassRParticlesList.push_back(pName);
  }
	glassRParticlesKinetic = ParticlesSingleAppenderAnyDouble(glassRParticlesKinetic, pName, kin);
	glassRParticlesEnergies = ParticlesSingleAppenderAnyDouble(glassRParticlesEnergies, pName, dep);
	glassRParticlesNums = ParticlesSingleAppenderAnyDouble(glassRParticlesNums, pName, sum);
}

std::multimap<std::string, G4double> RunAction::ParticlesSingleAppenderAnyDouble(std::multimap<std::string, G4double> obj, std::string pName, G4double value) {
	if (obj.count(pName) == 0) {
		obj.insert(std::pair<std::string, G4double>(pName, value));
	} else {
		obj.find(pName)->second += value;
	}
	return obj;
}

void RunAction::ParticlesMapAppender(std::string pName, unsigned long value) {
//G4cout<<pName<<": "<<value<<G4endl;
	if (mParticlesMapR.count(pName) == 0) {
		mParticlesMapR.insert(std::pair<std::string, unsigned long>(pName, value));
	} else {
		mParticlesMapR.find(pName)->second += value;
	}
}

void RunAction::ParticlesMapAppenderLayers(std::string pName, unsigned long value, unsigned long layer) {
//G4cout<<pName<<": "<<value<<G4endl;
	if (mParticlesMapRLayers[layer].count(pName) == 0) {
		mParticlesMapRLayers[layer].insert(std::pair<std::string, unsigned long>(pName, value));
	} else {
		mParticlesMapRLayers[layer].find(pName)->second += value;
	}
}

void RunAction::ParticlesEnergiesAppenderLayers(std::string pName, G4double energy, unsigned long layer) {
	//mParticlesREnergiesLayers[layer].insert(std::pair<std::string, G4double>(pName, energy));
	if (mParticlesREnergiesLayers[layer].count(pName) == 0) {
		mParticlesREnergiesLayers[layer].insert(std::pair<std::string, G4double>(pName, energy));
	} else {
		mParticlesREnergiesLayers[layer].find(pName)->second += energy;
	}
}

void RunAction::ParticlesKineticAppenderLayers(std::string pName, G4double energy, unsigned long layer) {
	//mParticlesRKineticLayers[layer].insert(std::pair<std::string, G4double>(pName, energy));
	if (mParticlesRKineticLayers[layer].count(pName) == 0) {
		mParticlesRKineticLayers[layer].insert(std::pair<std::string, G4double>(pName, energy));
	} else {
		mParticlesRKineticLayers[layer].find(pName)->second += energy;
	}
}

void RunAction::ParticlesAppenderSource(std::string pName, G4double energy, unsigned long layer) {
	ParticlesAppenderRAnyDouble(mParticlesRSourceLayers, pName, energy, layer);
}
void RunAction::ParticlesAppenderNumSource(std::string pName, G4double energy, unsigned long layer) {
	ParticlesAppenderRAnyDouble(mParticlesRSourceNumLayers, pName, energy, layer);
}

void RunAction::ParticlesAppenderRAnyDouble(std::multimap<std::string, G4double> *obj, std::string pName, G4double value , unsigned long layer) {
	if (obj[layer].count(pName) == 0) {
		obj[layer].insert(std::pair<std::string, G4double>(pName, value));
	} else {
		obj[layer].find(pName)->second += value;
	}
	//return obj
}

G4double RunAction::GetValueDouble(std::multimap<std::string, G4double> obj, std::string pName) {
	if (obj.count(pName) == 0) {
		return 0.0;
	} else {
		return obj.find(pName)->second;
	}
}

void RunAction::DisplayProgress(G4int eventID)
{
  if (eventID%printModulo == 0)
    G4cout << "Progress: " << 100*eventID/eventsNumber << "%\r" << std::flush;
}

void RunAction::FillEmitedHist(G4int photons)
{
  //if (photons > 0)
//    histEmited->fill(photons);
}

void RunAction::FillEnergyHist(G4double energy)
{
  //if (energy > 0)
//    histEnergy->fill(energy/MeV);
}

void RunAction::FillEnergyProfile(G4double* energy)
{
  //for (unsigned long i = 0; i < nLayers; ++i)
//    if (energy[i] > 0)
      //histProfile[i]->fill(energy[i]/MeV);
}

void RunAction::LayerOut(G4double* energy)
{
	//for (unsigned long i=0; i<=nLayers; ++i)
	//layen[i] += energy[i]; 
	//Selderey: отсюда убрала вывод и добавила суммирование энергии в каждом слое
}


void RunAction::EndOfRunAction(const G4Run*)
{
  // сохраняемся
  /*histEmited->save("spectrum-emited.csv", "\"#photons\", N");
  histEnergy->save("spectrum-energy.csv", "\"energy, MeV\", N");
  for (unsigned long i = 0; i < nLayers; ++i) 
    {
    char fname[100];
    sprintf(fname, "spectrum-energy-%d.csv", i);
    histProfile[i]->save(fname, "\"energy, MeV\", N");
    }
    //Selderey:а вывод добавила сюда
    
    std::ofstream fileout;
    G4String fname = "layers.out";
    fileout.open(fname);
    
    G4cout <<"Layer ...........Energy, MeV \n";
    fileout<<"Layer ...........Energy, MeV \n";
	for (unsigned long i=0; i<nLayers; ++i){
	G4cout << i << "                 " << layen[i]/MeV<<G4endl;
	
	fileout<< i << "                 " << layen[i]/MeV<<G4endl;
    }
    
	fileout.close();

  // и чистим
  delete histEmited;
  delete histEnergy;
  for (unsigned long i = 0; i < nLayers; ++i)
    delete histProfile[i];
  delete [] histProfile;
  layen = 0;*/

  finishedAt = clock();
  double elapsedSecs = double(finishedAt - beginAt) / CLOCKS_PER_SEC;
  G4cout<<"Time elapsed: "<<elapsedSecs<<G4endl;

  unsigned long sum;
  std::ofstream fileoutPart;
  G4String fnamePart = "particleLayers.out";
  fileoutPart.open(fnamePart);

  std::ofstream fileoutEnPart;
  G4String fnameEnPart = "particleDepositEnergies.out";
  fileoutEnPart.open(fnameEnPart);

  std::ofstream fileoutKinPart;
  G4String fnameKinPart = "particleKineticSum.out";
  fileoutKinPart.open(fnameKinPart);
  for (unsigned long i = 0; i < nLayers; ++i) {
    sum = 0;
	//G4cout<<"Layer "<<i<<G4endl;
	fileoutPart<<"Layer "<<i<<G4endl;
    for (std::multimap<std::string, unsigned long>::iterator it = mParticlesMapRLayers[i].begin();
       it != mParticlesMapRLayers[i].end();
       ++it) {
       //G4cout << "  [" << (*it).first << ", " << (*it).second << "]" << G4endl;
		   //if (mParticlesRSourceNumLayers[i].count((*it).first) != 0) {
		//	   fileoutPart << "  [" << (*it).first << ", " << (*it).second << "] from source "<<mParticlesRSourceNumLayers[i].find((*it).first)->second << G4endl;
		   //} else {

		       fileoutPart << "  [" << (*it).first << ", " << (*it).second << "]" << G4endl;
		   //}
	       sum += (*it).second;
    }

    //G4cout<<"Summary events in layer: "<<sum<<G4endl;
	fileoutPart<<"Summary events in layer: "<<sum<<G4endl;

	//G4cout<<G4endl;
	fileoutPart<<G4endl;

	fileoutEnPart<<"Layer "<<i<<G4endl;
    for (std::multimap<std::string, G4double>::iterator it = mParticlesREnergiesLayers[i].begin();
       it != mParticlesREnergiesLayers[i].end();
       ++it) {
       //G4cout << "  [" << (*it).first << ", " << (*it).second << "]" << G4endl;
	   //fileoutEnPart << "  [" << (*it).first << ", " << (*it).second << "]" << G4endl;
		   //if (mParticlesRSourceLayers[i].count((*it).first) != 0) {
			//   fileoutEnPart << "  [" << (*it).first << ", " << (*it).second << "] from source "<<mParticlesRSourceLayers[i].find((*it).first)->second << G4endl;
		   //} else {

		       fileoutEnPart << "  [" << (*it).first << ", " << (*it).second << "]" << G4endl;
		   //}
	   //sum += (*it).second;
    }
	fileoutEnPart<<G4endl;


	fileoutKinPart<<"Layer "<<i<<G4endl;
    for (std::multimap<std::string, G4double>::iterator it = mParticlesRKineticLayers[i].begin();
       it != mParticlesRKineticLayers[i].end();
       ++it) {
       //G4cout << "  [" << (*it).first << ", " << (*it).second << "]" << G4endl;
	   fileoutKinPart << "  [" << (*it).first << ", " << (*it).second << "]" << G4endl;
	   //sum += (*it).second;
    }
	fileoutKinPart<<G4endl;

	/*
    for (std::multimap<std::string, G4double>::iterator it = mParticlesRSourceLayers[i].begin();
       it != mParticlesRSourceLayers[i].end();
       ++it) {
       G4cout << i << "  [" << (*it).first << ", " << (*it).second << "]" << G4endl;
		//fileoutKinPart << "  [" << (*it).first << ", " << (*it).second << "]" << G4endl;
	   //sum += (*it).second;
    }*/
	//mParticlesRSourceLayers
  }
  fileoutPart.close();
  fileoutEnPart.close();
  fileoutKinPart.close();

  std::ofstream fileoutGlassData;
  G4String fnameGlassData = "glassData.out";
  fileoutGlassData.open(fnameGlassData);
  for (std::vector<std::string>::const_iterator it = glassRParticlesList.begin(); it != glassRParticlesList.end(); ++it) {
	  fileoutGlassData<<"Particle: "<< *it << " Kinetic sum " << GetValueDouble(glassRParticlesKinetic, *it) <<
		  " Dep " << GetValueDouble(glassRParticlesEnergies, *it)<<
		  " Num " << GetValueDouble(glassRParticlesNums, *it)<<G4endl;
  }

}

