# -*- coding: utf8 -*-
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
import os
import wx

def readDataFile(fName):
    x = []
    y = []
    z = []
    v = []
    for line in file(fName):
        splLine = line.split()
        x.append(int(splLine[0]))
        y.append(int(splLine[1]))
        z.append(int(splLine[2]))
        v.append(float(splLine[3]))
    print np.array(x), np.array(y), np.array(z), np.array(v)
    return len(x), np.array(x), np.array(y), np.array(z), np.array(v)

def parseX(x, y, z, v, eq):
    y = y[np.where(x==eq)]
    z = z[np.where(x==eq)]
    v = v[np.where(x==eq)]
    x = x[np.where(x==eq)]
    return x, y, z, v

def parseY(x, y, z, v, eq):
    z = z[np.where(y==eq)]
    v = v[np.where(y==eq)]
    x = x[np.where(y==eq)]
    y = y[np.where(y==eq)]
    return x, y, z, v

def parseZ(x, y, z, v, eq):
    y = y[np.where(z==eq)]
    v = v[np.where(z==eq)]
    x = x[np.where(z==eq)]
    z = z[np.where(z==eq)]
    return x, y, z, v


class a2i(object):
    def __init__(self, filename, outDir):
        self._filename = filename
        self._outDir = outDir
        self._data = readDataFile(filename)

        #self.panel = wx.Panel(self, -1, style=wx.TAB_TRAVERSAL)
        self.fig = plt.figure(dpi=300)
        #self.canvas = FigCanvas(self.panel, -1, self.fig)
        self.axes = self.fig.add_subplot(111)

        self.all2images()
        pass

    def all2images(self):
        xm = np.max(self._data[1])
        ym = np.max(self._data[2])
        zm = np.max(self._data[3])
        xmax = self._data[1][np.where(self._data[4]==np.max(self._data[4]))]
        ymax = self._data[2][np.where(self._data[4]==np.max(self._data[4]))]
        zmax = self._data[3][np.where(self._data[4]==np.max(self._data[4]))]

        xc = int(float(xm) / 2.0)
        yc = int(float(ym) / 2.0)

        print self._filename, xm, ym, zm, xc, yc, xmax, ymax, np.max(self._data[4])

        for i in range(zm):
            self.single1d(xc, -1, i)
            self.single1d(-1, yc, i)
            self.single1d(xmax, -1, i)
            self.single1d(-1, ymax, i)
        self.single1d(xc, yc, -1)
        self.single1d(xmax, ymax, -1)
        pass

    def single1d(self, xdd, ydd, zdd):
        self.fig.clf(True)
        self.axes = self.fig.add_subplot(111)
        x = self._data[1].copy()
        y = self._data[2].copy()
        z = self._data[3].copy()
        v = self._data[4].copy()
        sName = ''
        if xdd == -1:
            x, y, z, v = parseY(x, y, z, v, ydd)
            x, y, z, v = parseZ(x, y, z, v, zdd)

            self.fig.suptitle("Along x, y = %d, z = %d" %(ydd, zdd))
            self.axes.bar(x, v)
            sName = 'axisXpy%dpz%d.png' % (ydd, zdd)
        if ydd == -1:
            x, y, z, v = parseX(x, y, z, v, xdd)
            x, y, z, v = parseZ(x, y, z, v, zdd)

            self.fig.suptitle("Along y, x = %d, z = %d" %(xdd, zdd))
            self.axes.bar(y, v)
            sName = 'axisYpx%dpz%d.png' % (xdd, zdd)

        if zdd == -1:
            x, y, z, v = parseY(x, y, z, v, ydd)
            x, y, z, v = parseX(x, y, z, v, xdd)

            self.fig.suptitle("Along z, x = %d, y = %d" %(xdd, ydd))
            self.axes.bar(z, v)
            sName = 'axisZpx%dpy%d.png' % (xdd, ydd)
            pass
        self.fig.savefig(os.path.join(self._outDir, sName))

if __name__ == '__main__':
    c = a2i('D:\\dev\\data\\cubes_out\\cube1.txt', 'D:\\dev\\data\\cubes_out\\cube1img\\')
    c = a2i('D:\\dev\\data\\cubes_out\\cube2.txt', 'D:\\dev\\data\\cubes_out\\cube2img\\')
    c = a2i('D:\\dev\\data\\cubes_out\\cube3.txt', 'D:\\dev\\data\\cubes_out\\cube3img\\')
    c = a2i('D:\\dev\\data\\cubes_out\\cube4.txt', 'D:\\dev\\data\\cubes_out\\cube4img\\')
    c = a2i('D:\\dev\\data\\cubes_out\\cube5.txt', 'D:\\dev\\data\\cubes_out\\cube5img\\')

    pass
