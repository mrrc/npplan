#ifndef DetectorSD_h
#define DetectorSD_h 1

#include "G4VSensitiveDetector.hh"
//#include <map>
class G4Step;
class RunAction;
//class DetectorSDMessenger;

struct Slayer
{
	G4double depEnergy, depEnergy2, depEnergyError, dose, dose2, doseError;
	G4int nEvents;
};

class DetectorSD: public G4VSensitiveDetector 
{
  public:
    DetectorSD(G4String);
    DetectorSD(G4String, G4String, G4int);
    ~DetectorSD();
    void Initialize(G4HCofThisEvent*);
    G4bool ProcessHits(G4Step*, G4TouchableHistory*);
    void EndOfEvent(G4HCofThisEvent*);

    G4String outFileName;
    G4bool hasLayers;
    G4int numberOfLayers;

    //G4double *layeredSummator;
    Slayer *layeredSummator;
    Slayer *layeredSummatorProtons;
    //std::multimap<G4String, Slayer *> summatorMap;
    Slayer **layeredSummatorAll;
  private:
    G4double testVar;
    void setupSummator();
    std::ofstream fileoutPart;
    void addData(G4int, G4int, G4float, G4float);
};


#endif