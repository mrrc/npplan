#include "DetectorSD.hh"
//#include "RunAction.hh"
#include "DetectorConstruction.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"


DetectorSD::DetectorSD(G4String name) : G4VSensitiveDetector(name), outFileName(""), hasLayers(false), numberOfLayers(-1)
{

}
DetectorSD::DetectorSD(G4String name, G4String oF, G4int noL) : G4VSensitiveDetector(name), outFileName(oF), hasLayers(true), numberOfLayers(noL)
{
  G4cout<<"Layered summator enabled"<<G4endl;
  setupSummator();
}

DetectorSD::~DetectorSD()
{
}

void DetectorSD::setupSummator() {
  if (hasLayers) {
    //layeredSummator = new G4double[numberOfLayers];
    layeredSummator = new Slayer[numberOfLayers];
    layeredSummatorProtons = new Slayer[numberOfLayers];
    for (G4int i = 0; i < numberOfLayers; i++) {
      //layeredSummator[i] = 0;
      layeredSummator[i].depEnergy = 0;
      layeredSummator[i].depEnergy2 = 0;
      layeredSummator[i].depEnergyError = 0;
      layeredSummator[i].nEvents = 0;
      layeredSummator[i].dose = 0;
      layeredSummator[i].dose2 = 0;
      layeredSummator[i].doseError = 0;
      layeredSummatorProtons[i].depEnergy = 0;
      layeredSummatorProtons[i].depEnergy2 = 0;
      layeredSummatorProtons[i].depEnergyError = 0;
      layeredSummatorProtons[i].nEvents = 0;
      layeredSummatorProtons[i].dose = 0;
      layeredSummatorProtons[i].dose2 = 0;
      layeredSummatorProtons[i].doseError = 0;
    }

    layeredSummatorAll = new Slayer*[5];
    for (G4int i = 0; i < 5; i++) {
      layeredSummatorAll[i] = new Slayer[numberOfLayers];
      for (G4int j = 0; j < numberOfLayers; j++) {
        layeredSummatorAll[i][j].depEnergy = 0;
        layeredSummatorAll[i][j].depEnergy2 = 0;
        layeredSummatorAll[i][j].depEnergyError = 0;
        layeredSummatorAll[i][j].nEvents = 0;
        layeredSummatorAll[i][j].dose = 0;
        layeredSummatorAll[i][j].dose2 = 0;
        layeredSummatorAll[i][j].doseError = 0;
      }
    }
  }
}

void DetectorSD::Initialize(G4HCofThisEvent*) 
{

}

void DetectorSD::addData(G4int target, G4int layer, G4float energy, G4float dose) {
    layeredSummatorAll[target][layer].depEnergy += energy;
    layeredSummatorAll[target][layer].nEvents += 1;
    layeredSummatorAll[target][layer].depEnergy2 += energy * energy;
    layeredSummatorAll[target][layer].dose += dose;
    layeredSummatorAll[target][layer].dose2 += dose*dose;
}

G4bool DetectorSD::ProcessHits(G4Step* dStep, G4TouchableHistory* dHistory) 
{
  //G4cout<<"Outfile: "<<outFileName<<G4endl;
	G4StepPoint* prePoint = dStep->GetPreStepPoint();
  //G4cout<<dStep->GetTrack()->GetParentID()<<G4endl;
  /*if (dStep->GetTrack()->GetParentID() != 0) {
    G4cout<<"GOT IT"<<dStep->GetTrack()->GetParentID()<<G4endl;
  }*/
	G4TouchableHandle touchable = prePoint->GetTouchableHandle();
	G4String name = dStep->GetTrack()->GetDefinition()->GetParticleName();
  G4double density = dStep->GetPreStepPoint()->GetPhysicalVolume()->GetLogicalVolume()->GetMaterial()->GetDensity();
  G4double voxelVolume = 10 * cm * 10 * cm * 1 * mm;
  G4double voxelMass, dose;

  /*G4cout<<dStep->GetPreStepPoint()->GetKineticEnergy()<<G4endl;
	G4cout<<" "<<touchable->GetCopyNumber(0)<<
		" "<<touchable->GetCopyNumber(1)
		<<"  energy: "<<dStep->GetTotalEnergyDeposit()
		<<G4endl;*/
  G4int lId;
  if (hasLayers) {
    //layeredSummator[touchable->GetCopyNumber(0)] += dStep->GetTotalEnergyDeposit(); 
    voxelMass = density*voxelVolume;
    dose=dStep->GetTotalEnergyDeposit()/voxelMass;
    //G4cout<<dose<<G4endl;
    /*layeredSummator[touchable->GetCopyNumber(0)].depEnergy += dStep->GetTotalEnergyDeposit(); 
    layeredSummator[touchable->GetCopyNumber(0)].nEvents += 1;
    layeredSummator[touchable->GetCopyNumber(0)].depEnergy2 += dStep->GetTotalEnergyDeposit() * dStep->GetTotalEnergyDeposit();
    layeredSummator[touchable->GetCopyNumber(0)].dose += dose;
    layeredSummator[touchable->GetCopyNumber(0)].dose2 += dose*dose;
    if ((name == "proton") && (0 == dStep->GetTrack()->GetParentID())) {
      layeredSummatorProtons[touchable->GetCopyNumber(0)].depEnergy += dStep->GetTotalEnergyDeposit(); 
      layeredSummatorProtons[touchable->GetCopyNumber(0)].nEvents += 1;
      layeredSummatorProtons[touchable->GetCopyNumber(0)].depEnergy2 += dStep->GetTotalEnergyDeposit() * dStep->GetTotalEnergyDeposit();
      layeredSummatorProtons[touchable->GetCopyNumber(0)].dose += dose;
      layeredSummatorProtons[touchable->GetCopyNumber(0)].dose2 += dose*dose;
    }*/

    lId = touchable->GetCopyNumber(0);
    addData(0, lId, dStep->GetTotalEnergyDeposit(), dose);
    if ((name == "proton") && (0 == dStep->GetTrack()->GetParentID())) {
      addData(1, lId, dStep->GetTotalEnergyDeposit(), dose);
    }
    if (name == "proton") {
      addData(2, lId, dStep->GetTotalEnergyDeposit(), dose);
    }
    if (name == "gamma") {
      addData(3, lId, dStep->GetTotalEnergyDeposit(), dose);
    }
    if (name == "alpha") {
      addData(4, lId, dStep->GetTotalEnergyDeposit(), dose);
    }
  }
  return true;
}

void DetectorSD::EndOfEvent(G4HCofThisEvent*) 
{
  // @todo: to runAction instead of file saving here
  G4double v, vDo;
  G4double dd, d2, ddDo, d2Do;
  int n;
	G4String fnamePart = outFileName + ".out";
	fileoutPart.open(fnamePart);
  fileoutPart<<"Layer No. "<<" \t";
  fileoutPart<<"Summary edep "<<" \t";
  fileoutPart<<"Summary edep error "<<" \t";
  fileoutPart<<"Summary number of events "<<" \t";
  fileoutPart<<"Summary dose "<<" \t";
  fileoutPart<<"Summary dose error "<<" \t";

  fileoutPart<<"P. protons edep "<<" \t";
  fileoutPart<<"P. protons edep error "<<" \t";
  fileoutPart<<"P. protons number of events "<<" \t";
  fileoutPart<<"P. protons dose "<<" \t";
  fileoutPart<<"P. protons dose error "<<" \t";

  fileoutPart<<"All protons edep "<<" \t";
  fileoutPart<<"All protons edep error "<<" \t";
  fileoutPart<<"All protons number of events "<<" \t";
  fileoutPart<<"All protons dose "<<" \t";
  fileoutPart<<"All protons dose error "<<" \t";

  fileoutPart<<"Gamma edep "<<" \t";
  fileoutPart<<"Gamma edep error "<<" \t";
  fileoutPart<<"Gamma number of events "<<" \t";
  fileoutPart<<"Gamma dose "<<" \t";
  fileoutPart<<"Gamma dose error "<<" \t";

  fileoutPart<<"Alpha edep "<<" \t";
  fileoutPart<<"Alpha edep error "<<" \t";
  fileoutPart<<"Alpha number of events "<<" \t";
  fileoutPart<<"Alpha dose "<<" \t";
  fileoutPart<<"Alpha dose error "<<" \t";

  fileoutPart<<G4endl;
  if (hasLayers) {
    for (G4int j = 0; j < numberOfLayers; j++) {
      //G4cout<<"Layer "<<i<<" got energy: "<<layeredSummator[i]<<G4endl;
      /*n=layeredSummator[i].nEvents;
      dd=layeredSummator[i].depEnergy*layeredSummator[i].depEnergy;
			d2=layeredSummator[i].depEnergy2;

      ddDo=layeredSummator[i].dose*layeredSummator[i].dose;
			d2Do=layeredSummator[i].dose2;

      v=n*d2-dd;
      vDo = n*d2Do-ddDo;

      if (n>1) {
        layeredSummator[i].depEnergyError=1.0*std::sqrt(v/(n-1));
        layeredSummator[i].doseError = 1.0*std::sqrt(vDo/(n-1));
      }
      //fileoutPart<<fileoutPart.precision(8)<<"Layer "<<i<<" edep: "<<layeredSummator[i].depEnergy/(MeV/g)<<" "<<layeredSummator[i].depEnergy/(MeV)<<" dError: "<<layeredSummator[i].depEnergyError<<G4endl;
      fileoutPart<<i<<" \t";
      //fileoutPart<<fileoutPart.precision(8)<<layeredSummator[i].depEnergy/(MeV/g)<<" \t";
      fileoutPart<<std::fixed << std::setprecision(10)<<layeredSummator[i].depEnergy/(MeV)<<" \t";
      // fileoutPart<<std::fixed << std::setprecision(10)<<G4BestUnit(layeredSummator[i].depEnergy, "Dose")<<" \t"; // Wrong (!!!)
      fileoutPart<<std::fixed << std::setprecision(10)<<layeredSummator[i].depEnergy/layeredSummator[i].nEvents<<" \t";
      fileoutPart<<std::fixed << std::setprecision(10)<<layeredSummator[i].depEnergyError<<" \t";
      fileoutPart<<std::fixed << std::setprecision(10)<<layeredSummator[i].depEnergyError/(MeV)<<" \t";
      fileoutPart<<std::fixed << std::setprecision(10)<<layeredSummator[i].nEvents<<" \t";
      fileoutPart<<std::defaultfloat<<layeredSummator[i].dose/(MeV/g)<<" \t";
      fileoutPart<<std::defaultfloat<<G4BestUnit(layeredSummator[i].dose, "Dose")<<" \t";  // Seems correct (?)
      fileoutPart<<std::defaultfloat<<layeredSummator[i].doseError/(MeV/g)<<" \t";
      fileoutPart<<std::defaultfloat<<G4BestUnit(layeredSummator[i].doseError, "Dose")<<" \t";*/
      for (G4int i = 0; i < 5; i++) {
        n=layeredSummatorAll[i][j].nEvents;
        dd=layeredSummatorAll[i][j].depEnergy*layeredSummatorAll[i][j].depEnergy;
			  d2=layeredSummatorAll[i][j].depEnergy2;
        ddDo=layeredSummatorAll[i][j].dose*layeredSummatorAll[i][j].dose;
			  d2Do=layeredSummatorAll[i][j].dose2;

        v=n*d2-dd;
        vDo = n*d2Do-ddDo;

        if (n>1) {
          layeredSummatorAll[i][j].depEnergyError=1.0*std::sqrt(v/(n-1));
          layeredSummatorAll[i][j].doseError = 1.0*std::sqrt(vDo/(n-1));
        }

        fileoutPart<<j<<" \t";
        //fileoutPart<<std::fixed << std::setprecision(48);
        fileoutPart<<std::defaultfloat;
        fileoutPart<<G4BestUnit(layeredSummatorAll[i][j].depEnergy, "Energy")<<" \t";
        fileoutPart<<G4BestUnit(layeredSummatorAll[i][j].depEnergyError, "Energy")<<" \t";
        fileoutPart<<layeredSummatorAll[i][j].nEvents<<" \t";
        fileoutPart<<G4BestUnit(layeredSummatorAll[i][j].dose, "Dose")<<" \t";
        fileoutPart<<G4BestUnit(layeredSummatorAll[i][j].doseError, "Dose")<<" \t";
      }
      fileoutPart<<G4endl;
    }  
  }
  fileoutPart.close();
}