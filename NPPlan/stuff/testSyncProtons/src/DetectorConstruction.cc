/*
 * Base file for geant projects
 * Detector construction implementation
 * @author: mrxak
 * (c) MRRC, 2013
*/

#include "globals.hh"
#include "DetectorConstruction.hh"

#include "DetectorSD.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4PVParameterised.hh"
#include "G4PVDivision.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSEnergyDeposit.hh"
#include "G4PSCellFlux.hh"
#include "G4PSTrackCounter.hh"
#include "G4PSTrackLength.hh"
#include "G4PSDoseDeposit.hh"
#include "G4SDManager.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"

class DetectorMessenger;

//#define hasBone 1

DetectorConstruction::DetectorConstruction() : 
  worldSizeXYZ(0)
{
  //fMessenger = new DetectorMessenger(this);
}

DetectorConstruction::~DetectorConstruction() {

}


G4VPhysicalVolume* DetectorConstruction::Construct() {
  G4NistManager* nistManager = G4NistManager::Instance();
  defineMaterials();
  nistManager->FindOrBuildMaterial("G4_AIR");
  nistManager->FindOrBuildMaterial("G4_WATER");

  G4Material* air  = G4Material::GetMaterial("G4_AIR");
  G4Material* water  = G4Material::GetMaterial("G4_WATER");

  G4double worldXDimension = 0.5*m;
  G4double worldYDimension = 0.5*m;
  G4double worldZDimension = 0.5*m;

  G4Box *worldBox = new G4Box( "WorldSolid",
                             worldXDimension,
                             worldYDimension,
                             worldZDimension );

  G4LogicalVolume *worldLogic = new G4LogicalVolume( worldBox,
                                       air,
                                       "WorldLogical",
                                       0, 0, 0 );

  physWorld = new G4PVPlacement( 0,
                                    G4ThreeVector(0,0,0),
                                    "World",
                                    worldLogic,
                                    0,
                                    false,
                                    0 );

  G4double tissue1X = 10*cm;
  G4double tissue1Y = 10*cm;
#ifdef hasBone
  G4double tissue1Z = 10*cm;
#else
  G4double tissue1Z = 20*cm;
#endif

  G4Box *tissue1Box = new G4Box( "WorldSolid",
                             tissue1X / 2,
                             tissue1Y / 2,
                             tissue1Z / 2 );

  G4LogicalVolume *tissue1Logic = new G4LogicalVolume( tissue1Box,
                                       fTissue,
                                       "Tissue1Logical",
                                       0, 0, 0 );

  G4double fLayerThickness = 1 * mm;

  G4Box *fTissue1LayerBox = new G4Box("Layer1Box",tissue1X / 2,tissue1Y / 2,fLayerThickness/2.);
  G4LogicalVolume *fTissue1LayerBoxLogic
      = new G4LogicalVolume(fTissue1LayerBox,fTissue,"Tissue1Layer");
  new G4PVReplica("Tissue1LayersReplica", fTissue1LayerBoxLogic, tissue1Logic,
    kZAxis, tissue1Z / fLayerThickness, fLayerThickness);

#ifdef hasBone
  G4double bone1X = 10*cm;
  G4double bone1Y = 10*cm;
  G4double bone1Z = 1*cm;

  G4Box *bone1Box = new G4Box( "WorldSolid",
                             bone1X / 2,
                             bone1Y / 2,
                             bone1Z / 2 );

  G4LogicalVolume *bone1Logic = new G4LogicalVolume( bone1Box,
                                       fBone,
                                       "Bone1Logical",
                                       0, 0, 0 );
  G4double tissue2X = 10*cm;
  G4double tissue2Y = 10*cm;
  G4double tissue2Z = 10*cm;

  G4Box *tissue2Box = new G4Box( "WorldSolid",
                             tissue2X / 2,
                             tissue2Y / 2,
                             tissue2Z / 2 );

  G4LogicalVolume *tissue2Logic = new G4LogicalVolume( tissue2Box,
                                       fTissue,
                                       "Tissue2Logical",
                                       0, 0, 0 );

  G4Box *fTissue2LayerBox = new G4Box("Layer2Box",tissue1X / 2,tissue1Y / 2,fLayerThickness/2.);
  G4LogicalVolume *fTissue2LayerBoxLogic
      = new G4LogicalVolume(fTissue2LayerBox,fTissue,"Tissue2Layer");
  new G4PVReplica("Tissue2LayersReplica", fTissue2LayerBoxLogic, tissue2Logic,
    kZAxis, tissue2Z / fLayerThickness, fLayerThickness);
#endif

#ifdef hasBone
  new G4PVPlacement(0,                          //no rotation
                            G4ThreeVector(0, 0, (-tissue1Z-bone1Z) / 2),            //at (0,0,0)
                            tissue1Logic,                      //its logical volume
                            "Tissue1Place",       //its name
                            worldLogic,                          //its mother  volume
                            false,                      //no boolean operation
                            0,                    //copy number
                            true);                         
    new G4PVPlacement(0,                          //no rotation
                              G4ThreeVector(0, 0, 0),            //at (0,0,0)
                              bone1Logic,                      //its logical volume
                              "Bone1Place",       //its name
                              worldLogic,                          //its mother  volume
                              false,                      //no boolean operation
                              0,                    //copy number
                              true);                         
    new G4PVPlacement(0,                          //no rotation
                              G4ThreeVector(0, 0, (tissue2Z+bone1Z) / 2),            //at (0,0,0)
                              tissue2Logic,                      //its logical volume
                              "Tissue2Place",       //its name
                              worldLogic,                          //its mother  volume
                              false,                      //no boolean operation
                              0,                    //copy number
                              true);      
#else
  new G4PVPlacement(0,                          //no rotation
                            G4ThreeVector(0, 0, 0),            //at (0,0,0)
                            tissue1Logic,                      //its logical volume
                            "Tissue1Place",       //its name
                            worldLogic,                          //its mother  volume
                            false,                      //no boolean operation
                            0,                    //copy number
                            true);                         
#endif

#ifdef hasBone
  //setupDetectors(fTissue1LayerBoxLogic, tissue1Z / fLayerThickness, "tissuewb1det");
  setupDetectors(fTissue2LayerBoxLogic, tissue1Z / fLayerThickness, "tissuewb2det");
#else
  setupDetectors(fTissue1LayerBoxLogic, tissue1Z / fLayerThickness, "tissue1det_testSoU");
#endif

  return physWorld;
}

void DetectorConstruction::defineMaterials(void) {
  G4double z, a, density;
  G4String name, symbol;

    G4Element* elC = new G4Element( name = "Carbon",
                                   symbol = "C",
                                   z = 6.0, a = 12.011 * g/mole );
    G4Element* elH = new G4Element( name = "Hydrogen",
                                   symbol = "H",
                                   z = 1.0, a = 1.008  * g/mole );
    G4Element* elN = new G4Element( name = "Nitrogen",
                                   symbol = "N",
                                   z = 7.0, a = 14.007 * g/mole );
    G4Element* elO = new G4Element( name = "Oxygen",
                                   symbol = "O",
                                   z = 8.0, a = 16.00  * g/mole );
    G4Element* elNa = new G4Element( name = "Sodium",
                                    symbol = "Na",
                                    z= 11.0, a = 22.98977* g/mole );
    G4Element* elS = new G4Element( name = "Sulfur",
                                   symbol = "S",
                                   z = 16.0,a = 32.065* g/mole );
    G4Element* elCl = new G4Element( name = "Chlorine",
                                    symbol = "P",
                                    z = 17.0, a = 35.453* g/mole );
    G4Element* elK = new G4Element( name = "Potassium",
                                   symbol = "P",
                                   z = 19.0, a = 30.0983* g/mole );
    G4Element* elP = new G4Element( name = "Phosphorus",
                                   symbol = "P",
                                   z = 30.0, a = 30.973976* g/mole );
    G4Element* elFe = new G4Element( name = "Iron",
                                    symbol = "Fe",
                                    z = 26, a = 56.845* g/mole );
    G4Element* elMg = new G4Element( name = "Magnesium",
                                    symbol = "Mg",
                                    z = 12.0, a = 24.3050* g/mole );
    G4Element* elCa = new G4Element( name="Calcium",
                                    symbol = "Ca",
                                    z = 20.0, a = 40.078* g/mole );

    // Creating Materials :
    G4int numberofElements;

    // Air
    fBone = new G4Material( "Bone",
                          1.920*g/cm3,
                          numberofElements = 9 );
        /*cmap.append((H, 0.034000))
        cmap.append((C, 0.155000))
        cmap.append((N, 0.042000))
        cmap.append((O, 0.435000))
        cmap.append((Na, 0.001000))
        cmap.append((Mg, 0.002000))
        cmap.append((P, 0.103000))
        cmap.append((S, 0.003000))
        cmap.append((Ca, 0.225000))*/
    fBone->AddElement(elH, 0.034);
    fBone->AddElement(elC, 0.155);
    fBone->AddElement(elN, 0.042);
    fBone->AddElement(elO, 0.435);
    fBone->AddElement(elNa, 0.001);
    fBone->AddElement(elMg, 0.002);
    fBone->AddElement(elP, 0.103);
    fBone->AddElement(elS, 0.003);
    fBone->AddElement(elCa, 0.225);

    fTissue = new G4Material( "Tissue",
                          1.06*g/cm3,
                          numberofElements = 9 );
            /*cmap.append((H, 0.102))
        cmap.append((C, 0.143000))
        cmap.append((N, 0.034000))
        cmap.append((O, 0.708000))
        cmap.append((Na, 0.002000))
        cmap.append((P, 0.003000))
        cmap.append((S, 0.003000))
        cmap.append((Cl, 0.002000))
        cmap.append((K, 0.003000))*/
    fTissue->AddElement(elH, 0.102);
    fTissue->AddElement(elC, 0.143);
    fTissue->AddElement(elN, 0.034);
    fTissue->AddElement(elO, 0.708);
    fTissue->AddElement(elNa, 0.002);
    fTissue->AddElement(elP, 0.003);
    fTissue->AddElement(elS, 0.003);
    fTissue->AddElement(elCl, 0.002);
    fTissue->AddElement(elK, 0.003);
}

void DetectorConstruction::setupDetectors(G4LogicalVolume *pV, G4double noL, G4String tName) {
  DetectorSD* det = new DetectorSD(tName, tName, noL);
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  pV->SetSensitiveDetector(det);
  sdMan->AddNewDetector(det);
}