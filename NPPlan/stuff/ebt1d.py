# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 07.03.14
@summary: 
'''

import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import os

def readDataFile(fName):
    x = []
    y = []
    z = []
    v = []
    for line in file(fName):
        splLine = line.split()
        x.append(int(splLine[0]))
        y.append(int(splLine[1]))
        z.append(int(splLine[2]))
        v.append(float(splLine[3]))
    print np.array(x), np.array(y), np.array(z), np.array(v)
    return len(x), np.array(x), np.array(y), np.array(z), np.array(v)


#_data = readDataFile(r'C:\Temp\ebt\cubes_out\cube1z.txt')

def parseX(x, y, z, v, eq):
    y = y[np.where(x==eq)]
    z = z[np.where(x==eq)]
    v = v[np.where(x==eq)]
    x = x[np.where(x==eq)]
    return x, y, z, v

def parseY(x, y, z, v, eq):
    z = z[np.where(y==eq)]
    v = v[np.where(y==eq)]
    x = x[np.where(y==eq)]
    y = y[np.where(y==eq)]
    return x, y, z, v

def parseZ(x, y, z, v, eq):
    y = y[np.where(z==eq)]
    v = v[np.where(z==eq)]
    x = x[np.where(z==eq)]
    z = z[np.where(z==eq)]
    return x, y, z, v

'''
x = _data[1].copy()
y = _data[2].copy()
z = _data[3].copy()
v = _data[4].copy()


x, y, z, v = parseX(x, y, z, v, 200)
x, y, z, v = parseZ(x, y, z, v, 5)

fig = plt.figure()
ax = fig.gca()
plt.title('x=200, z=5')
plt.bar(y, v)
plt.show()

x = _data[1].copy()
y = _data[2].copy()
z = _data[3].copy()
v = _data[4].copy()


x, y, z, v = parseY(x, y, z, v, 200)
x, y, z, v = parseX(x, y, z, v, 150)

fig.clf(True)
ax = fig.gca()
plt.title('y=200, z=5')
plt.bar(z, v)
plt.show()'''


fig = plt.figure()
for fileI in range(1, 6, 1):
    fName = 'C:\\Temp\\ebt\\cubes_out\\cube%dz.txt' % fileI
    print fName
    dirOut = 'C:\\Temp\\ebt\\cubes_out\\cube%d_1d\\' % fileI
    _data = readDataFile(r'C:\Temp\ebt\cubes_out\cube1z.txt')
    os.chdir(dirOut)
    x = _data[1].copy()
    y = _data[2].copy()
    z = _data[3].copy()
    v = _data[4].copy()

    # slice x (each 10)
    #for xi in range(0, np.max(x)+1, 10):
    if 1:
        xi = -1
        os.mkdir(dirOut+'zy')
        for zi in range(0, np.max(z)+1, 1):
            for yi in range(0, np.max(y)+1, 10):
                x = _data[1].copy()
                y = _data[2].copy()
                z = _data[3].copy()
                v = _data[4].copy()
                outFile = dirOut + 'zy\\z%dy%d_x%d.png' % (zi, yi, xi)
                fig.clf(True)
                ax = fig.gca()
                x, y, z, v = parseZ(x, y, z, v, zi)
                x, y, z, v = parseY(x, y, z, v, yi)
                plt.title('x=%d, y=%d, z=%d' % (xi, yi, zi))
                plt.bar(x, v)
                plt.savefig(outFile, dpi=300)

    # slice y (each 1)
    #for yi in range(0, np.max(y)+1, 10):
    if 1:
        yi = -1
        os.mkdir(dirOut+'zx')
        for zi in range(0, np.max(z)+1, 1):
            for xi in range(0, np.max(x)+1, 10):
                x = _data[1].copy()
                y = _data[2].copy()
                z = _data[3].copy()
                v = _data[4].copy()
                outFile = dirOut + 'zx\\z%dx%d_y%d.png' % (zi, xi, yi)
                fig.clf(True)
                ax = fig.gca()
                x, y, z, v = parseZ(x, y, z, v, zi)
                x, y, z, v = parseX(x, y, z, v, yi)
                plt.title('x=%d, y=%d, z=%d' % (xi, yi, zi))
                plt.bar(y, v)
                plt.savefig(outFile, dpi=300)

    # slice z (each 1)
    #for zi in range(0, np.max(z)+1, 1):
    if 1:
        zi = -1
        os.mkdir(dirOut+'xy')
        for xi in range(0, np.max(x)+1, 10):
            for yi in range(0, np.max(y)+1, 10):
                x = _data[1].copy()
                y = _data[2].copy()
                z = _data[3].copy()
                v = _data[4].copy()
                outFile = dirOut + 'xy\\x%dy%d_z%d.png' % (xi, yi, zi)
                fig.clf(True)
                ax = fig.gca()
                x, y, z, v = parseX(x, y, z, v, xi)
                x, y, z, v = parseY(x, y, z, v, yi)
                plt.title('x=%d, y=%d, z=%d' % (xi, yi, zi))
                plt.bar(z, v)
                plt.savefig(outFile, dpi=300)