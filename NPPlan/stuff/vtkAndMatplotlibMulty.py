# -*- coding: utf8 -*-
'''
VTK and matplotlib integration

base: matplotlib
renderwindow: vtk


@author: mrxak
@copyright: A. Tsyb MRRC Obninsk 2021
@version: 1
@date: 30.04.2021
@summary: Works as intended for this example. Puts multiple matplotlib canvases on the textured boxes, boxes shifted with some random coefficient in Z direction (cf < 1)
'''
# 

import numpy as np

from vtk import *

import matplotlib
matplotlib.use('Agg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg
import pylab as p

#tmp0 = np.zeros((1024, 1024, 4, 3), dtype=np.byte)
tmpArr = []
cubeActorArr = []
for n in range(3):
#if True:
  importer = vtkImageImport()
  importer.SetDataScalarTypeToUnsignedChar()
  importer.SetNumberOfScalarComponents(4)
  fig = Figure()
  canvas = FigureCanvasAgg(fig)
  ax = fig.add_subplot(111)
  ax.grid(True)
  ax.set_xlabel('Hello from VTK!', size=16)
  ax.bar(np.arange(10), p.rand(10))
#ax.scatter(p.rand(10))



# Powers of 2 image to be clean
  w,h = 1024, 1024
  dpi = canvas.figure.get_dpi()
  print(dpi)
  fig.set_size_inches(w / dpi, h / dpi)
  canvas.draw() # force a draw
  print(np.where(canvas.buffer_rgba()!=255))
  tmp = canvas.buffer_rgba()
#  tmp[:, :, :, i] = canvas.buffer_rgba()

# The vtkImageImporter will treat a python string as a void pointer

  tmp = np.array(tmp)
  print(tmp.shape)
  tmpArr.append(tmp)
  # This is where we tell the image importer about the mpl image
  extent = (0, 1023, 0, 1023, 0, 0)
  #print canvas.tostring_rgb()
  importer.SetWholeExtent(extent)
  importer.SetDataExtent(extent)
  importer.SetImportVoidPointer(tmpArr[0], 1)
  importer.Update()
  # It's upside-down when loaded, so add a flip filter
  imflip = vtkImageFlip()
  imflip.SetInputConnection(importer.GetOutputPort())
  imflip.SetFilteredAxis(1)

  bbmap = vtkImageMapper()
  bbmap.SetColorWindow(255.5)
  bbmap.SetColorLevel(127.5)
  #bbmap.SetInput(importer.GetOutput())
  #bbmap.SetInput(imflip.GetOutput())

  #bbact = vtkActor2D()
  #bbact.SetMapper(bbmap)

  # Map the plot as a texture on a cube
  cube = vtkCubeSource()
 
  cubeMapper = vtkPolyDataMapper()
  cubeMapper.SetInputConnection(cube.GetOutputPort())
#cubeMapper.SetInput(bbmap.GetOutput())


  cubeActor = vtkActor()
  cubeActor.SetMapper(cubeMapper)

# Create a texture based off of the image
  cubeTexture = vtkTexture()
  cubeTexture.InterpolateOn()
  cubeTexture.SetInputConnection(imflip.GetOutputPort())
  cubeActor.SetTexture(cubeTexture)

  cubeActor.GetProperty().SetOpacity(0.5)
  cubeActor.SetPosition(0, 0, n*0.4);
  cubeActorArr.append(cubeActor)

ren = vtkRenderer()
for i in (cubeActorArr):
  ren.AddActor(i)
#ren.AddActor(cubeActor)
#ren.SetBackground(1, 0, 1)

renWin = vtkRenderWindow()
renWin.AddRenderer(ren)

iren = vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

# First create the data
# x y alpha NUM


#  print(canvas.buffer_rgba())



iren.Initialize()
iren.Start()