#include "RunAction.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
//#include "G4VUserDetectorConstruction.hh"
#include "DetectorSD.hh"
#include "DetectorConstruction.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4THitsMap.hh"
#include "G4SDManager.hh"
#include <ctime>

#define NO_RA 1

RunAction::RunAction() : G4UserRunAction()
{}

RunAction::~RunAction() {
}

void RunAction::addDetData(G4String dName, G4int ix, G4int iy, G4int iz, detLayer dd) {
  //G4cout<<"Got: "<<ix<<" "<<iy<<" "<<iz<<G4endl;
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  ax = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(dName))->getMX();
  ay = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(dName))->getMY();
  az = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(dName))->getMZ();
  if (0 == results.count(dName)) {
    resLayer ***rs = new resLayer**[ax];
    for (int i = 0; i < ax; i++) {
      rs[i] = new resLayer*[ay];
      for (int j = 0; j < ay; j++) {
        rs[i][j] = new resLayer[az];
        //for (int z = 0; z < aZ; z++) {
        rs[ix][iy][iz] += dd;
        //}
      }
    }
    results.insert(std::pair<G4String, resLayer***>(dName, rs));
  } else {
    results.find(dName)->second[ix][iy][iz] += dd;
  }
}

void RunAction::BeginOfRunAction(const G4Run* cRun)
{ 
  beginAt = clock();
#ifndef NO_RA
  layerDepToDets.clear();
  beginAt = clock();

  //((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->getMainDetector()->addIon(6, 12);
  DetectorSD1Layer *md = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->getMainDetector();
  md->addParticle("neutron");
  md->addParticle("proton");
  md->addParticle("alpha");
  md->addParticle("gamma");
  md->addParticle("e-");
  md->addIon(6, 12);
  md->addIon(8, 16);
#endif
}

void RunAction::EndOfRunAction(const G4Run* cRun)
{

  finishedAt = clock();
  double elapsedSecs = double(finishedAt - beginAt) / CLOCKS_PER_SEC;
  G4cout<<"Completed at: "<<elapsedSecs<<" sec."<<G4endl;

  for (std::map<G4String, resLayer***>::iterator it = results.begin();
    it != results.end();
    ++it) {
      G4cout<<it->first;
      for (int ix = 0; ix < ax; ix++) {
        for (int iy = 0; iy < ay; iy++) {
          for (int iz = 0; iz < az; iz++) {
            it->second[ix][iy][iz].calculateError();
            G4cout<<ix<<" "<<iy<<" "<<iz<<" ";
            G4cout<<it->second[ix][iy][iz].depEnergy / (MeV)<<" ";
            G4cout<<it->second[ix][iy][iz].depEnergyError / (MeV)<<" ";
            G4cout<<it->second[ix][iy][iz].dose / (MeV / g)<<" ";
            G4cout<<it->second[ix][iy][iz].doseError / (MeV / g)<<" ";
            G4cout<<it->second[ix][iy][iz].dose / (CLHEP::gray)<<" ";
            G4cout<<it->second[ix][iy][iz].doseError / (CLHEP::gray)<<" ";
            G4cout<<it->second[ix][iy][iz].cellFlux / (1/cm2)<<" ";
            G4cout<<it->second[ix][iy][iz].cellFluxError / (1/cm2)<<" ";
            G4cout<<G4endl;
          }
        }
      }
  }
  //G4SDManager::GetSDMpointer()->GetCollectionID("testDet/DoseDeposit");
  //G4SDManager::GetSDMpointer()->GetCollectionID("testDet/FluxMap");
#ifndef NO_RA
  std::ofstream fileoutPart;
  G4int nov = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->nov;

  for (std::map<G4String, resLayer*>::iterator it = detLayers.begin();
				it != detLayers.end();
				++it) {
    fileoutPart.open(it->first+"_sum.out");
    fileoutPart<<"Summary data\n";
    fileoutPart<<"Layer\tEnergy, MeV\tEnergy error, MeV\tDose, Mev/g\tDose error, MeV/g\tDose, Gy\tDose error, Gy\tNumber of events\tCell flux\t"<<G4endl;;
    for (int i = 0; i < nov; i++) {

      it->second[i].calculateError();
      fileoutPart<<std::setfill('0') << std::setw(5)<<i<<"\t";
      fileoutPart<<std::defaultfloat;
      fileoutPart<<it->second[i].depEnergy / MeV<<"\t";
      fileoutPart<<it->second[i].depEnergyError / MeV<<"\t";
      fileoutPart<<it->second[i].dose / (MeV / g)<<"\t";
      fileoutPart<<it->second[i].doseError / (MeV / g)<<"\t";
      fileoutPart<<G4BestUnit(it->second[i].dose, "Dose")<<"\t";
      fileoutPart<<G4BestUnit(it->second[i].doseError, "Dose")<<"\t";
      fileoutPart<<it->second[i].nEvents<<"\t";
      fileoutPart<<it->second[i].cellFlux<<"\t";
      fileoutPart<<G4endl;
    }
    fileoutPart.close();
  }

    for (std::map<detPartType, resLayer*>::iterator it = detPartLayers.begin();
				it != detPartLayers.end();
				++it) {
      fileoutPart.open(it->first.first+"_"+it->first.second->GetParticleName()+".out");
      fileoutPart<<"Particle "<<it->first.second->GetParticleName()<<"\n";
      fileoutPart<<"Layer\tEnergy, MeV\tEnergy error, MeV\tDose, Mev/g\tDose error, MeV/g\tDose, Gy\tDose error, Gy\tNumber of events\tCell flux\t"<<G4endl;
      for (int i = 0; i < nov; i++) {
        it->second[i].calculateError();
        fileoutPart<<i<<"\t";
        fileoutPart<<it->second[i].depEnergy / MeV<<"\t";
        fileoutPart<<it->second[i].depEnergyError / MeV<<"\t";
        fileoutPart<<it->second[i].dose / (MeV / g)<<"\t";
        fileoutPart<<it->second[i].doseError / (MeV / g)<<"\t";
        fileoutPart<<G4BestUnit(it->second[i].dose, "Dose")<<"\t";
        fileoutPart<<G4BestUnit(it->second[i].doseError, "Dose")<<"\t";
        fileoutPart<<it->second[i].nEvents<<"\t";
        fileoutPart<<it->second[i].cellFlux<<"\t";
        fileoutPart<<G4endl;
      }
      fileoutPart.close();
    }

  fileoutPart.open("test2.dump", std::ofstream::out);
  for (std::map<G4String, pdvec>::iterator it = allParticlesList.begin();
    it != allParticlesList.end();
    ++it) {
    fileoutPart<<"Detector: "<<it->first<<G4endl;
    for(int i = 0; i < it->second.size(); i++) {
      if (0 != detPartLayers.count(detPartType(it->first, it->second[i]))) {
        fileoutPart<<" * ";
      } else {
        fileoutPart<<"   ";
      }
      fileoutPart<<it->second[i]->GetParticleName()<<G4endl;
    }
  }
  fileoutPart.close();

  
  //std::ofstream fileoutPart;
  finishedAt = clock();
  double elapsedSecs = double(finishedAt - beginAt) / CLOCKS_PER_SEC;
  fileoutPart.open("runTimes.log", std::ofstream::out | std::ofstream::app);
  fileoutPart<<"Run "<<(G4int)G4RunManager::GetRunManager()->GetCurrentRun()->GetRunID()<<" time elapsed: "<<elapsedSecs<<G4endl;
  fileoutPart.close();
#endif
}

