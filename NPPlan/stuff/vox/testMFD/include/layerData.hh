#ifndef layerData_h
#define layerData_h 1

#include "globals.hh"

struct detLayer
{
	G4double depEnergy, depEnergy2, dose, dose2;
	G4int nEvents;
  G4double cellFlux, cellFlux2;
  detLayer();
  detLayer(G4double, G4double, G4double, G4double, G4int, G4double, G4double);
  void Nullify();
  bool Update(G4double, G4double, G4int, G4double);
  bool Update(G4double, G4double, G4double);
};

struct resLayer
{
  G4double depEnergy, depEnergy2, dose, dose2;
  G4double depEnergyError, doseError;
	G4int nEvents;
  G4double cellFlux, cellFlux2;
  G4double cellFluxError;
  G4bool _errorState;
  resLayer();
  resLayer(detLayer);
  resLayer(G4double, G4double, G4double, G4double, G4int, G4double, G4double);
  resLayer(G4double, G4double, G4double, G4double, G4double, G4double, G4int, G4double, G4double, G4double);
  void Nullify();
  void calculateError();
  resLayer operator+ (const resLayer &other);
  resLayer operator+ (const detLayer &other);
  resLayer& operator+=(const resLayer &other);
  resLayer& operator+=(const detLayer &other);
};

#endif