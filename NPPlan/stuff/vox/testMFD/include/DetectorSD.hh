#ifndef DetectorSD_3DLayer_h
#define DetectorSD_3DLayer_h 1
#include "G4VSensitiveDetector.hh"
#include <map>
#include <vector>
#include "G4ThreeVector.hh"
#include "layerData.hh"
class G4Step;
class RunAction;

class DetectorSD3DLayer: public G4VSensitiveDetector 
{
  public:
    DetectorSD3DLayer(G4String, G4int, G4int, G4int);
    DetectorSD3DLayer(G4String, G4ThreeVector);
    DetectorSD3DLayer(G4String, G4int, G4int, G4int, G4int);
    ~DetectorSD3DLayer();
    void createData();
    void nullify();
    void Initialize(G4HCofThisEvent*);
    G4bool ProcessHits(G4Step*, G4TouchableHistory*);
    void EndOfEvent(G4HCofThisEvent*);

    inline G4int getMX() { return noX; };
    inline G4int getMY() { return noY; };
    inline G4int getMZ() { return noZ; };
    inline G4String getOutName() { return outName; };
    inline void setPreVol(G4double _vol) { preVol = _vol; };
    inline void setWeighted(G4bool _w) { weighted = _w; };
    //void addIon(G4int, G4int);
  private:
    G4bool weighted;
    RunAction* runAction;
    G4String outName;
    G4int noX, noY, noZ;
    detLayer ***data;
    G4int targetLayer;
    G4double preVol;

    G4double ComputeVolume(G4Step* aStep, G4int idx);
};
#endif