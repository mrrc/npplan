#include "PhantomParameterisation.hh"

#include "G4VPhysicalVolume.hh"
#include "G4VTouchable.hh"
#include "G4ThreeVector.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4Element.hh"
#include "G4UnitsTable.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include "G4VisAttributes.hh"
#include "G4VVisManager.hh"

//#define NO_MATS 1
//#define NO_COLORS 1

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
PhantomParameterisation::
PhantomParameterisation(const G4ThreeVector& voxelSize, const G4ThreeVector& phantomBounds,
                                   G4int fnX_, G4int fnY_, G4int fnZ_)
:
  //G4VNestedParameterisation(),
  fdX(voxelSize.x()), fdY(voxelSize.y()), fdZ(voxelSize.z()),
  fnX(fnX_), fnY(fnY_), fnZ(fnZ_),
  phX(phantomBounds.x()), phY(phantomBounds.y()), phZ(phantomBounds.z()),
  matListCount(-1)
{
#ifndef NO_MATS
  readVoxelData();
  createMaterials();
#endif
  G4cout<<"fnXYZ at constructor"<<fnX<<" "<<fnY<<" "<<fnZ<<G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
PhantomParameterisation::~PhantomParameterisation()
{
}

void PhantomParameterisation::readVoxelData() {
  G4int _lim = fnX*fnY*fnZ;
  G4int *listOfMats = new G4int[_lim];
  std::ifstream ifData;
  ifData.open("voxels.in", std::ios::in);
  //G4int noM;
  //ifData>>noM;
  for(int i = 0; i < _lim; i++) {
    ifData>>listOfMats[i];
  }
  ifData.close();

  G4cout<<"------- Reading mats -------"<<G4endl;
  for(int i = 0; i < _lim; i++) {
    G4cout<<listOfMats[i]<<" ";
  }
  G4cout<<"------- EOF Reading mats -------"<<G4endl;
  matListCount = _lim;
  matListInts = listOfMats;
}

void PhantomParameterisation::createMaterials() {
  G4cout<<"Create materials called"<<G4endl;
  G4NistManager* nistMan = G4NistManager::Instance();
    G4double z, a, density;
    G4String name, symbol;
    G4double numberofElements;

    G4Element* elC = new G4Element( name = "Carbon",
                                   symbol = "C",
                                   z = 6.0, a = 12.011 * g/mole );
    G4Element* elB = new G4Element( name = "Borum",
                                   symbol = "B",
                                   z = 5.0, a = 10.812 * g/mole );
    G4Element* elH = new G4Element( name = "Hydrogen",
                                   symbol = "H",
                                   z = 1.0, a = 1.008  * g/mole );
    G4Element* elN = new G4Element( name = "Nitrogen",
                                   symbol = "N",
                                   z = 7.0, a = 14.007 * g/mole );
    G4Element* elO = new G4Element( name = "Oxygen",
                                   symbol = "O",
                                   z = 8.0, a = 16.00  * g/mole );
    G4Element* elNa = new G4Element( name = "Sodium",
                                    symbol = "Na",
                                    z= 11.0, a = 22.98977* g/mole );
    G4Element* elS = new G4Element( name = "Sulfur",
                                   symbol = "S",
                                   z = 16.0,a = 32.065* g/mole );
    G4Element* elCl = new G4Element( name = "Chlorine",
                                    symbol = "Cl",
                                    z = 17.0, a = 35.453* g/mole );
    G4Element* elK = new G4Element( name = "Potassium",
                                   symbol = "K",
                                   z = 19.0, a = 39.0983* g/mole );
    G4Element* elP = new G4Element( name = "Phosphorus",
                                   symbol = "P",
                                   z = 15.0, a = 30.973976* g/mole );
    G4Element* elFe = new G4Element( name = "Iron",
                                    symbol = "Fe",
                                    z = 26, a = 56.845* g/mole );
    G4Element* elMg = new G4Element( name = "Magnesium",
                                    symbol = "Mg",
                                    z = 12.0, a = 24.3050* g/mole );

    G4Element* elAr = new G4Element( name="Argon",
                                    symbol = "Ar",
                                    z = 18.0, a = 39.948* g/mole );
    G4Element* elCa = new G4Element( name="Calcium",
                                    symbol = "Ca",
                                    z = 20.0, a = 40.078* g/mole );

    elementsData.insert(std::pair<G4int, G4Element*>(1, elH));
    elementsData.insert(std::pair<G4int, G4Element*>(5, elB));
    elementsData.insert(std::pair<G4int, G4Element*>(6, elC));
    elementsData.insert(std::pair<G4int, G4Element*>(7, elN));
    elementsData.insert(std::pair<G4int, G4Element*>(8, elO));
    elementsData.insert(std::pair<G4int, G4Element*>(11, elNa));
    elementsData.insert(std::pair<G4int, G4Element*>(12, elMg));
    elementsData.insert(std::pair<G4int, G4Element*>(15, elP));
    elementsData.insert(std::pair<G4int, G4Element*>(16, elS));
    elementsData.insert(std::pair<G4int, G4Element*>(17, elCl));
    elementsData.insert(std::pair<G4int, G4Element*>(18, elAr));
    elementsData.insert(std::pair<G4int, G4Element*>(19, elK));
    elementsData.insert(std::pair<G4int, G4Element*>(20, elCa));

    
  std::ifstream ifData;
  ifData.open("matParamData.in", std::ios::in);
  G4int noM;
  ifData>>noM;
  
  //G4cout<<noM;
  //G4Material **loM;
  //loM = new G4Material*[noM];
  
  G4int mNum, mC, mNum2;
  G4int mIso;
  G4double mVal;
  G4double mDens;
  G4int mZZ = 0;
#ifndef NO_COLORS
  G4float mRed, mGreen, mBlue, mOpacity;
#endif

  for(int i = 0; i < noM; i++) {
    ifData>>mNum;
    ifData>>mDens;
#ifndef NO_COLORS
    ifData>>mRed>>mGreen>>mBlue>>mOpacity;
    G4Colour colour( mRed, mGreen, mBlue, mOpacity );
    G4VisAttributes* visAtt = new G4VisAttributes( colour );
    materialColours.insert(std::pair<G4int, G4VisAttributes*>(mNum, visAtt));
#endif
    materialDensities.insert(std::pair<G4int, G4double>(mNum, mDens));
  }
  ifData.close();

  ifData.open("matComposition.in", std::ios::in);
  ifData.seekg(0);
  for (int i = 0; i < noM; i++) {
    ifData>>mNum2;
    ifData>>mC;
    //G4cout<<"Mat and dens"<<" "<<mNum2<<" "<<mDens<<" "<<mC<<G4endl;

    /*if (mNum2 == 56) {
      materialData.insert(std::pair<G4int, G4Material*>(mNum2, nistMan->FindOrBuildMaterial("G4_AIR")));
      materialData2.insert(std::pair<G4int, G4Material*>(mZZ, nistMan->FindOrBuildMaterial("G4_AIR")));
      break;
    }*/

    G4Material* tt = new G4Material( G4String("EL")+std::to_string(mNum2),
                                            density = materialDensities.find(mNum2)->second*g/cm3,
                                            numberofElements = mC);
    for( int j = 0; j < mC; j++) {
      ifData>>mIso;
      ifData>>mVal;
      G4int mCurEl = ((G4int)mIso)/1000;
      ////G4cout<<mIso<<" "<<mVal<<" mCurEl "<<mCurEl<<G4endl;
      ////G4cout<<elementsData.find(mCurEl)->second<<" !<<"<<G4endl;
      tt->AddElement(elementsData.find(mCurEl)->second, mVal);
    }
    //G4cout<<"Adding material"<<tt->GetName()<<G4endl;
    materialData.insert(std::pair<G4int, G4Material*>(mNum2, tt));
    materialData2.insert(std::pair<G4int, G4Material*>(mZZ, tt));
    mZZ++;
  }

  ifData.close();
  //G4cout<<"MAT1:"<<G4endl;
  for( std::map<G4int, G4Material*>::iterator it = materialData.begin();
    it != materialData.end(); ++it) {
      //G4cout<<it->first<<" "<<it->second->GetName()<<G4endl;
  }
  
  //G4cout<<"MAT2:"<<G4endl;
  for( std::map<G4int, G4Material*>::iterator it = materialData2.begin();
    it != materialData2.end(); ++it) {
      G4cout<<it->first<<" "<<it->second->GetName()<<G4endl;
  }
  
}

G4Material* PhantomParameterisation::GetMyMaterial(G4int mid) {
#ifndef NO_MATS
  if (0 == mid || 0 == materialData.count(mid)) {
    G4NistManager* nistMan = G4NistManager::Instance();
    return nistMan->FindOrBuildMaterial("G4_WATER");
  }
  return materialData.find(mid)->second;
#else
    G4NistManager* nistMan = G4NistManager::Instance();
    return nistMan->FindOrBuildMaterial("G4_WATER");
#endif
}


//
// Transformation of voxels.
//
void PhantomParameterisation::
ComputeTransformation(const G4int copyNo, G4VPhysicalVolume* physVol) const
{
    //G4cout<<"Transformation computation for "<<copyNo<<G4endl;
    // Position of voxels.
    // x and y positions are already defined in DetectorConstruction by using
    // replicated volume. Here only we need to define is z positions of voxels.
    //physVol->SetTranslation(G4ThreeVector(0.,0.,(2.*static_cast<double>(copyNo)
                                                //+1.)*fdZ - fdZ*fnZ));
  G4double pN = static_cast<double>(copyNo);
  
  physVol->SetTranslation(G4ThreeVector(0.,0.,-phZ+fdZ + 2*fdZ*pN));
}

G4int PhantomParameterisation::GetNumberOfMaterials() const
{
#ifndef NO_MATS
  G4cout<<"Get number of materials called"<<G4endl;
  G4cout<<materialData.size()<<G4endl;
  return materialData.size();
#else
  return 1;
#endif
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//
// GetMaterial
//  This is needed for material scanner and realizing geometry.
//
G4Material* PhantomParameterisation::GetMaterial(G4int i) const
{
#ifndef NO_MATS
  G4cout<<"Materials getter for "<<i<<G4endl;
  G4NistManager* nistMan = G4NistManager::Instance();
 
  
  return materialData2.find(i)->second;
#else
  G4NistManager* nistMan = G4NistManager::Instance();
  return nistMan->FindOrBuildMaterial("G4_WATER");
#endif
}

G4Material* PhantomParameterisation::
ComputeMaterial(G4VPhysicalVolume* physVol, const G4int iz,
                const G4VTouchable* parentTouch)
{
#ifndef NO_MATS
    // protection for initialization and vis at idle state
    //
  //G4cout<<"Materials computation for "<<iz<<G4endl;

  G4NistManager* nistMan = G4NistManager::Instance();
  if(parentTouch==0) return nistMan->FindOrBuildMaterial("G4_WATER");

    // Copy number of voxels.
    // Copy number of X and Y are obtained from replication number.
    // Copy nymber of Z is the copy number of current voxel.
  G4int ix = parentTouch->GetReplicaNumber(1);
  G4int iy = parentTouch->GetReplicaNumber(0);

  //G4int copyID = ix + fnX*iy + fnX*fnY*iz;
  //G4int copyID = iz + fnZ*iy + fnY*fnZ*ix;
  G4int copyID = ix + iy*fnX + iz*fnX*fnY;
  //G4cout<<"Computer Material called"<<G4endl;
  //G4cout<<copyID<<" "<<ix<<" "<<iy<<" "<<iz<<" Have mat: "<<matListInts[copyID]<<G4endl;
  //G4cout<<copyID % fnY*fnZ<<" "<<(copyID - copyID % fnY*fnZ) % fnZ<<G4endl;
  //return zN * xLen * yLen + yN * xLen + xN
//          zN = int(v / (xLen * yLen))
        //yN = int((v - zN * (xLen * yLen)) / xLen)
        //xN = v - yN * xLen - zN * (xLen * yLen)
  //G4cout<<fnX<<" "<<fnY<<" "<<fnZ<<G4endl;
  //G4cout<<GetMyMaterial(matListInts[copyID])->GetName()<<G4endl;
  //return nistMan->FindOrBuildMaterial("G4_WATER");
#ifndef NO_COLORS
  if (physVol) {
    physVol->GetLogicalVolume()->
      SetVisAttributes(materialColours.find(matListInts[copyID])->second);
  }
#endif

  return GetMyMaterial(matListInts[copyID]);
#else
  G4NistManager* nistMan = G4NistManager::Instance();
  return nistMan->FindOrBuildMaterial("G4_WATER");
#endif
}

