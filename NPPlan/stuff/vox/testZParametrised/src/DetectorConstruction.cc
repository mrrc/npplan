#include "DetectorConstruction.hh"
#include "PhantomParameterisation.hh"
#include "G4PVParameterised.hh"
#include "DetectorSD.hh"
#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Orb.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4RotationMatrix.hh"
#include "G4NistManager.hh"
#include "globals.hh"
#include "G4VisAttributes.hh" 
#include "G4SDManager.hh"
#include "G4Region.hh"
#include "G4ProductionCuts.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4PSCellFlux3D.hh"
#include "G4PSDoseDeposit3D.hh"
#include "G4PartialPhantomParameterisation.hh"

DetectorConstruction::DetectorConstruction() {}

DetectorConstruction::~DetectorConstruction() {}

// ������ ��� ����������� ������ �������
#define SIZE(x) sizeof(x)/sizeof(*x)


G4VPhysicalVolume* DetectorConstruction::Construct()
{
    G4NistManager* nistMan = G4NistManager::Instance();
    G4Material *air = nistMan->FindOrBuildMaterial("G4_AIR");
    G4Material *dummyMat = nistMan->FindOrBuildMaterial("G4_WATER");

  G4double world_x_length = 2*m;
  G4double world_y_length = 2*m;
  G4double world_z_length = 2*m;

  G4Box* world_box = new G4Box("world", 0.5*world_x_length, 0.5*world_y_length, 0.5*world_z_length);
  G4LogicalVolume* world_log = new G4LogicalVolume(world_box, air, "world");
  G4VPhysicalVolume* world_phys = new G4PVPlacement(0, G4ThreeVector(), world_log, "world", 0, false, 0);


  G4double halfDetectorSizeX = 9*cm;
  G4double halfDetectorSizeY = 9*cm;
  G4double halfDetectorSizeZ = 1*cm;
  //G4PartialPhantomParameterisation()

  G4int numberOfVoxelsAlongX = 30;
  G4int numberOfVoxelsAlongY = 30;
  G4int numberOfVoxelsAlongZ = 10;

    G4double halfXVoxelSizeX = halfDetectorSizeX/numberOfVoxelsAlongX;
  G4double halfXVoxelSizeY = halfDetectorSizeY;
  G4double halfXVoxelSizeZ = halfDetectorSizeZ;
  G4double voxelXThickness = 2*halfXVoxelSizeX;

  G4Box *RODetector = new G4Box("RODetector", 
			       halfDetectorSizeX, 
			       halfDetectorSizeY, 
			       halfDetectorSizeZ);

  G4LogicalVolume *RODetectorLog = new G4LogicalVolume(RODetector,
						       dummyMat,
						      "RODetectorLog",
						      0,0,0);
  
  G4VPhysicalVolume *RODetectorPhys = new G4PVPlacement(0,
                            G4ThreeVector(0, 0, 0),
							"DetectorPhys",
							RODetectorLog,
							world_phys,
							false,0);

  G4Box *RODetectorXDivision = new G4Box("RODetectorXDivision",
					 halfXVoxelSizeX,
					 halfXVoxelSizeY,
					 halfXVoxelSizeZ);
  
  G4LogicalVolume *RODetectorXDivisionLog = new G4LogicalVolume(RODetectorXDivision,
							       dummyMat,
							       "RODetectorXDivisionLog",
							       0,0,0);

  G4VPhysicalVolume *RODetectorXDivisionPhys = new G4PVReplica("RODetectorXDivisionPhys",
                                                              RODetectorXDivisionLog,
                                                              RODetectorPhys,
                                                              kXAxis,
                                                              numberOfVoxelsAlongX,
                                                              voxelXThickness);

  // Division along Y axis: the slices along the X axis are divided along the Y axis

  G4double halfYVoxelSizeX = halfXVoxelSizeX;
  G4double halfYVoxelSizeY = halfDetectorSizeY/numberOfVoxelsAlongY;
  G4double halfYVoxelSizeZ = halfDetectorSizeZ;
  G4double voxelYThickness = 2*halfYVoxelSizeY;

  G4Box *RODetectorYDivision = new G4Box("RODetectorYDivision",
					halfYVoxelSizeX, 
					halfYVoxelSizeY,
					halfYVoxelSizeZ);

  G4LogicalVolume *RODetectorYDivisionLog = new G4LogicalVolume(RODetectorYDivision,
							       dummyMat,
							       "RODetectorYDivisionLog",
							       0,0,0);
 
  G4VPhysicalVolume *RODetectorYDivisionPhys = new G4PVReplica("RODetectorYDivisionPhys",
							      RODetectorYDivisionLog,
							      RODetectorXDivisionPhys,
							      kYAxis,
							      numberOfVoxelsAlongY,
							      voxelYThickness);
  
  // Division along Z axis: the slices along the Y axis are divided along the Z axis

  G4double halfZVoxelSizeX = halfXVoxelSizeX;
  G4double halfZVoxelSizeY = halfYVoxelSizeY;
  G4double halfZVoxelSizeZ = halfDetectorSizeZ/numberOfVoxelsAlongZ;
  G4double voxelZThickness = 2*halfZVoxelSizeZ;


 
  G4Box *RODetectorZDivision = new G4Box("RODetectorZDivision",
					halfZVoxelSizeX,
					halfZVoxelSizeY, 
					halfZVoxelSizeZ);


 
  G4LogicalVolume *RODetectorZDivisionLog = new G4LogicalVolume(RODetectorZDivision,
							       dummyMat,
							       "RODetectorZDivisionLog",
							       0,0,0);
    G4ThreeVector voxelSize(halfZVoxelSizeX, halfZVoxelSizeY, halfZVoxelSizeZ);
    G4ThreeVector phantomSize(halfDetectorSizeX, halfDetectorSizeY, halfDetectorSizeZ);
    G4cout<<"XYZ passed at constructor"<<numberOfVoxelsAlongX<<" "<<numberOfVoxelsAlongY<<" "<<numberOfVoxelsAlongZ<<G4endl;
    PhantomParameterisation* param =
    new PhantomParameterisation(voxelSize, phantomSize, numberOfVoxelsAlongX, numberOfVoxelsAlongY, numberOfVoxelsAlongZ);

    new G4PVParameterised("phantom",    // their name
                          RODetectorZDivisionLog, // their logical volume
                          RODetectorYDivisionLog,      // Mother logical volume
                          kZAxis,       // Are placed along this axis
                          //kUndefined,
                          // Are placed along this axis
                          numberOfVoxelsAlongZ,      // Number of cells
                          param);       // Parameterisation.

 
  /*G4VPhysicalVolume *RODetectorZDivisionPhys = new G4PVReplica("RODetectorZDivisionPhys",
					   RODetectorZDivisionLog,
					   RODetectorYDivisionPhys,
					   kZAxis,
					   numberOfVoxelsAlongZ,
					   voxelZThickness);*/

  //IORTDummySD *dummySD = new IORTDummySD;
  //RODetectorZDivisionLog -> SetSensitiveDetector(dummySD);

  /*G4MultiFunctionalDetector* MFDet = new G4MultiFunctionalDetector("testDet");
  G4VPrimitiveScorer* dosedep = new G4PSDoseDeposit3D("DoseDeposit", numberOfVoxelsAlongX, numberOfVoxelsAlongY, 
                            numberOfVoxelsAlongZ);
  MFDet->RegisterPrimitive(dosedep);

  G4VPrimitiveScorer* fluxmap = new G4PSCellFlux3D("FluxMap", numberOfVoxelsAlongX, numberOfVoxelsAlongY, 
                            numberOfVoxelsAlongZ);
  MFDet->RegisterPrimitive(fluxmap);

  //RODetectorZDivisionLog->SetSensitiveDetector(MFDet);
  SetSensitiveDetector(RODetectorZDivisionLog, MFDet);*/

  DetectorSD3DLayer *myDet = new DetectorSD3DLayer("testdet", numberOfVoxelsAlongX, numberOfVoxelsAlongY, numberOfVoxelsAlongZ);
  myDet->setPreVol(voxelXThickness*voxelYThickness*voxelZThickness);
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  sdMan->AddNewDetector(myDet);
  //RODetectorZDivisionLog->SetSensitiveDetector(myDet);
  SetSensitiveDetector(RODetectorZDivisionLog, myDet);

  /*detData *d = new detData;
  d->name = "testdet";
  d->outputName = "testdet";
  detectorsData.push_back(d);*/
  /*
  int zDirection = i % zLength;
int yDirection = (i / zLength) % yLength;
int xDirection = i / (yLength * zLength); 
*/

  return world_phys;

}

