# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 13.02.14
@summary: 
'''

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import codecs

#filename = u'C:\Temp\protvino120214\ФантомМРНЦОбнинск_12022014_115957.dfa'
#filename = u'C:\Temp\protvino120214\ФантомМРНЦОбнинск_12022014_131701.dfa'
#filename = u'C:/Users/Chernukha/Documents/mrrc/ForObninsk_20Feb/ФантомМРНЦОбнинск_20022014_122118.dfa'

#baseSourcePosition = 70


#fS = file(filename)
def diag(filename):
    fS = codecs.open(filename, 'r', 'cp1251')

    tx = []
    ty = []
    for i in fS.readlines():
        line = unicode(i)
        splLine = line.split(' ')
        print splLine
        if 4 == len(splLine):
            tx.append(splLine[0])
            ty.append(splLine[1])
        #print unicode(i)

    fig = plt.figure()

    X = np.array(tx)
    Y = np.array(ty)

    plt.plot(X, Y)
    plt.show()

#diag(filename)


def getSigma(sigmaTable, ene):
    for k in sorted(sigmaTable, reverse=True):
        if ene >= k:
            return sigmaTable[k]


def runMac(fromFile="", commonMac="", sigmaTable=""):
    baseSourcePosition = 70
    cmMac = file(commonMac)
    cmMacLines = []
    for i in cmMac.readlines():
        cmMacLines.append(i.replace('\n', ''))
    print cmMacLines
    fS = codecs.open(fromFile, 'r', 'cp1251')

    sigmaFile = file(sigmaTable)
    sigmaTable = {}
    for i in sigmaFile.readlines():
        ld = i.replace('\n', '').split('\t')
        sigmaTable[int(ld[0])] = (float(ld[1]), float(ld[2]))

    gunParams = {}
    curAngle = -1
    for i in fS.readlines():
        line = unicode(i)
        splLine = line.split(' ')
        print splLine
        if 3 == len(splLine):
            curAngle = float(splLine[0])
            if curAngle not in gunParams:
                gunParams[curAngle] = []
        elif 4 == len(splLine):
            angleX = float(splLine[0])
            angleY = float(splLine[1])
            ene = float(splLine[2])
            intens = float(splLine[3].replace('\r\n', ''))

            gunParams[curAngle].append((angleX, angleY, ene, intens))
    print gunParams
    gunParamsRaw = []
    for i in gunParams:
        gunParamsRaw.extend(gunParams[i])
    #print gunParamsRaw
    #return
    for k in gunParams:
        #i = gunParams[k]
        for i in gunParams[k]:
            #print k, i, np.radians(k), np.sin(np.radians(k))
            print '/gps/source/add %d' % i[3]
            for j in cmMacLines:
                print j
            #print '/gps/ene/mon %1.2f MeV' % i[2]
            #print '/gps/ene/min %1.2f MeV' % i[2]
            #print '/gps/ene/max %1.2f MeV' % i[2]
            print '/gps/pos/centre %2.2f %2.2f %2.2f' % (baseSourcePosition * np.sin(np.radians(k)),
                                                         baseSourcePosition * np.cos(np.radians(k)),
                                                         0
            )
            print '/gps/energy %1.2f MeV' % i[2]
            #print '/gps/direction %f %f 1' % (i[0], i[1])
            print '/gps/halfx %f mm' % (getSigma(sigmaTable, i[2])[0] / 2.0)
            print '/gps/halfy %f mm' % (getSigma(sigmaTable, i[2])[1] / 2.0)
            print '/gps/ang/rot1 %f %f 1' % (i[0], i[1])
            print '\n'


import sys
def printCmMac(cmMac, target=sys.stdout):
    for i in cmMac:
        print >>target, i
    pass

def runMac2():
    cm = r'C:\Temp\geantProjects\protonRealCase_build\Release\common.mac'
    cmMac = file(cm)
    cmMacLines = []
    for i in cmMac.readlines():
        cmMacLines.append(i.replace('\n', ''))
    print cmMacLines
    #fromFile = u'C:/Users/Chernukha/Documents/mrrc/ForObninsk_20Feb/ФантомМРНЦОбнинск_20022014_122118.dfa'
    #fromFile = u'C:\Temp\protvino120214\ФантомМРНЦОбнинск_12022014_115957.dfa'
    fromFile = u'C:\\Users\\Chernukha\\Documents\\mrrc\\ForObninsk_20Feb\\20cGy\\aa2.dfa'
    fS = codecs.open(fromFile, 'r', 'cp1251')
    gunParams = {}
    curAngle = -1
    for i in fS.readlines():
        line = unicode(i)
        splLine = line.split(' ')
        if 3 == len(splLine):
            curAngle = float(splLine[0])
            if curAngle not in gunParams:
                gunParams[curAngle] = []
        elif 4 == len(splLine):
            angleX = float(splLine[0])
            angleY = float(splLine[1])
            ene = float(splLine[2])
            intens = float(splLine[3].replace('\r\n', ''))

            gunParams[curAngle].append((angleX, angleY, ene, intens))
    import pprint
    pprint.pprint(gunParams)

    baseDistance = np.array([0, 700, 0])
    baseVector = np.array([0, -1, 0])

    for i in gunParams:
        baseAngle = float(i)
        sinD = np.sin(np.radians(i))
        cosD = np.cos(np.radians(i))
        newPos = (sinD * baseDistance[1],
                  cosD * baseDistance[1],
                  0)
        baseDirection = (-1.0*sinD, -1.0*cosD, 0)
        for curParams in gunParams[i]:
            newDirection = (baseDirection[0]+curParams[1],
                            baseDirection[1]+curParams[0],
                            0)
            #print newDirection
            print '/gps/source/add %d' % curParams[3]
            printCmMac(cmMacLines)
            print '/gps/ene/mono %1.2f MeV' % curParams[2]
            print '/gps/pos/centre %2.2f %2.2f %2.2f mm' % newPos
            print '/gps/direction %2.2f %2.2f %2.2f' % newDirection
            print ""

        #print newPos
        #cd = ()

def runMac3():
    cm = r'C:\Temp\geantProjects\protonRealCase_build\Release\common.mac'
    cmMac = file(cm)
    cmMacLines = []
    for i in cmMac.readlines():
        cmMacLines.append(i.replace('\n', ''))
    print cmMacLines
    #fromFile = u'C:/Users/Chernukha/Documents/mrrc/ForObninsk_20Feb/ФантомМРНЦОбнинск_20022014_122118.dfa'
    fromFile = u'C:\Temp\protvino120214\ФантомМРНЦОбнинск_12022014_115957.dfa'
    fS = codecs.open(fromFile, 'r', 'cp1251')
    gunParams = {}
    curAngle = -1
    for i in fS.readlines():
        line = unicode(i)
        splLine = line.split(' ')
        if 3 == len(splLine):
            curAngle = float(splLine[0])
            if curAngle not in gunParams:
                gunParams[curAngle] = []
        elif 4 == len(splLine):
            angleX = float(splLine[0])
            angleY = float(splLine[1])
            ene = float(splLine[2])
            intens = float(splLine[3].replace('\r\n', ''))

            gunParams[curAngle].append((angleX, angleY, ene, intens))
    import pprint
    pprint.pprint(gunParams)

    bd = r'C:\Temp\geantProjects\protonRealCase_build\Release\run36exp1'
    import os
    for i in gunParams:
        outFilePath = os.path.join(bd, 'runSd%d.mac' % i)
        outFile = open(outFilePath, "w")
        print >>outFile, "/run/verbose 1"
        print >>outFile, "/event/verbose 0"
        print >>outFile, "/tracking/verbose 0"
        print >>outFile, "/process/list"
        print >>outFile, '/detman/rotate -%d' % i
        print >>outFile, '/detman/detector c%id_' % i
        for curParams in gunParams[i]:
            print >>outFile, '/gps/source/add %d' % curParams[3]
            printCmMac(cmMacLines, outFile)
            print >>outFile, '/gps/ene/mono %1.2f MeV' % curParams[2]
            print >>outFile, '/gps/pos/centre %2.2f %2.2f %2.2f mm' % (0, 700, 0)
            print >>outFile, '/gps/direction %2.6f %2.6f %2.6f' % (curParams[0], -1, curParams[1])
            print >>outFile, ""
        print >>outFile, "/gps/source/list"
        print >>outFile, "/run/beamOn 1000000"
        pass
    pass

#runMac(filename, r'C:\Temp\geantProjects\protonRealCase_build\Release\common.mac',
#       r'C:\Temp\geantProjects\protonRealCase_build\Release\sigmaTable.in')

#diag(filename)

runMac2()