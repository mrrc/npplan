# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 20.11.13
@summary: 
'''

import wx
import serial
import ConfigParser
import numpy as np
import os
from time import mktime, localtime
import datetime
import wx.lib.agw.flatnotebook as FNB
import wx.dataview as dv
if os.name == 'nt': #sys.platform == 'win32':
    from serial.tools.list_ports_windows import *
elif os.name == 'posix':
    from serial.tools.list_ports_posix import *

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, Integer, DateTime, Float
from sqlalchemy.orm import sessionmaker, relationship

engine = create_engine('sqlite:///COMCounter.db', echo=False)
Base = declarative_base(engine)


class measure(Base):
    __tablename__ = 'measure'
    id = Column(Integer, primary_key=True)
    dateStart = Column(DateTime)
    dateEnd = Column(DateTime)
    dose = Column(Float)

    def __init__(self, dateStart, dateEnd, dose):
        self.dateStart = dateStart
        self.dateEnd = dateEnd
        self.dose = dose

if not os.path.exists('/COMCounter.db'):
    Base.metadata.create_all()

def compressLogs(path, **kwargs):
    period = kwargs.get('period', 12)
    print period
    import os
    oldDir = os.getcwd()
    curDate = localtime()
    os.chdir(path)
    i = 0
    while (True):
        archName = 'arch%s%s%s%d.tgz' %(curDate.tm_mday, curDate.tm_mon, curDate.tm_year, i)
        if (os.path.exists(archName)):
            i += 1
        else:
            break
    clist = []
    for fileName in os.listdir(path):
        try:
            ext = fileName.split('.')[-1]
        except:
            ext = ''
        if ('tgz' == ext):
            continue
        if ((mktime(curDate) - os.path.getctime(fileName)) > float(period*60*60)):
            clist.append(fileName)
    if (0 == len(clist)):
        print 'No files to archive'
        return
    import tarfile
    tar = tarfile.open("sample.tar", "w")
    for name in clist:
        tar.add(name)
    tar.close()
    import gzip
    f_in = open('sample.tar', 'rb')
    f_out = gzip.open(archName, 'wb')
    f_out.writelines(f_in)
    f_out.close()
    f_in.close()
    os.remove(path+'/sample.tar')
    print 'Archive %s created with following files:' %(archName)
    for i in clist:
        os.remove(i)
        print i
    os.chdir(oldDir)

def writeToFile(fileName, _str):
    if (not os.path.exists(fileName)):
        #f = codecs.open('unicode.rst', encoding='utf-8')
        _file = open(fileName, 'w')
        _file.write(_str)
        _file.close()
        return
    _file = open(fileName, 'a')
    _file.write(_str)
    _file.close()

def log(eventString, eventCode=-1):
    from time import strftime
    import os
    logStr = "%s> (%d): %s\n" %(strftime("%H:%M:%S> ", localtime()), eventCode, eventString)
    if not os.path.exists('./log'):
        os.mkdir('log')
    logFileName = './log/%s.log' % (strftime("%Y%m%d", localtime()))
    print logStr
    writeToFile(logFileName, logStr)
    pass


class MyFrame1 ( wx.Frame ):
    def __init__( self, parent, id, title, pos=wx.DefaultPosition, size=wx.Size( 500,300 ),
                  style=wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL):
        wx.Frame.__init__ ( self, parent, id, title, pos, size, style)
        #self.sess = sessionmaker()()
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(mainSizer)
        self.tpc = topPanel(self, wx.NewId())
        mainSizer.Add(self.tpc, 1, wx.EXPAND)
        #mainSizer.Add(MyPanel1(self), 1, wx.EXPAND)
        self.Bind(wx.EVT_CLOSE, self.onClose)
        self.Centre( wx.BOTH )
        self.SetIcon(wx.Icon('COMCounter.ico'))

    def onClose(self, event):
        if hasattr(self.tpc, '_con'):
            self.tpc._con.write('r')
        self.tpc.pageSettings.saveConfig(event)
        self.Destroy()
    
    def __del__( self ):
        pass


class topPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.book = FNB.FlatNotebook(self, wx.ID_ANY, agwStyle=FNB.FNB_NODRAG
                                                               | FNB.FNB_NO_NAV_BUTTONS
                                                               | FNB.FNB_NO_X_BUTTON
                                                               | FNB.FNB_VC8
        )
        self.sess = sessionmaker()()
        self._parent = parent
        self.pageMeasurement = measurePanel(self)
        self.book.AddPage(self.pageMeasurement, u"Измерение")
        self.pageSettings = settingsPanel(self)
        self.book.AddPage(self.pageSettings, u"Опции")
        self.pageArchive = archivePanel(self)
        self.book.AddPage(self.pageArchive, u"Архив")
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(mainSizer)
        mainSizer.Add(self.book, 6, wx.EXPAND)
        mainSizer.Layout()
        self.SendSizeEvent()


class settingsPanel(wx.Panel):
    def __init__(self, parent):
        """

        @param parent:
        @type parent: topPanel
        @return:
        """
        wx.Panel.__init__(self, parent, style=wx.TAB_TRAVERSAL)
        self._parent = parent
        self._dataFields = {}
        self._portList = []
        self._mode = 0
        self._powerEach = -1

        topSizer = wx.BoxSizer(wx.VERTICAL)
        if wx.Display().GetGeometry()[2] > 1024:
            sizerParams = wx.FlexGridSizer(cols=4)
        else:
            sizerParams = wx.FlexGridSizer(cols=2)
        sizerParams.Add( wx.StaticText( self, wx.ID_ANY, u"Режим", style=wx.ALIGN_CENTRE), flag=wx.ALIGN_CENTER)
        self._dataFields['mode'] = wx.Choice(self, wx.ID_ANY, choices=[u'Интегральная доза', u'Мощность дозы'])
        self._dataFields['mode'].SetSelection(0)
        sizerParams.Add(self._dataFields['mode'], flag=wx.ALIGN_CENTER)
        sizerParams.Add( wx.StaticText( self, wx.ID_ANY, u"Каждые ", style=wx.ALIGN_CENTRE), flag=wx.ALIGN_CENTER)
        self._dataFields['useEachPowerMode'] = wx.Choice(self, wx.ID_ANY, choices=[u'1', u'3', u'5', u'10'])
        self._dataFields['useEachPowerMode'].Disable()
        eachSecText = wx.StaticText(self, wx.ID_ANY, u" секунд")
        eachPowerModeSizer = wx.BoxSizer(wx.HORIZONTAL)
        #sizerParams.Add(self._dataFields['useEachPowerMode'], flag=wx.ALIGN_CENTER)
        eachPowerModeSizer.Add(self._dataFields['useEachPowerMode'])
        eachPowerModeSizer.Add(eachSecText)
        sizerParams.Add(eachPowerModeSizer, flag=wx.ALIGN_CENTER)
        sizerParams.Add( wx.StaticText( self, wx.ID_ANY, u"Порт устройства", style=wx.ALIGN_CENTRE), flag=wx.ALIGN_CENTER)
        self._dataFields['port'] = wx.Choice(self, wx.ID_ANY)
        sizerParams.Add(self._dataFields['port'], flag=wx.ALIGN_CENTER)
        sizerParams.Add( wx.StaticText( self, wx.ID_ANY, "r = ", style=wx.ALIGN_CENTRE), flag=wx.ALIGN_CENTER)
        self._dataFields['r'] = wx.TextCtrl(self, wx.ID_ANY, "")
        sizerParams.Add(self._dataFields['r'], flag=wx.ALIGN_CENTER)
        sizerParams.Add( wx.StaticText( self, wx.ID_ANY, "h = ", style=wx.ALIGN_CENTRE), flag=wx.ALIGN_CENTER)
        self._dataFields['h'] = wx.TextCtrl(self, wx.ID_ANY, "")
        sizerParams.Add(self._dataFields['h'], flag=wx.ALIGN_CENTER)
        sizerParams.Add( wx.StaticText( self, wx.ID_ANY, "R = ", style=wx.ALIGN_CENTRE), flag=wx.ALIGN_CENTER)
        self._dataFields['R'] = wx.TextCtrl(self, wx.ID_ANY, "")
        sizerParams.Add(self._dataFields['R'], flag=wx.ALIGN_CENTER)
        sizerParams.Add( wx.StaticText( self, wx.ID_ANY, u"Геофактор G", style=wx.ALIGN_CENTRE), flag=wx.ALIGN_CENTER)
        self._dataFields['G'] = wx.TextCtrl(self, wx.ID_ANY, "")
        self._dataFields['G'].Disable()
        sizerParams.Add(self._dataFields['G'], flag=wx.ALIGN_CENTER)
        sizerParams.Add( wx.StaticText( self, wx.ID_ANY, u"Калибровочный\nкоэффициент", style=wx.ALIGN_CENTRE), flag=wx.ALIGN_CENTER)
        self._dataFields['calib'] = wx.TextCtrl(self, wx.ID_ANY, "")
        sizerParams.Add(self._dataFields['calib'], flag=wx.ALIGN_CENTER)
        sizerParams.Add( wx.StaticText( self, wx.ID_ANY, u"Kerma = ", style=wx.ALIGN_CENTRE), flag=wx.ALIGN_CENTER)
        self._dataFields['kerma'] = wx.TextCtrl(self, wx.ID_ANY, "")
        sizerParams.Add(self._dataFields['kerma'], flag=wx.ALIGN_CENTER)
        sizerParams.Add( wx.StaticText( self, wx.ID_ANY, u"Поток нейтронов = ", style=wx.ALIGN_CENTRE), flag=wx.ALIGN_CENTER)
        self._dataFields['flow'] = wx.TextCtrl(self, wx.ID_ANY, "")
        sizerParams.Add(self._dataFields['flow'], flag=wx.ALIGN_CENTER)

        sizerParams.Add( wx.StaticText( self, wx.ID_ANY, u"Счётчик измерений ", style=wx.ALIGN_CENTRE), flag=wx.ALIGN_CENTER)
        self._dataFields['mtc'] = wx.TextCtrl(self, wx.ID_ANY, "")
        self._dataFields['mtc'].Disable()
        sizerParams.Add(self._dataFields['mtc'], flag=wx.ALIGN_CENTER)

        #if wx.Display().GetGeometry()[2] > 1024:
        #    sizerParams.Add((1, 1))
        #    sizerParams.Add((1, 1))
        btnFlushCounter = wx.Button(self, wx.ID_ANY, u"Обнулить счётчик измерений")
        btnSaveConfig = wx.Button(self, wx.ID_ANY, u"Сохранить конфигурацию")
        sizerParams.Add((1, 1))
        sizerParams.Add(btnFlushCounter)
        sizerParams.Add((1, 1))
        sizerParams.Add(btnSaveConfig)

        self.readConfig()
        self.detectPortsInit()
        self.Bind(wx.EVT_TEXT, self.modifiedG, self._dataFields['r'])
        self.Bind(wx.EVT_TEXT, self.modifiedG, self._dataFields['h'])
        self.Bind(wx.EVT_TEXT, self.modifiedG, self._dataFields['R'])
        self.Bind(wx.EVT_CHOICE, self.setMode, self._dataFields['mode'])
        self.Bind(wx.EVT_CHOICE, self.setPowerEach, self._dataFields['useEachPowerMode'])
        self.Bind(wx.EVT_BUTTON, self.flushCounter, btnFlushCounter)
        self.Bind(wx.EVT_BUTTON, self.saveConfig, btnSaveConfig)
        self.Bind(wx.EVT_CHOICE, self.setPort, self._dataFields['port'])

        self.Bind(wx.EVT_PAINT, self.onPaint)

        topSizer.Add(sizerParams, 1, wx.EXPAND)
        self.SetSizer(topSizer)
        topSizer.Fit(self)
        self.modifiedG(None)

    def detectPortsInit(self):
        self.portDetectorTimer = wx.PyTimer(self.detectPortsWork)
        self.portDetectorTimer.Start(800)
        self.findDeviceTimer = wx.PyTimer(self.findDeviceWork)
        self.findDeviceTimer.Start(300)

    def setMode(self, event):
        self._mode = self._dataFields['mode'].GetSelection()
        if 1 == self._mode:
            self._dataFields['useEachPowerMode'].Enable()
            if -1 == self._dataFields['useEachPowerMode'].GetSelection():
                self._dataFields['useEachPowerMode'].SetSelection(0)
        else:
            self._dataFields['useEachPowerMode'].Disable()
        self.setPowerEach()
        self._parent.pageMeasurement.setMode(self._mode)

    def setPowerEach(self, event=None):
        sel = self._dataFields['useEachPowerMode'].GetSelection()
        if -1 == sel:
            return
        elif 0 == sel:
            self._powerEach = 1
        elif 1 == sel:
            self._powerEach = 3
        elif 2 == sel:
            self._powerEach = 5
        elif 3 == sel:
            self._powerEach = 10
        self._parent.pageMeasurement.setMode(self._mode)

    def readConfig(self):
        config = ConfigParser.ConfigParser()
        config.read('COMCounter.ini')
        self._comPortValueConfig = self._comPortValue = str(config.get('port', 'port', 'COM3'))

        r = float(config.get('data', 'rm', '1.0'))
        h = float(config.get('data', 'H', '1.0'))
        R = float(config.get('data', 'R', '1.0'))
        coef = float(config.get('data', 'coef', '0.01'))
        calib = float(config.get('data', 'calib', '0.01'))
        kerma = float(config.get('data', 'kerma', '1.0'))
        flow = float(config.get('data', 'flow', '0.01'))
        #mtc = int(config.get('mtc', 'mtc', '0'))
        mtc = self._parent.sess.query(measure).count()

        self._dataFields['r'].SetValue(str(r))
        self._dataFields['h'].SetValue(str(h))
        self._dataFields['R'].SetValue(str(R))
        self._dataFields['calib'].SetValue(str(calib))
        self._dataFields['kerma'].SetValue(str(kerma))
        self._dataFields['coef'] = coef
        self._dataFields['flow'].SetValue(str(flow))
        #self._dataFields['mtc'].SetValue(str(int(mtc)).zfill(3))

        self.calculateG()

    def flushCounter(self, event):
        self._dataFields['mtc'].SetValue('0')
        #self.saveConfig(event)
        wx.GetApp().GetTopWindow().sess.query(measure).delete()
        wx.GetApp().GetTopWindow().sess.commit()

    def saveConfig(self, event=None):
        config = ConfigParser.RawConfigParser()
        config.add_section('port')
        config.set('port', 'port', self._comPortValueConfig)
        config.add_section('data')
        config.set('data', 'rm', self._dataFields['r'].GetValue())
        config.set('data', 'H', self._dataFields['h'].GetValue())
        config.set('data', 'R', self._dataFields['R'].GetValue())
        config.set('data', 'coef', self._dataFields['coef'])
        config.set('data', 'calib', self._dataFields['calib'].GetValue())
        config.set('data', 'kerma', self._dataFields['kerma'].GetValue())
        config.set('data', 'flow', self._dataFields['flow'].GetValue())
        #config.add_section('mtc')
        #config.set('mtc', 'mtc', self._dataFields['mtc'].GetValue())
        with open('COMCounter.ini', 'wb') as configfile:
            config.write(configfile)

    def modifiedG(self, event=None):
        self.calculateG()
        self._dataFields['G'].SetValue('%1.6f' % self.g)
        if event is not None:
            event.Skip()
        pass

    def get_r(self):
        return float(self._dataFields['r'].GetValue())
    def set_r(self, value):
        self._dataFields['r'].SetValue(float(value))
    def get_R(self):
        return float(self._dataFields['R'].GetValue())
    def set_R(self, value):
        self._dataFields['R'].SetValue(float(value))
    def get_h(self):
        return float(self._dataFields['h'].GetValue())
    def set_h(self, value):
        self._dataFields['r'].SetValue(float(value))
    r = property(get_r, set_r)
    R = property(get_R, set_R)
    h = property(get_h, set_h)

    def onPaint(self, event):
        mtc = self._parent.sess.query(measure).count()
        self._dataFields['mtc'].SetValue("%03d" % mtc)
        event.Skip()
        pass

    def calculateG(self, event=0):
        r = self.r
        R = self.R
        h = self.h
        r2 = r * r
        h2 = h * h
        R2 = R * R
        g = 1.0 / 4.0 / r2 / 3.14 * np.log( (h2+r2-R2+np.sqrt(r2*r2+2*r2*(h2-R2)+(h2+R2)*(h2+R2))) / (2*h2))
        self.g = g
        log('G (geofactor) modified, new value %s' % (str(g), ), 2)
        self._parent.pageMeasurement.setG(self.g)

    def detectPortsWork(self, event=None):
        i = 0
        j = -1
        for port, desc, hwid in sorted(comports()):
            i += 1
        if i == self._dataFields['port'].GetCount():
            return
        else:
            self._dataFields['port'].Clear()
            for i in range(len(self._portList)):
                self._portList.pop(0)
            for port, desc, hwid in sorted(comports()):
                self._portList.append(port)
                i = self._dataFields['port'].Append(port)
                self._dataFields['port'].SetClientData(i, port)

    def connectToPort(self, event=None):
        self._con = serial.Serial(baudrate=115200, bytesize=serial.EIGHTBITS, stopbits=serial.STOPBITS_ONE)
        #if self._comPortValue != self._dataFields['port'].GetValue():
        #    self._comPortValue = str(self._dataFields['port'].GetValue())
        self._con.port = self._comPortValue
        self._con.open()
        self._parent._con = self._con
        self._parent.pageMeasurement._con = self._con
        log('Port connection opened', 104)

    def setPort(self, event=None, **kwargs):
        if event is not None:
            #print 'Got port %s' % (self._dataFields['port'].GetClientData(self._dataFields['port'].GetSelection()))
            self._comPortValue = (self._dataFields['port'].GetClientData(self._dataFields['port'].GetSelection()))
            log('Manually set port to %s' % self._comPortValue)
            if hasattr(self, 'findDeviceTimer'):
                #self.portDetectorTimer.Stop()
                self.findDeviceTimer.Stop()
            self.connectToPort()

    def findDeviceWork(self):
        for index, i in enumerate(self._portList):
            try:
                _con = serial.Serial(baudrate=115200, bytesize=serial.EIGHTBITS, stopbits=serial.STOPBITS_ONE,
                                     timeout=0.1)
                _con.port = i
                _con.open()
                #print _con.read(1), i, self._portList
                if 'g' == _con.read(1):
                    log('Device autofound at port %s' % i, 14)
                    self.findDeviceTimer.Stop()
                    _con.write('s')
                    self._dataFields['port'].SetSelection(index)
                    self._comPortValue = i
                    wx.CallLater(500, self.connectToPort)
                _con.close()
            except serial.SerialException:
                pass
            #_con.write('1')
            pass
        pass


class archivePanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self._parent = parent
        self.dvlc = dvlc = dv.DataViewListCtrl(self)
        dvlc.AppendTextColumn(u'ID', width=40, flags=dv.DATAVIEW_COL_SORTABLE | dv.DATAVIEW_COL_RESIZABLE)
        dvlc.AppendTextColumn(u'Начало', width=150, flags=dv.DATAVIEW_COL_SORTABLE | dv.DATAVIEW_COL_RESIZABLE)
        dvlc.AppendTextColumn(u'Конец', width=150, flags=dv.DATAVIEW_COL_SORTABLE | dv.DATAVIEW_COL_RESIZABLE)
        dvlc.AppendTextColumn(u'Доза', width=100, flags=dv.DATAVIEW_COL_SORTABLE | dv.DATAVIEW_COL_RESIZABLE)
        dvlc.AppendTextColumn(u'Средняя мощность', width=100, flags=dv.DATAVIEW_COL_SORTABLE
                                                                    | dv.DATAVIEW_COL_RESIZABLE)
        self.Sizer = wx.BoxSizer()
        self.Sizer.Add(dvlc, 1, wx.EXPAND)

        self.Bind(wx.EVT_PAINT, self.onPaint)

    def onPaint(self, event):
        #print 'aaa'
        self.dvlc.DeleteAllItems()
        for i in self._parent.sess.query(measure).all():
            _id = str(i.id).zfill(3)
            dateStart = i.dateStart
            dateEnd = i.dateEnd
            dose = i.dose
            pwr = dateEnd - dateStart
            pwr = pwr.total_seconds()
            pwr = dose / pwr * 60
            dateStartStr = dateStart.strftime("%d.%m.%Y %H:%M:%S")
            dateEndStr = dateEnd.strftime("%d.%m.%Y %H:%M:%S")
            self.dvlc.AppendItem((_id, dateStartStr, dateEndStr, u'%1.2f Гр' % dose, u'%1.3f Гр/мин' % pwr))
            pass
        event.Skip()


#class MyPanel1 ( wx.Panel ):
class measurePanel(wx.Panel):
    def __init__(self, parent):
        """

        @param parent:
        @type parent: topPanel
        @return:
        """

        self._value = 0
        self._started = False
        self._strVal = ''
        self._intVal = 0
        self._dataFields = {}
        self._portList = []
        self._mode = -1

        wx.Panel.__init__(self, parent, id=wx.ID_ANY)
        self._parent = parent
        
        bSizer1 = wx.BoxSizer( wx.VERTICAL )

        self._integralImpulses = 0
        self._powerImpulses = 0
        self._powerValue = 0.0
        self._integralValue = 0.0
        self._mti = 0
        self._timers = {
            'start': -1,
            'end': -1,
            'lastPower': -1,
        }
        self._measurementImp = u"имп"
        self._measurementDose = u"Гр"

        self.valueText = wx.StaticText( self, wx.ID_ANY, u"Количество импульсов: ", style=wx.ALIGN_CENTRE )
        self.valueLabel = wx.StaticText( self, wx.ID_ANY, u"0 "+self._measurementImp, style=wx.ALIGN_CENTRE )
        valueSizer = wx.BoxSizer(wx.HORIZONTAL)
        valueSizer.Add(self.valueText, flag=wx.ALIGN_CENTER)
        valueSizer.Add(self.valueLabel, flag=wx.ALIGN_CENTER)
        #self.valueLabel.Wrap( -1 )
        #self.valueLabel.SetFont( wx.Font( 54, 70, 90, 90, False, wx.EmptyString ) )
        #hSizer = wx.BoxSizer(wx.HORIZONTAL)
        #hSizer.Add( self.valueLabel, 1, wx.ALL|wx.EXPAND|wx.CENTER, 5 )

        self.pwrText = wx.StaticText( self, wx.ID_ANY, u"Средняя мощность: ", style=wx.ALIGN_CENTRE )
        self.avgPwrLabel = wx.StaticText( self, wx.ID_ANY, u"0.00 Гр/мин", style=wx.ALIGN_CENTRE )
        pwrSizer = wx.BoxSizer(wx.HORIZONTAL)
        pwrSizer.Add(self.pwrText)
        pwrSizer.Add(self.avgPwrLabel)

        self.gyText = wx.StaticText( self, wx.ID_ANY, u"Интегральная доза: ", style=wx.ALIGN_CENTRE )
        self.gyLabel = wx.StaticText( self, wx.ID_ANY, u"0.00"+self._measurementDose, style=wx.ALIGN_CENTRE )
        if wx.Display().GetGeometry()[2] > 1024:
            self.gyLabel.SetFont( wx.Font( 54, 70, 90, 90, False, wx.EmptyString ) )
        else:
            self.gyLabel.SetFont( wx.Font( 26, 70, 90, 90, False, wx.EmptyString ) )
        gySizer = wx.BoxSizer(wx.HORIZONTAL)
        gySizer.Add(self.gyText, flag=wx.ALIGN_CENTER)
        gySizer.Add(self.gyLabel, flag=wx.ALIGN_CENTER)
        
        #bSizer1.Add( self.valueLabel, 1, wx.ALL|wx.EXPAND, 5 )
        bSizer1.Add(valueSizer, 1, wx.ALL|wx.EXPAND, 5)
        bSizer1.Add(pwrSizer, 1, wx.ALL|wx.EXPAND, 5)
        #bSizer1.Add( self.gyLabel, 2, wx.ALL|wx.EXPAND, 5 )
        bSizer1.Add(gySizer, 1, wx.ALL|wx.EXPAND, 5)


        self.m_button1 = wx.Button( self, wx.ID_ANY, u"Start", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_button1, 1, wx.ALL|wx.EXPAND, 5 )
        
        self.m_button2 = wx.Button( self, wx.ID_ANY, u"Stop", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_button2, 1, wx.ALL|wx.EXPAND, 5 )
        
        self.m_button3 = wx.Button( self, wx.ID_ANY, u"Clear", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.m_button3, 1, wx.ALL|wx.EXPAND, 5 )
        
        self.Bind(wx.EVT_TIMER, self.onTimerEvent)
        self.Bind(wx.EVT_BUTTON, self.onStart, self.m_button1)
        self.Bind(wx.EVT_BUTTON, self.onStop, self.m_button2)
        self.Bind(wx.EVT_BUTTON, self.onClear, self.m_button3)
        #self.Bind(wx.EVT_CHOICE, self.setPort, self._dataFields['port'])
        #self.Bind(wx.EVT_BUTTON, self.calculateG, calcBtn)

        self.SetSizer( bSizer1 )
        self.Layout()

        self.m_button2.Disable()
        self.initManagerPanel()
        self.setMode(0)
        #self.detectPortsInit()

    def setPort(self, event=None, **kwargs):
        if event is not None:
            #print 'Got port %s' % (self._dataFields['port'].GetClientData(self._dataFields['port'].GetSelection()))
            self._comPortValue = (self._dataFields['port'].GetClientData(self._dataFields['port'].GetSelection()))
            log('Manually set port to %s' % self._comPortValue)
            if hasattr(self, 'findDeviceTimer'):
                #self.portDetectorTimer.Stop()
                self.findDeviceTimer.Stop()



    def detectCorrectPort(self):
        pass

    def initManagerPanel(self):
        log('Appication started', 1)
        pass

    def setMode(self, mode):
        self._mode = mode
        if 0 == mode:
            self.valueText.SetLabel(u"Количество импульсов: ")
            self.gyText.SetLabel(u"Интегральная доза: ")
            self.pwrText.SetLabel(u"Средняя мощность дозы: ")
            self._measurementImp = u"имп"
            self._measurementDose = u"Гр"
        elif 1 == mode:
            self.valueText.SetLabel(u"Количество импульсов за %d сек: " % self._parent.pageSettings._powerEach)
            self.gyText.SetLabel(u"Мощность дозы (%d сек): " % self._parent.pageSettings._powerEach)
            self._measurementImp = u"имп/мин"
            self._measurementDose = u"Гр/мин"
            self.gyLabel.SetLabel(u"0.000" + self._measurementDose)
            self.pwrText.SetLabel(u"Интегральная доза: ")
            self.avgPwrLabel.SetLabel(u"0.00 Гр")

    def setG(self, g):
        self._G = g

    def onTimerEvent(self, event):
        val = self._con.read()
        oldVal = self.value
        #print val
        try:
            self._strVal = '%s%d' % (self._strVal, int(val))
        except ValueError:
            pass
        if val == "\r":
            self._intVal = int(self._strVal)
            self._strVal = ''
        self.value = self._intVal
        pass

    def onStart(self, event=None):
        self.t1 = wx.Timer(self)
        self.t1.Start(250)
        self.m_button1.Disable()
        self.m_button2.Enable()
        self.m_button3.Disable()
        #self._timers['start'] = mktime(localtime())
        self._timers['start'] = datetime.datetime.now()
        #self._parent.pageSettings.mtc = self._parent.pageSettings.mtc + 1
        self._started = True
        log('Start button clicked', 3)
        pass

    def onStop(self, event=None):
        #self._con.close()
        self.t1.Stop()
        del self.t1
        self._started = False
        self._timers['stop'] = datetime.datetime.now()
        self.m_button2.Disable()
        self.m_button1.Enable()
        self.m_button3.Enable()
        m = measure(self._timers['start'], self._timers['stop'], self._integralValue)
        wx.GetApp().GetTopWindow().sess.add(m)
        wx.GetApp().GetTopWindow().sess.commit()
        #log('Stop button clicked, impulses: %s, dose: %s' % (self._value, self.gyLabel.GetLabel()), 4)
        pass

    def onClear(self, event=None):
        #self.value = 0
        #self._intVal = 0
        #self._strVal = ''
        self._integralImpulses = 0
        self._powerImpulses = 0
        self._powerValue = 0.0
        self._integralValue = 0.0
        self._mti = 0
        self._timers = {
            'start': -1,
            'end': -1,
            'lastPower': -1,
        }
        self.valueLabel.SetLabel("%d %s" % (self._integralImpulses, self._measurementImp))
        self.avgPwrLabel.SetLabel("%1.3f %s" % (0, u"Гр/мин"))
        self.gyLabel.SetLabel('%1.2f %s' % (0, self._measurementDose))
        self.setMode(self._mode)
        #self._con.write('r')
        log('Clear button clicked', 5)

    def _setValue(self, value):
        self._mti += 1
        #print self._parent.pageSettings._powerEach
        self._value = value
        self._integralImpulses += value
        #self.valueLabel.SetLabel(str(value))
        fValue = float(value)
        fValue *= self._G
        fValue *= float(self._parent.pageSettings._dataFields['kerma'].GetValue())
        fValue *= self._parent.pageSettings._dataFields['coef']
        fValue *= (float(self._parent.pageSettings._dataFields['flow'].GetValue()) /
                   float(self._parent.pageSettings._dataFields['calib'].GetValue()))
        #fValue *= 60
        #print fValue

        if 0 == self._mode:
            self._integralValue += fValue
            strValue = u"%1.2f" % self._integralValue
            strValue += " "
            strValue += self._measurementDose
            self.gyLabel.SetLabel(strValue)
            self.valueLabel.SetLabel("%d %s" % (self._integralImpulses, self._measurementImp))

            #elapsed = mktime(localtime()) - self._timers['start']
            elapsed = datetime.datetime.now() - self._timers['start']
            elapsed = elapsed.total_seconds()
            pwrVal = self._integralValue / elapsed * 60
            self.avgPwrLabel.SetLabel("%1.3f %s" % (pwrVal, u"Гр/мин"))
        elif 1 == self._mode:
            self._integralValue += fValue
            pwe = self._parent.pageSettings._powerEach
            self.avgPwrLabel.SetLabel("%1.2f %s" % (self._integralValue, u"Гр"))
            if 1 == pwe:
                self.valueLabel.SetLabel(str(value))
                self.gyLabel.SetLabel("%1.3f %s" % (fValue * 60, u"Гр/мин"))
            else:
                if self._mti % pwe == 0:
                    self.valueLabel.SetLabel("%d" % (self._powerImpulses, ))
                    self.gyLabel.SetLabel("%1.3f %s" % (self._powerValue / pwe * 60, u"Гр/мин"))
                    self._powerImpulses = value
                    self._powerValue = fValue
                else:
                    self._powerImpulses += value
                    self._powerValue += fValue
            pass
        pass

    def _getValue(self):
        return int(self._value)

    value = property(_getValue, _setValue)
    
    def __del__( self ):
        pass


class testApp(wx.App):
    def OnInit(self):
        self.SetAppName("mrrcCounter")
        return True

if __name__ == '__main__':
    app = testApp(False)
    #print wx.Display().GetGeometry()
    if wx.Display().GetGeometry()[2] > 1024:
        size = (600, 600)
    else:
        size = (321, 600)
    topForm = MyFrame1(None, wx.NewId(), 'MRRC Counter Tool', size=size,
                      style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER ^ wx.MAXIMIZE_BOX,
                      pos=(10, 10))
    topForm.Show()
    app.MainLoop()

