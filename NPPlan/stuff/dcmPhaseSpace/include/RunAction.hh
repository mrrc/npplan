#ifndef RunAction_h
#define RunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"

class G4Run;
class DetectorConstruction;
class PGAPhaseSpace;

class RunAction : public G4UserRunAction
{
public:
  RunAction(DetectorConstruction *);
  virtual ~RunAction();

public:
  //G4Run* GenerateRun();
  virtual void BeginOfRunAction(const G4Run* aRun);
  virtual void EndOfRunAction(const G4Run* aRun);

  void eofRunAction(G4bool);
private:
  DetectorConstruction  *fDet;
};

#endif