#ifndef BiasOperation_hh
#define BiasOperation_hh 1

#include "G4VBiasingOperation.hh"
#include "G4ParticleChange.hh"

class myBiasOperation : public G4VBiasingOperation {
public:
  // -- Constructor :
  myBiasOperation(G4String name);
  // -- destructor:
  virtual ~myBiasOperation();

public:
  virtual const G4VBiasingInteractionLaw*
    ProvideOccurenceBiasingInteractionLaw(const G4BiasingProcessInterface*,
      G4ForceCondition&) final
  {
    return 0;
  }
  virtual G4VParticleChange*
    ApplyFinalStateBiasing(const G4BiasingProcessInterface*,
      const G4Track*,
      const G4Step*,
      G4bool&) final
  {
    return 0;
  }
  virtual G4double
    DistanceToApplyOperation(const G4Track*,
      G4double,
      G4ForceCondition* condition) final;
  virtual G4VParticleChange*
    GenerateBiasingFinalState(const G4Track*,
      const G4Step*) final;

private:
  G4ParticleChange    fParticleChange;
};

#endif