#ifndef EventInformation_h
#define EventInformation_h 1

#include "G4VUserEventInformation.hh"
#include "G4ThreeVector.hh"
#include "globals.hh"

class EventInformation : public G4VUserEventInformation
{
public:

  EventInformation();
  virtual ~EventInformation();

  inline virtual void Print()const {};
};

#endif