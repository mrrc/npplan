#ifndef BiasOperator_hh
#define BiasOperator_hh 1

#include "G4VBiasingOperator.hh"
#include "G4BiasingProcessInterface.hh"
#include "BiasOperation.hh"
#include "G4BOptnCloning.hh"
class G4BOptnChangeCrossSection;
class G4ParticleDefinition;
class G4VProcess;
#include <map>

class myBiasOperator : public G4VBiasingOperator {
public:
  // ------------------------------------------------------------
  // -- Constructor: takes the name of the particle type to bias:
  // ------------------------------------------------------------
  myBiasOperator(G4String particleToBias,
    G4String name = "SplitAndKillByXS");
  virtual ~myBiasOperator();

  // -- method called at beginning of run:
  virtual void StartRun();

private:
  // -----------------------------
  // -- Mandatory from base class:
  // -----------------------------
  // -- Not used:
  virtual G4VBiasingOperation*
    ProposeOccurenceBiasingOperation(const G4Track*,
      const G4BiasingProcessInterface*) final
  {
    return 0;
  }

  // -- Not used:
  virtual G4VBiasingOperation*
    ProposeFinalStateBiasingOperation(const G4Track*,
      const G4BiasingProcessInterface*) final
  {
    return 0;
  }

  // -- Used method : it will return the biasing operation that will split particles
  // -- with a probabilty depending on the total absorption cross-section.
  virtual G4VBiasingOperation*
    ProposeNonPhysicsBiasingOperation(const G4Track*,
      const G4BiasingProcessInterface*) final;

private:
  const G4ParticleDefinition*                      fParticleToBias;
  myBiasOperation* fMyBiasOperation;
  //G4BOptnCloning *fMyBiasOperation;
};

#endif