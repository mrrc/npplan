#ifndef TrackInformation_h
#define TrackInformation_h 1

#include "G4VUserTrackInformation.hh"
#include "globals.hh"
#include "G4Allocator.hh"

enum TrackStatus {
  active = 1, 
  bornInCollimatorLeaf = 2,
  diedInCollimatorLeaf = 3,
  inactive = 10
};

class TrackInformation : public G4VUserTrackInformation
{
public:

  TrackInformation();
  TrackInformation(const TrackInformation* aTrackInfo);
  TrackInformation& operator =(const TrackInformation& right);
  virtual ~TrackInformation();

  inline void *operator new(size_t);
  inline void operator delete(void *aTrackInfo);

  void setNeutronVal1(G4bool _val) { neutronVal1 = _val; };
  inline G4bool getNeutronVal1() { return neutronVal1;  };

  void setNeutronVal2(G4bool _val) { neutronVal2 = _val; };
  inline G4bool getNeutronVal2() { return neutronVal2; };

private:
  int fStatus;

  G4bool neutronVal1;
  G4bool neutronVal2;

};

extern G4ThreadLocal
G4Allocator<TrackInformation> * aTrackInformationAllocator;

inline void* TrackInformation::operator new(size_t)
{
  if (!aTrackInformationAllocator)
    aTrackInformationAllocator = new G4Allocator<TrackInformation>;
  return (void*)aTrackInformationAllocator->MallocSingle();
}

inline void TrackInformation::operator delete(void *aTrackInfo)
{
  aTrackInformationAllocator->FreeSingle((TrackInformation*)aTrackInfo);
}

#endif