#ifndef neutronPhysics_hh
#define neutronPhysics_hh 1

#include "globals.hh"
#include "G4VPhysicsConstructor.hh"


class neutronPhysics : public G4VPhysicsConstructor
{
public:
  neutronPhysics(const G4String& name = "neutron");
  ~neutronPhysics();

public:
  virtual void ConstructParticle() { };
  virtual void ConstructProcess();

};



#endif
