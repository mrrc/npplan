#ifndef WorkerInitialization_H
#define WorkerInitialization_H 1

#include "globals.hh"
#include "G4UserWorkerInitialization.hh"

class WorkerInitialization : public G4UserWorkerInitialization
{
public:
  WorkerInitialization();
  virtual ~WorkerInitialization();

public:
  virtual void WorkerInitialize() const;
  virtual void WorkerRunStart() const;
  virtual void WorkerRunEnd() const;
  virtual void WorkerStop() const;
};

#endif