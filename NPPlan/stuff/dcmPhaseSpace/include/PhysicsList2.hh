#ifndef PhysicsList2_hh
#define PhysicsList2_hh 1

#include "G4VModularPhysicsList.hh"
#include "globals.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class PhysicsList2 : public G4VModularPhysicsList
{
public:
  PhysicsList2();
  ~PhysicsList2();

public:
  virtual void ConstructParticle();
  virtual void SetCuts();
};

#endif