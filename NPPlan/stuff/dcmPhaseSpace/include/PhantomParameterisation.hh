#ifndef PhantomParameterisation_hh
#define PhantomParameterisation_hh 1

#include "G4Types.hh"
#include "G4VNestedParameterisation.hh" 
#include "G4ThreeVector.hh"
#include <vector>
#include <map>

class G4VPhysicalVolume;
class G4VTouchable;
class G4VSolid;
class G4Material;

class G4Box;
typedef std::map<std::vector<G4int>, G4String> ltP;
typedef std::map<G4int, G4String> atP;
typedef std::vector<G4Material*> vtP;

class NestedPhantomParameterisation : public G4VNestedParameterisation
{
public:  // with description

  NestedPhantomParameterisation(const G4ThreeVector& voxelSize,
    G4int nz,
    std::vector<G4Material*>& mat,
    ltP dicomMap);
  NestedPhantomParameterisation(const G4ThreeVector& voxelSize,
    G4int nx,
    G4int ny,
    G4int nz,
    std::vector<G4Material*>& mat,
    atP dicomMap);
  NestedPhantomParameterisation(const G4ThreeVector& voxelSize,
    G4int nx,
    G4int ny,
    G4int nz,
    std::vector<G4Material*>& mat,
    vtP dicomMap);
  ~NestedPhantomParameterisation();

  // Methods required in derived classes
  // -----------------------------------
  G4Material* ComputeMaterial(G4VPhysicalVolume *currentVol,
    const G4int repNo,
    const G4VTouchable *parentTouch = 0
  );
  // Required method, as it is the reason for this class.
  //   Must cope with parentTouch=0 for navigator's SetupHierarchy

  G4int       GetNumberOfMaterials() const;
  G4Material* GetMaterial(G4int idx) const;
  // Needed to define materials for instances of Nested Parameterisation 
  //   Current convention: each call should return the materials 
  //   of all instances with the same mother/ancestor volume.

  void ComputeTransformation(const G4int no,
    G4VPhysicalVolume *currentPV) const;

  // Methods optional in derived classes
  // -----------------------------------

  // Additional standard Parameterisation methods, 
  //   which can be optionally defined, in case solid is used.

  void ComputeDimensions(G4Box &,
    const G4int,
    const G4VPhysicalVolume *) const;

private:
  using G4VNestedParameterisation::ComputeMaterial;

  ltP _dicomMap;
  atP _dicomMap2;
  vtP _dicomMap3;

  G4double fdX, fdY, fdZ;
  G4int fNx, fNy;
  G4int fNz;
  //
  std::vector<G4double>  fpZ;
  std::vector<G4Material*> fMat;
};
#endif