#ifndef DetectorSD_Oth
#define DetectorSD_Oth 1

#include "globals.hh"
#include "G4VSensitiveDetector.hh"
#include <map>
#include <vector>
#include "NPLayerData.hh"

class DetectorSDOth : public G4VSensitiveDetector
{
public:
  DetectorSDOth(G4String);
  DetectorSDOth(G4String, G4int, G4int, G4int);
  ~DetectorSDOth();
  void Initialize(G4HCofThisEvent*);
  G4bool ProcessHits(G4Step*, G4TouchableHistory*);
  void EndOfEvent(G4HCofThisEvent*);
private:
  G4String ownName;
  G4int targetX;
  G4int targetY;

  G4int _topX, _topY, _topZ;
};

#endif