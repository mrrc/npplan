#ifndef PGAPhaseSpace_h
#define PGAPhaseSpace_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ParticleGun.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include <map>

#include <vector>

#include <boost/serialization/map.hpp> 
#include <boost/serialization/string.hpp> 
#include <boost/serialization/list.hpp> 
#include <boost/serialization/vector.hpp>
#include <boost/serialization/level_enum.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/access.hpp>
#include <boost/serialization/collection_traits.hpp>
#include <boost/serialization/is_bitwise_serializable.hpp>

class PhaseSpace1 {
public:
  int pid;
  double ene;
  float x;
  float y;
  float z;
  float ux;
  float uy;
  float uz;
private:
  friend class boost::serialization::access;
  template <class Archive> void serialize(Archive &ar, const unsigned int version) {
    ar & pid & ene & x & y & z & ux & uy & uz;
  }
};

class G4Event;

class PGAPhaseSpace : public G4VUserPrimaryGeneratorAction
{
public:
  PGAPhaseSpace();
  ~PGAPhaseSpace();
  virtual void GeneratePrimaries(G4Event*);
  //G4GeneralParticleSource* GetParticleGun() {return fParticleGun;};
  G4ParticleGun* GetParticleGun() { return fParticleGun; };

  void printPhaseSpaceVector();
  size_t getPhaseSpaceVectorSize() { return PhaseSpaceVector.size(); };

private:
  G4ParticleGun*        fParticleGun;

  std::vector<PhaseSpace1> PhaseSpaceVector;
  G4ThreeVector translationVector;
  G4RotationMatrix* rotationMatrix;

  G4bool usePhaseSpace;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


#endif