#include "RunAction.hh"

#include "Randomize.hh"
#include "globals.hh"

#include "g4root.hh"
#include "NPParamReader.hh"
#include "NPLayerData.hh"
#include "G4Run.hh"

#ifdef WIN32
#include "windows.h"
#include "psapi.h"
#endif

RunAction::RunAction(DetectorConstruction *_fd) : fDet(_fd)
{
}

RunAction::~RunAction()
{
}

//G4Run * RunAction::GenerateRun()
//{
//  return nullptr;
//}

void RunAction::BeginOfRunAction(const G4Run *)
{
  CLHEP::HepRandom::showEngineStatus();

#ifdef WIN32
  PROCESS_MEMORY_COUNTERS_EX pmc;
  GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
  SIZE_T virtualMemUsedByMe = pmc.PrivateUsage;
  G4cout << " vMemUsed at BeginOfRunAction: " << pmc.PrivateUsage << " " << pmc.WorkingSetSize << G4endl;
#endif

  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  NPLibrary::NPParamReaderMain *cmdParam = NPLibrary::NPParamReaderMain::Instance();
  G4String fFileName = cmdParam->getPriorityParamAsG4String("binaryFileName", "test.root");
  analysisManager->SetFileName(fFileName);
  analysisManager->SetVerboseLevel(1);
  
  //analysisManager->CreateH1("bornInelastic", "Born, inelastic energy", 140, 0 * MeV, 14 * MeV);
  //analysisManager->CreateH1("bornInelasticOnZoneInterest", "Born in zone of interest, inelastic energy", 140, 0 * MeV, 14 * MeV);
  //analysisManager->CreateH2("testMid41", "test dose at mid", 334, 0, 333, 334, 0, 333);
  //analysisManager->CreateH1("spcAtTarget", "Spectrum at target from born in zone of interest", 140, 0 * MeV, 14 * MeV);

  analysisManager->CreateH1("srcRandomChoice", "Source randomness choice test", 25045, 0, 25044);

  G4int xyCount = cmdParam->getPriorityParamAsG4Int("sDicomXYCount", "512");
  G4int zCount = cmdParam->getPriorityParamAsG4Int("sDicomZCount", "83");
  G4double sDicomPixelXY = cmdParam->getPriorityParamAsG4Double("sDicomPixelXY", "1.0");
  G4double sDicomPixelZ = cmdParam->getPriorityParamAsG4Double("sDicomPixelZ", "1.25");

  analysisManager->CreateH2("XYcenterMeV", "MeV deposit at XY center slice", xyCount, 0, xyCount - 1, xyCount, 0, xyCount - 1);
  analysisManager->CreateH2("XZcenterMeV", "MeV deposit at XZ center slice", xyCount, 0, xyCount - 1, zCount, 0, zCount - 1);
  analysisManager->CreateH2("YZcenterMeV", "MeV deposit at YZ center slice", xyCount, 0, xyCount - 1, zCount, 0, zCount - 1);

  analysisManager->OpenFile();
}

void RunAction::EndOfRunAction(const G4Run *aRun)
{
#ifdef WIN32
  PROCESS_MEMORY_COUNTERS_EX pmc;
  GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
  SIZE_T virtualMemUsedByMe = pmc.PrivateUsage;
  G4cout << " vMemUsed at EndOfRunAction: " << pmc.PrivateUsage << " " << pmc.WorkingSetSize << G4endl;
#endif
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  //if (analysisManager->IsActive()) {
  analysisManager->Write();
  analysisManager->CloseFile();
  //}

  NPLibrary::NPLayerData::NPVoxelMain *vxData = NPLibrary::NPLayerData::NPVoxelMain::Instance();
  if (G4Threading::IsMasterThread()) vxData->dumpAllData();
  if (G4Threading::IsMasterThread()) vxData->dumpDoseDepositWithError();

  NPLibrary::NPParamReaderMain *cmdParam = NPLibrary::NPParamReaderMain::Instance();
  G4String fFileName = cmdParam->getPriorityParamAsG4String("binaryFileName", "test.root");

  //vxData->finalize();

  std::stringstream ss;
  ss << fFileName << ".gz.bin" << aRun->GetRunID();

  G4cout << "Binary dump to: " << ss.str() << G4endl;

  if (G4Threading::IsMasterThread()) vxData->storeBinary(ss.str());

  //vxData->dumpAllData();
}

void RunAction::eofRunAction(G4bool isInterrupted) {
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  //if (analysisManager->IsActive()) {
  analysisManager->Write();
  analysisManager->CloseFile();
  //}

  NPLibrary::NPLayerData::NPVoxelMain *vxData = NPLibrary::NPLayerData::NPVoxelMain::Instance();

  vxData->finalize();

  vxData->dumpAllData();
}
