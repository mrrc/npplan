#include "PGAPhaseSpace.hh"


#include "G4Event.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"
#include "G4IonTable.hh"
#include "G4ParticleGun.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"

#include "NPParamReader.hh"
#include "boost/lexical_cast.hpp"
#include <boost/serialization/map.hpp> 
#include <boost/serialization/string.hpp> 
#include <boost/serialization/list.hpp> 
#include <boost/serialization/vector.hpp>
#include <boost/serialization/level_enum.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/access.hpp>
#include <boost/serialization/collection_traits.hpp>
#include <boost/serialization/is_bitwise_serializable.hpp>
#include <sstream>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/polymorphic_binary_iarchive.hpp>
#include <boost/archive/polymorphic_binary_oarchive.hpp>
#include <vector>
#include <iostream>
#include <sstream>
#include "g4root.hh"
#include <map>
//#include "G4Mutex.hh"

#include <random>

namespace {
  //Mutex to lock updating the global ion map
  G4Mutex specMapMutex = G4MUTEX_INITIALIZER;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PGAPhaseSpace::PGAPhaseSpace()
  :G4VUserPrimaryGeneratorAction(),
  fParticleGun(0)
{
  //fParticleGun  = new G4GeneralParticleSource();
  fParticleGun = new G4ParticleGun();

  //  myParamReader &paramReader = myParamReader::GetInstance();
  //useStaticEne = boost::lexical_cast<G4double>(paramReader.getPriorityParamAsString("useStaticEne", "0"));
  NPLibrary::NPParamReaderMain *cmdParam = NPLibrary::NPParamReaderMain::Instance();
  G4String phaseSpacePath = cmdParam->getPriorityParamAsG4String("phaseSpacePath", "D:\\Temp\\geantProjects\\neutronNewColmat_build105\\Release\\vniiavar0\\run2cnc.phasespace.gz.bin");
  G4String phaseSpaceFormat = cmdParam->getPriorityParamAsG4String("phaseSpaceFormat", "gzip_binary");

  if ("gzip_binary" == phaseSpaceFormat) {
    std::ifstream ifStream(phaseSpacePath, std::ios::binary);
    {
      boost::iostreams::filtering_istreambuf filtStream;

      filtStream.push(boost::iostreams::zlib_decompressor());
      filtStream.push(ifStream);

      //boost::archive::polymorphic_binary_iarchive ia(filtStream);
      boost::archive::binary_iarchive ia(filtStream);
      ia >> BOOST_SERIALIZATION_NVP(PhaseSpaceVector);
    }
    ifStream.close();
    usePhaseSpace = true;
  }
  else if ("textWFirstLine" == phaseSpaceFormat) {
    std::ifstream ifStream(phaseSpacePath, std::ios::in);
    G4int cNums;
    std::string buf;
    std::getline(ifStream, buf);
    boost::trim(buf);
    cNums = boost::lexical_cast<G4int>(buf);
    for (int i = 0; i < cNums; i++) {
      std::getline(ifStream, buf);
      boost::trim(buf);
      std::vector<G4double> vVec = cmdParam->splitString2G4double(buf, "\t");
      PhaseSpace1 ph;
      ph.pid = (int)vVec[0];
      ph.ene = vVec[1]*MeV;
      ph.x = vVec[2]*mm;
      ph.y = vVec[3]*mm;
      ph.z = vVec[4]*mm;
      ph.ux = vVec[5];
      ph.uy = vVec[6];
      ph.uz = vVec[7];
      PhaseSpaceVector.push_back(ph);
    }
    //std::getline(ifStream);
    //cNums << ifStream;
    usePhaseSpace = true;
  }
  else if ("textInCm" == phaseSpaceFormat) {
    usePhaseSpace = true;
  }
  else if ("noPhaseSpace" == phaseSpaceFormat) {
    usePhaseSpace = false;
  }


  translationVector = cmdParam->getPriorityParamAsG4ThreeVector("phaseSpaceTranslation");
  rotationMatrix = cmdParam->getPriorityParamAsG4RotationMatrixPointer("phaseSpaceRotation");
  //printPhaseSpaceVector();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PGAPhaseSpace::~PGAPhaseSpace()
{
  delete fParticleGun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


void PGAPhaseSpace::GeneratePrimaries(G4Event* anEvent)
{

  G4double xpp, ypp, zpp;
  G4double xag, yag, zag;

  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  //G4ParticleDefinition* particle
  //  = particleTable->FindParticle("chargedgeantino");
  G4ParticleDefinition* neutron
    = particleTable->FindParticle("neutron");
  G4ParticleDefinition* gamma
    = particleTable->FindParticle("gamma");

  //G4int Z = 9, A = 18;
  //G4double ionCharge = 0.*eplus;
  //G4double excitEnergy = 0.*keV;

  //G4ParticleDefinition* ion
  //  = G4IonTable::GetIonTable()->GetIon(Z, A, excitEnergy);
  //fParticleGun->SetParticleDefinition(ion);
  //fParticleGun->SetParticleCharge(ionCharge);
  //fParticleGun->SetParticleDefinition(particle);

  ////tvVals.size()
  ////G4int curCp = (G4UniformRand() * tvVals.size()) % tvVals.size();
  ////CLHEP::RandFlat::

  ////fParticleGun->SetParticlePosition(G4ThreeVector(x0 + biX, y0 + biY, z0));
  //fParticleGun->SetParticlePosition(G4ThreeVector(xpp, ypp, -140 * cm));

  ////direction uniform in solid angle
  ////
  //G4double fCosAlphaMin = 0;
  //G4double fCosAlphaMax = 1.0;
  //G4double cosAlpha = fCosAlphaMin - G4UniformRand()*(fCosAlphaMin - fCosAlphaMax);
  //G4double sinAlpha = std::sqrt(1. - cosAlpha*cosAlpha);
  //G4double psi = 0.2 + G4UniformRand()*(0.6);

  //G4double rX = 1 - G4UniformRand() * 2;
  ////G4cout << rX << G4endl;

  //std::random_device rd;
  //std::mt19937 rng(rd());
  //std::uniform_int_distribution<G4int> uni(0, tvVals.size());

  ////G4int rChoice = uni(rng);
  if (!usePhaseSpace) {
    fParticleGun->SetParticleDefinition(neutron);
    fParticleGun->SetParticleEnergy(14.8*MeV);

    xpp = G4UniformRand() * 25 - 12.5;
    xpp += translationVector.x();
    ypp = G4UniformRand()*25 - 12.5;
    ypp += translationVector.y();
    zpp = 0;
    zpp += translationVector.z();
    G4ThreeVector a(xpp, ypp, zpp);
    a.rotate(rotationMatrix->phi(), rotationMatrix->theta(), rotationMatrix->psi());
    fParticleGun->SetParticlePosition(a);

    //xag = ph.ux;
    G4ThreeVector b(0, 0, 1);
    xag = G4UniformRand() * 70 - 35;
    xag *= deg;
    b.rotateX(xag);
    yag = G4UniformRand() * 70 - 35;
    yag *= deg;
    b.rotateY(yag);
    b.rotate(rotationMatrix->phi(), rotationMatrix->theta(), rotationMatrix->psi());
    fParticleGun->SetParticleMomentumDirection(b);


    fParticleGun->GeneratePrimaryVertex(anEvent);
    return;
  }
  G4int rChoice = CLHEP::RandFlat::shootInt(PhaseSpaceVector.size());

  //std::vector<G4double> curParams = tvVals.find(rChoice)->second;
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  G4int rHid = analysisManager->GetH1Id("srcRandomChoice");
  if (rHid != -1) {
    analysisManager->FillH1(rHid, rChoice);
  }

  PhaseSpace1 ph = PhaseSpaceVector[rChoice];
  if (ph.pid == 4)
    fParticleGun->SetParticleDefinition(neutron);
  else if (ph.pid == 1)
    fParticleGun->SetParticleDefinition(gamma);
  fParticleGun->SetParticleEnergy(ph.ene);

  xpp = ph.x;
  xpp += translationVector.x();
  ypp = ph.y;
  ypp += translationVector.y();
  zpp = ph.z;
  zpp += translationVector.z();
  G4ThreeVector a(xpp, ypp, zpp);
  a.rotate(rotationMatrix->phi(), rotationMatrix->theta(), rotationMatrix->psi());
  fParticleGun->SetParticlePosition(a);

  //xag = ph.ux;
  G4ThreeVector b(ph.ux, ph.uy, ph.uz);
  b.rotate(rotationMatrix->phi(), rotationMatrix->theta(), rotationMatrix->psi());
  fParticleGun->SetParticleMomentumDirection(b);
  


  //G4cout << " src:" << rChoice << " curParams: " << curParams[0] << " " << curParams[1] << " " << curParams[2] << " " << curParams[5] << G4endl;

  //G4double rYmin = 7 / std::sqrt(7 * 7 + 4 * 4);
  //G4double rY = 1.0*(rYmin + G4UniformRand()*(rYmin - 1));

  //G4double ux = sinAlpha*std::cos(psi),
  //  uy = sinAlpha*std::sin(psi),
  //  uz = -1.0*cosAlpha;
  //G4ThreeVector a(0, 0, 1);
  //a.rotateX(curParams[1] * radian);
  //a.rotateY(curParams[2] * radian);
  ////G4RandGauss::shootArray
  ////CLHEP::Ranlux64Engine
  ////fParticleGun->SetParticleMomentumDirection(G4ThreeVector(rX, rY, uz));
  //fParticleGun->SetParticleMomentumDirection(a);
  ////G4RandGauss::shoot();
  //if (true) {
  //  G4double tTime = (curParams[5] + G4UniformRand() * (curParams[6] - curParams[5])) * millisecond;
  //  fParticleGun->SetParticleTime(tTime);
  //  analysisManager->FillH1(rHid + 1, tTime);
  //}

  ////G4double theta = std::atan(std::sqrt(rX*rX + rY*rY) / (uz*uz));

  ////G4cout<<theta<<" "<<(theta*radian) / degree<<G4endl;

  ////G4double ene;
  ////if (!useStaticEne) {
  ////  ene = getEnergyByThetaAngle(theta*radian);
  ////}
  ////else {
  ////  ene = 14.5*MeV;
  ////}

  ////G4double eneFluct = 0.125 - G4UniformRand() * 0.25 * MeV;

  ////fParticleGun->SetParticleEnergy(ene + eneFluct);
  //fParticleGun->SetParticleEnergy((curParams[3] + G4UniformRand()*0.01*curParams[3])*MeV);

  fParticleGun->GeneratePrimaryVertex(anEvent);


}

void PGAPhaseSpace::printPhaseSpaceVector(void)
{
  for (size_t i = 0; i < PhaseSpaceVector.size(); i++) {
    G4cout << PhaseSpaceVector[i].pid << "\t" << PhaseSpaceVector[i].ene << "\t" << G4endl;
  }
}

