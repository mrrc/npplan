#include "DetectorConstruction.hh"

// Geant4 includes
#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"

#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4Transform3D.hh"
#include "G4TwoVector.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4Cons.hh"
#include "G4Trd.hh"
#include "G4Orb.hh"
#include "G4GenericTrap.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4EllipticalTube.hh"
#include "G4UnionSolid.hh"
#include "G4AssemblyVolume.hh"

#include "G4LogicalVolume.hh"
#include "G4NistManager.hh"
#include "G4Material.hh"

#include "G4Region.hh"
#include "G4RegionStore.hh"
#include "G4ProductionCuts.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"

#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4PVParameterised.hh"
#include "G4VisAttributes.hh" 

#include "G4SDManager.hh"
#include "G4TransportationManager.hh"
#include "G4RunManager.hh"
#include "G4GeometryManager.hh"
#include "G4GeometryTolerance.hh"
#include "G4IStore.hh"

// boost includes
#include "boost/lexical_cast.hpp"
#include <boost/algorithm/string.hpp>

// NPLibrary
#include "NPParamReader.hh"
#include "NPLayerData.hh"
#include "NPPhantom.hh"

// App
#include "DetectorMaterials.hh"
#include "PhantomParameterisation.hh"
#include "BOptrMultiParticleForceCollision.hh"
#include "BiasOperator.hh"
#include <map>
typedef std::map<std::vector<G4int>, G4String> ltP;
typedef std::map<std::vector<G4int>, G4double> ltD;
typedef std::map<G4int, G4String> atP;
typedef std::vector<G4Material*> vtP;

#include "DetectorSDOth.hh"


DetectorConstruction::DetectorConstruction() : G4VUserDetectorConstruction()
{
  G4cerr << "DetectronConstruction main constructor" << G4endl;
  //pMaterial = new DetectorMaterials();
  pMaterial = DetectorMaterials::GetInstance();
}

DetectorConstruction::~DetectorConstruction()
{
}

G4VPhysicalVolume * DetectorConstruction::Construct()
{
  //G4GeometryManager::GetInstance()->OpenGeometry();
  //G4PhysicalVolumeStore::GetInstance()->Clean();
  //G4LogicalVolumeStore::GetInstance()->Clean();
  //G4SolidStore::GetInstance()->Clean();

  NPLibrary::NPParamReaderMain *cmdParam = NPLibrary::NPParamReaderMain::Instance();
  G4NistManager* nistMan = G4NistManager::Instance();
  G4Material *Galactic = nistMan->FindOrBuildMaterial("G4_Galactic");
  G4Material *worldMaterial = pMaterial->getMaterialIfDefined("worldMaterial", Galactic);
  G4double worldXLength = cmdParam->getPriorityParamAsG4Double("WorldXLength", "60")*cm;
  G4double worldYLength = cmdParam->getPriorityParamAsG4Double("WorldYLength", "60")*cm;
  G4double worldZLength = cmdParam->getPriorityParamAsG4Double("WorldZLength", "60")*cm;

  pMaterial->DefineMaterials();

  G4Box* worldBox = new G4Box("world", 0.5*worldXLength, 0.5*worldYLength, 0.5*worldZLength);
  G4LogicalVolume* worldBoxLV = new G4LogicalVolume(worldBox, worldMaterial, "worldLV");
  G4VPhysicalVolume* worldBoxPV = new G4PVPlacement(0, G4ThreeVector(), worldBoxLV, "worldPV", 0, false, 0);

  //G4IStore *aIstore = G4IStore::GetInstance();
  //aIstore->AddImportanceGeometryCell(1, *worldBoxPV);

  if (true) {
    G4Material *air = nistMan->FindOrBuildMaterial("G4_AIR");
    G4Material *water = nistMan->FindOrBuildMaterial("G4_WATER");
    G4double airBoxZLength = cmdParam->getPriorityParamAsG4Double("airBoxZLength", "60")*cm;
    G4Box *airBox = new G4Box("airBox", 0.4*worldXLength, 0.4*worldYLength, 0.5*airBoxZLength);
    G4LogicalVolume *airBoxLV = new G4LogicalVolume(airBox, water, "airBoxLV");
    new G4PVPlacement(0, G4ThreeVector(), airBoxLV, "airBoxPV", worldBoxLV, false, 0, true);
    constructDummyTarget(airBoxLV);
  }
  else {
    //constructDummyTarget(worldBoxLV);
  }

  return worldBoxPV;
}

void DetectorConstruction::constructDummyTarget(G4LogicalVolume *parentLV)
{
  NPLibrary::NPParamReaderMain *cmdParam = NPLibrary::NPParamReaderMain::Instance();
  G4NistManager* nistMan = G4NistManager::Instance();
  G4Material *water = nistMan->FindOrBuildMaterial("G4_WATER");
  
  std::vector<G4Material*> fMat;
  G4String sDicomMatList = cmdParam->getPriorityParamAsG4String("sDicomMatList", "mats.list");
  std::ifstream fDicomMatList(sDicomMatList, std::ios::in);
  while (!fDicomMatList.eof()) {
    std::string buf;
    //fDicomMap.read
    std::getline(fDicomMatList, buf);
    boost::trim(buf);
    if (buf.empty())
      break;
    G4String buf2 = G4String(buf);
    fMat.push_back(nistMan->FindOrBuildMaterial(buf2));
  }
  G4cout << "fMat size: " << fMat.size() << G4endl;
//  fMat.push_back(nistMan->FindOrBuildMaterial("air0"));



  G4double topBoxX = cmdParam->getPriorityParamAsG4Double("topBoxX", "-1")*mm;
  G4double topBoxY = cmdParam->getPriorityParamAsG4Double("topBoxY", "-1")*mm;
  G4int xyCount = cmdParam->getPriorityParamAsG4Int("sDicomXYCount", "512");
  G4int zCount = cmdParam->getPriorityParamAsG4Int("sDicomZCount", "83");
  G4double sDicomPixelXY = cmdParam->getPriorityParamAsG4Double("sDicomPixelXY", "1.0");
  G4double sDicomPixelZ = cmdParam->getPriorityParamAsG4Double("sDicomPixelZ", "1.25");
  if (topBoxX < 0)
    topBoxX = xyCount * sDicomPixelXY * mm;
  if (topBoxY < 0)
    topBoxY = xyCount * sDicomPixelXY * mm;
  G4double topBoxZ = sDicomPixelZ * mm * zCount;
  G4double paramResolution = sDicomPixelXY * mm;

  G4cout << " -= topBoxData: " << topBoxX / mm << " x " << topBoxY / mm << " x " << topBoxZ / mm << " mm3; " << G4endl;
  G4cout << " -= paramResolution: " << paramResolution / mm << " mm " << "Z: "<<sDicomPixelZ << " mm" << G4endl;
  G4cout << " -= xyCount: " << xyCount << G4endl;
  G4cout << " -= zCount: " << zCount << G4endl;

  G4Box *topBox = new G4Box("topBox", 0.5 * topBoxX, 0.5 * topBoxY, 0.5 * topBoxZ);
  G4LogicalVolume *topBoxLV = new G4LogicalVolume(topBox, nistMan->FindOrBuildMaterial("G4_WATER"), "topBoxLV");
  /*G4RotationMatrix *topBoxRot = new G4RotationMatrix();
  topBoxRot->rotateX(135 * deg);
  topBoxRot->rotateY(-90 * deg);*/
  G4RotationMatrix *topBoxRot = cmdParam->getPriorityParamAsG4RotationMatrixPointer("topBoxRot");
  G4ThreeVector topBoxShift = cmdParam->getPriorityParamAsG4ThreeVector("topBoxShift");
  G4cout << " -= topBoxShift: " << topBoxShift.x() / mm << " : " << topBoxShift.y() / mm <<" : " << topBoxShift.z() / mm << G4endl;
  new G4PVPlacement(topBoxRot, topBoxShift, topBoxLV, "topBoxPV", parentLV, false, 0, true);

  G4Box *repX = new G4Box("repX", 0.5*paramResolution, 0.5*topBoxY, 0.5 * topBoxZ);
  G4LogicalVolume *repXLV = new G4LogicalVolume(repX, nistMan->FindOrBuildMaterial("G4_WATER"), "repXLV");
  new G4PVReplica("repXPV", repXLV, topBoxLV, kXAxis, (G4int)(topBoxX / paramResolution), paramResolution, 0.0);

  G4Box *repY = new G4Box("repY", 0.5*paramResolution, 0.5*paramResolution, 0.5* topBoxZ);
  G4LogicalVolume *repYLV = new G4LogicalVolume(repY, nistMan->FindOrBuildMaterial("G4_WATER"), "repYLV");
  new G4PVReplica("repYPV", repYLV, repXLV, kYAxis, (G4int)(topBoxY / paramResolution), paramResolution, 0.0);

  G4Box *repZ = new G4Box("repZ", 0.5*paramResolution, 0.5*paramResolution, 0.5*sDicomPixelZ*mm);
  G4LogicalVolume *repZLV = new G4LogicalVolume(repZ, nistMan->FindOrBuildMaterial("G4_WATER"), "repZLV");
  scoringLV = repZLV;

  ltP dicomMap;
  ltD dicomDens;
  atP dicomMap2;
  vtP dicomMap3;/* = new vtP[xyCount*xyCount*zCount];*/
  dicomMap3.reserve(xyCount*xyCount*zCount);
  //G4String sDicomMap = cmdParam->getPriorityParamAsG4String("sDicomMap", "D:\\Temp\\geantProjects\\dcm\\DICOM_g4104_dcmtk363_build\\Release\\CT.rtp1.1.167.CT1.T.-53.CT.txt.raw");
  G4String sDicomMap = cmdParam->getPriorityParamAsG4String("sDicomMap", "D:\\Temp\\geantProjects\\dcm\\PlannerTest\\PatientTestCT\\Case8\\testF3.raw");
  //G4String sDicomMap = cmdParam->getPriorityParamAsG4String("sDicomMap", "D:\\NPPlan3\\NPPlan\\stuff\\vox\\testGoritco.raw");
  G4bool sDicomMapFormat = cmdParam->getPriorityParamAsCppInt("sDicomMapFormatNew", "1");
  std::ifstream fDicomMap(sDicomMap, std::ios::in);
  while (!fDicomMap.eof()) {
    std::string buf;
    //fDicomMap.read
    std::getline(fDicomMap, buf);
    if (buf.empty())
      break;
    std::vector<std::string> strs;
    boost::split(strs, buf, boost::is_any_of(" "));
    G4int x = boost::lexical_cast<G4int>(strs[0]);
    G4int y = boost::lexical_cast<G4int>(strs[1]);
    G4int z = boost::lexical_cast<G4int>(strs[2]);
    boost::trim(strs[3]);
    G4String mat(strs[3]);
    std::vector<G4int> coord1;
    coord1.push_back(x);
    coord1.push_back(y);
    coord1.push_back(z);
    // ix + rX*iy + rX*rY*iz
    if (true) {
      G4int coord = x + xyCount*y + xyCount*xyCount*z;
      dicomMap2.insert(std::pair<G4int, G4String>(coord, mat));
      //dicomMap3.push_back(nistMan->FindOrBuildMaterial(mat));
      dicomMap3[coord] = nistMan->FindOrBuildMaterial(mat);
    }
    else {
      if (!sDicomMapFormat) {
        boost::trim(strs[4]);
        G4double den = boost::lexical_cast<G4double>(strs[4]);
        dicomDens.insert(std::pair<std::vector<G4int>, G4double>(coord1, den));
      }
      dicomMap.insert(std::pair<std::vector<G4int>, G4String>(coord1, mat));
      //dicomDens.insert(std::pair<std::vector<G4int>, G4double>(coord1, den));
    }
  }


  /* @todo: send to NPLibrary 
     @sidenote: run only in master thread
  */
  /*G4cout << "self test" << G4endl;
  G4String fFileName = cmdParam->getPriorityParamAsG4String("binaryFileName", "test.root");
  std::stringstream ss;
  ss << fFileName << ".dosemap";
  G4cout << "Dosemap dump to: " << ss.str() << G4endl;

  std::ofstream doseMapStream(ss.str(), std::ios::out | std::ios::trunc);

  for (auto it = dicomMap.begin(); it != dicomMap.end(); ++it) {
    doseMapStream << it->first[0] << "\t" << it->first[1] << "\t" << it->first[2] << "\t" << it->second << "\t";
    if (!sDicomMapFormat) {
      doseMapStream << (1 * MeV / (repZ->GetCubicVolume() * (dicomDens.find(it->first)->second * g / cm3))) / CLHEP::gray<< G4endl;
    } else {
      G4double dens = nistMan->FindOrBuildMaterial(it->second)->GetDensity();
      doseMapStream << (1 * MeV / (repZ->GetCubicVolume() * dens)) / CLHEP::gray << G4endl;
    }
  }*/

  //NestedPhantomParameterisation* paramPhantom
  //  = new NestedPhantomParameterisation(G4ThreeVector(paramResolution, paramResolution, sDicomPixelZ*mm) / 2, zCount, fMat, dicomMap);

  NestedPhantomParameterisation* paramPhantom
    = new NestedPhantomParameterisation(G4ThreeVector(paramResolution, paramResolution, sDicomPixelZ*mm) / 2, xyCount, xyCount, zCount, fMat, dicomMap2);

  //NestedPhantomParameterisation* paramPhantom
  //  = new NestedPhantomParameterisation(G4ThreeVector(paramResolution, paramResolution, sDicomPixelZ*mm) / 2, xyCount, xyCount, zCount, fMat, dicomMap3);

  G4VPhysicalVolume *phantom = new G4PVParameterised("PhantomSens",     // their name
    repZLV,    // their logical volume
    repYLV,           // Mother logical volume
    kZAxis,        // Are placed along this axis 
    zCount,           // Number of cells
    paramPhantom);     // Parameterisation.


  //G4IStore *aIstore = G4IStore::GetInstance();
  //aIstore->AddImportanceGeometryCell(4, *phantom);

  G4Region *regPhantom = new G4Region("regPhantom");
  setCutsForRegionFromCMDString(regPhantom);
  parentLV->SetRegion(regPhantom);
  topBoxLV->SetRegion(regPhantom);
  repXLV->SetRegion(regPhantom);
  repYLV->SetRegion(regPhantom);
  repZLV->SetRegion(regPhantom);
  regPhantom->AddRootLogicalVolume(parentLV);
}



void DetectorConstruction::ConstructSDandField()
{

  G4cout << "ConstructSDandField called" << G4endl;

  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  NPLibrary::NPParamReaderMain *cmdParam = NPLibrary::NPParamReaderMain::Instance();

  sdMan->SetVerboseLevel(1);
  if ((bool)cmdParam->getPriorityParamAsG4Int("disableDCSDandField", "0")) {
    return;
  }

  G4int xyCount = cmdParam->getPriorityParamAsG4Int("sDicomXYCount", "512");
  G4int zCount = cmdParam->getPriorityParamAsG4Int("sDicomZCount", "83");

  NPLibrary::NPLayerData::NPVoxelMain *vxData = NPLibrary::NPLayerData::NPVoxelMain::Instance();
  vxData->addFilter("secondaryProtons", NPLibrary::NPLayerData::NPFilterType::npF_userData);

  vxData->setVoxelData(xyCount, xyCount, zCount, 0.2*cm, 0.2*cm, 0.2*cm, G4ThreeVector(2 * cm, 2 * cm, 3 * cm));
  
  DetectorSDOth *det = new DetectorSDOth("det", xyCount, xyCount, zCount);
  scoringLV->SetSensitiveDetector(det);
  sdMan->AddNewDetector(det);

  //BOptrMultiParticleForceCollision* testMany = new BOptrMultiParticleForceCollision();
  ////testMany->AddParticle("gamma");
  //testMany->AddParticle("neutron");
  //testMany->AttachTo(scoringLV);
  //G4cout << " Attaching biasing operator " << testMany->GetName()
  //  << " to logical volume " << scoringLV->GetName()
  //  << G4endl;

  //myBiasOperator *fBiasOperator = new myBiasOperator("neutron");
  //fBiasOperator->AttachTo(scoringLV);

  //G4IStore *aIstore = G4IStore::GetInstance();

}

void DetectorConstruction::setCutsForRegionFromCMDString(G4Region *targetRegion, G4String regionName) {
  // from former NP Extra
  NPLibrary::NPParamReaderMain *cmdParam = NPLibrary::NPParamReaderMain::Instance();
  G4String regionCuts = cmdParam->getPriorityParamAsG4String(G4String("useCut4" + regionName), "-1");
  if ("-1" == regionCuts) {
    regionCuts = cmdParam->getPriorityParamAsG4String("WorldCuts", "700,700,700,700");
  }
  std::vector<G4double> cutsData = cmdParam->splitString2G4double(regionCuts, ",");
  G4ProductionCuts* pCut = new G4ProductionCuts(); // @todo: UserSpecialCuts ??
  pCut->SetProductionCut(cutsData[0] * um, "gamma");
  pCut->SetProductionCut(cutsData[1] * um, "e-");
  pCut->SetProductionCut(cutsData[2] * um, "e+");
  pCut->SetProductionCut(cutsData[3] * um, "proton");
  targetRegion->SetProductionCuts(pCut);

}

void DetectorConstruction::setCutsForRegionFromCMDString(G4Region *tgr)
{
  setCutsForRegionFromCMDString(tgr, tgr->GetName());
}
