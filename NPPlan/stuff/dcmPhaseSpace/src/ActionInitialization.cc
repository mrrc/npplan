#include "ActionInitialization.hh"
#include "EventAction.hh"
#include "RunAction.hh"
#include "PGAPhaseSpace.hh"
#include "SteppingAction.hh"
#include "TrackingAction.hh"


ActionInitialization::ActionInitialization(DetectorConstruction* fdet) : G4VUserActionInitialization(), fDetector(fdet)
{
  //masterRunAction = new RunAction();
}


ActionInitialization::~ActionInitialization()
{}


void ActionInitialization::Build() const
{

  //SetUserAction(run);
  NPLibrary::NPParamReaderMain *cmdParam = NPLibrary::NPParamReaderMain::Instance();
  //G4String PGAMode = cmdParam->getPriorityParamAsG4String("PGAMode", "gps");
  //if ("gps" == PGAMode) {
  //  PrimaryGeneratorAction *prim = new PrimaryGeneratorAction();
  //  SetUserAction(prim);
  //}
  //else if ("oth" == PGAMode) {
  //  PGAOth *prim = new PGAOth();
  //  SetUserAction(prim);
  //}
  //else {
  //  PrimaryGeneratorAction *prim = new PrimaryGeneratorAction();
  //  SetUserAction(prim);
  //}
  PGAPhaseSpace *prim = new PGAPhaseSpace();
  SetUserAction(prim);


  RunAction *run = new RunAction(fDetector);
  SetUserAction(run);

  G4bool disableSteppingAction = cmdParam->getPriorityParamAsCppInt("AppDisableSteppingAction", "0");
  G4bool disableTrackingAction = cmdParam->getPriorityParamAsCppInt("AppDisableTrackingAction", "0");

  if (!disableTrackingAction) {
    TrackingAction *tra = new TrackingAction(/*prim*//*evt*/);
    SetUserAction(tra);
  }
  if (!disableSteppingAction) {
    SteppingAction *sa = new SteppingAction();
    SetUserAction(sa);
  }
  //if (!disableEventAction) {
  //  EventAction *evt = new EventAction(run);
  //  SetUserAction(evt);
  //  if (!disableTrackingAction) {
  //    TrackingAction *tra = new TrackingAction(/*prim*//*evt*/);
  //    SetUserAction(tra);
  //    if (!disableSteppingAction) {
  //      SteppingAction *sa = new SteppingAction(evt, tra);
  //      SetUserAction(sa);
  //    }
  //  }
  //}

  ////SetUserAction(new SteppingAction(run, evt, tra));
  ////SetUserAction(run);

}

void ActionInitialization::BuildForMaster() const
{
#ifdef G4MULTITHREADED
  SetUserAction(new RunAction(fDetector));
#endif
}