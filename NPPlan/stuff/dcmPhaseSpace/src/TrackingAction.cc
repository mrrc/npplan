#include "TrackingAction.hh"
#include "TrackInformation.hh"
#include "G4Neutron.hh"
#include "G4TrackingManager.hh"
#include "G4Track.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "g4root.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo...... 
TrackingAction::TrackingAction()
  :G4UserTrackingAction()
{
  ;
}

void TrackingAction::PreUserTrackingAction(const G4Track* aTrack)
{

  //if (aTrack->GetParticleDefinition() == G4Neutron::NeutronDefinition() && aTrack->GetKineticEnergy() < 1000 * keV /*&& aTrack->GetParentID() > 0*/) {
  //  //G4cout << "called 1" << G4endl;
  //  // This is new track which is neutron below threshold 
  //  // updating neutronVal1 with `true` 
  //  // this value will also be propagated at the end of tracking action for every secondary of this track
  //  TrackInformation* infoNew = new TrackInformation();
  //  infoNew->setNeutronVal1(true);
  //  aTrack->SetUserInformation(infoNew);
  //  return;
  //}
  //if (aTrack->GetParentID() > 0) {
  //  G4cout << aTrack->GetCreatorProcess()->GetProcessName() << " " << aTrack->GetTouchable()->GetVolume()->GetName() << G4endl;
  //  
  //}
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  //if (aTrack->GetParentID() > 0) {
  //  if (aTrack->GetCreatorProcess()->GetProcessName() == "neutronInelastic" && aTrack->GetParticleDefinition() == G4Neutron::NeutronDefinition()) {
  //    G4int hId = analysisManager->GetH1Id("bornInelastic", false);
  //    analysisManager->FillH1(hId, aTrack->GetKineticEnergy());
  //    //aTrack->GetPosition()
  //    //G4cout << aTrack->GetTouchable()->GetVolume()->GetName() << G4endl;
  //    G4String volName = aTrack->GetTouchable()->GetVolume()->GetName();
  //    if (volName == "krishkaNizPV"
  //      ||
  //      volName == "tubeExternFePV"
  //      ||
  //      volName == "tubeMasloPV"
  //      ) {
  //      TrackInformation* infoNew = new TrackInformation();
  //      infoNew->setNeutronVal1(true);
  //      aTrack->SetUserInformation(infoNew);
  //      hId = analysisManager->GetH1Id("bornInelasticOnZoneInterest", false);
  //      analysisManager->FillH1(hId, aTrack->GetKineticEnergy());
  //      return;
  //    }
  //  }
  //}

  // empty user information here or we will get zero pointer on PostUserTrackingAction
  TrackInformation* infoNew = new TrackInformation();
  aTrack->SetUserInformation(infoNew);

  return;
  TrackInformation* trackInfo =
    (TrackInformation*)(aTrack->GetUserInformation());

  /*if (trackInfo->GetTrackingStatus() > 0)
  {
    fpTrackingManager->SetStoreTrajectory(true);
    fpTrackingManager->SetTrajectory(new RE01Trajectory(aTrack));
  }
  else
  {
    fpTrackingManager->SetStoreTrajectory(false);
  }*/
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo...... 
void TrackingAction::PostUserTrackingAction(const G4Track* aTrack)
{
  G4TrackVector* secondaries = fpTrackingManager->GimmeSecondaries();
  //if (secondaries)
  //{
  //  // seems nothing to do here if we don't have any secondaries with this track
  //  TrackInformation* info =
  //    (TrackInformation*)(aTrack->GetUserInformation());

  //  // but TrackInformation may be unset ??

  //  if (info->getNeutronVal1()) {
  //    // if neutronVal1 is set
  //    // we propagate it to all secondaries
  //    // neutronVal1 means that the original particle is neutron and has kinetic energy below threshold
  //    // so we need all the secondaries from this track to be caught
  //    size_t nSeco = secondaries->size();
  //    if (nSeco > 0)
  //    {
  //      for (size_t i = 0; i < nSeco; i++)
  //      {
  //        TrackInformation* infoNew = new TrackInformation(info);
  //        (*secondaries)[i]->SetUserInformation(infoNew);
  //      }
  //    }
  //  }
  //}

  //// here we need to propagate TrackInformation with neutronVal2 
  //if (secondaries) {
  //  TrackInformation* info =
  //    (TrackInformation*)(aTrack->GetUserInformation());
  //  if (info->getNeutronVal2()) {
  //    size_t nSeco = secondaries->size();
  //    if (nSeco > 0)
  //    {
  //      for (size_t i = 0; i < nSeco; i++)
  //      {
  //        TrackInformation* infoNew = new TrackInformation(info);
  //        (*secondaries)[i]->SetUserInformation(infoNew);
  //      }
  //    }
  //  }
  //}
}


