#include "PhantomParameterisation.hh"

#include "G4VPhysicalVolume.hh"
#include "G4VTouchable.hh"
#include "G4ThreeVector.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4Material.hh"
#include "G4VisAttributes.hh"
#include "G4NistManager.hh"
#include "G4Colour.hh"

#include <map>

typedef std::map<std::vector<G4int>, G4String> ltP;
typedef std::map<G4int, G4String> atP;
typedef std::vector<G4Material*> vtP;


NestedPhantomParameterisation
::NestedPhantomParameterisation(const G4ThreeVector& voxelSize,
  G4int nz,
  std::vector<G4Material*>& mat, ltP dicomMap) :
  G4VNestedParameterisation(),
  fdX(voxelSize.x()), fdY(voxelSize.y()), fdZ(voxelSize.z()),
  fNx(0), fNy(0),
  fNz(nz), fMat(mat), _dicomMap(dicomMap)
{
  // Position of voxels. 
  // x and y positions are already defined in DetectorConstruction 
  // by using replicated volume. Here only we need to define is z positions
  // of voxles.
  fpZ.clear();
  G4double zp;
  for (G4int iz = 0; iz < fNz; iz++) {
    zp = (-fNz + 1 + 2 * iz)*fdZ;
    fpZ.push_back(zp);
  }

}
NestedPhantomParameterisation
::NestedPhantomParameterisation(const G4ThreeVector& voxelSize,
  G4int nx, G4int ny, G4int nz,
  std::vector<G4Material*>& mat, atP dicomMap) :
  G4VNestedParameterisation(),
  fdX(voxelSize.x()), fdY(voxelSize.y()), fdZ(voxelSize.z()),
  fNx(nx), fNy(ny),
  fNz(nz), fMat(mat), _dicomMap2(dicomMap)
{
  // Position of voxels. 
  // x and y positions are already defined in DetectorConstruction 
  // by using replicated volume. Here only we need to define is z positions
  // of voxles.
  fpZ.clear();
  G4double zp;
  for (G4int iz = 0; iz < fNz; iz++) {
    zp = (-fNz + 1 + 2 * iz)*fdZ;
    fpZ.push_back(zp);
  }

}
NestedPhantomParameterisation
::NestedPhantomParameterisation(const G4ThreeVector& voxelSize,
  G4int nx, G4int ny, G4int nz,
  std::vector<G4Material*>& mat, vtP dicomMap) :
  G4VNestedParameterisation(),
  fdX(voxelSize.x()), fdY(voxelSize.y()), fdZ(voxelSize.z()),
  fNx(nx), fNy(ny),
  fNz(nz), fMat(mat), _dicomMap3(dicomMap)
{
  // Position of voxels. 
  // x and y positions are already defined in DetectorConstruction 
  // by using replicated volume. Here only we need to define is z positions
  // of voxles.
  fpZ.clear();
  G4double zp;
  for (G4int iz = 0; iz < fNz; iz++) {
    zp = (-fNz + 1 + 2 * iz)*fdZ;
    fpZ.push_back(zp);
  }

}

G4Material* NestedPhantomParameterisation
::ComputeMaterial(G4VPhysicalVolume* currentVol, const G4int copyNo,
  const G4VTouchable* parentTouch)
{
  /*if (parentTouch == 0) return fMat[0]; /**/// protection for initialization and
                                            // vis at idle state
                                            // Copy number of voxels. 
                                            // Copy number of X and Y are obtained from replication number.
                                            // Copy nymber of Z is the copy number of current voxel.
  G4int ix = parentTouch->GetReplicaNumber(1);
  G4int iy = parentTouch->GetReplicaNumber(0);
  G4int iz = copyNo;

  //std::cout << "ixiyiz: " << ix << " " << iy << " " << iz << " ";
  // For demonstration purpose,a couple of materials are chosen alternately.
  G4Material* mat = 0;
  G4NistManager* nistMan = G4NistManager::Instance();

  if (fNx > 0) { 
    // // ix + rX*iy + rX*rY*iz
    G4int tg = ix + fNx*iy + fNx*fNy*iz;
    if (_dicomMap3.size() > 0) {
      mat = _dicomMap3[tg];
    }
    else {
      if (0 != _dicomMap2.count(tg)) {
        mat = nistMan->FindOrBuildMaterial(_dicomMap2.find(tg)->second);
      }
      else {
        mat = nistMan->FindOrBuildMaterial("G4_AIR");
      }
    }
  }
  else {
    //_dicomMap.find()
    std::vector<G4int> tgVect;
    tgVect.push_back(ix);
    tgVect.push_back(iy);
    tgVect.push_back(iz);
    if (0 != _dicomMap.count(tgVect)) {
      mat = nistMan->FindOrBuildMaterial(_dicomMap.find(tgVect)->second);
      //return nistMan->FindOrBuildMaterial(_dicomMap.find(tgVect)->second);
    }
    else {
      mat = nistMan->FindOrBuildMaterial("G4_AIR");
    }

    //for (auto it = _dicomMap.begin(); it != _dicomMap.end(); ++it) {
    //  std::vector<G4int> tmp = it->first;
    //  //std::cout << ix << " " << iy << " " << tmp[0] << " " << tmp[1] << " " << it->second << G4endl;
    //  if (ix == tmp[0] && iy == tmp[1] && iz == tmp[2]) {
    //    mat = nistMan->FindOrBuildMaterial(it->second);
    //    
    //    break;
    //  }
    //}
    //std::cout << mat->GetName() << G4endl;
    //if (mat->GetName() == "air0") {
    //  G4Colour colour(25, 05, 255, 0.8);
    //  G4VisAttributes* visAttr = new G4VisAttributes(colour);
    //  visAttr->SetVisibility(false);
    //  currentVol->GetLogicalVolume()->SetVisAttributes(visAttr);
    //}
  }
  return mat;

}

NestedPhantomParameterisation::~NestedPhantomParameterisation() {
  fpZ.clear();
}

G4int NestedPhantomParameterisation::GetNumberOfMaterials() const {
  return fMat.size();
}


G4Material* NestedPhantomParameterisation::GetMaterial(G4int i) const {
  std::cout << fMat[i]->GetName() << " i-th (" << i << G4endl;
  return fMat[i];
}


void NestedPhantomParameterisation
::ComputeTransformation(const G4int copyNo, G4VPhysicalVolume* physVol) const {
  G4ThreeVector position(0., 0., fpZ[copyNo]);
  physVol->SetTranslation(position);
}


void NestedPhantomParameterisation
::ComputeDimensions(G4Box& box, const G4int, const G4VPhysicalVolume*) const {
  box.SetXHalfLength(fdX);
  box.SetYHalfLength(fdY);
  box.SetZHalfLength(fdZ);
}