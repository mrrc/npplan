#include "SteppingAction.hh"
//#include "RE01RegionInformation.hh"
#include "TrackInformation.hh"

#include "G4Track.hh"
#include "G4Step.hh"
#include "G4StepPoint.hh"
#include "G4TrackStatus.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4Region.hh"
#include "G4SteppingManager.hh"
#include "G4Neutron.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "g4root.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
SteppingAction::SteppingAction()
  : G4UserSteppingAction()
{
  ;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
SteppingAction::~SteppingAction()
{
  ;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void SteppingAction::UserSteppingAction(const G4Step * aStep)
{
  //// Suspend a track if it is entering into the calorimeter

  //// check if it is alive
  //G4Track * theTrack = theStep->GetTrack();
  //if (theTrack->GetTrackStatus() != fAlive) { return; }

  //// get region information
  //G4StepPoint * thePrePoint = theStep->GetPreStepPoint();
  //G4String thePrePV = thePrePoint->GetPhysicalVolume()->GetName();
  ////RE01RegionInformation* thePreRInfo
  ////  = (RE01RegionInformation*)(thePreLV->GetRegion()->GetUserInformation());
  //G4StepPoint * thePostPoint = theStep->GetPostStepPoint();
  //G4String thePostLV = thePostPoint->GetPhysicalVolume()->GetName();
  //RE01RegionInformation* thePostRInfo
  //  = (RE01RegionInformation*)(thePostLV->GetRegion()->GetUserInformation());

  //// check if it is entering to the calorimeter volume
  //if (!(thePreRInfo->IsCalorimeter()) && (thePostRInfo->IsCalorimeter()))
  //{
  //  // if the track had already been suspended at the previous step, let it go.
  //  RE01TrackInformation* trackInfo
  //    = static_cast<RE01TrackInformation*>(theTrack->GetUserInformation());
  //  if (trackInfo->GetSuspendedStepID()>-1)
  //  {
  //    if (fpSteppingManager->GetverboseLevel()>0)
  //    {
  //      G4cout << "++++ This track had already been suspended at step #"
  //        << trackInfo->GetSuspendedStepID() << ". Tracking resumed."
  //        << G4endl;
  //    }
  //  }
  //  else
  //  {
  //    trackInfo->SetSuspendedStepID(theTrack->GetCurrentStepNumber());
  //    theTrack->SetTrackStatus(fSuspend);
  //  }
  //}

  G4Track *theTrack = aStep->GetTrack();
  TrackInformation* trackInfo
    = static_cast<TrackInformation*>(theTrack->GetUserInformation());

  if (theTrack->GetKineticEnergy() > 15.2 * MeV) {
    theTrack->SetTrackStatus(fKillTrackAndSecondaries);
    return;
  }
  if (theTrack->GetParentID() == 0 && theTrack->GetKineticEnergy() < 0.04*MeV) {
    theTrack->SetTrackStatus(fKillTrackAndSecondaries);
    return;
  }

  const G4StepPoint* endPoint = aStep->GetPostStepPoint();
  const G4StepPoint* startPoint = aStep->GetPreStepPoint();
  G4StepStatus stepStatus = endPoint->GetStepStatus();
  G4bool transmit = (stepStatus == fGeomBoundary || stepStatus == fWorldBoundary);
  //if (transmit) {
  //  UserSAonTransmit(aStep);
  //  return;
  //}
  if (stepStatus == fWorldBoundary) {
    return;
  }
  G4String volName;
  G4Track* track = aStep->GetTrack();
  if (track->GetVolume()) volName = track->GetVolume()->GetName();
  G4String nextVolName;
  if (track->GetNextVolume()) nextVolName = track->GetNextVolume()->GetName();

  //if (trackInfo->getNeutronVal1()) {
  //  //G4cout << " flagged: from " << volName << " to " << nextVolName << G4endl;
  //  // we already flagged this, nothing to do further
  //  if (volName != "target1PV" && nextVolName == "target1PV") {
  //    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  //    G4int hId = analysisManager->GetH1Id("spcAtTarget", false);
  //    analysisManager->FillH1(hId, track->GetKineticEnergy());
  //  }
  //  return;
  //}

  //G4bool flag = false;

  //if (theTrack->GetParticleDefinition() == G4Neutron::NeutronDefinition() && theTrack->GetKineticEnergy() < 1000 * keV) {
  //  //flag = true;
  ////}

  //  G4TrackVector* secVec = fpSteppingManager->GetfSecondary();
  //  G4int nSecAtRest = fpSteppingManager->GetfN2ndariesAtRestDoIt();
  //  G4int nSecAlong = fpSteppingManager->GetfN2ndariesAlongStepDoIt();
  //  G4int nSecPost = fpSteppingManager->GetfN2ndariesPostStepDoIt();
  //  G4int nSecTotal = nSecAtRest + nSecAlong + nSecPost;


  //  for (size_t lp1 = (*secVec).size() - nSecTotal; lp1 < (*secVec).size(); lp1++) {
  //    //TrackInformation
  //    G4Track *tmpTrack = (*secVec)[lp1];
  //    TrackInformation *tmpInfo = static_cast<TrackInformation*>(tmpTrack->GetUserInformation());
  //    if (flag) {
  //      tmpInfo->setNeutronVal2(true);
  //    }
  //    //(*secVec)[lp1]->GetUserInformation();
  //    //G4cout << "    : "
  //    //  << G4BestUnit((*secVec)[lp1]->GetPosition(), "Length") << " "
  //    //  << std::setw(9) << G4BestUnit((*secVec)[lp1]->GetKineticEnergy(), "Energy") << " "
  //    //  << std::setw(18) << (*secVec)[lp1]->GetDefinition()->GetParticleName()
  //    //  << " generated by " << (*secVec)[lp1]->GetCreatorProcess()->GetProcessName()
  //    //  << " from parent" << theTrack->GetTrackID() << " " << theTrack->GetParticleDefinition()->GetParticleName()
  //    //  << G4endl;
  //  }
  //}
}
