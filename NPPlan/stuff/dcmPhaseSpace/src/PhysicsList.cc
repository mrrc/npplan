#include "PhysicsList.hh"

#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"

// particles
#include "G4BosonConstructor.hh"
#include "G4LeptonConstructor.hh"
#include "G4MesonConstructor.hh"
#include "G4BosonConstructor.hh"
#include "G4BaryonConstructor.hh"
#include "G4IonConstructor.hh"
#include "G4ShortLivedConstructor.hh"
#include "G4EmStandardPhysics_option3.hh"
#include "G4EmStandardPhysics_option4.hh"

#include "G4HadronElasticProcess.hh"
#include "G4NeutronElasticXS.hh"
#include "G4ChipsElasticModel.hh"
#include "G4ProcessManager.hh"
#include "G4PreCompoundModel.hh"
#include "G4HadronicInteractionRegistry.hh"
#include "G4HadronInelasticProcess.hh"
#include "G4NeutronInelasticXS.hh"
#include "G4BinaryCascade.hh"
#include "G4INCLXXNeutronBuilder.hh"
#include "G4NeutronBuilder.hh"
#include "G4QGSPNeutronBuilder.hh"
#include "G4INCLXXInterface.hh"

#include "G4EmParameters.hh"
#include "G4LossTableManager.hh"
#include "G4hMultipleScattering.hh"
#include "G4hIonisation.hh"
#include "G4hBremsstrahlung.hh"
#include "G4hPairProduction.hh"
#include "G4NuclearStopping.hh"
#include "G4ionIonisation.hh"
#include "G4IonParametrisedLossModel.hh"
#include "G4ePairProduction.hh"
#include "G4eMultipleScattering.hh"
#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#include "G4SeltzerBergerModel.hh"
#include "G4Generator2BS.hh"
#include "G4PhotoElectricEffect.hh"
#include "G4ComptonScattering.hh"
#include "G4LivermorePhotoElectricModel.hh"
#include "G4KleinNishinaModel.hh"
#include "G4GammaConversion.hh"
#include "G4WentzelVIModel.hh"
#include "G4CoulombScattering.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PhysicsList::PhysicsList()
  :G4VModularPhysicsList()
{
  SetVerboseLevel(2);

  //add new units
  //
  new G4UnitDefinition("millielectronVolt", "meV", "Energy", 1.e-3*eV);
  new G4UnitDefinition("mm2/g", "mm2/g", "Surface/Mass", mm2 / g);
  new G4UnitDefinition("um2/mg", "um2/mg", "Surface/Mass", um*um / mg);

  // Neutron Physics
  //RegisterPhysics(new NeutronHPphysics("neutronHP"));

  //RegisterPhysics(new G4EmStandardPhysics_option3());
}

PhysicsList::~PhysicsList()
{ }

void PhysicsList::ConstructParticle()
{
  G4BosonConstructor  pBosonConstructor;
  pBosonConstructor.ConstructParticle();

  G4LeptonConstructor pLeptonConstructor;
  pLeptonConstructor.ConstructParticle();

  G4MesonConstructor pMesonConstructor;
  pMesonConstructor.ConstructParticle();

  G4BaryonConstructor pBaryonConstructor;
  pBaryonConstructor.ConstructParticle();

  G4IonConstructor pIonConstructor;
  pIonConstructor.ConstructParticle();

  // gamma
  G4Gamma::Gamma();

  // leptons
  G4Electron::Electron();
  G4Positron::Positron();

  // barions
  G4Proton::Proton();
  G4Neutron::Neutron();

  // ions
  G4Deuteron::Deuteron();
  G4Triton::Triton();
  G4He3::He3();
  G4Alpha::Alpha();
  G4GenericIon::GenericIonDefinition();

  //G4ShortLivedConstructor pShortLivedConstructor;
  //pShortLivedConstructor.ConstructParticle();
}

void PhysicsList::ConstructProcess() {
  AddTransportation();

  //emPhysicsList->ConstructProcess();

  //for (size_t i = 0; i<hadronPhys.size(); i++) {
  //  hadronPhys[i]->ConstructProcess();
  //}

  //G4EmProcessOptions emOptions;
  //emOptions.SetDEDXBinning(12 * 20);
  //emOptions.SetLambdaBinning(12 * 20);
  //emOptions.SetBuildCSDARange(true);
  //emOptions.SetMaxEnergyForCSDARange(10 * GeV);
  ////G4VUserPhysicsList::ConstructProcess();
  ////myParamReader &paramReader = myParamReader::GetInstance();
  ////G4bool useSensRegion = boost::lexical_cast<G4bool>(paramReader.getPriorityParamAsString("tmCameraV2UseSensRegion", "0"));
  ////if (useSensRegion) {
  ////  AddIonGasModels("tmCameraV2Sens");
  ////}
  //////AddIonGasModels("mylarChamberModel");

  G4HadronElasticProcess* hel = nullptr;
  G4HadronInelasticProcess *had = nullptr;
  G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();

  G4hBremsstrahlung* pb = new G4hBremsstrahlung();
  G4hPairProduction* pp = new G4hPairProduction();
  G4double nielEnergyLimit = /*G4EmParameters::Instance()->MaxNIELEnergy();*/ 1 * MeV;
  G4NuclearStopping* pnuc = new G4NuclearStopping();
  pnuc->SetMaxKinEnergy(nielEnergyLimit);
  G4hMultipleScattering* hmsc = new G4hMultipleScattering("ionmsc");
  G4ePairProduction* ee = new G4ePairProduction();


  auto myParticleIterator = GetParticleIterator();
  myParticleIterator->reset();
  while ((*myParticleIterator)())
  {
    G4ParticleDefinition* particle = myParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    G4String pname = particle->GetParticleName();
    if (pname == "neutron") {
      G4cout << "--==Registering own for neutron" << G4endl;
      hel = new G4HadronElasticProcess();
      hel->AddDataSet(new G4NeutronElasticXS());
      hel->RegisterMe(new G4ChipsElasticModel());
      pmanager->AddDiscreteProcess(hel);


      G4PreCompoundModel* thePreCompound = nullptr;
      G4HadronicInteraction* p =
        G4HadronicInteractionRegistry::Instance()->FindModel("PRECO");
      thePreCompound = static_cast<G4PreCompoundModel*>(p);
      if (!thePreCompound) { thePreCompound = new G4PreCompoundModel(); }
      //thePreCompound->SetMaxEnergy(16 * MeV);
      //G4BinaryCascade* bic = new G4BinaryCascade(thePreCompound);
      //G4INCLXXInterface* incl = new G4INCLXXInterface(thePreCompound);
      //incl->SetMinEnergy(15.5*MeV);
      //bic->SetMinEnergy(15.5*MeV);
      had = new G4HadronInelasticProcess("neutronInelastic", particle);
      had->AddDataSet(new G4NeutronInelasticXS());
      had->RegisterMe(thePreCompound);
      //had->RegisterMe(incl);
      //had->RegisterMe(bic);

      pmanager->AddDiscreteProcess(had);
    }
    else if (pname == "gamma") {
      G4cout << "--==Registering own for gamma" << G4endl;
      G4PhotoElectricEffect* pee = new G4PhotoElectricEffect();
      pee->SetEmModel(new G4LivermorePhotoElectricModel());

      G4ComptonScattering* cs = new G4ComptonScattering();
      cs->SetEmModel(new G4KleinNishinaModel());

      ph->RegisterProcess(pee, particle);
      ph->RegisterProcess(cs, particle);
      ph->RegisterProcess(new G4GammaConversion(), particle);
      //ph->RegisterProcess(new G4RayleighScattering(), particle);
    }
    else if (pname == "proton") {
      G4cout << "--==Registering own for proton" << G4endl;
      G4hMultipleScattering* pmsc = new G4hMultipleScattering();
      G4hIonisation* hIoni = new G4hIonisation();

      ph->RegisterProcess(pmsc, particle);
      ph->RegisterProcess(hIoni, particle);
      ph->RegisterProcess(pb, particle);
      ph->RegisterProcess(pp, particle);
      //ph->RegisterProcess(pnuc, particle);

      /*G4hMultipleScattering* pmsc = new G4hMultipleScattering();
      pmsc->SetEmModel(new G4WentzelVIModel());

      ph->RegisterProcess(new G4hIonisation(), particle);
      ph->RegisterProcess(pb, particle);
      ph->RegisterProcess(pp, particle);
      ph->RegisterProcess(new G4CoulombScattering(), particle);*/
    }
    else if (pname == "GenericIon") {
      G4cout << "--==Registering own for GenericIon" << G4endl;
      G4ionIonisation* ionIoni = new G4ionIonisation();
      ionIoni->SetEmModel(new G4IonParametrisedLossModel());
      ionIoni->SetStepFunction(0.1, 1 * um);

      ph->RegisterProcess(hmsc, particle);
      ph->RegisterProcess(ionIoni, particle);
      //ph->RegisterProcess(pnuc, particle);
    }
    else if (pname == "e-") {
      G4cout << "--==Registering own for e-" << G4endl;
      G4eMultipleScattering* msc = new G4eMultipleScattering();

      G4eIonisation* eIoni = new G4eIonisation();

      G4eBremsstrahlung* brem = new G4eBremsstrahlung();
      G4SeltzerBergerModel* br1 = new G4SeltzerBergerModel();
      br1->SetAngularDistribution(new G4Generator2BS());
      brem->SetEmModel(br1);

      // register processes
      ph->RegisterProcess(msc, particle);
      ph->RegisterProcess(eIoni, particle);
      ph->RegisterProcess(brem, particle);
      ph->RegisterProcess(ee, particle);
    }
    else if (pname == "alpha" ||
      pname == "He3") {
      G4cout << "--==Registering own for alpha && He3" << G4endl;
      G4hMultipleScattering* msc = new G4hMultipleScattering();
      G4ionIonisation* ionIoni = new G4ionIonisation();
      ionIoni->SetStepFunction(0.1, 10 * um);

      ph->RegisterProcess(msc, particle);
      ph->RegisterProcess(ionIoni, particle);
      ph->RegisterProcess(pnuc, particle);
    }
    else if (pname == "deuteron" || pname == "triton") {
      G4cout << "--==Registering own for deuteron and triton" << G4endl;
      ph->RegisterProcess(hmsc, particle);
      ph->RegisterProcess(new G4hIonisation(), particle);
      ph->RegisterProcess(pnuc, particle);
    }
  }

  /*auto neu = new G4NeutronBuilder(false);
  //AddBuilder(neu);
  G4PhysicsBuilderInterface* string = nullptr;
  string = new G4QGSPNeutronBuilder(true);
  string->SetMinEnergy(15.*GeV);
  //AddBuilder(string);
  neu->RegisterMe(string);

  auto inclxxn = new G4INCLXXNeutronBuilder;
  inclxxn->SetMaxEnergy(20.*GeV);
  //AddBuilder(inclxxn);
  neu->RegisterMe(inclxxn);

  inclxxn->UsePreCompound(true);
  inclxxn->SetMinPreCompoundEnergy(0.0*MeV);
  inclxxn->SetMaxPreCompoundEnergy(16.0*MeV);
  inclxxn->SetMinEnergy(1.0*MeV);


  neu->Build();*/



  return;
}

void PhysicsList::SetCuts()
{
  SetCutValue(5 * mm, "proton");
  SetCutValue(5 * mm, "gamma");
  SetCutValue(5 * mm, "e-");
  SetCutValue(5 * mm, "e+");

  DumpCutValuesTable();
}