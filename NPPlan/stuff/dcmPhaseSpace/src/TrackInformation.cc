#include "TrackInformation.hh"
#include "globals.hh"
#include "G4ios.hh"
#include "G4SystemOfUnits.hh"    

G4ThreadLocal G4Allocator<TrackInformation> *
aTrackInformationAllocator = 0;

TrackInformation::TrackInformation() : G4VUserTrackInformation()
{
  neutronVal1 = false;
  neutronVal2 = false;
}

TrackInformation
::TrackInformation(const TrackInformation* aTrackInfo)
  : G4VUserTrackInformation()
{
  /*fOriginalTrackID = aTrackInfo->fOriginalTrackID;
  fParticleDefinition = aTrackInfo->fParticleDefinition;
  fOriginalPosition = aTrackInfo->fOriginalPosition;
  fOriginalMomentum = aTrackInfo->fOriginalMomentum;
  fOriginalEnergy = aTrackInfo->fOriginalEnergy;
  fOriginalTime = aTrackInfo->fOriginalTime;
  fTrackingStatus = aTrackInfo->fTrackingStatus;
  fSourceTrackID = aTrackInfo->fSourceTrackID;
  fSourceDefinition = aTrackInfo->fSourceDefinition;
  fSourcePosition = aTrackInfo->fSourcePosition;
  fSourceMomentum = aTrackInfo->fSourceMomentum;
  fSourceEnergy = aTrackInfo->fSourceEnergy;
  fSourceTime = aTrackInfo->fSourceTime;
  fSuspendedStepID = -1;*/
  neutronVal1 = aTrackInfo->neutronVal1;
  neutronVal2 = aTrackInfo->neutronVal2;
}

TrackInformation& TrackInformation
::operator =(const TrackInformation& aTrackInfo)
{
  /*fOriginalTrackID = aTrackInfo.fOriginalTrackID;
  fParticleDefinition = aTrackInfo.fParticleDefinition;
  fOriginalPosition = aTrackInfo.fOriginalPosition;
  fOriginalMomentum = aTrackInfo.fOriginalMomentum;
  fOriginalEnergy = aTrackInfo.fOriginalEnergy;
  fOriginalTime = aTrackInfo.fOriginalTime;
  fTrackingStatus = aTrackInfo.fTrackingStatus;
  fSourceTrackID = aTrackInfo.fSourceTrackID;
  fSourceDefinition = aTrackInfo.fSourceDefinition;
  fSourcePosition = aTrackInfo.fSourcePosition;
  fSourceMomentum = aTrackInfo.fSourceMomentum;
  fSourceEnergy = aTrackInfo.fSourceEnergy;
  fSourceTime = aTrackInfo.fSourceTime;
  fSuspendedStepID = -1;*/
  neutronVal1 = aTrackInfo.neutronVal1;
  neutronVal2 = aTrackInfo.neutronVal2;

  return *this;
}

TrackInformation::~TrackInformation()
{
}
