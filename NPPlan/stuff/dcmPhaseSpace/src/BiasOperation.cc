#include "BiasOperation.hh"
#include "Randomize.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"

myBiasOperation::myBiasOperation(G4String name) : G4VBiasingOperation(name),
fParticleChange()
{
}

myBiasOperation::~myBiasOperation()
{
}

G4double myBiasOperation::DistanceToApplyOperation(const G4Track *, G4double, G4ForceCondition * condition)
{
  *condition = NotForced;

  return (1./3.)*2 * mm; // 1/3 of size of voxel
  //return 0;
}

G4VParticleChange * myBiasOperation::GenerateBiasingFinalState(const G4Track *track, const G4Step *)
{
  G4double splittingFactor = 2.0;

  G4double initialWeight = track->GetWeight();
  fParticleChange.Initialize(*track);
  G4double weightOfTrack = initialWeight / splittingFactor;
  fParticleChange.ProposeParentWeight(weightOfTrack);
  fParticleChange.SetNumberOfSecondaries(1);
  G4Track* clone = new G4Track(*track);
  clone->SetWeight(weightOfTrack);
  fParticleChange.AddSecondary(clone);
  fParticleChange.SetSecondaryWeightByProcess(true);

  return &fParticleChange;
}
