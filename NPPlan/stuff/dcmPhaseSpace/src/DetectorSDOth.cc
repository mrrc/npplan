#include "DetectorSDOth.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4RunManager.hh"
#include "RunAction.hh"
#include "G4ParticleTypes.hh"
#include "G4IonTable.hh"
#include "G4GenericIon.hh"
#include "NPLayerData.hh"
#include "G4Analyser.hh"
#include "g4root.hh"
#include "G4RunManager.hh"
#include "G4Run.hh"
#include "Run.hh"
#include "G4EmCalculator.hh"
#include "TrackInformation.hh"

DetectorSDOth::DetectorSDOth(G4String name) : G4VSensitiveDetector(name), ownName(name)
{
}

DetectorSDOth::DetectorSDOth(G4String name, G4int x, G4int y, G4int z) : G4VSensitiveDetector(name), ownName(name),
_topX(x), _topY(y), _topZ(z)
{}

DetectorSDOth::~DetectorSDOth()
{
}

void DetectorSDOth::Initialize(G4HCofThisEvent *)
{
}

G4bool DetectorSDOth::ProcessHits(G4Step *aStep, G4TouchableHistory *aHandle)
{

  NPLibrary::NPLayerData::NPVoxelMain *vxData = NPLibrary::NPLayerData::NPVoxelMain::Instance();
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

  NPLibrary::NPLayerData::myIntType vz = static_cast<NPLibrary::NPLayerData::myIntType>(aStep->GetTrack()->GetTouchable()->GetCopyNumber(0));
  NPLibrary::NPLayerData::myIntType vy = static_cast<NPLibrary::NPLayerData::myIntType>(aStep->GetTrack()->GetTouchable()->GetCopyNumber(1));
  NPLibrary::NPLayerData::myIntType vx = static_cast<NPLibrary::NPLayerData::myIntType>(aStep->GetTrack()->GetTouchable()->GetCopyNumber(2));

  G4double edep = aStep->GetTotalEnergyDeposit() * aStep->GetTrack()->GetWeight();

  G4cout << aStep->GetTrack()->GetKineticEnergy() / MeV << " with " << aStep->GetTrack()->GetParticleDefinition()->GetParticleName() << " on " << vz << " " << vy << " " << vx << " wgt: " << aStep->GetTrack()->GetWeight() << "edep with weight: " << edep / MeV << G4endl;
  //vxData->addData(vxData->getNumFromXYZCoord(vx, vy, vz), aStep->GetTotalEnergyDeposit(), aStep->GetStepLength(), aStep->GetTrack()->GetKineticEnergy(), aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessSubType());
  vxData->addData(vxData->getNumFromXYZCoord(vx, vy, vz), edep, aStep->GetStepLength(), aStep->GetTrack()->GetKineticEnergy(), aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessSubType());

  if (aStep->GetTrack()->GetParticleDefinition() == G4Proton::ProtonDefinition() && aStep->GetTrack()->GetParentID() > 0) {
    //vxData->addData(vxData->getNumFromXYZCoord(vx, vy, vz), aStep->GetTotalEnergyDeposit(), aStep->GetStepLength(), aStep->GetTrack()->GetKineticEnergy(), aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessSubType(), "secondaryProtons");
    vxData->addData(vxData->getNumFromXYZCoord(vx, vy, vz), edep, aStep->GetStepLength(), aStep->GetTrack()->GetKineticEnergy(), aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessSubType(), "secondaryProtons");
  }

  if (aStep->GetTotalEnergyDeposit() > 0) {
	  if (vz == (int)(_topZ / 2)) {
		  analysisManager->FillH2(analysisManager->GetH2Id("XYcenterMeV"), vx, vy, edep);
	  }
	  if (vx == (int)(_topX / 2)) {
		  analysisManager->FillH2(analysisManager->GetH2Id("YZcenterMeV"), vy, vz, edep);
	  }
	  if (vy == (int)(_topY / 2)) {
		  analysisManager->FillH2(analysisManager->GetH2Id("XZcenterMeV"), vx, vz, edep);
	  }
  }

  return true;
}

void DetectorSDOth::EndOfEvent(G4HCofThisEvent *)
{
}
