#include "WorkerInitialization.hh"
#include "NPLayerData.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

WorkerInitialization::WorkerInitialization()
  : G4UserWorkerInitialization()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

WorkerInitialization::~WorkerInitialization()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void WorkerInitialization::WorkerInitialize() const
{
  NPLibrary::NPLayerData::NPVoxelMain *vxData = NPLibrary::NPLayerData::NPVoxelMain::Instance();
}

void WorkerInitialization::WorkerRunStart() const
{
  NPLibrary::NPLayerData::NPVoxelMain *vxData = NPLibrary::NPLayerData::NPVoxelMain::Instance();
  vxData->NullifyInThreads();
  //vxData->Nullify();
  G4cout << "Worker Run Start called" << G4endl;
}

void WorkerInitialization::WorkerRunEnd() const
{
  NPLibrary::NPLayerData::NPVoxelMain *vxData = NPLibrary::NPLayerData::NPVoxelMain::Instance();
  vxData->finalize();
  //std::ostringstream os;
  //os << G4Threading::G4GetThreadId();
  //G4cout << "Worker Run End called" << G4endl;
}

void WorkerInitialization::WorkerStop() const
{
  //G4cout << "Worker Stop called" << G4endl;
  //if multiple run/beamOn, this is called after last run
}