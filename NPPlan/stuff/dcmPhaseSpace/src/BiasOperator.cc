#include "BiasOperator.hh"
#include "G4Neutron.hh"
#include "G4BiasingProcessInterface.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"
#include "G4VProcess.hh"
#include "BiasOperation.hh"
#include "G4BOptnCloning.hh"

myBiasOperator::myBiasOperator(G4String particleToBias, G4String name): G4VBiasingOperator(name)
{
  //fParticleToBias = G4Neutron::NeutronDefinition();
  fParticleToBias = G4ParticleTable::GetParticleTable()->FindParticle(particleToBias);
  fMyBiasOperation = new myBiasOperation("splitterFor_neutron");
  //fMyBiasOperation = new G4BOptnCloning("cloning1");
  //fMyBiasOperation->SetCloneWeights(0.5, 0.5);
}

myBiasOperator::~myBiasOperator()
{
}

void myBiasOperator::StartRun()
{
}

G4VBiasingOperation * myBiasOperator::ProposeNonPhysicsBiasingOperation(const G4Track *track, const G4BiasingProcessInterface *)
{
  if (track->GetDefinition() != fParticleToBias) return 0;

  return fMyBiasOperation;
}
