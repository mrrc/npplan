// global includes
#include <ctime>
#if defined (WIN32)
#define NOMINMAX 1
#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#else
#include <signal.h>
#endif

// Geant4 includes
#include "globals.hh"
#include "DetectorConstruction.hh"
#include "ActionInitialization.hh"
#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif
#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif
#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#include "G4UImanager.hh"
#endif

#include "G4PhysListFactory.hh"
#include "PhysicsList.hh"
#include "PhysicsList2.hh"
#include "G4EmStandardPhysics_option3.hh"
#include "G4GenericBiasingPhysics.hh"

// NPLibrary include
#include "NPParamReader.hh"

#include "WorkerInitialization.hh"

int main(int argc, char** argv) {
  clock_t beginAt = clock();
  G4cerr << "Main started" << G4endl;

  // read params stuff
  NPLibrary::NPParamReaderMain *cmdParam = NPLibrary::NPParamReaderMain::Instance();
  cmdParam->setVerbose(2);
  cmdParam->setCmdInput(argc, argv);
  cmdParam->fillAppJson();

  G4cerr << "ParamReader inited" << G4endl;

#ifdef G4MULTITHREADED
  G4int nThreads = cmdParam->getPriorityParamAsCppInt("nThreads", "0");
#endif

  CLHEP::Ranlux64Engine defaultEngine(1234567, 4);
  G4Random::setTheEngine(&defaultEngine);
  int seedType = cmdParam->getRESeedType();
  G4long seed;
  if (1 == seedType) {
    seed = time(NULL);
    std::cout << "RE seed set to: " << seed << std::endl;
  }
  else if (0 == seedType) {
    seed = 1029384756;
  }
  else {
    seed = boost::lexical_cast<G4long>(cmdParam->getPriorityParamAsG4String("RESeedValue", "123654"));
  }
  G4Random::setTheSeed(seed);


#ifdef G4MULTITHREADED
  G4MTRunManager * runManager = new G4MTRunManager;
  if (nThreads > 0) {
    runManager->SetNumberOfThreads(nThreads);
  }
#else
  G4RunManager * runManager = new G4RunManager;
#endif

  DetectorConstruction* fdet = new DetectorConstruction();
  runManager->SetUserInitialization(fdet);

#ifdef G4MULTITHREADED
  runManager->SetUserInitialization(new WorkerInitialization());
#endif

  //G4VUserPhysicsList* physicsList = 0;
  G4VModularPhysicsList* physicsList = nullptr;
  if (cmdParam->getIsBasePhysics()) {
    G4PhysListFactory factory;
    G4VModularPhysicsList* phys = nullptr;
    //phys = factory.GetReferencePhysList("QGSP_INCLXX");
    phys = factory.GetReferencePhysList("QGSP_BIC_HP");
    phys->ReplacePhysics(new G4EmStandardPhysics_option3());
    physicsList = phys;
  }
  else if (cmdParam->getPhysNum() == 1) {
    physicsList = new PhysicsList();
  }
  else {
    physicsList = new PhysicsList2();
  }

  //G4GenericBiasingPhysics* biasingPhysics = new G4GenericBiasingPhysics();
  ////biasingPhysics->Bias("gamma");
  //biasingPhysics->Bias("neutron");
  //physicsList->RegisterPhysics(biasingPhysics);

  //G4GenericBiasingPhysics* biasingPhysics = new G4GenericBiasingPhysics();
  //std::vector< G4String > neutronProcessesToBias;
  //neutronProcessesToBias.push_back("neutronInelastic");
  //biasingPhysics->PhysicsBias("neutron", neutronProcessesToBias);
  //physicsList->RegisterPhysics(biasingPhysics);

  G4GenericBiasingPhysics* biasingPhysics = new G4GenericBiasingPhysics();
  biasingPhysics->BeVerbose();
  biasingPhysics->Bias("neutron");
  physicsList->RegisterPhysics(biasingPhysics);

  runManager->SetUserInitialization(physicsList);
  G4cerr << "physList initialized" << G4endl;

  ActionInitialization* AI = new ActionInitialization(fdet);
  runManager->SetUserInitialization(AI);
  G4cerr << "AI initialized" << G4endl;

  runManager->Initialize();

  G4cout << "Vis_Use " << G4VIS_USE << G4endl;

  G4UImanager* UImanager = G4UImanager::GetUIpointer();

#ifdef G4VIS_USE
  G4VisManager* visManager;
  //if (paramReader.isUseUI()) {  // @todo: check this case
  visManager = new G4VisExecutive;
  visManager->Initialize();
  //}
#endif

  if (cmdParam->isVisUse()) {
#ifdef G4UI_USE
    G4UIExecutive* ui = new G4UIExecutive(argc, argv);
#ifdef G4VIS_USE
    UImanager->ApplyCommand("/control/execute " + cmdParam->getInputMacro());
#else
    // @todo: case if no vis use but -u presented at cmd
    UImanager->ApplyCommand("/control/execute init.mac");
#endif
    ui->SessionStart();
    delete ui;
#endif
  }
  else  if (cmdParam->getInputMacro().size()) {
    // batch mode
    G4String command = "/control/execute ";
    UImanager->ApplyCommand(command + cmdParam->getInputMacro());
  }

#ifdef G4VIS_USE
  delete visManager;
#endif
  delete runManager;


  G4cerr << "Total time: " << (clock() - beginAt) / CLOCKS_PER_SEC << G4endl;
  return 0;
}