__author__ = '_mrxak'

from multiprocessing import Process, Queue, current_process
import time
import random

class waiter(object):
    def __init__(self, num):
        while True:
            t = random.randint(1, 5)
            print 'Waiter %d will sleep for %d\n' %(num, t)
            time.sleep(t)
            print 'Waiter %d waken up after %d\n' %(num, t)

def makeWaiter(func):
    wtr = waiter(func)

def worker(inputQueue, outputQueue):
    for func in iter(inputQueue.get, 'STOP'):
        result = makeWaiter(func)
        outputQueue.put(result)

def main():
    # Create queues
    task_queue = Queue()
    done_queue = Queue()

    #submit tasks
    for i in range(10):
        task_queue.put(i)

    #Start worker processes
    for i in range(10):
        Process(target=worker, args=(task_queue, done_queue)).start()

        # Get and print results
    print 'Unordered results:'
    for i in range(10):
        print '\t', done_queue.get().query

    # Tell child processes to stop
    for i in range(10):
        task_queue.put('STOP')
        print "Stopping Process #%s" % i

if __name__ == "__main__":
    main()