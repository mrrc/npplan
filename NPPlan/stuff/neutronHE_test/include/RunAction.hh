#ifndef RunAction_h
#define RunAction_h 1

#include "G4UserRunAction.hh"
#include "G4ParticleDefinition.hh"
#include "globals.hh"
#include <map>
#include <vector>
#include <ctime>
#include "layerData.hh"


struct SlayerRA
{
	G4double depEnergy, depEnergy2, depEnergyError, dose, dose2, doseError;
	G4int nEvents;
  G4double cellFlux, cellFlux2;
  SlayerRA();
};

typedef std::map<G4String, SlayerRA*> parte;
typedef std::pair<G4String, G4String> ppr;
typedef std::pair<G4String, G4ParticleDefinition*> detPartType;
typedef std::vector<G4ParticleDefinition*> pdvec;

class G4Run;
class RunAction : public G4UserRunAction
{
  public:
    RunAction();
    RunAction(bool);
    virtual ~RunAction();

    virtual void BeginOfRunAction(const G4Run* run);
    virtual void   EndOfRunAction(const G4Run* run);
    void addDetData(G4String, G4double, G4double, G4double, G4double, G4int, G4double);
    void addLayeredDetData(G4String, G4int, G4double, G4double, G4double, G4double, G4int, G4double);
    bool checkHasBestSolution();
    void addDetParticleData(G4String, G4String, G4double, G4double, G4double, G4double, G4int, G4double);

    //void addDetData(G4String, detLayer);
    //void addDetData(G4String, G4ParticleDefinition*, detLayer);

    void addLayeredDetData(G4String, detLayer*);
    void addLayeredDetData(G4String, G4ParticleDefinition*, detLayer*);

    bool checkDetListParticles(G4String, G4int);
    void updateDetListParticles(G4String, pdvec);

  private:

    bool useContinue;

    std::map<G4String, SlayerRA> depToDets;
    std::map<G4String, SlayerRA*> layerDepToDets;
    SlayerRA makeZero(SlayerRA);

    SlayerRA prepareError(SlayerRA target);
    void prepareErrorS(SlayerRA *target);
    void dumpDep(SlayerRA target, G4String filename);
    void dumpDep(G4String particle, SlayerRA *target, G4String filename);

    void dumpStruct2File(SlayerRA, G4String);
    void dumpStruct2File(SlayerRA*, G4String);
    SlayerRA readStructFromFile(SlayerRA, G4String);
    void readStructParticlesFromFile(G4String, G4String);
    void dumpList(std::vector<ppr>);
    void readList();


    G4int previousRuns;
    void dumpNoE();
    void readNoE();

    std::map<ppr, SlayerRA*> dets2Energies;

    std::map<G4String, resLayer*> detLayers;
    std::map<detPartType, resLayer*> detPartLayers;

    // list of all particles registered per each detector
    std::map<G4String, pdvec> allParticlesList;

	  clock_t beginAt;
	  clock_t finishedAt;
};

#endif