#include "DetectorSDSingle.hh"

#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4RunManager.hh"
#include "RunAction.hh"

#include "G4ParticleTypes.hh"


DetectorSDSingle::DetectorSDSingle(G4String name) : G4VSensitiveDetector(name)
{
  runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
  outFileNameAll = name;
  setupSummator();
  _givenVolume = 0.0*cm3;
}

DetectorSDSingle::DetectorSDSingle(G4String name, G4double _vol) : G4VSensitiveDetector(name), _givenVolume(_vol)
{
  runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
  outFileNameAll = name;
  setupSummator();
}

void DetectorSDSingle::setupSummator() {
  depEnergy = depEnergy2 = depEnergyError = dose = dose2 = doseError = 0.0;
  nEvents = 0;
  cellFlux = cellFlux2 = 0.0;
}

DetectorSDSingle::~DetectorSDSingle() {

}

void DetectorSDSingle::Initialize(G4HCofThisEvent*) {
  setupSummator();
}

G4bool DetectorSDSingle::ProcessHits(G4Step* step, G4TouchableHistory* aHistory) {
  G4double density = step->GetPreStepPoint()->GetPhysicalVolume()->GetLogicalVolume()->GetMaterial()->GetDensity();
  //G4double density = 1 * g / (cm * cm * cm);
  //G4double volume = 1 * cm * cm * cm;
  //G4double volume = (0.62/4.0)*(0.62/4.0)*(0.62/4.0) * 4.0 / 3.0 * 3.1415;
  //G4double volume = 4.0 / 3.0 * 3.1415 * sphDiam*sphDiam*sphDiam;
  G4double volume;
  if ((0.0*cm3) == _givenVolume) {
    /*if (outFileNameAll[0] != 'o') {
      volume = 10*cm*10*cm*10*cm;
    } else {
       volume = 50*cm*50*cm*50*cm;
    }*/
    // simple case, won't work with voxel volumes
    volume = step->GetPreStepPoint()->GetPhysicalVolume()->GetLogicalVolume()->GetSolid()->GetCubicVolume();
  } else {
    volume = _givenVolume;
  }
  //   G4int idx = ((G4TouchableHistory*)
	       //(aStep->GetPreStepPoint()->GetTouchable()))
           //    ->GetReplicaNumber(indexDepth);
  //G4double cubicVolume = ComputeVolume(aStep, idx);
  // G4VPhysicalVolume* physVol = aStep->GetPreStepPoint()->GetPhysicalVolume();
  // solid = physVol->GetLogicalVolume()->GetSolid();
  // return solid->GetCubicVolume();
  G4double stepLength = step->GetStepLength();

  G4double edep = step->GetTotalEnergyDeposit();
  //G4cout<<edep;
  edep *= step->GetPreStepPoint()->GetWeight();
  /*if (edep <= 0.0) {
    return false;
  }*/

	G4StepPoint* prePoint = step->GetPreStepPoint();
	G4TouchableHandle touchable = prePoint->GetTouchableHandle();
	G4String name = step->GetTrack()->GetDefinition()->GetParticleName();
	G4int parentTrackId = step->GetTrack()->GetParentID();  

  G4double voxelMass, _dose;

  cellFlux += stepLength / volume;
  if (edep >= 0.0) {
    cellFlux2 += stepLength / volume;
  }

  voxelMass = density*volume;
  //voxelMass = 1*g;
  _dose=edep / voxelMass;

  dose += _dose;
  dose2 += _dose*_dose;
  depEnergy += edep;
  depEnergy2 += edep*edep;

  nEvents++;
  return true;
}
void DetectorSDSingle::EndOfEvent(G4HCofThisEvent*) {
  //G4cout<<G4BestUnit(depEnergy, "Energy")<<" "<<G4BestUnit(dose, "Dose")<<" "<<nEvents<<G4endl;
  //runAction->addSingleData(depEnergy, depEnergy2, dose, dose2, nEvents, cellFlux, cellFlux2);
  if (0.0 != depEnergy) {
    runAction->addDetData(outFileNameAll, depEnergy, depEnergy2, dose, dose2, nEvents, cellFlux);
  }
}