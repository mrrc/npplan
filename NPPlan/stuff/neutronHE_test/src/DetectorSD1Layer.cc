#include "DetectorSD1Layer.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4RunManager.hh"
#include "RunAction.hh"
#include "G4ParticleTypes.hh"
#include "G4IonTable.hh"
#include "G4GenericIon.hh"
#include "layerData.hh"

DetectorSD1Layer::DetectorSD1Layer(G4String name, G4int _nov, G4int _target): G4VSensitiveDetector(name), outName(name), nov(_nov), targetLayer(_target),
  preVol(0.0)
{
  data = new detLayer[_nov];
  for (int i = 0; i < nov; i++) {
    data[i].Nullify();
  }
  runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
}

DetectorSD1Layer::DetectorSD1Layer(G4String name, G4int _nov): G4VSensitiveDetector(name), outName(name), nov(_nov), targetLayer(1),
  preVol(0.0)
{
  data = new detLayer[_nov];
  for (int i = 0; i < nov; i++) {
    data[i].Nullify();
  }
  runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
}

DetectorSD1Layer::DetectorSD1Layer(G4String name, G4int _nov, G4double _vol): G4VSensitiveDetector(name), outName(name), nov(_nov), targetLayer(1),
  preVol(_vol)
{
  data = new detLayer[_nov];
  for (int i = 0; i < nov; i++) {
    data[i].Nullify();
  }
  runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
}

DetectorSD1Layer::DetectorSD1Layer(G4String name, G4int _nov, G4int _target, G4double _vol): G4VSensitiveDetector(name), outName(name), nov(_nov), targetLayer(_target),
  preVol(_vol)
{
  data = new detLayer[_nov];
  for (int i = 0; i < nov; i++) {
    data[i].Nullify();
  }
  runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
}

DetectorSD1Layer::~DetectorSD1Layer() {
}

void DetectorSD1Layer::Initialize(G4HCofThisEvent*) {
  for (int i = 0; i < nov; i++) {
    data[i].Nullify();
  }
  
  // @todo: optimize
  for (std::map<G4ParticleDefinition*, detLayer*>::iterator it = partedData.begin();
      it != partedData.end();
		  ++it)
  {
    for (int i = 0; i < nov; i++) {
      it->second[i].Nullify();
    }
  }
}

void DetectorSD1Layer::addParticle(G4String particleName) {
  G4ParticleDefinition* pd = G4ParticleTable::GetParticleTable()->FindParticle(particleName);
  if(!pd) {
    G4String msg = "Particle <";
    msg += particleName;
    msg += "> not found.";
    G4Exception("G4SD1Layer::G4SD1Layer",
                "addParticle",FatalException,msg);
  }
  detLayer *_d = new detLayer[nov];
  for (int i = 0; i < nov; i++) {
    _d[i].Nullify();
  }
  partedData.insert(std::pair<G4ParticleDefinition*, detLayer*>(pd, _d));
}

void DetectorSD1Layer::addIon(G4int Z, G4int A) {
//G4ParticleDefinition* pd = G4ParticleTable::GetParticleTable()->FindParticle(particleName);
  //G4ParticleDefinition* pd = G4ParticleTable::GetParticleTable()->GetIonTable()->GetIon(Z, A);

  G4ParticleDefinition* pd = G4ParticleTable::GetParticleTable()->GetIonTable()->GetIon(Z, A, 0.);
  if(!pd) {
    G4String msg = "Particle <";
    msg += std::to_string(Z);
    msg += " ";
    msg += std::to_string(A);
    msg += "> not found.";
    G4Exception("G4SD1Layer::G4SD1Layer",
                "addIon",FatalException,msg);
  }
  detLayer *_d = new detLayer[nov];
  for (int i = 0; i < nov; i++) {
    _d[i].Nullify();
  }
  partedData.insert(std::pair<G4ParticleDefinition*, detLayer*>(pd, _d));
}

G4bool DetectorSD1Layer::ProcessHits(G4Step* step, G4TouchableHistory*) {
	G4StepPoint* prePoint = step->GetPreStepPoint();
	G4TouchableHandle touchable = prePoint->GetTouchableHandle();
  G4double density = step->GetPreStepPoint()->GetPhysicalVolume()->GetLogicalVolume()->GetMaterial()->GetDensity();

  G4int tlr = touchable->GetCopyNumber(targetLayer);

	/*G4cout<<" "<<touchable->GetCopyNumber(0)<<
		" "<<touchable->GetCopyNumber(1)<<
		" "<<touchable->GetCopyNumber(2)<<
		" "<<touchable->GetCopyNumber(3)
		<<"  energy: "<<step->GetTotalEnergyDeposit()
		<<G4endl;*/
  G4double stepLength = step->GetStepLength();
  G4double volume;
  if (0.0 != preVol) {
    //volume = 10*cm*10*cm*50*cm/500;
    volume = preVol;
  } else {
    G4int idx = ((G4TouchableHistory*)
	       (step->GetPreStepPoint()->GetTouchable()))
               ->GetReplicaNumber(tlr);
  //G4double cubicVolume = ComputeVolume(step, idx);
    volume = ComputeVolume(step, idx);
  }
  G4double edep = step->GetTotalEnergyDeposit();
  G4double edep2 = edep*edep;
  G4double voxelMass = density*volume;
  G4double dose = edep / voxelMass;
  G4double dose2 = dose*dose;
  G4double cf = stepLength / volume;

  data[tlr].Update(edep, dose, cf);

  G4ParticleDefinition *pdNow = step->GetTrack()->GetDefinition();
  if (std::find(allParticlesList.begin(), allParticlesList.end(), pdNow) != allParticlesList.end()) {
  } else {
    allParticlesList.push_back(pdNow);
  }
  if (0 != partedData.count(pdNow)) {
    partedData.find(pdNow)->second[tlr].Update(edep, dose, cf);
  }

  return true;
}

void DetectorSD1Layer::EndOfEvent(G4HCofThisEvent*) {
  for (int i = 0; i < nov; i++) {
    //runAction->addLayeredDetData(outName, i, data[i].depEnergy, data[i].depEnergy2, data[i].dose, data[i].dose2, data[i].nEvents, data[i].cellFlux);
    runAction->addLayeredDetData(outName, data);
  }
    for (std::map<G4ParticleDefinition*, detLayer*>::iterator it = partedData.begin();
      it != partedData.end();
		  ++it)
  {
    runAction->addLayeredDetData(outName, it->first, it->second);
  }

  if (!runAction->checkDetListParticles(outName, allParticlesList.size())) {
    runAction->updateDetListParticles(outName, allParticlesList);
  }
}

G4double DetectorSD1Layer::ComputeVolume(G4Step* aStep, G4int idx){
  G4VPhysicalVolume* physVol = aStep->GetPreStepPoint()->GetPhysicalVolume();
  G4VPVParameterisation* physParam = physVol->GetParameterisation();
  G4VSolid* solid = 0;
  if(physParam)
  { // for parameterized volume
    if(idx<0)
    {
      G4ExceptionDescription ED;
      ED << "Incorrect replica number --- GetReplicaNumber : " << idx << G4endl;
      G4Exception("DetectorSD1Layer::ComputeVolume","Detector",JustWarning,ED);
    }
    solid = physParam->ComputeSolid(idx, physVol);
    solid->ComputeDimensions(physParam,idx,physVol);
  }
  else
  { // for ordinary volume
    solid = physVol->GetLogicalVolume()->GetSolid();
  }
  
  return solid->GetCubicVolume();
}