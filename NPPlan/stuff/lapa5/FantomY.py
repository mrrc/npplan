# -*- coding: cp1251 -*-

from pylab import *
from matplotlib.mlab import griddata
import matplotlib.pyplot as plt
import matplotlib.image as image
import numpy as np
import matplotlib
import Image
import string
import re

rMax = 5
def readMix():
	# ������ mix-�����
	# rMax - ������������ �������� � mix-�����
	global rMax
	fl = open("56Mix20Pct.mix", "r")

	lines = []
	i = 0
	for line in fl.xreadlines():
		lines.append(line.strip())
		i = i + 1
	fl.close()
	mixes = []
	for q in lines:
		r = q.split()
		if (len(r) != 5):
			continue
		if (r[0] == 'AIR'):
			continue
		mixes.append(r)
	for q in range(len(mixes)):
		for k in range(4):
			mixes[q][k] = float(mixes[q][k]) / rMax
	return mixes

def retAvMix():
	# ����������� ����� (���������� ����� � ������� ������ �������
	global mixes
	rNums = []
#	print mixes
	for i in mixes:
		if i[0] > 0 or i[1] > 0:
			continue
		else:
			rNums.append(int(i[4]))
	return rNums

def readOt(fname):
	# ������ mcnp-���� ����
	global qX, qY, qZ
	fl = open(fname, "r")
	lines = []
	st = 0
	while 1:
		line=fl.readline()
		if string.find(line, "c end table materials")!=-1:
			break
		if string.find(line, "c")==0:
			continue
		if string.find(line,"lat=1 fill=")!=-1:
			st = 1
#			pat = '\W+'
#			q = re.split(pat, line)
#			print q
			q = line.split()
			print q
			qX = [int(q[2]), int(q[4])]   # ��������� ������� �������
			qY = [int(q[5]), int(q[7])]
			qZ = [int(q[8]), int(q[10])]
			continue
#		if (st == 1):
#			lines.append(line.strip())

	fl.close()
	mats = []
	for i in lines:
		for j in i.split():
			print j
			if string.find(j, "r"):
				break
			mats.append(int(j))   # ��������� ���������
	print qX
	print qY
	print qZ
	return {'mats' : mats, 'qX' : qX, 'qY' : qY, 'qZ' : qZ}

def readOtm(fname):
	# ������ m-����
#	global qX, qY, qZ
#	global doses2
	fl = open(fname, "r")
	lines = []
	lines2 = []
	uvals = 0
	while 1:
		line=fl.readline()
		if (len(line.strip())<1):
			break
		if (line == "vals\n" and uvals==0):
			uvals = 1   # �������� ������ �������� �����
			continue
		if (line == "vals\n" and uvals==2):
			uvals = 3  # �������� ������ �������� �����
			continue
		if (uvals == 3 and line[0] == 't'):
#			uvals = 2   # ��������� ����� 2� ��������
			break;
		if (uvals == 1 and line[0] == 't'):
			uvals = 2  # ��������� ����� 1� ��������
#			break;
                if (uvals == 1):
			lines.append(line.strip())
		if (uvals == 3):
			lines2.append(line.strip())
	fl.close()
	doses = []
	doses2 = []
	# ��������� ����
	for i in lines:
		cc = 0
		for j in i.split():
			if cc % 2 == 0:
				doses.append(float(j))
			cc = cc + 1
	for i in lines2:
		cc = 0
		for j in i.split():
			if cc % 2 == 0:
				doses2.append(float(j))
			cc = cc + 1
	kmax = 0
	for i in range(len(doses)):
		if (doses[i] > doses[kmax]):
			kmax = i
	global max
	max = doses[kmax]
	print max
	for i in range(len(doses)):
		doses[i] = doses[i] / max
	return [doses, doses2]

# vozvrashyaet griddata iz materialov nmats v sreze k
        
def getGridData(mats, doses, i):
#	global qX, qY, qZ
	qX = mats['qX'][1] - mats['qX'][0] + 1   # ������� ������� �������
	qY = mats['qY'][1] - mats['qY'][0] + 1
	qZ = mats['qZ'][1] - mats['qZ'][0] + 1	
	print qX	
	global x, y, z
	XN = 10.   # ���������� ������� ������� ������� (�� ������� ������������� ������� (�� -XN/2 �� +XN/2))
	YN = 10.
	ZN = 10.
	x = []                                                     
	y = []                                                     
	z = []                                                     
#	rNums = retAvMix()                                         
#	rNums.append(60)                                           
# 	print rNums
	for k in range(qZ):
		for j in range(qY):
			rI = (k)*qX*qY + qX*(j) + (i)  # index
			# ������� ����������
			x.append((k+0.5)*ZN/qZ-ZN/2.)  # 0.5 chtobi bilo poseredine yacheiki
			y.append((j+0.5)*YN/qY)
			z.append(doses[rI])	# ��������������� ����
	xi = np.linspace(-ZN/2.,ZN/2.,50)   # �������� ����
	yi = np.linspace(0.,YN,50)
	x = np.array(x)
	y = np.array(y)
	z = np.array(z)
	zi = griddata(x, y, z, xi, yi)
	return {'x': x, 'y' : y, 'z' : z, 'xi' : xi, 'yi' : yi, 'zi' : zi}

def showContour():
# srfct1n  slice 4: ����� ������ ������� �������
	x = [2.09,  1.62,   0.74,   0.29,   -0.74, -1.95, -2,     -1.35, 0,  1.296, 1.94,  2.09  ]
	y = [0.144, 1.3165, 1.9617, 2.0801, 1.896, 0.75,  -0.184, -1.56, -2, -1.63, -0.76, 0.144 ]
	
	return {'x': x, 'y' : y}

# ������ �������
# mats - array() - ������ ���������� (��� �����)
# dos - array() - ������ ��� (��� �����)
# k - array() - ������ ������ ��� ������� ����� (������� � ������������ � �����������/������)
			
def showImage(mats, dos, k):
	global colors
	global max
	plt.figure()
	for q in range(len(dos)):
		doses = dos[q][0]  # ������ ����
		doses2 = dos[q][1] # ������ ����
		print 'd: '+str(len(doses))
		nM = getGridData(mats[q], doses, k[q])   # ���������� griddata
		plt.title('(YZ %d  max %1.3e)' % (k[q], max)) # ���������
		CS = plt.contour(nM['xi'],nM['yi'],nM['zi'],10,linewidths=1.0,colors=colors[q])  # �������
		plt.clabel(CS, fontsize=10, inline=1,fmt='%0.2f')  # ������� � ���������
		CS = plt.contourf(nM['xi'],nM['yi'],nM['zi'],10,cmap=plt.cm.jet)
		CB = plt.colorbar(CS, shrink=0.8, extend='both')  # ��������
	plt.show()

import sys
list = sys.argv[1:] # ��������� ��������� ������
# 0(1)� �������� - ����������, / ���� � �������
# ��������� ��������� - ����� ������
#print list
mats = []
doses = []
if (list[0] == ''):
	exit
for q in range(len(list)):
	if (q == 0):
		continue
	if (list[0] == '/'):
		list[0] = ''
	mats.append(readOt(list[0]+list[q]))
#	print mats
#	print len(mats[q])
	doses.append(readOtm(list[0]+list[q]+'m'))
mixes = readMix()
# available values
#nmats = [55, 11, 6, 4, 18, 20, 21, 45, 56]
#print doses
#print len(doses)

#print qX, qY, qZ
#nM = getGridData(mats, doses, 0, nmats)
#print nM
# ����� ������, � ������������ � ��������
colors = ['#000000', '#0000ff', '#00ff00']
showImage(mats, doses, [6])

