void CML2ExpVoxels::calculateNormalizedEd(std::vector <Svoxel> &vox)
{
	int i,j;
	G4double cs, cc;
	int n;
	G4double d2, dd;
        G4double v;
	for (j=0;j<nCurves;j++)
	{
		cs=cc=0.;
		for (i=startCurve[j]-1;i<stopCurve[j];i++)
		{
			cs+=vox[i].depEnergy*vox[i].expDose;
			cc+=vox[i].depEnergy*vox[i].depEnergy;
		}
		if (cc>0.)
		{
			chi2Factor[j]=cs/cc; 
		}
		for (i=startCurve[j]-1;i<stopCurve[j];i++)
		{
			dd=vox[i].depEnergy*vox[i].depEnergy;
			d2=vox[i].depEnergy2;
			n=vox[i].nEvents;
			vox[i].depEnergyNorm=chi2Factor[j]*vox[i].depEnergy;
			v=n*d2-dd;
			if (v<0.){v=0;}
			if (n>1){vox[i].depEnergyNormError=chi2Factor[j]*std::sqrt(v/(n-1));}
			if (n==1){vox[i].depEnergyNormError=vox[i].depEnergyNorm;}
		}
	}
}