#include "DetectorSD.hh"
#include "RunAction.hh"
#include "DetectorConstruction.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4RunManager.hh"

#include "G4ParticleTypes.hh"


DetectorSD::DetectorSD(G4String name, G4ThreeVector voxelSize, G4int vq) : G4VSensitiveDetector(name),
  voxelSizeX(voxelSize.x()), voxelSizeY(voxelSize.y()), voxelSizeZ(voxelSize.z()), voxelQuantity(vq)
{
  /*outFileNameAll = "R:\\"+name+"_all.out";
  outFileNameProtons = "R:\\"+name+"_protons.out";*/
  runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
  outFileNameAll = name+"_all.out";
  outFileNameProtons = name+"_protons.out";
  setupSummator();
}

DetectorSD::~DetectorSD()
{
  
}

void DetectorSD::setupSummator() {
  depAll = new Slayer**[voxelQuantity];
  depProtons = new Slayer**[voxelQuantity];
  for(int ix = 0; ix < voxelQuantity; ix++) {
    depAll[ix] = new Slayer*[voxelQuantity];
    depProtons[ix] = new Slayer*[voxelQuantity];
    for(int iy = 0; iy < voxelQuantity; iy++) {
      depAll[ix][iy] = new Slayer[voxelQuantity];
      depProtons[ix][iy] = new Slayer[voxelQuantity];
      /*for(int iz = 0; iz < voxelQuantity; iz++) {
        depAll[ix][iy][iz].depEnergy = 0;
        depAll[ix][iy][iz].depEnergy2 = 0;
        depAll[ix][iy][iz].depEnergyError = 0;
        depAll[ix][iy][iz].nEvents = 0;
        depAll[ix][iy][iz].dose = 0;
        depAll[ix][iy][iz].dose2 = 0;
        depAll[ix][iy][iz].doseError = 0;

        depProtons[ix][iy][iz].depEnergy = 0;
        depProtons[ix][iy][iz].depEnergy2 = 0;
        depProtons[ix][iy][iz].depEnergyError = 0;
        depProtons[ix][iy][iz].nEvents = 0;
        depProtons[ix][iy][iz].dose = 0;
        depProtons[ix][iy][iz].dose2 = 0;
        depProtons[ix][iy][iz].doseError = 0;
      }*/
    }
  }
  zeroSummator();
}

void DetectorSD::zeroSummator() {
   for(int ix = 0; ix < voxelQuantity; ix++) {
    for(int iy = 0; iy < voxelQuantity; iy++) {
      for(int iz = 0; iz < voxelQuantity; iz++) {
        depAll[ix][iy][iz].depEnergy = 0;
        depAll[ix][iy][iz].depEnergy2 = 0;
        depAll[ix][iy][iz].depEnergyError = 0;
        depAll[ix][iy][iz].nEvents = 0;
        depAll[ix][iy][iz].dose = 0;
        depAll[ix][iy][iz].dose2 = 0;
        depAll[ix][iy][iz].doseError = 0;

        for (std::map<G4String, Slayer***>::iterator it = depFParticles.begin();
				it != depFParticles.end();
				++it) {
          it->second[ix][iy][iz].depEnergy = 0;
          it->second[ix][iy][iz].depEnergy2 = 0;
          it->second[ix][iy][iz].depEnergyError = 0;
          it->second[ix][iy][iz].nEvents = 0;
          it->second[ix][iy][iz].dose = 0;
          it->second[ix][iy][iz].dose2 = 0;
          it->second[ix][iy][iz].doseError = 0;
        }

        /*depProtons[ix][iy][iz].depEnergy = 0;
        depProtons[ix][iy][iz].depEnergy2 = 0;
        depProtons[ix][iy][iz].depEnergyError = 0;
        depProtons[ix][iy][iz].nEvents = 0;
        depProtons[ix][iy][iz].dose = 0;
        depProtons[ix][iy][iz].dose2 = 0;
        depProtons[ix][iy][iz].doseError = 0;*/
      }
    }
  }
}

void DetectorSD::Initialize(G4HCofThisEvent*) 
{
  zeroSummator();
}

void DetectorSD::addData(Slayer*** target, G4int x, G4int y, G4int z, G4double ene, G4double dose) {
  target[x][y][z].nEvents += 1;
  target[x][y][z].depEnergy += ene;
  target[x][y][z].depEnergy2 += ene*ene;
  target[x][y][z].dose += dose;
  target[x][y][z].dose2 += dose*dose;
}

bool DetectorSD::addParticle2Store(G4String newParticle) {
  if (0 !=depFParticles.count(newParticle)) {
    return false;
  }
  Slayer ***t = new Slayer**[voxelQuantity];
  for(int ix = 0; ix < voxelQuantity; ix++) {
    t[ix] = new Slayer*[voxelQuantity];
    for(int iy = 0; iy < voxelQuantity; iy++) {
      t[ix][iy] = new Slayer[voxelQuantity];
      for(int iz = 0; iz < voxelQuantity; iz++) {
        t[ix][iy][iz].depEnergy = 0.0;
        t[ix][iy][iz].depEnergy2 = 0.0;
        t[ix][iy][iz].depEnergyError = 0.0;
        t[ix][iy][iz].dose = 0.0;
        t[ix][iy][iz].dose2 = 0.0;
        t[ix][iy][iz].doseError = 0.0;
        t[ix][iy][iz].nEvents = 0;
      }
    }
  }
  depFParticles.insert(std::pair<G4String, Slayer***>(newParticle, t));
/*	if (obj.count(pName) == 0) {
		obj.insert(std::pair<std::string, G4double>(pName, value));
	} else {
		obj.find(pName)->second += value;
	}
	return obj;*/
  return true;
}

void DetectorSD::addParticlesData(G4String pName, G4int x, G4int y, G4int z, G4double ene, G4double dose) {
  addData(depFParticles.find(pName)->second, x, y, z, ene, dose);
}

G4bool DetectorSD::ProcessHits(G4Step* step, G4TouchableHistory* dTouchable) 
{
  G4double edep = step->GetTotalEnergyDeposit();
  if (edep <= 0.0) {
    return false;
  }

	G4StepPoint* prePoint = step->GetPreStepPoint();
	G4TouchableHandle touchable = prePoint->GetTouchableHandle();
	G4String name = step->GetTrack()->GetDefinition()->GetParticleName();
	G4int parentTrackId = step->GetTrack()->GetParentID();  
  

  G4double density = step->GetPreStepPoint()->GetPhysicalVolume()->GetLogicalVolume()->GetMaterial()->GetDensity();
  G4double voxelVolume = voxelSizeX * voxelSizeY * voxelSizeZ;
  G4double voxelMass, dose;

  voxelMass = density*voxelVolume;
  dose=edep / voxelMass;

	G4int layer0 = touchable->GetCopyNumber(0);
	G4int layer1 = touchable->GetCopyNumber(1);
	G4int layer2 = touchable->GetCopyNumber(2);
	G4int layer3 = touchable->GetCopyNumber(3);

	/*G4cout<<" "<<layer0
		<<" "<<layer1
		<<" "<<layer2
		<<" "<<layer3
		<<"  energy: "<<step->GetTotalEnergyDeposit()
		<<G4endl;  */
  addData(depAll, layer0, layer1, layer2, edep, dose);
  if (depFParticles.count(name) == 0) {
  } else {
    addParticlesData(name, layer0, layer1, layer2, edep, dose);
  }
  if (("proton" == name) && (0 == step->GetTrack()->GetParentID())) {
    addParticlesData("0proton", layer0, layer1, layer2, edep, dose);
    //addData(depProtons, layer0, layer1, layer2, edep, dose);
  }
  return true;
}

void DetectorSD::prepareError(Slayer ***target) {
  G4int n;
  G4double dd, d2;
  G4double ddDo, d2Do;
  G4double v, vDo;
  for(int ix = 0; ix < voxelQuantity; ix++) {
    for(int iy = 0; iy < voxelQuantity; iy++) {
      for(int iz = 0; iz < voxelQuantity; iz++) {
        n=target[ix][iy][iz].nEvents;
        dd=target[ix][iy][iz].depEnergy*target[ix][iy][iz].depEnergy;
			  d2=target[ix][iy][iz].depEnergy2;
        ddDo=target[ix][iy][iz].dose*target[ix][iy][iz].dose;
			  d2Do=target[ix][iy][iz].dose2;

        v=n*d2-dd;
        vDo = n*d2Do-ddDo;

        if (n>1) {
          target[ix][iy][iz].depEnergyError=1.0*std::sqrt(v/(n-1));
          target[ix][iy][iz].doseError = 1.0*std::sqrt(vDo/(n-1));
        }
      }
    }
  }
}

void DetectorSD::EndOfEvent(G4HCofThisEvent*) 
{
  for(int ix = 0; ix < voxelQuantity; ix++) {
    for(int iy = 0; iy < voxelQuantity; iy++) {
      for(int iz = 0; iz < voxelQuantity; iz++) {
        //runAction->addAllDep(ix, iy, iz, depAll[ix][iy][iz].depEnergy, depAll[ix][iy][iz].dose);
        runAction->addAllDep(ix, iy, iz, depAll[ix][iy][iz].depEnergy, depAll[ix][iy][iz].depEnergy2, 
          depAll[ix][iy][iz].dose, depAll[ix][iy][iz].dose2, 
          depAll[ix][iy][iz].nEvents);

        for (std::map<G4String, Slayer***>::iterator it = depFParticles.begin();
				it != depFParticles.end();
				++it) {
          runAction->addParticleDep(it->first, 
            ix, iy, iz,
            it->second[ix][iy][iz].depEnergy, it->second[ix][iy][iz].depEnergy2,
            it->second[ix][iy][iz].dose, it->second[ix][iy][iz].dose2,
            it->second[ix][iy][iz].nEvents
            );
        }
      }
    }
  }
  
}