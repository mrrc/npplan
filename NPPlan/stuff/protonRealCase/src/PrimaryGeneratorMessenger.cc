#include "PrimaryGeneratorMessenger.hh"
#include "PrimaryGeneratorAction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"

PrimaryGeneratorMessenger::PrimaryGeneratorMessenger(
                                             PrimaryGeneratorAction* Gun)
:G4UImessenger(),fAction(Gun)
{
  fGunParams = new G4UIdirectory("/irrcaseGun/");
  fGunParams->SetGuidance("gun control");

  distanceFromCenter = new G4UIcmdWithADoubleAndUnit("/irrcaseGun/distance", this);
  distanceFromCenter->SetGuidance("Source from rotation center distance");
  distanceFromCenter->SetParameterName("distanceFromCenter", false);
  //fRndmCmd->SetRange("rBeam>=0.&&rBeam<=1.");
  distanceFromCenter->AvailableForStates(G4State_PreInit,G4State_Idle);  

  irrCase = new G4UIcmdWithAnInteger("/irrcaseGun/case", this);
  irrCase->SetParameterName("irradiationCaseNo", false);
  G4cout<<"Messenger created"<<G4endl;

  //fSigmaParams = new G4UIdirectory("/irrcaseGun/sigma");
  //fSigmaParams->SetGuidance("sigma control");


}

PrimaryGeneratorMessenger::~PrimaryGeneratorMessenger()
{
  delete distanceFromCenter;
  delete fGunParams;
}

void PrimaryGeneratorMessenger::SetNewValue(G4UIcommand* command,
                                               G4String newValue)
{ 
  G4cout<<"Messenger parsing "<<command->GetCommandName()<<" "<<newValue<<G4endl;
  if( command == distanceFromCenter ) {
    fAction->setSourceDistance(distanceFromCenter->GetNewDoubleValue(newValue));
  }
  if (command == irrCase ){
    
    fAction->selectCase(irrCase->GetNewIntValue(newValue));
  }
  /*if( command == fDefaultCmd )
   { fAction->SetDefaultKinematic();}
   
  if( command == fRndmCmd )
   { fAction->SetRndmBeam(fRndmCmd->GetNewDoubleValue(newValue));}   */
}

