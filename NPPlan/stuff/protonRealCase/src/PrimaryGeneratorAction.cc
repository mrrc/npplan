//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file hadronic/Hadr03/src/PrimaryGeneratorAction.cc
/// \brief Implementation of the PrimaryGeneratorAction class
//
// $Id: PrimaryGeneratorAction.cc 70759 2013-06-05 12:26:43Z gcosmo $
// 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "PrimaryGeneratorAction.hh"

#include "PrimaryGeneratorMessenger.hh"
#include "DetectorConstruction.hh"
#include "G4GeneralParticleSource.hh"

#include "G4Event.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"

#include <vector>
#include <iostream>
#include <sstream>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::PrimaryGeneratorAction()
:G4VUserPrimaryGeneratorAction(),
 fParticleGun(0),
 distanceShift(0)
{
  fParticleGun  = new G4GeneralParticleSource();
 

  fGunMessenger = new PrimaryGeneratorMessenger(this);
  /*GetPlannedData();

  selectCase(14);
  printCurrentCase();
  selectCase(40.0, 2);
  printCurrentCase();*/
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
  delete fGunMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  fParticleGun->GeneratePrimaryVertex(anEvent);
}

void PrimaryGeneratorAction::GetPlannedData() {
  G4String fname = "C:\\Temp\\protvino120214\\case1.dfa";
  std::ifstream in;
  std::vector<G4String> tokens;
  in.open(fname, std::ios::in);

  char a[1000];
  in.getline(a, 1000, '\n');
  in.getline(a, 1000, '\n');

  G4int cases;

  in>>cases;

  G4double pos, q;
  G4double angleX, angleY, en, charge;
  irradiationCase sCase;
  for(int i = 0; i < cases; i++) {
    in>>pos>>q;
    for (int j = 0; j < q; j++) {
      in>>angleX>>angleY>>en>>charge;
      sCase.angleX = angleX;
      sCase.angleY = angleY;
      sCase.position = pos;
      sCase.charge = charge;
      sCase.energy = en;

      allCases.push_back(sCase);

      //G4cout<<G4endl;
    }
  }

  /*G4cout<<"SZ: "<<allCases.size();
  for (unsigned i=0; i<allCases.size(); i++) {
    G4cout<<allCases[i].position<<" "<<allCases[i].angleX
      <<" "<<allCases[i].angleY
      <<" "<<allCases[i].energy
      <<" "<<allCases[i].charge
      <<G4endl;
  }*/
}

void PrimaryGeneratorAction::selectCase(int num) {
  if (0 == allCases.size()) {
    GetPlannedData();
  }
  current = allCases[num];
  G4cout<<"Case "<<num<<" selected: "<<G4endl;
  printCurrentCase();
}

void PrimaryGeneratorAction::selectCase(G4double pos, int num) {
  if (0 == allCases.size()) {
    GetPlannedData();
  }
  unsigned int t = 0;
  for (unsigned i=0; i<allCases.size(); i++) {
    if (fabs(allCases[i].position - pos) < 1e-5) {
      if (t++ == num) {
        current = allCases[i];
        break;
      }
    }
  }
}

void PrimaryGeneratorAction::printCurrentCase() {
  G4cout<<G4endl<<"Current case: ";
  G4cout<<current.position<<" "<<current.angleX
      <<" "<<current.angleY
      <<" "<<current.energy
      <<" "<<current.charge
      <<G4endl;
}

void PrimaryGeneratorAction::setSourceDistance(G4double _d) {
  distanceShift = _d;
  G4cout<<"Distance set to: "<<(_d/m)<<G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

