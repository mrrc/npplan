/*
 * Base file for geant projects
 * Main program definition
 * @author: mrxak
 * (c) MRRC, 2013
*/

#include "globals.hh"
#include "DetectorConstruction.hh"
#include "ActionInitialization.hh"
//#include "Randomize.h"

#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif

#include "PhysicsList.hh"

#include "G4UImanager.hh"
#include "G4VModularPhysicsList.hh"
#include "G4StepLimiterPhysics.hh"
#include "FTFP_BERT.hh"
#include "QGSP_BIC.hh"
#include "QBBC.hh"


#ifdef G4VIS_USE
 #include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif

#include <ctime>

namespace {
  void PrintUsage() {
    G4cerr << " Usage: " << G4endl;
    G4cerr << " Test04 [-re <random engine>] [--help] [-m <macros>] [-u <use UI>] [-ph <usePhysics>]" << G4endl;
    G4cerr << " /?, --help - This help " << G4endl;
    G4cerr << " [-re <random engine>] Choose random engine" << G4endl;
    G4cerr << "                  Available <random engine>: " << G4endl;
    G4cerr << "                  ranecu " << G4endl;
    G4cerr << "                  ranlux64 " << G4endl;
    G4cerr << "                  mtwist " << G4endl;
    G4cerr << " [-m <macros file>] Choose macros file" << G4endl;
  }
}

 
int main(int argc,char** argv) {
#ifdef G4MULTITHREADED
  G4int nThreads = 0;
#endif
  G4String cmd;
  G4String randomEngine = "ranecu";
  G4bool useUi = false;
  G4bool usePhys = false;
  G4int standartPhysNum = -1;

  for ( G4int i=1; i<argc; i=i+2 ) {
    if ( G4String(argv[i]) == "-m" ) {
      cmd = argv[i+1];
    }
    if ( G4String(argv[i]) == "-re" ) {
      randomEngine = argv[i+1];
    }
    if (G4String(argv[i]) == "-u") {
      useUi = true;
    }
    if (G4String(argv[i]) == "-ph" && G4String(argv[i+1]) == "-1") {
      G4cout<<"Using default FTFP_BERT physics"<<G4endl;
    }
    else if (G4String(argv[i]) == "-ph" && G4String(argv[i+1]) == "-2") {
      G4cout<<"Using default QBBC physics"<<G4endl;
      standartPhysNum = -2;
    } else if (G4String(argv[i]) == "-ph" && G4String(argv[i+1]) == "-3") {
      G4cout<<"Using default QGSP_BIC physics"<<G4endl;
      standartPhysNum = -3;
    } else if (G4String(argv[i]) == "-ph" && G4String(argv[i+1]) == "1") {
      G4cout<<"Using physList"<<G4endl;
      usePhys = true;
    }
    if ((G4String(argv[i]) == "--help") || (G4String(argv[i]) == "/?")) {
      PrintUsage();
      return 0;
    }
  }

  //choose the Random engine
  G4cout<<"Random engine set: "<<randomEngine<<G4endl;
  if ("ranecu" == randomEngine) {
    CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);
  } else if ("ranlux64" == randomEngine) {
    CLHEP::HepRandom::setTheEngine(new CLHEP::Ranlux64Engine);
  } else if ("mtwist" == randomEngine) {
    CLHEP::HepRandom::setTheEngine(new CLHEP::MTwistEngine);
  }
  //CLHEP::HepRandom::setTheSeed(24534575684783);
  //long seeds[2];
  //seeds[0] = 534524575674523;
  //seeds[1] = 526345623452457;
  //CLHEP::HepRandom::setTheSeeds(seeds);
  CLHEP::HepRandom::setTheSeed(time(NULL));
  

#ifdef G4MULTITHREADED
  G4MTRunManager * runManager = new G4MTRunManager;
  if ( nThreads > 0 ) { 
    runManager->SetNumberOfThreads(nThreads);
  }  
#else
  G4RunManager * runManager = new G4RunManager;
#endif

  runManager->SetUserInitialization(new DetectorConstruction());

  G4VModularPhysicsList* physicsList;
if (usePhys) {
    physicsList= new PhysicsList();
  } else {
  if (-1 == standartPhysNum) {
    physicsList= new FTFP_BERT;
  } else if (-2 == standartPhysNum) {
    physicsList = new QBBC;
  } else if (-3 == standartPhysNum) {
    physicsList = new QGSP_BIC;
  }
    //physicsList= new QBBC;
  }
  //physicsList->SetCuts();
  /*physicsList->SetCutValue(5*cm, "gamma");
  physicsList->SetCutValue(5*cm, "proton");
  physicsList->SetCutValue(5*cm, "e-");
  physicsList->SetCutValue(5*cm, "alpha");*/
  //physicsList->SetCuts();
  //physicsList->defaultCutValue = 0.5 * mm;
  runManager->SetUserInitialization(physicsList);

  runManager->SetUserInitialization(new ActionInitialization());

  runManager->Initialize();

  G4cout<<"Vis_Use "<<G4VIS_USE<<G4endl;

  G4UImanager* UImanager = G4UImanager::GetUIpointer();

#ifdef G4VIS_USE
  G4VisManager* visManager = new G4VisExecutive;
  visManager->Initialize();
#endif

  G4cout<<"Got cmd "<<cmd<<G4endl;

  if (useUi) {
#ifdef G4UI_USE
    G4UIExecutive* ui = new G4UIExecutive(argc, argv);
  #ifdef G4VIS_USE
    UImanager->ApplyCommand("/control/execute "+cmd); 
  #else
    UImanager->ApplyCommand("/control/execute init.mac"); 
  #endif
    ui->SessionStart();
    delete ui;
#endif
  } else  if ( cmd.size() ) {
    // batch mode
    G4String command = "/control/execute ";
    G4cout<<command+cmd<<G4endl;
    UImanager->ApplyCommand(command+cmd);
  }

#ifdef G4VIS_USE
  delete visManager;
#endif
  delete runManager;
  
  return 0;
}
