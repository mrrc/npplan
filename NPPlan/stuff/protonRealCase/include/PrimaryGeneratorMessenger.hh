#ifndef PrimaryGeneratorMessenger_h
#define PrimaryGeneratorMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class PrimaryGeneratorAction;
class G4UIdirectory;
class G4UIcmdWithoutParameter;
class G4UIcmdWithADouble;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAnInteger;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class PrimaryGeneratorMessenger: public G4UImessenger
{
  public:
    PrimaryGeneratorMessenger(PrimaryGeneratorAction*);
   ~PrimaryGeneratorMessenger();
    
    virtual void SetNewValue(G4UIcommand*, G4String);
    
  private:
    PrimaryGeneratorAction*    fAction;
    
    G4UIdirectory*             fGunParams;
    G4UIcmdWithADoubleAndUnit* distanceFromCenter;
    G4UIcmdWithAnInteger*        irrCase;

    G4UIdirectory*             fSigmaParams;
    G4UIcmdWithADouble*        fSigmaX;
    G4UIcmdWithADouble*        fSigmaY;

    //G4UIcmdWithoutParameter*   fDefaultCmd;
    //G4UIcmdWithADouble*        fRndmCmd;
};
#endif