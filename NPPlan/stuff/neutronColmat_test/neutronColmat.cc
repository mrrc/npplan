/*
 * Base file for geant projects
 * Main program definition
 * @author: mrxak
 * (c) MRRC, 2013
*/

//#define IS_EXP 1

#include "globals.hh"
#include "DetectorConstruction.hh"
#include "ActionInitialization.hh"
//#include "Randomize.h"

#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif

#include "RunAction.hh"

#include "G4UImanager.hh"
#include "G4VModularPhysicsList.hh"
#include "G4StepLimiterPhysics.hh"
#include "FTFP_BERT.hh"
#include "QGSP_BIC.hh"
#include "QGSP_BIC_HP.hh"
#include "FTFP_BERT_HP.hh"
#include "QBBC.hh"
#include "G4GenericIon.hh"

#ifdef _WIN32
// @todo: handling Ctrl+C, e.g. http://msdn.microsoft.com/en-us/library/ms685049%28VS.85%29.aspx

#endif

#if defined (WIN32)
#include <windows.h>
#else
#include <signal.h>
#endif


#ifdef G4VIS_USE
 #include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif

#include <ctime>

namespace {
  G4RunManager *runManagerTop;
  void PrintUsage() {
    G4cerr << " Usage: " << G4endl;
    G4cerr << " Test04 [-re <random engine>] [--help] [-m <macros>] [-u <use UI>] [-ph <usePhysics>]" << G4endl;
    G4cerr << "        [-cont <continue>] [-rbb <run_till_error>]" << G4endl;
    G4cerr << " /?, --help - This help " << G4endl;
    G4cerr << " [-re <random engine>] Choose random engine" << G4endl;
    G4cerr << "                  Available <random engine>: " << G4endl;
    G4cerr << "                  ranecu " << G4endl;
    G4cerr << "                  ranlux64 " << G4endl;
    G4cerr << "                  mtwist " << G4endl;
    G4cerr << " [-m <macros file>] Choose macros file" << G4endl;
  }
  void makeClearRun() {
    // @todo: write deleting all files
  }
#if defined (WIN32)
  BOOL CtrlHandler( DWORD fdwCtrlType ) 
{ 
  G4String a;
  G4double pt;
  G4int pn;
  switch( fdwCtrlType ) 
  { 
    case CTRL_C_EVENT: 
      printf( "Ctrl-C event got\n\n" );
      //runManagerTop->AbortRun();
      //((RunAction*)runManagerTop->GetUserRunAction())->checkHasBestSolution();
      //runManagerTop->GetUserRunAction()

      //G4cin>>a;
      //G4cout<<"Got :"<<a;
      runManagerTop->AbortRun();
      return FALSE; 
      //return( TRUE );

    case CTRL_BREAK_EVENT: 
      printf( "Ctrl-Break event\n\n" );
      pt = ((RunAction*)runManagerTop->GetUserRunAction())->getCurrentRunElapsed();
      G4cout<<"Current run elapsed time: "<<pt<<" secs"<<G4endl;
      pt = ((RunAction*)runManagerTop->GetUserRunAction())->getAllRunElapsed();
      G4cout<<"All runs elapsed time: "<<pt<<" secs"<<G4endl;
      ((RunAction*)runManagerTop->GetUserRunAction())->printDetectorsError();
      //return FALSE; 
      return( TRUE );

    default: 
      return FALSE; 
  } 
} 
#else
  void CtrlBreakBackend(int sig) {
    printf( "Ctrl-Break event\n\n" );
    pt = ((RunAction*)runManagerTop->GetUserRunAction())->getCurrentRunElapsed();
    G4cout<<"Current run elapsed time: "<<pt<<" secs"<<G4endl;
    ((RunAction*)runManagerTop->GetUserRunAction())->printDetectorsError();
  }
#endif

  void handleCtrlCEventInit(G4RunManager *runManagerMain) {
    runManagerTop = runManagerMain;
#if defined (WIN32)
    SetConsoleCtrlHandler( (PHANDLER_ROUTINE) CtrlHandler, TRUE );
#else
  #ifdef SIGBREAK
    signal(SIGBREAK, CtrlBreakBackend);
  #endif
#endif

  }
}

 
int main(int argc,char** argv) {
#ifdef G4MULTITHREADED
  G4int nThreads = 0;
#endif
  G4String cmd;
  G4String randomEngine = "ranecu";
  G4bool useUi = false;
  G4bool usePhys = false;
  G4int standartPhysNum = -1;
  bool useContinue = false;
  bool run2Best = false;
  bool run2Time = false;
  G4double ctmeValue = -1;
  G4int singleRunEvents = 100000;

  for ( G4int i=1; i<argc; i=i+2 ) {
    if ( G4String(argv[i]) == "-m" ) {
      cmd = argv[i+1];
    }
    if ( G4String(argv[i]) == "-re" ) {
      randomEngine = argv[i+1];
    }
    if (G4String(argv[i]) == "-u") {
      useUi = true;
    }
    if (G4String(argv[i]) == "-ph" && G4String(argv[i+1]) == "-1") {
      G4cout<<"Using default FTFP_BERT physics"<<G4endl;
    }
    else if (G4String(argv[i]) == "-ph" && G4String(argv[i+1]) == "-2") {
      G4cout<<"Using default QBBC physics"<<G4endl;
      standartPhysNum = -2;
    } else if (G4String(argv[i]) == "-ph" && G4String(argv[i+1]) == "-3") {
      G4cout<<"Using default QGSP_BIC physics"<<G4endl;
      standartPhysNum = -3;
    } else if (G4String(argv[i]) == "-ph" && G4String(argv[i+1]) == "-4") {
      G4cout<<"Using default QGSP_BIC_HP physics"<<G4endl;
      standartPhysNum = -4;
    } else if (G4String(argv[i]) == "-ph" && G4String(argv[i+1]) == "-5") {
      G4cout<<"Using default FTFP_BERT_HP physics"<<G4endl;
      standartPhysNum = -5;
    } else if (G4String(argv[i]) == "-ph" && G4String(argv[i+1]) == "1") {
      G4cout<<"Using physList"<<G4endl;
      usePhys = true;
    }
    if ((G4String(argv[i]) == "-cont")/* && (G4String(argv[i+1]) == "1")*/) {
      useContinue = true;
    } else if ((G4String(argv[i]) == "-cont") && (G4String(argv[i+1]) == "0")) {
      useContinue = false;
    }
    if (G4String(argv[i]) == "-rbb") {
      run2Best = true;
    }
    if ((G4String(argv[i]) == "--help") || (G4String(argv[i]) == "/?")) {
      PrintUsage();
      return 0;
    }
    if ((G4String(argv[i]) == "--clear")) { 
      makeClearRun();
    }
    if ((G4String(argv[i]) == "-ctme")) {
      run2Time = true;
      ctmeValue = std::strtod(argv[i+1], NULL);
    }
    if ((G4String(argv[i]) == "-sr")) {
      singleRunEvents = std::strtol(argv[i+1], NULL, 10);
      G4cout<<"Events to be processed in one run: "<<singleRunEvents<<G4endl;
    }
  }

  

  //choose the Random engine
  G4cout<<"Random engine set: "<<randomEngine<<G4endl;
  if ("ranecu" == randomEngine) {
    CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);
  } else if ("ranlux64" == randomEngine) {
    CLHEP::HepRandom::setTheEngine(new CLHEP::Ranlux64Engine);
  } else if ("mtwist" == randomEngine) {
    CLHEP::HepRandom::setTheEngine(new CLHEP::MTwistEngine);
  }
  //CLHEP::HepRandom::setTheSeed(24534575684783);
  //long seeds[2];
  //seeds[0] = 534524575674523;
  //seeds[1] = 526345623452457;
  //CLHEP::HepRandom::setTheSeeds(seeds);
  CLHEP::HepRandom::setTheSeed((int)clock());
  

#ifdef G4MULTITHREADED
  G4MTRunManager * runManager = new G4MTRunManager;
  if ( nThreads > 0 ) { 
    runManager->SetNumberOfThreads(nThreads);
  }  
#else
  G4RunManager * runManager = new G4RunManager;
#endif

  runManager->SetUserInitialization(new DetectorConstruction());

  G4VModularPhysicsList* physicsList;
if (usePhys) {
    physicsList= new FTFP_BERT;
  } else {
    if (-1 == standartPhysNum) {
      physicsList= new FTFP_BERT;
    } else if (-2 == standartPhysNum) {
      physicsList = new QBBC;
    } else if (-3 == standartPhysNum) {
      physicsList = new QGSP_BIC;
    } else if (-4 == standartPhysNum) {
      physicsList = new QGSP_BIC_HP;
    } else if (-5 == standartPhysNum) {
      physicsList = new FTFP_BERT_HP;
    }
  }
  runManager->SetUserInitialization(physicsList);

  ActionInitialization* AI = new ActionInitialization(useContinue);

  runManager->SetUserInitialization(AI);

  runManager->Initialize();

  G4cout<<"Vis_Use "<<G4VIS_USE<<G4endl;

  G4UImanager* UImanager = G4UImanager::GetUIpointer();

#ifdef G4VIS_USE
  G4VisManager* visManager = new G4VisExecutive;
  visManager->Initialize();
#endif

  G4cout<<"Got cmd "<<cmd<<G4endl;

  handleCtrlCEventInit(runManager);
  //runManager->AbortRun

  if (useUi) {
#ifdef G4UI_USE
    G4UIExecutive* ui = new G4UIExecutive(argc, argv);
  #ifdef G4VIS_USE
    UImanager->ApplyCommand("/control/execute "+cmd); 
  #else
    UImanager->ApplyCommand("/control/execute init.mac"); 
  #endif
    ui->SessionStart();
    delete ui;
#endif
  } else  if ( cmd.size() ) {
    // batch mode
    G4String command = "/control/execute ";
    G4cout<<command+cmd<<G4endl;
    UImanager->ApplyCommand(command+cmd);
  }

  G4int roe = 1000000;

#ifdef IS_EXP
  // /gps/particle ion
  // /gps/ion 6 12
  UImanager->ApplyCommand("/gps/energy 2.4 GeV");
  runManager->BeamOn(roe);

  UImanager->ApplyCommand("/gps/energy 3.6 GeV");
  runManager->BeamOn(roe);

  UImanager->ApplyCommand("/gps/energy 4.8 GeV");
  runManager->BeamOn(roe);

  UImanager->ApplyCommand("/gps/energy 5.04 GeV");
  runManager->BeamOn(roe);

  UImanager->ApplyCommand("/gps/energy 5.16 GeV");
  runManager->BeamOn(roe);

  UImanager->ApplyCommand("/gps/energy 5.4 GeV");
  runManager->BeamOn(roe);


#else
  if (run2Best) {
    while ( !((RunAction*)runManager->GetUserRunAction())->checkHasBestSolution()) {
      runManager->BeamOn(singleRunEvents);
    }
  }
  if (run2Time) {
    while ( !((RunAction*)runManager->GetUserRunAction())->checkHasTimeExpired(ctmeValue)) {
      runManager->BeamOn(singleRunEvents);
    }
  }
#endif
  
#ifdef G4VIS_USE
  delete visManager;
#endif
  delete runManager;
  
  return 0;
}
