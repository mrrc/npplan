#include "DetectorConstruction.hh"

#include "DetectorSDSingle.hh"
#include "DetectorSDSinglePart.hh"
//#include "DetectorSD1Layer.hh"
#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Orb.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4RotationMatrix.hh"
#include "G4NistManager.hh"
#include "globals.hh"
#include "G4VisAttributes.hh" 
#include "G4SDManager.hh"
#include "G4Region.hh"
#include "G4ProductionCuts.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4Cons.hh"
#include "G4IntersectionSolid.hh"

DetectorConstruction::DetectorConstruction() {}

DetectorConstruction::~DetectorConstruction() {}

// ������ ��� ����������� ������ �������
#define SIZE(x) sizeof(x)/sizeof(*x)


G4VPhysicalVolume* DetectorConstruction::Construct()
{
    G4NistManager* nistMan = G4NistManager::Instance();
  G4Material* air = nistMan->FindOrBuildMaterial("G4_AIR");
  //G4Material* plex = nistMan->FindOrBuildMaterial("G4_PLEXIGLASS");
  //G4Material* polystyrene = nistMan->FindOrBuildMaterial("G4_POLYSTYRENE");
  //G4Material* cells = nistMan->FindOrBuildMaterial("G4_MUSCLE_SKELETAL_ICRP");
  G4Material* water = nistMan->FindOrBuildMaterial("G4_WATER");
  G4Material* concrete = nistMan->FindOrBuildMaterial("G4_CONCRETE");
  //G4Material* Fe2O3 = nistMan->FindOrBuildMaterial("G4_FERRIC_OXIDE");
  //G4Material* Al2O3 = nistMan->FindOrBuildMaterial("G4_ALUMINUM_OXIDE");
  G4Material* tissue = nistMan->FindOrBuildMaterial("G4_TISSUE_SOFT_ICRP");
  
  
 G4Element* elH
= new G4Element("Hydrogen", "H" , 1., 1.01*g/mole);
 G4Element* elC
= new G4Element("Carbon", "C" , 6., 12.01*g/mole);
 G4Element* elN
= new G4Element("Nitrogen", "N" , 7., 14.01*g/mole);
 G4Element* elO
= new G4Element("Oxygen", "O" , 8., 16.00*g/mole);
 G4Element* elLi
= new G4Element("Lithium", "Li" , 3., 6.94*g/mole);
 G4Element* elCl
= new G4Element("Chlorine", "Cl" , 17., 35.45*g/mole);
 G4Element* elK
= new G4Element("Potassium", "K", 19., 39.10*g/mole);
 G4Element* elBr
= new G4Element("Bromine", "Br", 35., 79.90*g/mole);
 G4Element* elCa 
= new G4Element("Calcium", "Ca", 20.0, 40.078*g/mole);
  G4Element* elFe 
= new G4Element("Iron", "Fe", 26, 56.845*g/mole);
 G4Element* elAl
= new G4Element("Aluminium", "Al", 13, 26.98*g/mole);
  G4Element* elSi 
= new G4Element("Silicium", "Si", 14, 28.086*g/mole);

  G4double world_x_length = 4*m;
  G4double world_y_length = 4*m;
  G4double world_z_length = 4*m;

  G4Box* world_box = new G4Box("world", 0.5*world_x_length, 0.5*world_y_length, 0.5*world_z_length);
  G4LogicalVolume* world_log = new G4LogicalVolume(world_box, air, "world");
  G4VPhysicalVolume* world_phys = new G4PVPlacement(0, G4ThreeVector(), world_log, "world", 0, false, 0);

  G4double phSx, phSy, phSz, zPos;
  phSx = 5*cm;
  phSy = 5*cm;
  phSz = 25*cm;

  zPos = 23*mm;

  nov = 50000;
  //G4Box* phBox = new G4Box("phantomBox", 0.5*phSx, 0.5*phSy, 0.5*phSz);
  //G4LogicalVolume* phLogic = new G4LogicalVolume(phBox, water, "phLogic");
  //new G4PVPlacement(0, G4ThreeVector(), phLogic, "world", world_log, false, 0);


	/*G4Box* solidBox =    
    new G4Box("OBoxS", phSx, phSy, phSz);
      
	G4LogicalVolume* logicBox =                         
		new G4LogicalVolume(solidBox,            //its solid
							water,              //its material
							"OBoxL");              //its name
          
    new G4PVPlacement(0,                     //no rotation
                    G4ThreeVector(0, 0, 0),  //at (0,0,0)
                    logicBox,                //its logical volume
                    "OBoxP",                   //its name
                    world_log,              //its mother  volume
                    false,                   //no boolean operation
                     0,                     //copy number
                     true);        //overlaps checking
                     */
  
  //Collimator(world_log);

  qOfDets = 0;
	G4Box* det_box = new G4Box("detector", 1*cm, 1*cm, 1*cm);
  G4LogicalVolume *det_log1 = new G4LogicalVolume(det_box, water, "detectorL1");
  new G4PVPlacement(0, G4ThreeVector(0, 0, 24*cm), det_log1, "detectorP1", world_log, false, 0);
  addDetParted(det_log1, "det1");

  G4LogicalVolume *det_log2 = new G4LogicalVolume(det_box, water, "detectorL2");
  new G4PVPlacement(0, G4ThreeVector(0, 0.5*m, 24*cm), det_log2, "detectorP2", world_log, false, 0);
  addDetParted(det_log2, "det2");

  G4LogicalVolume *det_log3 = new G4LogicalVolume(det_box, water, "detectorL3");
  new G4PVPlacement(0, G4ThreeVector(0, 1*m, 24*cm), det_log3, "detectorP3", world_log, false, 0);
  addDetParted(det_log3, "det3");

  G4LogicalVolume *det_log2a = new G4LogicalVolume(det_box, water, "detectorL2a");
  new G4PVPlacement(0, G4ThreeVector(0, 0.5*m, 49*cm), det_log2a, "detectorP2a", world_log, false, 0);
  addDetParted(det_log2a, "det2a");

  G4LogicalVolume *det_log3a = new G4LogicalVolume(det_box, water, "detectorL3a");
  new G4PVPlacement(0, G4ThreeVector(0, 1*m, 49*cm), det_log3a, "detectorP3a", world_log, false, 0);
  addDetParted(det_log3a, "det3a");

  //mainDet = det_log1;

  /*G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  DetectorSD1Layer* det = new DetectorSD1Layer("detl", nov, 1, 10*10*cm2*10*micrometer);
  //det->addIon(6, 12);
  mainDet = det;
  sdMan->AddNewDetector(det);
  //phantomLayerLog->SetSensitiveDetector(det);
  det_log1->SetSensitiveDetector(det);
  qOfDets++;*/

  return world_phys;

}

void DetectorConstruction::addDet(G4LogicalVolume* vol, G4String name) {
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  DetectorSDSingle* det = new DetectorSDSingle(name, vol->GetSolid()->GetCubicVolume());
  sdMan->AddNewDetector(det);
  vol->SetSensitiveDetector(det);
  qOfDets++;
  lOfDets.push_back(name);
}

#ifdef DetectorSD_Single_Particles_h
void DetectorConstruction::addDetParted(G4LogicalVolume* vol, G4String name) {
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  DetectorSDSinglePart* det = new DetectorSDSinglePart(name, vol->GetSolid()->GetCubicVolume());
  det->addParticle("proton");
  det->addParticle("alpha");
  det->addParticle("e-");
  det->addParticle("gamma");
  det->addParticle("neutronHE", "neutron", 10, 100);
  det->addParticle("neutronLE", "neutron", 0, 10);
  sdMan->AddNewDetector(det);
  vol->SetSensitiveDetector(det);
  qOfDets++;
  lOfDets.push_back(name);
}
#endif

void DetectorConstruction::addDet(G4LogicalVolume* vol, G4String name, G4double volume) {
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  DetectorSDSingle* det;
  if (0.0 == volume) {
    det = new DetectorSDSingle(name);
  } else {
    det = new DetectorSDSingle(name, volume);
  }
  sdMan->AddNewDetector(det);
  vol->SetSensitiveDetector(det);
  qOfDets++;
  lOfDets.push_back(name);
}

G4LogicalVolume* DetectorConstruction::addZVoxelsLayer(G4LogicalVolume* parentVolume, 
										  G4ThreeVector position, 
										  G4String lName, 
										  G4Material* material,
										  G4double blX,
										  G4double blY,
										  G4double blZ,
										  G4int qLayers
                      ) 
{
	//G4Material* water = nistMan->FindOrBuildMaterial("G4_WATER");
	// Voxels container
	G4Box* solidBox =    
    new G4Box(lName+"BoxS",                         //its name
        blX, blY, blZ); //its size
      
	G4LogicalVolume* logicBox =                         
		new G4LogicalVolume(solidBox,            //its solid
							material,              //its material
							lName+"BoxL");              //its name
          
    new G4PVPlacement(0,                     //no rotation
                    position,  //at (0,0,0)
                    logicBox,                //its logical volume
                    lName+"BoxP",                   //its name
                    parentVolume,              //its mother  volume
                    false,                   //no boolean operation
                     0,                     //copy number
                     true);        //overlaps checking

	// Voxels replica
	G4VSolid* layerX =
		new G4Box(lName+"layerZ", blX, blY, blZ / qLayers);
    
	G4LogicalVolume* logicLayer =
		new G4LogicalVolume(layerX, material, lName+"layerZL");
   
    new G4PVReplica(lName+"layerZR", logicLayer, 
                 logicBox, kZAxis, qLayers, 2 * blZ / qLayers);  

	G4LogicalVolume* det_log;
	G4Box* det_box = new G4Box(lName+"detector", blX, blY, blZ / qLayers);
  det_log = new G4LogicalVolume(det_box, material, lName+"detectorL");
  G4VPhysicalVolume* det_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), det_log, lName+"detectorP", logicLayer, false, 0);

	//DetectorSDVox* vox1 = new DetectorSDVox(lName+"vox", qLayers);
	//sdMan->AddNewDetector(vox1);
	//det_log->SetSensitiveDetector(vox1);

	return det_log;
}

G4int DetectorConstruction::getNumberOfLayers() {
  return nov;
}

DetectorSD1Layer* DetectorConstruction::getMainDetector() {
  return mainDet;
}

void DetectorConstruction::Collimator(G4LogicalVolume *worldLogic)
  {  
	  G4NistManager* nistMan = G4NistManager::Instance();
	   
G4Material* vacuum = nistMan->FindOrBuildMaterial("G4_AIR");
G4Material* W = nistMan->FindOrBuildMaterial("G4_W");
G4Material* Fe = nistMan->FindOrBuildMaterial("G4_Fe");

 G4Element* elH
= new G4Element("Hydrogen","H" , 1., 1.01*g/mole);
 G4Element* elC
= new G4Element("Carbon" ,"C" , 6., 12.01*g/mole);
G4Element* elLi
= new G4Element("Lithium", "Li",  3., 6.94*g/mole);
 G4Element* elBr
= new G4Element("Bromine","Br" , 35., 79.90*g/mole);
 G4Element* elB 
= new G4Element("Boron", "B", 5., 10.81*g/mole);
 G4Element* elTa 
= new G4Element("Tantalum", "Ta", 73., 180.95*g/mole);

G4Material* BCH2 = new G4Material("BCH2", 0.97*g/cm3, 3);
BCH2->AddElement(elH, 2);
BCH2->AddElement(elC, 1);  
BCH2->AddElement(elB, 1);

G4Material* TaH = new G4Material("TaH", 15.04*g/cm3, 2);
TaH->AddElement(elTa, 1);
TaH->AddElement(elH, 1);  

G4Material* TaHBCH2 = new G4Material("TaHBCH2", 13.76*g/cm3, 2);
TaHBCH2->AddMaterial(TaH, 0.993589);
TaHBCH2->AddMaterial(BCH2, 0.006411);

G4Material* FeBCH2 = new G4Material("FeBCH2", 4.41*g/cm3, 2);
FeBCH2->AddMaterial(Fe, 0.8809);
FeBCH2->AddMaterial(BCH2, 0.1191);

G4Material* WBCH2 = new G4Material("WBCH2", 10.1*g/cm3, 2);
WBCH2->AddMaterial(W, 0.9521);
WBCH2->AddMaterial(BCH2, 0.0479);

double angle_x; 
double angle_y; 


//double angle_x_rad = (angle_x/180)*M_PI;
//double angle_y_rad = (angle_y/180)*M_PI;
	  
//G4RotationMatrix* Rot = new G4RotationMatrix;
//Rot->rotateX(angle_x*deg);
//Rot->rotateY(angle_y*deg);

 double col_x = 0;
 double col_y = 0;
 double col_z = 49;  
 
	  G4Tubs* Collimator = new G4Tubs("Collimator", 0*cm, 30*cm, 24*cm, 0*deg, 360*deg);
G4LogicalVolume* Collimator_log = new G4LogicalVolume(Collimator, vacuum, "Collimator");  
G4VPhysicalVolume* Collimator_phys = new G4PVPlacement(0, G4ThreeVector(col_x*cm, col_y*cm, col_z*cm), Collimator_log, "Collimator",
 worldLogic, false, 0); 
 
 G4Tubs* Air = new G4Tubs("Air", 0*cm, 30*cm, 10*cm, 90*deg, 360*deg);
G4LogicalVolume* Air_log = new G4LogicalVolume(Air, vacuum, "Air");  
G4VPhysicalVolume* Air_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 14*cm), Air_log, "Air", Collimator_log, false, 0); 

 G4Tubs* Collimator_1 = new G4Tubs("Collimator_1", 0*cm, 30*cm, 14*cm, 90*deg, 360*deg);
G4LogicalVolume* Collimator_1_log = new G4LogicalVolume(Collimator_1, vacuum, "Collimator_1");  
G4VPhysicalVolume* Collimator_1_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, -10*cm), Collimator_1_log, "Collimator_1",
 Collimator_log, false, 0);  
 
  G4Tubs* Collimator_2 = new G4Tubs("Collimator_2", 0*cm, 30*cm, 8.5*cm, 90*deg, 360*deg);
G4LogicalVolume* Collimator_2_log = new G4LogicalVolume(Collimator_2, vacuum, "Collimator_2");  
G4VPhysicalVolume* Collimator_2_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 5.5*cm), Collimator_2_log, "Collimator_2",
 Collimator_1_log, false, 0);  

 G4Tubs* det_coll1 = new G4Tubs("det_coll1", 0*cm, 30*cm, 7*cm, 90*deg, 360*deg);
G4LogicalVolume* det_coll1_log = new G4LogicalVolume(det_coll1, FeBCH2, "det_coll1");  
G4VPhysicalVolume* det_coll1_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 1.5*cm), det_coll1_log, "det_coll1",
 Collimator_2_log, false, 0); 

 G4Tubs* det_coll2 = new G4Tubs("det_coll2", 0*cm, 30*cm, 1.5*cm, 90*deg, 360*deg);
G4LogicalVolume* det_coll2_log = new G4LogicalVolume(det_coll2, WBCH2, "det_coll2");   
G4VPhysicalVolume* det_coll2_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, -7*cm), det_coll2_log, "det_coll2",
 Collimator_2_log, false, 0); 

G4Cons* det_coll3 = new G4Cons("det_coll3", 0*cm, 24.6*cm, 0*cm, 2.1*cm, 8.5*cm, 90*deg, 360*deg); 

  
  G4IntersectionSolid* Intersection1 = new G4IntersectionSolid("Intersection1", det_coll1, det_coll3, 0, G4ThreeVector(0, 0, -1.5*cm));
  G4LogicalVolume* Intersection1_log = new G4LogicalVolume(Intersection1, TaHBCH2, "Intersection1");
G4VPhysicalVolume* Intersection1_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), Intersection1_log, "Intersection1", 
det_coll1_log, false, 0);
 
 G4IntersectionSolid* Intersection2 = new G4IntersectionSolid("Intersection2", det_coll2, det_coll3, 0, G4ThreeVector(0, 0, 7*cm));
  G4LogicalVolume* Intersection2_log = new G4LogicalVolume(Intersection2, TaHBCH2, "Intersection2");
G4VPhysicalVolume* Intersection2_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), Intersection2_log, "Intersection2",
 det_coll2_log, false, 0);
 
  
 G4Cons* det_coll4 = new G4Cons("det_coll4", 0*cm, 3.4*cm, 0*cm, 2.1*cm, 8.5*cm, 90*deg, 360*deg); 


 G4IntersectionSolid* Intersection3 = new G4IntersectionSolid("Intersection3", Intersection1, det_coll4, 0, G4ThreeVector(0, 0, -1.5*cm));
  G4LogicalVolume* Intersection3_log = new G4LogicalVolume(Intersection3, W, "Intersection3");
G4VPhysicalVolume* Intersection3_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), Intersection3_log, "Intersection3",
Intersection1_log, false, 0);
 
 G4IntersectionSolid* Intersection4 = new G4IntersectionSolid("Intersection4", Intersection2, det_coll4, 0, G4ThreeVector(0, 0, 7*cm));
  G4LogicalVolume* Intersection4_log = new G4LogicalVolume(Intersection4, W, "Intersection4");
G4VPhysicalVolume* Intersection4_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), Intersection4_log, "Intersection4",
 Intersection2_log, false, 0);
  
 G4Cons* det_coll5 = new G4Cons("det_coll5", 0*cm, 3*cm, 0*cm, 1*cm, 14*cm, 90*deg, 360*deg); 
 
  G4IntersectionSolid* Intersection5 = new G4IntersectionSolid("Intersection5", Intersection3, det_coll5, 0, G4ThreeVector(0, 0, -7*cm));
  G4LogicalVolume* Intersection5_log = new G4LogicalVolume(Intersection5, vacuum, "Intersection5");
G4VPhysicalVolume* Intersection5_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), Intersection5_log, "Intersection5",
 Intersection3_log, false, 0);
  
  G4IntersectionSolid* Intersection6 = new G4IntersectionSolid("Intersection6", Intersection4, det_coll5, 0, G4ThreeVector(0, 0, 1.5*cm));
  G4LogicalVolume* Intersection6_log = new G4LogicalVolume(Intersection6, vacuum, "Intersection6");
G4VPhysicalVolume* Intersection6_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), Intersection6_log, "Intersection6",
 Intersection4_log, false, 0);
 
 G4Tubs* det_coll6 = new G4Tubs("det_coll6", 0*cm, 30*cm, 0.5*cm, 90*deg, 360*deg);
G4LogicalVolume* det_coll6_log = new G4LogicalVolume(det_coll6, W, "det_coll6");  
G4VPhysicalVolume* det_coll6_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, -3.5*cm), det_coll6_log, "det_coll6",
 Collimator_1_log, false, 0);
 
 G4Cons* det_coll7 = new G4Cons("det_coll7", 0*cm, 6.5*cm, 0*cm, 9.1*cm, 1*cm, 90*deg, 360*deg); 
G4LogicalVolume* det_coll7_log = new G4LogicalVolume(det_coll7, TaHBCH2, "det_coll7");
G4VPhysicalVolume* det_coll7_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, -5*cm), det_coll7_log, "det_coll7",
 Collimator_1_log, false, 0);
 
 G4Cons* det_coll8 = new G4Cons("det_coll8", 0*cm, 4.7*cm, 0*cm, 6.5*cm, 4*cm, 90*deg, 360*deg); 
G4LogicalVolume* det_coll8_log = new G4LogicalVolume(det_coll8, TaHBCH2, "det_coll8");
G4VPhysicalVolume* det_coll8_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, -10*cm), det_coll8_log, "det_coll8",
 Collimator_1_log, false, 0);

 G4Cons* det_coll9 = new G4Cons("det_coll9", 0*cm, 4.1*cm, 0*cm, 3.4*cm, 5.5*cm, 90*deg, 360*deg); 
 
  G4IntersectionSolid* Intersection10 = new G4IntersectionSolid("Intersection10", det_coll7, det_coll9, 0, G4ThreeVector(0, 0, -3.5*cm));
  G4LogicalVolume* Intersection10_log = new G4LogicalVolume(Intersection10, Fe, "Intersection10");
G4VPhysicalVolume* Intersection10_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), Intersection10_log, "Intersection10",
det_coll7_log, false, 0);

G4IntersectionSolid* Intersection11 = new G4IntersectionSolid("Intersection11", det_coll8, det_coll9, 0, G4ThreeVector(0, 0, -0.5*cm));
  G4LogicalVolume* Intersection11_log = new G4LogicalVolume(Intersection11, Fe, "Intersection11");
G4VPhysicalVolume* Intersection11_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), Intersection11_log, "Intersection11",
det_coll8_log, false, 0);

G4IntersectionSolid* Intersection12 = new G4IntersectionSolid("Intersection12", det_coll6, det_coll9, 0, G4ThreeVector(0, 0, -5*cm));
  G4LogicalVolume* Intersection12_log = new G4LogicalVolume(Intersection12, Fe, "Intersection12");
G4VPhysicalVolume* Intersection12_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), Intersection12_log, "Intersection12",
det_coll6_log, false, 0);

G4IntersectionSolid* Intersection7 = new G4IntersectionSolid("Intersection7", Intersection12, det_coll5, 0, G4ThreeVector(0, 0, 3.5*cm));
  G4LogicalVolume* Intersection7_log = new G4LogicalVolume(Intersection7, vacuum, "Intersection7");
G4VPhysicalVolume* Intersection7_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), Intersection7_log, "Intersection7",
 Intersection12_log, false, 0);

G4IntersectionSolid* Intersection8 = new G4IntersectionSolid("Intersection8", Intersection10, det_coll5, 0, G4ThreeVector(0, 0, 4.5*cm));
  G4LogicalVolume* Intersection8_log = new G4LogicalVolume(Intersection8, vacuum, "Intersection8");
G4VPhysicalVolume* Intersection8_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), Intersection8_log, "Intersection8",
 Intersection10_log, false, 0);
 
 G4IntersectionSolid* Intersection9 = new G4IntersectionSolid("Intersection9", Intersection11, det_coll5, 0, G4ThreeVector(0, 0, 9*cm));
  G4LogicalVolume* Intersection9_log = new G4LogicalVolume(Intersection9, vacuum, "Intersection9");
G4VPhysicalVolume* Intersection9_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), Intersection9_log, "Intersection9",
Intersection11_log, false, 0);

/*
 Collimator_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Red()));
 det_coll1_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Blue()));
 det_coll2_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Yellow())); 
 
 Intersection1_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Green()));
 Intersection2_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Green()));
 Intersection3_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Blue()));
 Intersection4_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Blue()));
 Intersection5_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Red()));
 Intersection6_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Red()));
 Intersection7_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Red()));
 Intersection8_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Red()));
 Intersection9_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Red()));
 Intersection10_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Yellow()));
 Intersection11_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Yellow()));
 Intersection12_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Yellow()));
 det_coll6_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Blue()));
 det_coll7_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Green()));
 det_coll8_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Green()));*/
  }