# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 19.06.13
@summary: 
'''

import json
import pprint
import NPPlan

matFile = open('mat.json', 'r')

u = json.load(matFile, )
pprint.pprint(u)

NPPlan.init(programPath="c:/NPPlan3/", test=True, log=False, utest=True)
#NPPlan.mongoConnection.isotope.remove()
for i in u:
    isotope = NPPlan.mongoConnection.isotope()
    isotope.name = u[i][u'Name']
    isotope.symbol = i
    isotope.atomicMass = u[i][u'Atomic mass']
    isotope.atomicNum = u[i][u'Atomic no']
    isotope.pid = '0'
    isotope.save()