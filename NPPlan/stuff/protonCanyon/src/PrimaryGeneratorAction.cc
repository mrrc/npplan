#include "PrimaryGeneratorAction.hh"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleDefinition.hh"
#include "Randomize.hh"
#include "G4RandomDirection.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction()
{
  particleGun = new G4ParticleGun(1);

}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete particleGun;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* event)
{
  /*G4double r0,phi0,  x0, y0;
  
     r0 = 3.5*cm*std::sqrt(G4UniformRand());
      phi0 = twopi*G4UniformRand();
      x0 = r0*std::cos(phi0);
      y0 = r0*std::sin(phi0);*/
  
  particleGun->SetParticleDefinition(G4Proton::ProtonDefinition());
  particleGun->SetParticleEnergy(250*MeV);

  G4double twopi = CLHEP::twopi;
  
  G4double r = G4RandGauss::shoot(0., 2.5);
  G4double phi0 = twopi*G4UniformRand();
  G4double x = r*cos(phi0);
  G4double y = r*sin(phi0);
  
  
  
  //particleGun->SetParticleMomentumDirection(G4RandomDirection());
  particleGun->SetParticleMomentumDirection(G4ThreeVector(0, 0, 1));
  //particleGun->SetParticleMomentumDirection(G4ThreeVector(-1, 0, 0));
  //particleGun->SetParticlePosition(G4ThreeVector(1.4525*m,-1.65*m,2.9175*m)); //real position
  particleGun->SetParticlePosition(G4ThreeVector(1.4525*m+x*mm,-1.65*m+y*mm,3.4175*m)); //phantom border
  //particleGun->SetParticlePosition(G4ThreeVector(-3*m,-1.65*m,5.4175*m));
  particleGun->GeneratePrimaryVertex(event);
  
}

