#ifndef DetectorConstruction_H
#define DetectorConstruction_H 1

#include "G4VUserDetectorConstruction.hh"
#include <vector>

class DetectorConstruction: public G4VUserDetectorConstruction
{                 
  public:
    DetectorConstruction();
    ~DetectorConstruction();

    G4VPhysicalVolume* Construct();
    G4int nLayers1;
    G4int nLayers2;
    G4int nLayers3;
    G4int nLayers4;
    G4int nLayers5;
    G4int nLayers6;

    G4int qOfDets;
    std::vector<G4String> lOfDets;

    void addDet(G4LogicalVolume*, G4String);
    void addDet(G4LogicalVolume*, G4String, G4double);
    void addDetParted(G4LogicalVolume*, G4String);
};

#endif

