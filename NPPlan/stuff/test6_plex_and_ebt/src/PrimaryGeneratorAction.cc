#include "PrimaryGeneratorAction.hh"

#include "G4RunManager.hh"
//#include "G4LogicalVolumeStore.hh"
//#include "G4LogicalVolume.hh"
//#include "G4Box.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction()
 : G4VUserPrimaryGeneratorAction(),
   fParticleGun(0)
{
  G4int nofParticles = 1;
  fParticleGun = new G4ParticleGun(nofParticles);

  // default particle kinematic
  //
  G4ParticleDefinition* particleDefinition 
    = G4ParticleTable::GetParticleTable()->FindParticle("e-");
  fParticleGun->SetParticleDefinition(particleDefinition);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));
  fParticleGun->SetParticleEnergy(50.*MeV);
}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  //G4RandGauss
  G4double randRad = G4RandFlat::shoot();
  G4double randAngle = G4RandFlat::shoot();

  //G4cout<<"randRad, randAngle "<<randRad<<" "<<randAngle<<G4endl;
  
  randRad *= 2;
  randRad -= 1;
  randAngle *= 2*pi;

  G4double x = randRad * cos(randAngle);
  G4double y = randRad * sin(randAngle);

  //G4cout<<"randRad, randAngle 2 "<<randRad<<" "<<randAngle<<G4endl;

  //G4cout<<"Generator X,Y "<<x<<" "<<y<<G4endl;

  x *= cm;
  y *= cm;

  //G4UniformRand();
  fParticleGun
    ->SetParticlePosition(G4ThreeVector(x, y, -5*m));

  fParticleGun->GeneratePrimaryVertex(anEvent);
}