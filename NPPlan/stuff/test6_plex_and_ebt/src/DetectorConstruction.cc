/*
 * Base file for geant projects
 * Detector construction implementation
 * @author: mrxak
 * (c) MRRC, 2013
*/

#include "globals.hh"
#include "DetectorConstruction.hh"
#include "DetectorMessenger.hh"

#include "DetectorSD.hh"
#include "DetectorSDVox.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4PVParameterised.hh"
#include "G4PVDivision.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSEnergyDeposit.hh"
#include "G4PSCellFlux.hh"
#include "G4PSTrackCounter.hh"
#include "G4PSTrackLength.hh"
#include "G4PSDoseDeposit.hh"
#include "G4SDManager.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"

class DetectorMessenger;

DetectorConstruction::DetectorConstruction() : 
  worldSizeXYZ(0)
{
  //fMessenger = new DetectorMessenger(this);
}

DetectorConstruction::~DetectorConstruction() {

}

void DetectorConstruction::DefineMaterials() {
  nistManager = G4NistManager::Instance();

  nistManager->FindOrBuildMaterial("G4_AIR");

  G4cout << *(G4Material::GetMaterialTable()) << G4endl;
}

G4VPhysicalVolume* DetectorConstruction::Construct() {
  DefineMaterials();

  return DefineVolumes();
}

void DetectorConstruction::setWorldSize(G4double newWorldSize) {
  G4cout<<"Set new world size "<<newWorldSize<<G4endl;
  worldSizeXYZ = newWorldSize;
  solidWorld->SetXHalfLength(newWorldSize / 2.0);
  solidWorld->SetYHalfLength(newWorldSize / 2.0);
  solidWorld->SetZHalfLength(newWorldSize / 2.0);
  logicWorld->SetSolid(solidWorld);
  physWorld->SetLogicalVolume(logicWorld);
  //delete physWorld;
  //DefineVolumes();
  
  G4RunManager::GetRunManager()->DefineWorldVolume(physWorld);
  G4RunManager::GetRunManager()->GeometryHasBeenModified();
  //G4RunManager::GetRunManager()->ResetNavigator();
  G4RunManager::GetRunManager()->Initialize();
}

G4VPhysicalVolume* DetectorConstruction::DefineVolumes() {
  if (0.0 == worldSizeXYZ) {
    worldSizeXYZ = 10 * m;
  }

  G4Material *air = nistManager->FindOrBuildMaterial("G4_AIR");
  G4Material *plex = nistManager->FindOrBuildMaterial("G4_PLEXIGLASS");
  G4Material *polys = nistManager->FindOrBuildMaterial("G4_POLYSTYRENE");

  G4double z, a, density;
  G4String name, symbol;
  elC = new G4Element( name = "Carbon",
                                   symbol = "C",
                                   z = 6.0, a = 12.011 * g/mole );
    elH = new G4Element( name = "Hydrogen",
                                   symbol = "H",
                                   z = 1.0, a = 1.008  * g/mole );
    elN = new G4Element( name = "Nitrogen",
                                   symbol = "N",
                                   z = 7.0, a = 14.007 * g/mole );
    elO = new G4Element( name = "Oxygen",
                                   symbol = "O",
                                   z = 8.0, a = 16.00  * g/mole );
    elNa = new G4Element( name = "Sodium",
                                    symbol = "Na",
                                    z= 11.0, a = 22.98977* g/mole );
    elS = new G4Element( name = "Sulfur",
                                   symbol = "S",
                                   z = 16.0,a = 32.065* g/mole );
    elLi
      = new G4Element( name = "Lithium",
                                   symbol = "Li",
                                   z = 3.0,a = 6.941* g/mole );
    elK
      = new G4Element( name = "Kalium",
                                   symbol = "K",
                                   z = 19.0,a = 39.098* g/mole );
    elCl
      = new G4Element( name = "Chlorum",
                                   symbol = "Li",
                                   z = 17.0,a = 35.242* g/mole );
    elBr = new G4Element( name = "Bromum",
                                   symbol = "Br",
                                   z = 35.0,a = 79.904* g/mole );

    G4int numberofElements;

    /*
    ����	���������, �/��3	������, ���� %
		                 H	  Li	  C    	N	  O	    Na	  S  	Cl 	K	   Br
���������� ��������	1,35	36,4	0	45,5	0	18,2	0	0	0	0	0
������� ����	1,2	57,1	0	33,3	0	9,5	0	0	0	0	0
������������� ����	1,2	56,9	0,9	25,7	0	15,6	0	0	0,9	0	0
�������� ����	 1,2  58,3	0,8	 29,6	 0,1	10,7	0	    0	  0,3	0,1	  0,1
���������� ��������	1,35	36,4	0	45,5	0	18,2	0	0	0	0	0
����� ������		40,85	0,1	42,37	0,01	16,59	0	0	0,04	0,01	0,01
*/

    l1m = new G4Material( "Layer1Material",
                          1.350*mg/cm3,
                          numberofElements = 3 );
    l1m->AddElement(elH, 0.364);
    l1m->AddElement(elC, 0.455);
    l1m->AddElement(elO, 0.182);

    l2m = new G4Material( "Layer2Material",
                          1.200*mg/cm3,
                          numberofElements = 3 );
    l2m->AddElement(elH, 0.571);
    l2m->AddElement(elC, 0.334);
    l2m->AddElement(elO, 0.095);

    l3m = new G4Material( "Layer3Material",
                          1.200*mg/cm3,
                          numberofElements = 5 );
    l3m->AddElement(elH, 0.569);
    l3m->AddElement(elLi, 0.009);
    l3m->AddElement(elC, 0.257);
    l3m->AddElement(elO, 0.156);
    l3m->AddElement(elCl, 0.009);

    l4m = new G4Material( "Layer4Material",
                          1.200*mg/cm3,
                          numberofElements = 8 );
    l4m->AddElement(elH, 0.583);
    l4m->AddElement(elLi, 0.008);
    l4m->AddElement(elC, 0.296);
    l4m->AddElement(elN, 0.001);
    l4m->AddElement(elO, 0.107);
    //l4m->AddElement(elNa, 0.156);
    //l4m->AddElement(elS, 0.156);
    l4m->AddElement(elCl, 0.003);
    l4m->AddElement(elK, 0.001);
    l4m->AddElement(elBr, 0.001);

    l5m = new G4Material( "Layer5Material",
                          1.35*mg/cm3,
                          numberofElements = 3 );
    l5m->AddElement(elH, 0.364);
    //l5m->AddElement(elLi, 0.008);
    l5m->AddElement(elC, 0.455);
    //l5m->AddElement(elN, 0.001);
    l5m->AddElement(elO, 0.182);
    //l5m->AddElement(elNa, 0.156);
    //l5m->AddElement(elS, 0.156);
    //l5m->AddElement(elCl, 0.003);
    //l5m->AddElement(elK, 0.001);
    //l5m->AddElement(elBr, 0.001);

  G4bool checkOverlaps = true;

  G4cout <<"World size: "<<G4BestUnit(worldSizeXYZ, "Length")<<G4endl;

  solidWorld =    
    new G4Box("World",
       0.5*worldSizeXYZ, 0.5*worldSizeXYZ, 0.5*worldSizeXYZ);

  logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        air,           //its material
                        "World");            //its name

  physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "World",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      checkOverlaps);        //overlaps checking

  G4double glassSizeX = 7*cm;
  G4double glassSizeY = 7.5*cm;
  G4double glassSizeZ = 0.5*cm;
  G4double numOfLayers = 50;

  G4VSolid* openBox 
    = new G4Box("openBox",           // its name
    glassSizeX/2, glassSizeY/2, (glassSizeZ*numOfLayers)/2); // its size

  G4LogicalVolume* openBoxLV
    = new G4LogicalVolume(
                 openBox,           // its solid
                 air,  // its material
                 "openBoxLogic");         // its name

    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      openBoxLV,            //its logical volume
                      "openBoxPlacement",               //its name
                      logicWorld,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      checkOverlaps);        //overlaps checking

  G4VSolid* layerS 
    = new G4Box("glassBox",           // its name
    glassSizeX/2, glassSizeY/2, glassSizeZ/2); // its size
                         
  G4LogicalVolume* layerLV
    = new G4LogicalVolume(
                 layerS,           // its solid
                 polys,  // its material
                 "glassBoxLogic");         // its name

  new G4PVReplica(
                 "glassBoxReplica",          // its name
                 layerLV,          // its logical volume
                 openBoxLV,          // its mother
                 kZAxis,           // axis of replication
                 numOfLayers,        // number of replica
                 glassSizeZ);  // witdth of replica

  G4double l1Z = 50e-3*mm;
  G4double l2Z = 25e-3*mm;
  G4double l3Z = 5e-3*mm;
  G4double l4Z = 30e-3*mm;
  G4double l5Z = 175e-3*mm;
  G4double fullL = l1Z + l2Z + l3Z + l4Z + l5Z;

  G4VSolid* filmBox 
    = new G4Box("filmBox",           // its name
    glassSizeX/2, glassSizeY/2, fullL/2); // its size

  G4LogicalVolume* filmBoxLV
    = new G4LogicalVolume(
                 filmBox,           // its solid
                 air,  // its material
                 "filmBoxLogic");         // its name

  addSingleFilm(filmBoxLV);

  new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(0, 0, (glassSizeZ*numOfLayers)/2 + fullL),       //at (0,0,0)
                      filmBoxLV,            //its logical volume
                      "filmBoxPlacement",               //its name
                      logicWorld,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      checkOverlaps);        //overlaps checking
  //addGeometry(logicWorld);
  return physWorld;
}

void DetectorConstruction::addGeometry(G4LogicalVolume* parent) {
  G4Material* plex = nistManager->FindOrBuildMaterial("G4_PLEXIGLASS");
  /*
    G4Box* solidPlexLayer = new G4Box("PlexLayer",                         //its name
        16.4*cm, 11.25*cm, 2.75*mm); //its size
      
    G4LogicalVolume* logicPlexLayer =                         
    new G4LogicalVolume(solidPlexLayer,            //its solid
                        plex,              //its material
                        "PlexLayerLV");              //its name
          
    new G4PVPlacement(0,                     //no rotation
                    G4ThreeVector(0, 0, -21.275*cm),  //at (0,0,0)
                    logicPlexLayer,                //its logical volume
                    "PlexLayer",                   //its name
                    parent,              //its mother  volume
                    false,                   //no boolean operation
                     0,                     //copy number
                     true);        //overlaps checking
                     */
    //setDetectors(logicPlexLayer);
}

void DetectorConstruction::setDetectors(G4LogicalVolume* activeLayer) {
  /*G4SDManager::GetSDMpointer()->SetVerboseLevel(1);
  G4MultiFunctionalDetector* absDetector 
    = new G4MultiFunctionalDetector("Absorber");

  G4VPrimitiveScorer* primitive;
  primitive = new G4PSEnergyDeposit("Edep");
  absDetector->RegisterPrimitive(primitive);
  primitive = new G4PSDoseDeposit("DDep");
  absDetector->RegisterPrimitive(primitive);
  primitive = new G4PSCellFlux("CellFlux");
  absDetector->RegisterPrimitive(primitive);
  primitive = new G4PSTrackLength("TrackLength");
  absDetector->RegisterPrimitive(primitive);
  //primitive = new G4PSTrackCounter("TrackCounter");
  //absDetector->RegisterPrimitive(primitive);

  //SetSensitiveDetector("PlexLayerLV",absDetector);
  activeLayer->SetSensitiveDetector(absDetector);


  G4SDManager::GetSDMpointer()->AddNewDetector(absDetector);/**/
  /**/DetectorSD *test = new DetectorSD("testdet");
  activeLayer->SetSensitiveDetector(test);
  G4SDManager::GetSDMpointer()->AddNewDetector(test);/**/
}

void DetectorConstruction::addSingleFilm(G4LogicalVolume *parent) {
  G4double glassSizeX = 7*cm;
  G4double glassSizeY = 7.5*cm;
  G4double l1Z = 50e-3*mm;
  G4double l2Z = 25e-3*mm;
  G4double l3Z = 5e-3*mm;
  G4double l4Z = 30e-3*mm;
  G4double l5Z = 175e-3*mm;
  G4double fullL = l1Z + l2Z + l3Z + l4Z + l5Z;

  addSingleFilmPart(parent, l1m, (-1.0*fullL + l1Z)/2, l1Z, "l1");
  addSingleFilmPart(parent, l2m, (-1.0*fullL + l1Z*2 + l2Z)/2, l2Z, "l2");
  addSingleFilmPart(parent, l3m, (-1.0*fullL + l1Z*2 + l2Z*2 + l3Z)/2, l3Z, "l3");
  G4LogicalVolume* activeLayer = 
    addSingleFilmPart(parent, l4m, (-1.0*fullL + l1Z*2 + l2Z*2 + l3Z*2 + l4Z)/2, l4Z, "l4");
  addSingleFilmPart(parent, l5m, (-1.0*fullL + l1Z*2 + l2Z*2 + l3Z*2 + l4Z*2 + l5Z)/2, l5Z, "l5");

  //setDetectors(activeLayer);
  //G4SDManager::GetSDMpointer()->AddNewDetector(detLog);
  addVoxelsLayer(activeLayer, G4ThreeVector(0, 0, 0), "det1", l4m, glassSizeX/4, glassSizeY/2, l4Z/2, 70);
}

G4LogicalVolume * DetectorConstruction::addSingleFilmPart(G4LogicalVolume *parent, G4Material* mat, G4double zPos, G4double zSize, G4String name) {
  G4double glassSizeX = 7*cm;
  G4double glassSizeY = 7.5*cm;
  G4VSolid* filmBox 
    = new G4Box("filmSingle_"+name,           // its name
    glassSizeX/2, glassSizeY/2, zSize / 2); // its size

  G4LogicalVolume* filmBoxLV
    = new G4LogicalVolume(
                 filmBox,           // its solid
                 mat,  // its material
                 "filmSingleLogic_"+name);         // its name

  new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(0, 0, zPos),       //at (0,0,0)
                      filmBoxLV,            //its logical volume
                      "filmSingleLVPlacement_"+name,               //its name
                      parent,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      true);        //overlaps checking

  return filmBoxLV;
}

G4LogicalVolume* DetectorConstruction::addZVoxelsLayer(G4LogicalVolume* parentVolume, 
										  G4ThreeVector position, 
										  G4String lName, 
										  G4Material* material,
										  G4double blX,
										  G4double blY,
										  G4double blZ,
										  G4int qLayers) 
{
	//G4Material* water = nistMan->FindOrBuildMaterial("G4_WATER");

	// Voxels container
	G4Box* solidBox =    
    new G4Box(lName+"BoxS",                         //its name
        blX, blY, blZ); //its size
      
	G4LogicalVolume* logicBox =                         
		new G4LogicalVolume(solidBox,            //its solid
							material,              //its material
							lName+"BoxL");              //its name
          
    new G4PVPlacement(0,                     //no rotation
                    position,  //at (0,0,0)
                    logicBox,                //its logical volume
                    lName+"BoxP",                   //its name
                    parentVolume,              //its mother  volume
                    false,                   //no boolean operation
                     0,                     //copy number
                     true);        //overlaps checking

	// Voxels replica
	G4VSolid* layerX =
		new G4Box(lName+"layerZ", blX, blY, blZ / qLayers);
    
	G4LogicalVolume* logicLayer =
		new G4LogicalVolume(layerX, material, lName+"layerZL");
   
    new G4PVReplica(lName+"layerZR", logicLayer, 
                 logicBox, kZAxis, qLayers, 2 * blZ / qLayers);  

	G4LogicalVolume* det_log;
	G4Box* det_box = new G4Box(lName+"detector", blX, blY, blZ / qLayers);
    det_log = new G4LogicalVolume(det_box, material, lName+"detectorL");
    G4VPhysicalVolume* det_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), det_log, lName+"detectorP", logicLayer, false, 0);

	DetectorSDVox* vox1 = new DetectorSDVox(lName+"vox", qLayers);
	//sdMan->AddNewDetector(vox1);
	det_log->SetSensitiveDetector(vox1);
  //logicBox->SetSensitiveDetector(vox1);
  G4SDManager::GetSDMpointer()->AddNewDetector(vox1);
	return det_log;
}

G4LogicalVolume* DetectorConstruction::addVoxelsLayer(G4LogicalVolume* parentVolume, 
										  G4ThreeVector position, 
										  G4String lName, 
										  G4Material* material,
										  G4double blX,
										  G4double blY,
										  G4double blZ,
										  G4int qLayers) 
{
	//G4Material* water = nistMan->FindOrBuildMaterial("G4_WATER");

	// Voxels container
	G4Box* solidBox =    
    new G4Box(lName+"BoxS",                         //its name
        blX, blY, blZ); //its size
      
	G4LogicalVolume* logicBox =                         
		new G4LogicalVolume(solidBox,            //its solid
							material,              //its material
							lName+"BoxL");              //its name
          
    new G4PVPlacement(0,                     //no rotation
                    position,  //at (0,0,0)
                    logicBox,                //its logical volume
                    lName+"BoxP",                   //its name
                    parentVolume,              //its mother  volume
                    false,                   //no boolean operation
                     0,                     //copy number
                     true);        //overlaps checking

	// Voxels replica
	G4VSolid* layerX =
		new G4Box(lName+"layerX", blX, blY / qLayers, blZ);
    
	G4LogicalVolume* logicLayer =
		new G4LogicalVolume(layerX, material, lName+"layerXL");
   
    new G4PVReplica(lName+"layerXR", logicLayer, 
                 logicBox, kYAxis, qLayers, 2 * blY / qLayers);  


	G4VSolid* layersY =
		new G4Box(lName+"layerY", blX / qLayers, blY / qLayers, blZ);
    G4LogicalVolume* logicLayerY =
		new G4LogicalVolume(layerX, material, lName+"layerYL");
   
    new G4PVReplica(lName+"layerYR", logicLayerY, 
                 logicLayer, kXAxis, qLayers, 2 * blX / qLayers);  

	G4LogicalVolume* det_log;
    G4Box* det_box = new G4Box(lName+"detector", blX / qLayers, blY / qLayers, blZ);
    det_log = new G4LogicalVolume(det_box, material, lName+"detectorL");
    G4VPhysicalVolume* det_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), det_log, lName+"detectorP", logicLayerY, false, 0);

	DetectorSDVox* vox1 = new DetectorSDVox(lName+"vox", qLayers);
//sdMan->AddNewDetector(vox1);
  G4SDManager::GetSDMpointer()->AddNewDetector(vox1);
	det_log->SetSensitiveDetector(vox1);

	return logicBox;
}