/*
 * Base file for geant projects
 * Detector messenger implementation
 * @author: mrxak
 * (c) MRRC, 2013
*/

#include "DetectorMessenger.hh"
#include "DetectorConstruction.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"

DetectorMessenger::DetectorMessenger(DetectorConstruction* Det)
 : G4UImessenger(),
   fDetectorConstruction(Det)
{
  topDirectory = new G4UIdirectory("/test2/");
  topDirectory->SetGuidance("UI commands specific to this example.");

  dDetDirectory = new G4UIdirectory("/test2/det/");
  dDetDirectory->SetGuidance("Detector construction control");

  fWorldSizeXYZ = new G4UIcmdWithADoubleAndUnit("/test2/det/worldSize",this);
  fWorldSizeXYZ->SetGuidance("Define a world size");
  fWorldSizeXYZ->SetParameterName("worldSize", true);   // can be ommitted 
  fWorldSizeXYZ->SetUnitCategory("Length");
  fWorldSizeXYZ->AvailableForStates(G4State_Idle);
}

DetectorMessenger::~DetectorMessenger()
{
  delete fWorldSizeXYZ;
  delete dDetDirectory;
  delete topDirectory;
}

void DetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  G4cout<<"Set New Value "<<command<<" "<<"newValue"<<G4endl;
  if( command == fWorldSizeXYZ ) {
    G4cout<<"Got command for new world size"<<G4endl;
    fDetectorConstruction
      ->setWorldSize(fWorldSizeXYZ->GetNewDoubleValue(newValue));
  }   
}