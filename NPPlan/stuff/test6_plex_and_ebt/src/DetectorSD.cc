#include "DetectorSD.hh"
//#include "RunAction.hh"
#include "DetectorConstruction.hh"
#include "DetectorSDMessenger.hh"

DetectorSD::DetectorSD(G4String name) : G4VSensitiveDetector(name), outFileName("")
{
  fMessenger = new DetectorSDMessenger(this);

}

DetectorSD::~DetectorSD()
{
}

void DetectorSD::Initialize(G4HCofThisEvent*) 
{

}

G4bool DetectorSD::ProcessHits(G4Step* dStep, G4TouchableHistory* dHistory) 
{
  G4cout<<"Outfile: "<<outFileName<<G4endl;
  G4cout<<dStep->GetPreStepPoint()->GetKineticEnergy()<<G4endl;

  return true;
}

void DetectorSD::EndOfEvent(G4HCofThisEvent*) 
{

}