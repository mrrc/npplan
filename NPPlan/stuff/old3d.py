# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 21.12.12
@summary: 
'''

# -*- coding: utf8 -*-
"""@summary:
Модуль позиционирования
2 базовых класса -
posPrefPanel - настройки позиционирования (выбор параметров коллиматора, количество пучков, РИП и т. п.)
positionPanel - непосредственно позиционирование (внешнее окно с кнопкой сохранения и выводом параметров о текущем
расположении (поворот, косинусы, центр опухоли))
"""
import wx
import logic.patients as patient
import logic.config as params
import os
from vtk.wx.wxVTKRenderWindowInteractor import *
from vtk.util.colors import chocolate, mint
import vtk
import wx.lib.platebtn as platebtn
import logic.organs as organs
try:
    from agw import genericmessagedialog as GMD
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.genericmessagedialog as GMD

class posPrefPanel(wx.Panel):
    """
    Панель настроек позиционирования
    """
    def __init__(self, parent, **kwargs):
        """
        @keyword restore: флаг восстановления
        @type restore: boolean
        """
        wx.Panel.__init__(self, parent, style=0)
        self.pat = patient.patientInfo()
        self.restore = kwargs.get('restore', False)
        self.SetBackgroundColour(wx.WHITE)

    def draw(self, **kwargs):
        """
        Функция отрисовки панели
        @keyword restore: флаг восстановления
        @type restore: boolean
        """
        self.restore = kwargs.get('restore', False)
        sizer = wx.BoxSizer(wx.VERTICAL)
        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSave = wx.NewId()
        btnSizer.Add(platebtn.PlateButton(self, btnSave, _(' btn save'), wx.Bitmap('images/iconsave.png', wx.BITMAP_TYPE_PNG)))
        self.Bind(wx.EVT_BUTTON, self.save, id=btnSave)
        sizer.Add(btnSizer)
        gSizer = wx.FlexGridSizer(cols=2, hgap=6, vgap=6)
        beamTypeText = wx.StaticText(self, wx.NewId(), _(' Type:'))
        shapeText = wx.StaticText(self, wx.NewId(), _(' shape:'))
        sizeText = wx.StaticText(self, wx.NewId(), _(' size:'))
        beamCountText = wx.StaticText(self, wx.NewId(), _(' Beam count:'))
        beam = params.getBeamParams()
        self.beam = beam
        beamList = []
        self.shapes = {}
        self.sizes = {}
        for i in beam:
            #print i
            beamList.append(i['name'])
            self.shapes[i['name']] = []
            self.sizes[i['name']] = {}
            for j in i['colmats']:
                self.shapes[i['name']].append(j['name'])
                self.sizes[i['name']][j['name']] = j['sizes']

        self.beamTypeField = wx.ComboBox(self, wx.NewId(), beamList[0],
                         size=(500, -1), choices=beamList,
                         style=wx.CB_DROPDOWN
                         | wx.TE_PROCESS_ENTER
                         | wx.CB_READONLY
                         #| wx.CB_SORT
                         )
        self.beamShape = wx.ComboBox(self, wx.NewId(),'',
                         size=(500, -1), choices=[],
                         style=wx.CB_DROPDOWN
                         | wx.TE_PROCESS_ENTER
                         | wx.CB_READONLY
                         #| wx.CB_SORT
                         )
        self.beamSize = wx.ComboBox(self, wx.NewId(), '',
                         size=(500, -1), choices=[],
                         style=wx.CB_DROPDOWN
                         | wx.TE_PROCESS_ENTER
                         | wx.CB_READONLY
                         #| wx.CB_SORT
                         )
        self.evtBeamType(0)
        self.Bind(wx.EVT_COMBOBOX, self.evtBeamType, self.beamTypeField)
        self.Bind(wx.EVT_COMBOBOX, self.evtShapeType, self.beamShape)
        sourceSkinDistanceText = wx.StaticText(self, wx.NewId(), _(' Source-skin distance (mm):'))
        self.sourceSkinDistanceField = wx.TextCtrl(self, wx.NewId(), size=(500, 20))
        self.sourceSkinDistanceField.SetValue('100')
        self.beamCount = wx.ComboBox(self, wx.NewId(), '1',
                         size=(500, -1), choices=['1','2'],
                         style=wx.CB_DROPDOWN
                         | wx.TE_PROCESS_ENTER
                         | wx.CB_READONLY
                         #| wx.CB_SORT
                         )
        gSizer.AddMany([beamTypeText, self.beamTypeField,
                        shapeText, self.beamShape,
                        sizeText, self.beamSize,
                        sourceSkinDistanceText, self.sourceSkinDistanceField,
                        beamCountText, self.beamCount])
        sizer.Add(gSizer)
        self.SetSizer(sizer)
        self.SendSizeEvent()
        self.Refresh()
        if (self.restore):
            self.setField(self.pat.getColmatPref())
        params.log('Position settings panel created')

    def evtShapeType(self, event):
        shape = self.beamShape.GetStringSelection()
        self.beamSize.Clear()
        self.beamSize.AppendItems(self.sizes[self.beamTypeField.GetStringSelection()][shape])
        self.beamSize.SetSelection(0)
        pass

    def setField(self, obj):
        """
        Функция восстановление параметров
        @see: collect
        @param obj: объект для восстановления
        @type obj: object
        """
        params.log('Restoring old position settings')
        params.log(obj)
        self.beamTypeField.SetStringSelection(obj['beam'])
        self.evtBeamType(0)
        self.beamShape.SetStringSelection(obj['collimatorShape'])
        self.evtShapeType(0)
        self.beamSize.SetStringSelection(obj['collimatorSize'])
        self.sourceSkinDistanceField.SetValue(obj['sourceSkinDistance'])
        self.beamCount.SetStringSelection(obj['beamCount'])
        pass

    def evtBeamType(self, event):
        name = self.beamTypeField.GetStringSelection()
        self.beamShape.Clear()
        self.beamSize.Clear()
        self.beamShape.AppendItems(self.shapes[name])
        self.beamShape.SetSelection(0)
        self.beamSize.AppendItems(self.sizes[name][self.shapes[name][0]])
        self.beamSize.SetSelection(0)
        pass

    def save(self, event):
        if (1 == wx.GetApp().GetTopWindow().GetState()):
            dlg = GMD.GenericMessageDialog(self,
                                            _('You can\'t save during view mode'),
                                            _('Saving in view mode'),
                                            GMD.GMD_USE_GRADIENTBUTTONS | wx.OK | wx.ICON_ERROR
                                            )
            result = dlg.ShowModal()
            dlg.Destroy()
            params.log('Saving in view mode attempt (from positioning).')
            return
        c = self.collect()
        self.pat.setCurrentPatient({'colmatPref' : c,
                                    'status' : 3})
        params.log('New position settings: ')
        params.log(c)
        pass

    def collect(self):
        """ Функция-сборщик
        @return: object {'beam' : название ускорителя (NG, KG), 'collimatorShape' : тип коллиматора, 'collimatorSize' : размер коллиматора, 'sourceSkinDistance' : расстояние источник-пациент (РИП), 'beamCount' : количество пучков}
        @rtype: object
        """
        return {'beam' : self.beamTypeField.GetStringSelection(),
                'collimatorShape' : self.beamShape.GetStringSelection(),
                'collimatorSize' : self.beamSize.GetStringSelection(),
                'sourceSkinDistance' : self.sourceSkinDistanceField.GetValue(),
                'beamCount' : self.beamCount.GetStringSelection()}
        pass


class positionPanel(wx.Panel):
    """
    Верхняя панель позиционирования. Отрисовывает нижнюю панель, кнопку сохранения, флаги прозрачности и режима вращения,
    количество пучков (если надо) и сообщения о положениях центров пучка, опухоли, текущем положении на срезах в подокнах и т. п.
    """
    def __init__(self, parent, **kwargs):
        wx.Panel.__init__(self, parent, style=0)
        self.pat = patient.patientInfo()
        self.SetBackgroundColour(wx.WHITE)

    def draw(self, **kwargs):
        """
        @keyword restore: флаг восстановления
        @type restore: boolean
        """
        sizer = wx.BoxSizer(wx.VERTICAL)
        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSave = wx.NewId()
        btnSizer.Add(platebtn.PlateButton(self, btnSave, _(' btn save'), wx.Bitmap('images/iconsave.png', wx.BITMAP_TYPE_PNG)))
        self.Bind(wx.EVT_BUTTON, self.save, id=btnSave)
        sizer.Add(btnSizer)
        lrsizer = wx.BoxSizer(wx.HORIZONTAL)
        infoSizer = wx.BoxSizer(wx.VERTICAL)
        #self.opacityChanger = wx.CheckBox(self, -1, _('Change opacity'))
        opacityLabel = wx.StaticText(self, wx.NewId(), _('Change opacity'))
        self.slider = wx.Slider(
            self, wx.NewId(), 100, 0, 100, size=(120, -1),
            style=wx.SL_HORIZONTAL | wx.SL_AUTOTICKS | wx.SL_LABELS
            )
        self.slider.SetTickFreq(20, 1)
        self.Bind(wx.EVT_SCROLL, self.sliderChange, self.slider)

        infoSizer.Add(opacityLabel)
        infoSizer.Add(self.slider, 0, wx.ALIGN_RIGHT)
        if (int(self.pat.getColmatPref()['beamCount'])>1):
            infoSizer.Add(wx.StaticText(self, wx.NewId(), _('Current beam:')))
            self.curBeam = wx.ComboBox(self, wx.NewId(), '1',
                             size=(50, -1), choices=['1','2'],
                             style=wx.CB_DROPDOWN
                             | wx.TE_PROCESS_ENTER
                             | wx.CB_READONLY
                             #| wx.CB_SORT
                             )
            infoSizer.Add(self.curBeam, 0, wx.ALIGN_RIGHT)
        self.beamMode = wx.CheckBox(self, -1, _('Rotate beam'))
        self.Bind(wx.EVT_CHECKBOX, self.changeMode, self.beamMode)
        infoSizer.Add(self.beamMode)
        infoSizer.Add(wx.StaticText(self, wx.NewId(), _('Tumour center:')))
        self.tmCenterLabel = wx.StaticText(self, wx.NewId(), ('(xx, yy, zz)'))
        infoSizer.Add(self.tmCenterLabel, 0, wx.ALIGN_RIGHT)
        infoSizer.Add(wx.StaticText(self, wx.NewId(), _('Cosines:')))
        self.cosinesLabel = wx.StaticText(self, wx.NewId(), ('(xx, xy, xz)\n(yx,yy,yz)\n(zx,zy,zz)'))
        infoSizer.Add(self.cosinesLabel, 0, wx.ALIGN_RIGHT)
        infoSizer.Add(wx.StaticText(self, wx.NewId(), _('Beam center:')))
        self.beamLabel = wx.StaticText(self, wx.NewId(), ('(xx, yy, zz)'))
        infoSizer.Add(self.beamLabel, 0, wx.ALIGN_RIGHT)
        infoSizer.Add(wx.StaticText(self, wx.NewId(), _('Distances from tumour center')))
        infoSizer.Add(wx.StaticText(self, wx.NewId(), _('Axial distance:')))
        self.axialDistanceLabel = wx.StaticText(self, wx.NewId(), ('0.00 mm'))
        infoSizer.Add(self.axialDistanceLabel, 0, wx.ALIGN_RIGHT)
        infoSizer.Add(wx.StaticText(self, wx.NewId(), _('Sagittal distance:')))
        self.sagittalDistanceLabel = wx.StaticText(self, wx.NewId(), ('0.00 mm'))
        infoSizer.Add(self.sagittalDistanceLabel, 0, wx.ALIGN_RIGHT)
        infoSizer.Add(wx.StaticText(self, wx.NewId(), _('Coronal distance:')))
        self.coronalDistanceLabel = wx.StaticText(self, wx.NewId(), ('0.00 mm'))
        infoSizer.Add(self.coronalDistanceLabel, 0, wx.ALIGN_RIGHT)
        self.organsLabel = wx.StaticText(self, wx.NewId(), _('Organs (use opacity flag to look inside):'))
        infoSizer.Add(self.organsLabel)
        org = organs.organs()
        for i in org.getOrganList():
            orgLabel = wx.StaticText(self, wx.NewId(), i['rusname'])
            orgLabel.SetFont(wx.Font(12, wx.SWISS, wx.NORMAL, wx.NORMAL))
            orgLabel.SetBackgroundColour(i['color']['colorTuple'])
            orgLabel.SetForegroundColour((0, 0, 0))
            infoSizer.Add(orgLabel, 0, wx.ALIGN_RIGHT)
        self.vtkWin = vtkWindowPanel(self)
        lrsizer.Add(self.vtkWin)
        lrsizer.Add(infoSizer)
        sizer.Add(lrsizer)
        self.SetSizer(sizer)
        self.SendSizeEvent()
        self.Refresh()
        #self.Bind(wx.EVT_CHECKBOX, self.changeOpacity, self.opacityChanger)
        if (int(self.pat.getColmatPref()['beamCount'])>1):
            self.Bind(wx.EVT_COMBOBOX, self.changeBeam, self.curBeam)
        if (kwargs.get('restore', False)):
            self.vtkWin.space.setField(self.pat.getPosPref())
        params.log('Position panel created')

    def updateDistance(self, type, val):
        """
        Устанавливает надпись о текущем положении среза
        @param type: [Axial, Coronal, Sagittal] - какой срез
        @type type: string
        @param val: значение
        @type val: float
        """
        if 'Axial' == type:
            self.axialDistanceLabel.SetLabel(_('%2.2f mm') %val)
        elif 'Coronal' == type:
            self.coronalDistanceLabel.SetLabel(_('%2.2f mm') %val)
        elif 'Sagittal' == type:
            self.sagittalDistanceLabel.SetLabel(_('%2.2f mm') %val)
        params.log('Updating %s distance to %2.2f' %(type, val))

    def changeMode(self, event):
        """
        @see: SpaceWin.changeMode()
        """
        if (1 == event.GetSelection()):
            self.vtkWin.space.changeMode(1)
        else:
            self.vtkWin.space.changeMode(0)

    def changeBeam(self, event):
        print 'selected %d' %event.GetSelection()
        self.vtkWin.space.setCurBeam(event.GetSelection()+1)

    def sliderChange(self, event):
        #print self.slider.GetValue()
        self.vtkWin.space.changeOpacity(float(self.slider.GetValue())/100)

    def save(self, event):
        """
        Функция сохранения
        @see: SpaceWin.collect()
        """
        if (1 == wx.GetApp().GetTopWindow().GetState()):
            dlg = GMD.GenericMessageDialog(self,
                                            _('You can\'t save during view mode'),
                                            _('Saving in view mode'),
                                            GMD.GMD_USE_GRADIENTBUTTONS | wx.OK | wx.ICON_ERROR
                                            )
            result = dlg.ShowModal()
            dlg.Destroy()
            return
        c = self.vtkWin.space.collect()
        self.pat.setCurrentPatient({'posPref' : c,
                                    'status' : 4})
        params.log('New position settings:')
        params.log(c)
        #print c

class vtkWindowPanel(wx.Panel):
    """
    Нижняя панель позиционирования
    """
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.NewId(), style=0)
        self.pat = self.GetParent().pat
        #print self.pat.getId()
        wSpace = wxVTKRenderWindowInteractor(self, -1, size=(parent.GetSize()[1]-40, parent.GetSize()[1]-40), name='SpaceWin')
        self.space = SpaceWin(self, wSpace)
        self.Show()

    def updateDistance(self, type, val):
        """
        @see: positionPanel.updateDistance()
        """
        self.GetParent().updateDistance(type, val)

    def setCosinesLabel(self, txt):
        self.GetParent().cosinesLabel.SetLabel(txt)

    def setTmCenterLabel(self, txt):
        """
        Установка надписи о положении центра опухоли
        @param txt: рекомендуемый формат - '%2.2f %2.2f %2.2f'
        @type txt: string
        """
        self.GetParent().tmCenterLabel.SetLabel(txt)

    def setBeamLabel(self, txt):
        self.GetParent().beamLabel.SetLabel(txt)


class SpaceWin:
    """
    Окно рендера 3D-изображения и axial/coronal/sagittal-срезов
    """
    def __init__(self, parent, Interactor):
        """
        Окно рендера 3D-картинки
        """
        self.mode = 0
        self.parent = parent
        self.patC = patient.patientInfo()
        self.pat = self.patC.getSinglePatient()
        self.colmatPref = self.patC.getColmatPref()
        if (int(self.patC.getColmatPref()['beamCount'])>1):
            self.curBeam = 1
        else:
            self.curBeam = 0

        self.curAxialSlice = 0
        self.curCoronalSlice = 0
        self.curSagittalSlice = 0

        self._Interactor = Interactor
        self._Interactor.SetInteractorStyle(None)
        self._Interactor.AddObserver("LeftButtonPressEvent", self.ButtonEvent)
        self._Interactor.AddObserver("LeftButtonReleaseEvent", self.ButtonEvent)
        self._Interactor.AddObserver("MiddleButtonPressEvent", self.ButtonEvent)
        self._Interactor.AddObserver("MiddleButtonReleaseEvent", self.ButtonEvent)
        self._Interactor.AddObserver("RightButtonPressEvent", self.ButtonEvent)
        self._Interactor.AddObserver("RightButtonReleaseEvent", self.ButtonEvent)
        self._Interactor.AddObserver("MouseMoveEvent", self.MouseMove)

        self._RenWin = vtk.vtkRenderWindow()

        self._Interactor.SetRenderWindow(self._RenWin)

        self._s = self.pat['pixelSpacing']
#        self._ls = self._s.split('/')
        self._PixelSpacing = float(self._s[0])
        self._SliceSpacing = float(self.pat['sliceSpacing'])
        self._NumberOfSlices = int(self.pat['numberOfSlices'])
#        print self._PixelSpacing, self._SliceSpacing, self._NumberOfSlices

        self._FilePrefix = self.patC.getPatDir() + '/Segmented/slice'

# Calculate tumor center
        self._reader = vtk.vtkPNGReader()
        self._reader.SetFilePrefix(self._FilePrefix)
        self._reader.SetDataSpacing(self._PixelSpacing, self._PixelSpacing, self._SliceSpacing)
        self._reader.SetDataExtent(0, 511, 0, 511, 0, self._NumberOfSlices-1)

        self._threshTumor=vtk.vtkImageThreshold()
        self._threshTumor.SetInput(self._reader.GetOutput())
        self._threshTumor.ThresholdBetween(156,156)
        self._threshTumor.SetInValue(255)
        self._threshTumor.SetOutValue(0)
        self._threshTumor.ReleaseDataFlagOff()

        self._tumorExtractor = vtk.vtkContourFilter()
        self._tumorExtractor.SetInput(self._threshTumor.GetOutput())
        self._tumorExtractor.SetValue(0, 255)

        self._tumorNormals = vtk.vtkPolyDataNormals()
        self._tumorNormals.SetInput(self._tumorExtractor.GetOutput())
        self._tumorNormals.SetFeatureAngle(60.0)

        self._tumorMapper = vtk.vtkPolyDataMapper()
        self._tumorMapper.SetInput(self._tumorNormals.GetOutput())
        self._tumorMapper.ScalarVisibilityOff()

        self._tumorActor = vtk.vtkActor()
        self._tumorActor.SetMapper(self._tumorMapper)
        self._dX, self._dY, self._dZ = self._tumorActor.GetCenter()
        self.tumour = {'tumourCenterX' : self._dX,
                       'tumourCenterY' : self._dY,
                       'tumourCenterZ' : self._dZ}

        self.parent.setTmCenterLabel('%2.2f %2.2f %2.2f' %(self._dX, self._dY, self._dZ))

# Set body origin to tumor center

        self._reader2 = vtk.vtkPNGReader()
        self._reader2.SetFilePrefix(self._FilePrefix)
        self._reader2.SetDataSpacing(self._PixelSpacing, self._PixelSpacing, self._SliceSpacing)
        self._reader2.SetDataExtent(0, 511, 0, 511, 0, self._NumberOfSlices-1)
        self._reader2.SetDataOrigin(-self._dX, -self._dY, -self._dZ)

        self._shrinkFactor = 4
        self._shrink = vtk.vtkImageShrink3D()
        self._shrink.SetShrinkFactors(self._shrinkFactor, self._shrinkFactor, 1)
        self._shrink.SetInput(self._reader2.GetOutput())

# Create and show 3D model
        self._threshSkin=vtk.vtkImageThreshold()
        self._threshSkin.SetInput(self._shrink.GetOutput())
        self._threshSkin.ThresholdBetween(128,128)
        self._threshSkin.SetInValue(255)
        self._threshSkin.SetOutValue(0)
        self._threshSkin.ReleaseDataFlagOff()
#        self._skinExtractor = vtk.vtkContourFilter()
        self._skinExtractor = vtk.vtkMarchingCubes()
        self._skinExtractor.SetInput(self._threshSkin.GetOutput())
        self._skinExtractor.SetValue(0, 128)
        self._dec = vtk.vtkDecimatePro()
        self._dec.SetInput(self._skinExtractor.GetOutput())
        self._dec.SetFeatureAngle(30)
        self._dec.PreserveTopologyOn()
        self._skinNormals = vtk.vtkPolyDataNormals()
        self._skinNormals.SetInput(self._dec.GetOutput())
        self._skinNormals.SetFeatureAngle(70.0)
        self._skinStripper = vtk.vtkStripper()
        self._skinStripper.SetInput(self._skinNormals.GetOutput())
        self._skinMapper = vtk.vtkPolyDataMapper()
        self._skinMapper.SetInput(self._skinStripper.GetOutput())
        self._skinMapper.ScalarVisibilityOff()
        self._skinActor = vtk.vtkActor()
        self._skinActor.SetMapper(self._skinMapper)
#        self._skinActor.GetProperty().SetColor(chocolate)
#        self._skinActor.GetProperty().SetDiffuseColor(1, .49, .25)
        self._skinActor.GetProperty().SetSpecular(.3)
        self._skinActor.GetProperty().SetSpecularPower(20)

        organs = self.patC.getContours()
        #print organs
        params.log('Organs: ')
        params.log(organs)

        k = 0
        self._threshOrgans = []
        self._organsExtractor = []
        self._organsNormals = []
        self._organsMappers = []
        self._organsActors = []
        for i in organs['colors']:
            self._threshOrgans.append(vtk.vtkImageThreshold())
            self._threshOrgans[k].SetInput(self._reader2.GetOutput())
            self._threshOrgans[k].ThresholdBetween(float(organs['colors'][i]),float(organs['colors'][i]))
            self._threshOrgans[k].SetInValue(255)
            self._threshOrgans[k].SetOutValue(0)
            self._threshOrgans[k].ReleaseDataFlagOff()

            self._organsExtractor.append(vtk.vtkContourFilter())
            self._organsExtractor[k].SetInput(self._threshOrgans[k].GetOutput())
            self._organsExtractor[k].SetValue(0, 255)

            self._organsNormals.append(vtk.vtkPolyDataNormals())
            self._organsNormals[k].SetInput(self._organsExtractor[k].GetOutput())
            self._organsNormals[k].SetFeatureAngle(60.0)

            self._organsMappers.append(vtk.vtkPolyDataMapper())
            self._organsMappers[k].SetInput(self._organsNormals[k].GetOutput())
            self._organsMappers[k].ScalarVisibilityOff()

            self._organsActors.append(vtk.vtkActor())
            self._organsActors[k].SetMapper(self._organsMappers[k])
            #print organs['origColors']
            clr = organs['origColors'][str(organs['colors'][i])]
            self._organsActors[k].GetProperty().SetColor(int(clr[0]), int(clr[1]), int(clr[2]))
            self._organsActors[k].GetProperty().SetLineWidth( 2.0 )

            k += 1


        #self._threshTumor2=vtk.vtkImageThreshold()
        #self._threshTumor2.SetInput(self._reader2.GetOutput())
        #self._threshTumor2.ThresholdBetween(156,156)
        #self._threshTumor2.SetInValue(255)
        #self._threshTumor2.SetOutValue(0)
        #self._threshTumor2.ReleaseDataFlagOff()

        #self._tumorExtractor2 = vtk.vtkContourFilter()
        #self._tumorExtractor2.SetInput(self._threshTumor2.GetOutput())
        #self._tumorExtractor2.SetValue(0, 255)

        #self._tumorNormals2 = vtk.vtkPolyDataNormals()
        #self._tumorNormals2.SetInput(self._tumorExtractor2.GetOutput())
        #self._tumorNormals2.SetFeatureAngle(60.0)

        #self._tumorMapper2 = vtk.vtkPolyDataMapper()
        #self._tumorMapper2.SetInput(self._tumorNormals2.GetOutput())
        #self._tumorMapper2.ScalarVisibilityOff()

        #self._tumorActor2 = vtk.vtkActor()
        #self._tumorActor2.SetMapper(self._tumorMapper2)
        #self._tumorActor2.GetProperty().SetColor(255, 0, 255)
        #self._tumorActor2.GetProperty().SetLineWidth( 2.0 )

#        self._skinActor.SetOrigin(self._dX, self._dY, self._dZ)


    #     Add beam
        if self.colmatPref['collimatorShape'] == 'Rectangle':
            s = self.colmatPref['collimatorSize']
            ls = s.split('x')
            self._beam1=vtk.vtkCubeSource()
            self._beam1.SetXLength(300)
            self._beam1.SetYLength(float(ls[0])*10)
            self._beam1.SetZLength(float(ls[1])*10)
        elif self.colmatPref['collimatorShape'] == 'Circle':
            s = self.colmatPref['collimatorSize']
            self._beam1=vtk.vtkCylinderSource()
            self._beam1.SetHeight(300)
            self._beam1.SetRadius (float(s)*10/2)
            self._beam1.SetResolution(18)
            self._beam1.CappingOn()
            if (int(self.patC.getColmatPref()['beamCount'])>1):
                self._beamx1=vtk.vtkCylinderSource()
                self._beamx1.SetHeight(300)
                self._beamx1.SetRadius (float(s)*10/2)
                self._beamx1.SetResolution(18)
                self._beamx1.CappingOn()
        #else:
        #    print 'Bad collimator shape'

#        self._beam1.SetCenter(self._dX, self._dY, self._dZ)

        self._beam1Normals = vtk.vtkPolyDataNormals()
        self._beam1Normals.SetInput(self._beam1.GetOutput())

        self._beam1Mapper = vtk.vtkPolyDataMapper()
        self._beam1Mapper.SetInput(self._beam1Normals.GetOutput())
        self._beam1Actor = vtk.vtkActor()
        self._beam1Actor.SetMapper(self._beam1Mapper)
#        self._beam1Actor.SetOrigin(self._dX,self._dY,self._dZ)
        self._beam1Actor.GetProperty().SetColor(1, 0, 0)

        if (int(self.patC.getColmatPref()['beamCount'])>1):
            self._beamx1Normals = vtk.vtkPolyDataNormals()
            self._beamx1Normals.SetInput(self._beamx1.GetOutput())

            self._beamx1Mapper = vtk.vtkPolyDataMapper()
            self._beamx1Mapper.SetInput(self._beamx1Normals.GetOutput())
            self._beamx1Actor = vtk.vtkActor()
            self._beamx1Actor.SetMapper(self._beamx1Mapper)
    #        self._beam1Actor.SetOrigin(self._dX,self._dY,self._dZ)
            self._beamx1Actor.GetProperty().SetColor(0, 1, 0)

        if self.colmatPref['collimatorShape'] == 'Circle':
            self._beam1Actor.RotateZ(90)
            if (int(self.patC.getColmatPref()['beamCount'])>1):
                self._beamx1Actor.RotateZ(0)

        self._spaceCamera = vtk.vtkCamera()
        self._spaceCamera.SetViewUp(0, 0, 1)
        self._spaceCamera.SetPosition(-500, 0, 0)
        self._spaceCamera.SetFocalPoint(0, 0, 0)
        self._spaceCamera.ParallelProjectionOn()

        self._spaceRen = vtk.vtkRenderer()
        self._spaceRen.AddActor(self._skinActor)
        #self._spaceRen.AddActor(self._tumorActor2)
    #    spaceRen.AddActor(tumorActor)
        self._spaceRen.AddActor(self._beam1Actor)
        self.drawCross()
        self._spaceRen.AddActor(self._crossXActor)
        self._spaceRen.AddActor(self._crossYActor)
        for i in self._organsActors:
            self._spaceRen.AddActor(i)
        if (int(self.patC.getColmatPref()['beamCount'])>1):
            self._spaceRen.AddActor(self._beamx1Actor)
#        self._spaceRen.InteractiveOn()
        self._spaceRen.SetBackground(1, 1, 1)
        self._spaceRen.SetViewport(0.0, 0.5, 0.5, 1.0)
        self._spaceRen.SetActiveCamera(self._spaceCamera)
        self._spaceRen.ResetCamera()

        self._RenWin.AddRenderer(self._spaceRen)
        self._spaceRen.Render()
        self._Interactor.StartPickCallback()

# Beam moving relative to body sections

    #     Add long beam

        self._transBase = vtk.vtkTransform()
        self._transBase.RotateZ(90)

        if self.colmatPref['collimatorShape'] == 'Rectangle':
            s = self.colmatPref['collimatorSize']
            ls = s.split('x')

            self._beam2 = vtk.vtkCubeSource()
            self._beam2.SetYLength(10000)
            self._beam2.SetXLength(float(ls[0])*10)
            self._beam2.SetZLength(float(ls[1])*10)

        elif self.colmatPref['collimatorShape'] == 'Circle':
            s = self.colmatPref['collimatorSize']
            self._beam2=vtk.vtkCylinderSource()
            self._beam2.SetRadius (float(s)*10/2)
            self._beam2.SetHeight (300)
            self._beam2.SetResolution(18)
            if (int(self.patC.getColmatPref()['beamCount'])>1):
                self._beamx2=vtk.vtkCylinderSource()
                self._beamx2.SetRadius (float(s)*10/2)
                self._beamx2.SetHeight (300)
                self._beamx2.SetResolution(18)
        #else:
            #print 'Bad collimator shape'

        self._trans = vtk.vtkTransform()
#        self._trans.SetInput(self._transBase)
#        self._trans.PostMultiply()

        if (int(self.patC.getColmatPref()['beamCount'])>1):
            self._beamx2Transform = vtk.vtkTransformPolyDataFilter()
            self._beamx2Transform.SetInput(self._beamx2.GetOutput())
            if self.colmatPref['collimatorShape'] == 'Circle':
                self._beamx2Transform.SetTransform(self._transBase)
            self._beamx2Transform.SetTransform(self._trans)

        self._beam2Transform = vtk.vtkTransformPolyDataFilter()
        self._beam2Transform.SetInput(self._beam2.GetOutput())
        if self.colmatPref['collimatorShape'] == 'Circle':
            self._beam2Transform.SetTransform(self._transBase)
        self._beam2Transform.SetTransform(self._trans)

        self._body = vtk.vtkImageReslice()
        self._body.SetInput(self._shrink.GetOutput())
        #self._body.SetResliceAxes(self._trans.GetMatrix())
        #self._body.SetResliceTransform(self._trans)
        self._body.AutoCropOutputOn()
#        self._body.SetOutputOrigin(0, 0, 0)
        self._body2 = vtk.vtkImageReslice()
        self._body2.SetInput(self._shrink.GetOutput())
        self._body2.SetResliceAxes(self._trans.GetMatrix())
        self._body2.SetResliceTransform(self._trans)
        self._body2.AutoCropOutputOn()
#        self._body.SetOutputOrigin(0, 0, 0)


# Create and show beam intersection on axial projection
        self._planeAxial = vtk.vtkPlane()
        self._planeAxial.SetOrigin(0, 0, 0)
        self._planeAxial.SetNormal(0, 0, 1)

        self._beamAxialCutter = vtk.vtkCutter()
        self._beamAxialCutter.SetInput(self._beam2Transform.GetOutput())
        #if (int(self.patC.getColmatPref()['beamCount'])>1):
        self._beamAxialCutter.SetCutFunction(self._planeAxial)
        self._beamAxialCutter.GenerateCutScalarsOn()

        self._beamAxialCutterMapper = vtk.vtkPolyDataMapper()
        self._beamAxialCutterMapper.SetInput(self._beamAxialCutter.GetOutput())
        self._beamAxialCutterMapper.ScalarVisibilityOn()

#        self._beamAxialCutterActor = vtk.vtkActor()
#        self._beamAxialCutterActor.SetMapper(self._beamAxialCutterMapper)
#        self._beamAxialCutterActor.GetProperty().SetColor(1, 0, 0)
#        self._beamAxialCutterActor.GetProperty().SetLineWidth( 2.0 )
        self._beamAxialCutterActor = vtk.vtkActor()
        self._beamAxialCutterActor.SetMapper(self._beamAxialCutterMapper)
        self._beamAxialCutterActor.GetProperty().SetColor(1, 0, 0)
        self._beamAxialCutterActor.GetProperty().SetLineWidth( 2.0 )
        self._beamAxialCutterActor.GetProperty().SetOpacity(1.0)
        self._beamAxialCutterActor.GetProperty().SetDiffuseColor(1,1,0)
        self._beamAxialCutterActor.GetProperty().SetEdgeColor(1.0, 0.0, 0.0)
        self._beamAxialCutterActor.GetProperty().SetDiffuse(0.8)
        self._beamAxialCutterActor.GetProperty().SetSpecular(0.3)
        self._beamAxialCutterActor.GetProperty().SetSpecularPower(20)
        self._beamAxialCutterActor.GetProperty().SetRepresentationToSurface()
        self._beamAxialCutterActor.GetProperty().EdgeVisibilityOn()

        self._imgAxialCutter = vtk.vtkCutter()
        self._imgAxialCutter.SetInput(self._body.GetOutput())
        self._imgAxialCutter.SetCutFunction(self._planeAxial)
        self._imgAxialCutter.GenerateCutScalarsOff()
#        self._imgAxialCutter.SetSortByToSortByCell()

        self._imgAxialCutterMapper = vtk.vtkPolyDataMapper()
        self._imgAxialCutterMapper.SetInput(self._imgAxialCutter.GetOutput())
        self._imgAxialCutterMapper.Update()

        self._imgAxialCutterActor = vtk.vtkActor()
        self._imgAxialCutterActor.SetMapper(self._imgAxialCutterMapper)

        self._AxialCamera = vtk.vtkCamera()
        self._AxialCamera.SetViewUp(0, 0, -1)
        self._AxialCamera.SetPosition(0, 0, 100)
        self._AxialCamera.SetFocalPoint(0, 0, 0)
        self._AxialCamera.ParallelProjectionOn()

        self._AxialRen = vtk.vtkRenderer()
#        l = self._AxialRen.MakeLight()
#        l.SetLightTypeToHeadlight()
        self._AxialRen.AddActor(self._imgAxialCutterActor)
        self._AxialRen.AddActor(self._beamAxialCutterActor)
#        self._AxialRen.ResetCameraClippingRange()
        self._AxialRen.SetBackground(0, 0, 0)
        self._AxialRen.SetViewport(0.5, 0.5, 1.0, 1.0)
        self._AxialRen.SetActiveCamera(self._AxialCamera)
        self._AxialRen.ResetCamera(self._imgAxialCutterMapper.GetBounds())

        self._RenWin.AddRenderer(self._AxialRen)
        self._Interactor.StartPickCallback()

# Create and show beam intersection on sagittal projection along beam axes
        self._planeSagittal = vtk.vtkPlane()
        self._planeSagittal.SetOrigin(0, 0, 0)
        self._planeSagittal.SetNormal(1, 0, 0)

#        self._planeSagittalTrans = vtk.vtkTransform()
#        self._planeSagittalTrans.Identity()
##        self._planeSagittalTrans.PostMultiply()
#
#        self._planeSagittal.SetTransform(self._planeSagittalTrans)
        self._beamSagittalCutter = vtk.vtkCutter()
        self._beamSagittalCutter.SetInput(self._beam2Transform.GetOutput())
        self._beamSagittalCutter.SetCutFunction(self._planeSagittal)

        self._beamSagittalCutterMapper = vtk.vtkPolyDataMapper()
        self._beamSagittalCutterMapper.SetInput(self._beamSagittalCutter.GetOutput())
        ##boxCutMapper.SetScalarRange(0, 1)
        self._beamSagittalCutterMapper.ScalarVisibilityOff()

        self._beamSagittalCutterActor = vtk.vtkActor()
        self._beamSagittalCutterActor.SetMapper(self._beamSagittalCutterMapper)
        self._beamSagittalCutterActor.GetProperty().SetLineWidth( 2.0 )
        self._beamSagittalCutterActor.GetProperty().SetOpacity(1.0)
        self._beamSagittalCutterActor.GetProperty().SetDiffuseColor(1,1,0)
        self._beamSagittalCutterActor.GetProperty().SetEdgeColor(1.0, 0.0, 0.0)
        self._beamSagittalCutterActor.GetProperty().SetDiffuse(0.8)
        self._beamSagittalCutterActor.GetProperty().SetSpecular(0.3)
        self._beamSagittalCutterActor.GetProperty().SetSpecularPower(20)
        self._beamSagittalCutterActor.GetProperty().SetRepresentationToSurface()
        self._beamSagittalCutterActor.GetProperty().EdgeVisibilityOn()

        self._imgSagittalCutter = vtk.vtkCutter()
        self._imgSagittalCutter.SetInput(self._body.GetOutput())
        self._imgSagittalCutter.SetCutFunction(self._planeSagittal)
        self._imgSagittalCutter.GenerateCutScalarsOff()
        self._imgSagittalCutter.SetSortByToSortByCell()

        self._imgSagittalCutterMapper = vtk.vtkPolyDataMapper()
        self._imgSagittalCutterMapper.SetInput(self._imgSagittalCutter.GetOutput())
#        print self._imgSagittalCutterMapper.GetBounds()
        self._imgSagittalCutterMapper.Update()

        self._imgSagittalCutterActor = vtk.vtkActor()
        self._imgSagittalCutterActor.SetMapper(self._imgSagittalCutterMapper)

        self._SagittalCamera = vtk.vtkCamera()
        self._SagittalCamera.SetViewUp(0, 0, 1)
        self._SagittalCamera.SetPosition(-100, 0, 0)
        self._SagittalCamera.SetFocalPoint(0, 0, 0)
#        self._SagittalCamera.OrthogonalizeViewUp()
#        print self._SagittalCamera.GetDirectionOfProjection()
#        self._SagittalCamera.ComputeViewPlaneNormal()
        self._SagittalCamera.ParallelProjectionOn()
#        self._SagittalCamera.ApplyTransform(self._planeSagittalTrans)

        self._SagittalRen = vtk.vtkRenderer()
        self._SagittalRen.AddActor(self._imgSagittalCutterActor)
        self._SagittalRen.AddActor(self._beamSagittalCutterActor)
        self._SagittalRen.SetActiveCamera(self._SagittalCamera)
        self._SagittalRen.ResetCamera(self._imgSagittalCutterMapper.GetBounds())
#        self._SagittalRen.ResetCameraClippingRange()
        self._SagittalRen.SetBackground(0, 0, 0)
        self._SagittalRen.SetViewport(0.0, 0.0, 0.5, 0.5)
#        self._SagittalRen.TwoSidedLightingOn()
#        self._SagittalRen.UpdateLightsGeometryToFollowCamera()
#        self._SagittalRen.ResetCamera()

        self._RenWin.AddRenderer(self._SagittalRen)
        self._Interactor.StartPickCallback()

# Create and show beam intersection on coronal projection
        self._planeCoronal = vtk.vtkPlane()
        self._planeCoronal.SetOrigin(0, 0, 0)
        self._planeCoronal.SetNormal(0, 1, 0)

#        self._planeCoronalTrans = vtk.vtkTransform()
#        self._planeCoronalTrans.Identity()
##        self._planeCoronalTrans.PostMultiply()
#
#        self._planeCoronal.SetTransform(self._planeCoronalTrans)

        self._beamCoronalCutter = vtk.vtkCutter()
        self._beamCoronalCutter.SetInput(self._beam2Transform.GetOutput())
        self._beamCoronalCutter.SetCutFunction(self._planeCoronal)

        self._beamCoronalCutterMapper = vtk.vtkPolyDataMapper()
        self._beamCoronalCutterMapper.SetInput(self._beamCoronalCutter.GetOutput())
        ##boxCutMapper.SetScalarRange(0, 1)
        self._beamCoronalCutterMapper.ScalarVisibilityOff()

        self._beamCoronalCutterActor = vtk.vtkActor()
        self._beamCoronalCutterActor.SetMapper(self._beamCoronalCutterMapper)
        self._beamCoronalCutterActor.GetProperty().SetLineWidth( 2.0 )
        self._beamCoronalCutterActor.GetProperty().SetOpacity(1.0)
        self._beamCoronalCutterActor.GetProperty().SetDiffuseColor(1,1,0)
        self._beamCoronalCutterActor.GetProperty().SetEdgeColor(1.0, 0.0, 0.0)
        self._beamCoronalCutterActor.GetProperty().SetDiffuse(0.8)
        self._beamCoronalCutterActor.GetProperty().SetSpecular(0.3)
        self._beamCoronalCutterActor.GetProperty().SetSpecularPower(20)
        self._beamCoronalCutterActor.GetProperty().SetRepresentationToSurface()
        self._beamCoronalCutterActor.GetProperty().EdgeVisibilityOn()

        self._imgCoronalCutter = vtk.vtkCutter()
        self._imgCoronalCutter.SetInput(self._body.GetOutput())
        self._imgCoronalCutter.SetCutFunction(self._planeCoronal)
#        self._imgCoronalCutter.GenerateCutScalarsOn()
#        self._imgCoronalCutter.SetSortByToSortByCell()

        self._imgCoronalCutterMapper = vtk.vtkPolyDataMapper()
        self._imgCoronalCutterMapper.SetInput(self._imgCoronalCutter.GetOutput())
        self._imgCoronalCutterMapper.Update()

        self._imgCoronalCutterActor = vtk.vtkActor()
        self._imgCoronalCutterActor.SetMapper(self._imgCoronalCutterMapper)

        self._CoronalCamera = vtk.vtkCamera()
        self._CoronalCamera.SetViewUp(0, 1, 0)
        self._CoronalCamera.SetPosition(0, 100, 0)
        self._CoronalCamera.SetFocalPoint(0, 0, 0)
#        self._CoronalCamera.ComputeViewPlaneNormal()
        self._CoronalCamera.ParallelProjectionOn()
#        self._CoronalCamera.ApplyTransform(self._planeCoronalTrans)

        self._CoronalRen = vtk.vtkRenderer()
        self._CoronalRen.AddActor(self._imgCoronalCutterActor)
        self._CoronalRen.AddActor(self._beamCoronalCutterActor)
        self._CoronalRen.InteractiveOn()
        self._CoronalRen.SetActiveCamera(self._CoronalCamera)
        self._CoronalRen.ResetCamera(self._imgCoronalCutterMapper.GetBounds())
        self._CoronalRen.SetBackground(0, 0, 0)
        self._CoronalRen.SetViewport(0.5, 0.0, 1.0, 0.5)
#        self._CoronalRen.ResetCamera()

        self._RenWin.AddRenderer(self._CoronalRen)

        self._Interactor.StartPickCallback()
        self._Interactor.UpdateSize(512,512)
        self._Interactor.Start()

        self._rotX = 0.0
        self._rotZ = 0.0
        self._RotatingZ = 0
        self._RotatingX = 0
        params.log('First run:')
        params.log('normalized skin angles')
        params.log(self.getAngle(self._skinActor.GetMatrix()))
        params.log('normalized beam1 angles')
        params.log(self.getAngle(self._beam1Actor.GetMatrix()))
        self._trans.RotateZ(90)
        self.update()
        #print

    def drawCross(self):
        """
        Рисует перекрестье, создаёт глобальные параметры self._crossYActor и self._crossXActor
        @todo: сделать рисование до тела
        """
        if self.colmatPref['collimatorShape'] == 'Circle':
            s = self.colmatPref['collimatorSize']
        else:
            s = self.colmatPref['collimatorSize'].split('x')[0]

        crossX=vtk.vtkCubeSource()
        crossX.SetXLength(300)
        crossX.SetYLength(20+2*float(s)*10/2)
        crossX.SetZLength(2)

        crossXNormals = vtk.vtkPolyDataNormals()
        crossXNormals.SetInput(crossX.GetOutput())

        crossXMapper = vtk.vtkPolyDataMapper()
        crossXMapper.SetInput(crossXNormals.GetOutput())
        self._crossXActor = vtk.vtkActor()
        self._crossXActor.SetMapper(crossXMapper)
        self._crossXActor.GetProperty().SetColor(0,0,0)

        crossY=vtk.vtkCubeSource()
        crossY.SetXLength(300)
        crossY.SetZLength(20+2*float(s)*10/2)
        crossY.SetYLength(2)

        crossYNormals = vtk.vtkPolyDataNormals()
        crossYNormals.SetInput(crossY.GetOutput())

        crossYMapper = vtk.vtkPolyDataMapper()
        crossYMapper.SetInput(crossYNormals.GetOutput())
        self._crossYActor = vtk.vtkActor()
        #self._crossYActor.SetPosition(100, 0, 0)
        self._crossYActor.SetMapper(crossYMapper)
        self._crossYActor.GetProperty().SetColor(0,0,0)

    def rotateCross(self, type, angle):
        """
        Поворачивает перекрестье по оси type на угол angle
        @param type: [X, Y, Z] - по какой оси
        @type type: string
        @param angle: угол
        @type angle: int
        """
        if ('Z' == type):
            self._crossXActor.RotateZ(angle)
            self._crossYActor.RotateZ(angle)
        elif ('Y' == type):
            self._crossXActor.RotateY(angle)
            self._crossYActor.RotateY(angle)
        elif ('X' == type):
            self._crossXActor.RotateX(angle)
            self._crossYActor.RotateX(angle)

    def setCrossOpacity(self, val):
        """
        Устанавливает прозрачность перекрестья
        @param val: значение прозрачности
        @type val: float[0,1]
        """
        self._crossXActor.GetProperty().SetOpacity(val)
        self._crossYActor.GetProperty().SetOpacity(val)

    def ButtonEvent(self, obj, event):
        """
        Отработка нажатия на клавишу мыши с учётом положения среди окон. Если в окне space, то вызывает события поворотов\n
        Если в других - вызывает соответствующие переключения по слоям
        @param obj: ?
        @type obj: ?
        @param event: событие
        @type event: VTKEvent
        """
        xypos = self._Interactor.GetEventPosition()
        x = xypos[0]
        y = xypos[1]
        pokedRen = self._Interactor.FindPokedRenderer(x,y)
        pokedRenVP = pokedRen.GetViewport()
        #print pokedRenVP

        if pokedRenVP == (0.0, 0.5, 0.5, 1.0):
            portName = 'Space'
        elif pokedRenVP == (0.5, 0.5, 1.0, 1.0):
            portName = 'Axial'
        elif pokedRenVP == (0.0, 0.0, 0.5, 0.5):
            portName = 'Sagittal'
        elif pokedRenVP == (0.5, 0.0, 1.0, 0.5):
            portName = 'Coronal'
        #print portName

        # переключение срезов в окнах
        # axial переключается с шагом sliceSpacing
        # coronal и sagittal с шагом pixelSpacing*10
        if 'Axial' == portName:
            if 'LeftButtonReleaseEvent' == event:
                self.curAxialSlice += 1
            elif 'RightButtonReleaseEvent' == event:
                self.curAxialSlice -= 1
            elif 'MiddleButtonReleaseEvent' == event:
                self.curAxialSlice = 0
            self.setAxialSlice(self.curAxialSlice*float(self.pat['sliceSpacing']))
        elif 'Sagittal' == portName:
            if 'LeftButtonReleaseEvent' == event:
                self.curSagittalSlice += 1
            elif 'RightButtonReleaseEvent' == event:
                self.curSagittalSlice -= 1
            elif 'MiddleButtonReleaseEvent' == event:
                self.curSagittalSlice = 0
            self.setSagittalSlice(self.curSagittalSlice*float(self.pat['pixelSpacing'][0])*10)
            pass
        elif 'Coronal' == portName:
            if 'LeftButtonReleaseEvent' == event:
                self.curCoronalSlice += 1
            elif 'RightButtonReleaseEvent' == event:
                self.curCoronalSlice -= 1
            elif 'MiddleButtonReleaseEvent' == event:
                self.curCoronalSlice = 0
            self.setCoronalSlice(self.curCoronalSlice*float(self.pat['pixelSpacing'][1])*10)
            pass

        if event == "LeftButtonPressEvent" and portName == 'Space':
            self._RotatingZ = 1
        if event == "LeftButtonReleaseEvent" and portName == 'Space':
            self._RotatingZ = 0
        if event == "RightButtonPressEvent" and portName == 'Space':
            self._RotatingX = 1
        if event == "RightButtonReleaseEvent" and portName == 'Space':
            self._RotatingX = 0
        if 'MiddleButtonReleaseEvent' == event and 'Space' == portName:
            self.normalize()

    def normalize(self):
        """
        Вращает пучок до исходного и тело до соответствующего положения
        @todo: сделать поворот self.trans для отображения на срезах и расчёта
        """
        #self.setCrossOpacity(0)
        old = self._beam1Actor.GetMatrix()
        #print old
        el = self.getAngle(self._beam1Actor.GetMatrix())
        #print el
        #self._skinActor.RotateX(-el[0])
        self._skinActor.RotateY(-el[1])
        self._skinActor.RotateZ(90-el[2])

        #self._tumorActor2.RotateX(-el[0])
        #self._tumorActor2.RotateY(-el[1])
        #self._tumorActor2.RotateZ(90-el[2])
        self.rotateOrgans('Y', -el[1])
        self.rotateOrgans('Z', 90-el[2])

        #self._trans.RotateX(-el[0])
        #self._trans.RotateY(-el[1])
        #self._trans.RotateZ(90-el[2])
        #self._beam1Actor.RotateX(-el[0])
        #self._beam1Actor.RotateY(-el[1])
        #self._beam1Actor.RotateZ(90-el[2])
        #q = self.getAngle(self._trans.GetMatrix())
        #print q
        #self._trans.RotateX(-q[0])
        self._beam1Actor.GetMatrix().DeepCopy((0, -1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1))
        self._crossXActor.GetMatrix().DeepCopy((1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1))
        self._crossYActor.GetMatrix().DeepCopy((1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1))
        self._RenWin.Render()
        #print old


    def setAxialSlice(self, val):
        """
        Устанавливает отступ для Axial-среза (правый верхний угол)
        @param val: отступ для среза
        @type val: float
        """
        self._planeAxial.SetOrigin(0, 0, val)
        self._imgAxialCutter.Update()
        self._imgAxialCutterMapper.Update()
        self._AxialRen.ResetCamera(self._imgAxialCutterMapper.GetBounds())
        self._RenWin.Render()
        self.parent.updateDistance('Axial', val)

    def setCoronalSlice(self, val):
        """
        Устанавливает отступ для коронального среза (правый нижний угол)
        @param val: отступ
        @type val: float
        """
        self._planeCoronal.SetOrigin(0, val, 0)
        self._imgCoronalCutter.Update()
        self._imgCoronalCutterMapper.Update()
        self._CoronalRen.ResetCamera(self._imgCoronalCutterMapper.GetBounds())
        self._RenWin.Render()
        self.parent.updateDistance('Coronal', val)

    def setSagittalSlice(self, val):
        """
        Устанавливает отступ для сагиттального среза
        @param val: отступ
        @type val: float
        """
        self._planeSagittal.SetOrigin(val, 0, 0)
        self._imgSagittalCutter.Update()
        self._imgSagittalCutterMapper.Update()
        self._SagittalRen.ResetCamera(self._imgSagittalCutterMapper.GetBounds())
        self._RenWin.Render()
        self.parent.updateDistance('Sagittal', val)


    def MouseMove(self, obj, event):
        """
        Обработчик движения мыши
        @param obj: ?
        @param event: ?
        """
        lastXYpos = self._Interactor.GetLastEventPosition()
        lastX = lastXYpos[0]
        lastY = lastXYpos[1]
        xypos = self._Interactor.GetEventPosition()
        x = xypos[0]
        y = xypos[1]
        pokedRen = self._Interactor.FindPokedRenderer(x,y)
        pokedRenVP = pokedRen.GetViewport()
        if pokedRenVP == (0.0, 0.5, 0.5, 1.0):
            portName = 'Space'
        else:
            portName = None
        if portName is not None and (self._RotatingZ == 1 or self._RotatingX == 1):
            self.Rotate(x, y, lastX, lastY)

    def rotateOrgans(self, type, angle):
        if ('Y' == type):
            for i in self._organsActors:
                i.RotateY(angle)
        elif ('Z' == type):
            for i in self._organsActors:
                i.RotateZ(angle)


    def Rotate(self, x, y, lastX, lastY):
        """
        Функция поворота, в зависимости от режима (self.mode) поворачивает либо объект, либо пучок
        @param x: текущее положение по X
        @type x: int
        @param y: текущее положение по Y
        @type y: int
        @param lastX: предыдущее положение по X
        @type lastX: int
        @param lastY: предыдущее положение по Y
        @type lastY: int
        """
        #cosines = self._body2.GetResliceAxesDirectionCosines()
        #self.parent.setCosinesLabel('%2.2f %2.2f %2.2f \n %2.2f %2.2f %2.2f \n %2.2f %2.2f %2.2f' %cosines)
        #print cosines
        if (0 == self.mode):
            if self._RotatingZ == 1:
                self._rotZ = (x-lastX)/2.0
                self._skinActor.RotateZ(self._rotZ)
                #self._tumorActor2.RotateZ(self._rotZ)
                self._trans.RotateZ(-self._rotZ)
                self.rotateOrgans('Z', self._rotZ)
                if (1 == self.curBeam):
                    self._beamx1Actor.RotateZ(self._rotZ)
                elif (2 == self.curBeam):
                    self._beam1Actor.RotateZ(self._rotZ)
                self.update()

            if self._RotatingX == 1:
                self._rotX = (y-lastY)/2.0
                self._skinActor.RotateY(self._rotX)
                #self._tumorActor2.RotateY(self._rotX)
                #self._trans.RotateY(-self._rotX)
                self.rotateOrgans('Y', self._rotX)
                self._trans.RotateX(-self._rotX)
                if (1 == self.curBeam):
                    self._beamx1Actor.RotateY(self._rotX)
                elif (2 == self.curBeam):
                    self._beam1Actor.RotateY(self._rotX)
                self.update()

        else:
            if self._RotatingZ == 1:
                self._rotZ = (x-lastX)/2.0
                self._beam1Actor.RotateZ(self._rotZ)
                self.rotateCross('Z', self._rotZ)
                self._trans.RotateZ(self._rotZ)
            if self._RotatingX == 1:
                self._rotX = (y-lastY)/2.0
                self._beam1Actor.RotateX(self._rotX)
                self.rotateCross('Y', self._rotX)
                #self._trans.RotateY(self._rotX)
                self._trans.RotateX(self._rotX)


        self._RenWin.Render()
        #pSource = self.getpSource()
        #self.parent.setBeamLabel('%2.2f %2.2f %2.2f' %(pSource[0], pSource[1], pSource[2]))

    def getCosines(self, actor=None):
        """
        Возвращает направляющие косинусы для переданного актёра или косинусы _body2
        @param actor: actor
        @type actor: VTKActor
        @return: 9 элементов косинусов (xx, xy, xz, yx, yy, yz, zx, zy, zz)
        @rtype: tuple
        """
        if (0 == self.mode or actor is None):
            return self._body2.GetResliceAxesDirectionCosines()
        else:
            return (actor.GetMatrix().GetElement(0,0),
                    actor.GetMatrix().GetElement(0,1),
                    actor.GetMatrix().GetElement(0,2),
                    actor.GetMatrix().GetElement(1,0),
                    actor.GetMatrix().GetElement(1,1),
                    actor.GetMatrix().GetElement(1,2),
                    actor.GetMatrix().GetElement(2,0),
                    actor.GetMatrix().GetElement(2,1),
                    actor.GetMatrix().GetElement(2,2),
                )

    def clamp(self, angle):
        """
        Приводит углы в градусах к интервалу [0, 360]\n
        Например: self.clamp(45) == 45\n
        self.clamp(-45) == 315
        @param angle: угол в градусах
        @type angle: float
        @return: угол в градусах
        @rtype: float
        """
        if (angle < -0.0):
            return 360+angle
        return angle

    def getAngle(self, mat):
        """
        Получает направляющие углы в градусах из матрицы косинусов\n
        @use: math
        @param mat:  матрица элементов
        @type mat: VTKMatrix4x4
        @return: (angle_x, angle_y, angle_z)
        @rtype: tuple
        """
        radians = 1 / math.radians(1)
        #mat = self._skinActor.GetMatrix()
        angle_y = D = math.asin(float('%2.2f' %mat.GetElement(0, 2)))
        C = math.cos(angle_y)
        angle_y *= radians
        if (math.fabs(C) > 0.005):
            trX = mat.GetElement(2,3)
            trY = -mat.GetElement(1,3)

            angle_x = math.atan2(trY, trX)*radians

            trX = mat.GetElement(0, 0) / C
            trY = -mat.GetElement(0, 1) / C

            angle_z = math.atan2(trY, trX)*radians
        else:
            angle_x = 0

            trX = mat.GetElement(1, 1)
            trY = mat.GetElement(1,0)

            angle_z = math.atan2(trY, trX)*radians

        return (self.clamp(angle_x), self.clamp(angle_y), self.clamp(angle_z))

    def update(self):
        """
        Вынесено из Rotate, обновляет _trans, _body, _body2, срезы
        """
        #self._skinStripper.Update()
        self._trans.Update()
        self._body.Update()
        self._body2.Update()
        self._beam2Transform.Update()
        self._AxialRen.ResetCamera(self._imgAxialCutterMapper.GetBounds())
        self._SagittalRen.ResetCamera(self._imgSagittalCutterMapper.GetBounds())
        self._CoronalRen.ResetCamera(self._imgCoronalCutterMapper.GetBounds())


    def getpSource(self):
        """
        Вычисляет положение центра пучка для MCNP
        Если необходимо, поворачивает пучок на экране до начального положения, поворачивает объект на нужные углы
        @return: положение центра
        @rtype: list(3)
        """
        self.normalize()
        self.update()
        SourceSkinDistance = int(self.colmatPref['sourceSkinDistance'])
        p = [0,0,0,0]
        #self._trans.MultiplyPoint([0, -10000, 0, 1], p)
        self._trans.MultiplyPoint([0, -10000, 0, 1], p)
        p.pop()

        tree = vtk.vtkOBBTree()
        tree.SetDataSet(self._skinStripper.GetOutput())
        tree.BuildLocator()

        #intersect the locator with the line
        lineP0 = [0.0, 0.0, 0.0]
        lineP1 = p

        intersectPoints = vtk.vtkPoints()

        ids = vtk.vtkIdList()

        tree.IntersectWithLine(lineP0, lineP1, intersectPoints, ids)
        n = intersectPoints.GetNumberOfPoints()
#        print n

        # Find coord beam-skin intersection

        m = vtk.vtkMath()
        lengs = 0
        coord = [0.0,0.0,0.0]
        for i in range(n):
            c = intersectPoints.GetPoint(i)
            d = m.Distance2BetweenPoints (c, lineP0)
            #print d
            if d > lengs:
                lengs = d
                coord = c
        #print coord

        # Rotate vector
        p = [0,0,0,0]
        self._trans.MultiplyPoint([0, -SourceSkinDistance, 0, 1], p)
        #self._trans.MultiplyPoint([0, -SourceSkinDistance, 0, 1], p)
        p.pop()

        # Shift intersection point and get source centre
        pSource =[0,0,0]
        for i in range(3):
            pSource[i] = (coord[i] + p[i])/10
        #if (1 == self.mode):
        #    pSource[2] = -pSource[2]
        self.changeMode(0)
        self.parent.GetParent().beamMode.SetValue(0)
        return pSource

    def collect(self):
        """
        Сборщик значений
        @return: {'cosines' : матрица косинусов,'sourceCenterMcnp' : положение центра пучка, 'tumourCenter' : положение центра опухоли, 'angles' : поворотные углы, 'mcnpSourceLabel' : надпись о положении центра}
        @rtype: object
        """

        pSource = self.getpSource()
        cosines = self.getCosines(self._skinActor)
        self.parent.setCosinesLabel('%2.2f %2.2f %2.2f \n %2.2f %2.2f %2.2f \n %2.2f %2.2f %2.2f' %cosines)
        self.parent.setBeamLabel('%2.2f %2.2f %2.2f' %(pSource[0], pSource[1], pSource[2]))
        #print self.colmatPref['sourceSkinDistance']
        params.log('SourceSkinDistance: %s' %self.colmatPref['sourceSkinDistance'])
        # Calculate coord of point after rotation

        #pSource = self.getpSource()

        #print 'SourceCenterMcnp'
        #print pSource
        w2if = vtk.vtkWindowToImageFilter()
        w2if.SetInput(self._RenWin)
        wr = vtk.vtkPNGWriter()
        wr.SetInput(w2if.GetOutput())
        outfile = self.patC.getPatDir()+"/screen.png"
        wr.SetFileName(outfile)
        wr.Write()
        params.log('Screenshot created at %s' %outfile)
        params.log('normalized skin angles')
        params.log(self.getAngle(self._skinActor.GetMatrix()))
        params.log('normalized beam1 angles')
        params.log(self.getAngle(self._beam1Actor.GetMatrix()))

        return {'cosines' : cosines,
                'sourceCenterMcnp' : pSource,
                'tumourCenter' : self.tumour,
                'angles' : self.getAngle(self._skinActor.GetMatrix()),
                'mcnpSourceLabel' : ('%2.2f %2.2f %2.2f' %(pSource[0], pSource[1], pSource[2]))}

    def changeOpacity(self, op=1):
        """
        Изменяет прозрачность пучка/пучков и кожи
        @param op: прозрачность (1 - непрозрачный, 0 - полностью прозрачный)
        @type op: float
        """
        self._skinActor.GetProperty().SetOpacity(op)
        self._beam1Actor.GetProperty().SetOpacity(op)
        if (int(self.patC.getColmatPref()['beamCount'])>1):
            self._beamx1Actor.GetProperty().SetOpacity(op)
        self._RenWin.Render()

    def changeMode(self, mode=0):
        """
        Устанавливает режим. 0 - крутится тело, 1 - крутится пучок
        @param mode: режим (0,1) default: 0
        @type mode: int
        """
        if (0 == mode):
            params.log('Set current mode to: ROTATE SKIN')
            self.setCrossOpacity(100)
        else:
            params.log('Set current mode to: ROTATE BEAM')
            self.setCrossOpacity(0)
        self.mode = mode

    def setField(self, obj):
        """
        Восстанавливает положение тела относительно пучка при загрузке
        @param obj: параметры
        @type obj: object
        @see: self.collect()
        """
        #print obj
        params.log('Current beam position restored:')
        params.log(obj)
        self.parent.setBeamLabel(obj['mcnpSourceLabel'])
        el = obj['angles']
        self._skinActor.RotateX(el[0])
        self._skinActor.RotateY(el[1])
        self._skinActor.RotateZ(el[2])

        #self._tumorActor2.RotateX(el[0])
        #self._tumorActor2.RotateY(el[1])
        #self._tumorActor2.RotateZ(el[2])
        self.rotateOrgans('Y', el[1])
        self.rotateOrgans('Z', el[2])

        self._trans.RotateX(-el[1])
        self._trans.RotateY(-el[0])
        self._trans.RotateZ(-el[2])
        self.update()
        #self._RenWin.Render()
        #self._rotX = obj['rotX']
        #self._rotZ = obj['rotZ']
        #self.rotateTo()
        pass

    def setCurBeam(self, q):
        """
        Устанавливает, какой из пучков (в случае 2х и более) сейчас поворачивается
        @param q: номер пучка
        @type q: int
        """
        self.curBeam = q