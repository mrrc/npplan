# -*- coding: utf8 -*-
'''
VTK and matplotlib integration

base: matplotlib
renderwindow: vtk


@author: mrxak
@copyright: A. Tsyb MRRC Obninsk 2021
@version: 2
@date: 30.04.2021
@summary: натянутые на vtk-куб в виде текстуры вывод (canvas) окна matplotlib со случайными значениями по одной оси (1D-гистограмма)
@todo: - протянуть прозрачные графики matplotlib в оси Z при вращении куба
       - fix: dpi
       - проблема сохранения CP1251/UTF8 в FAR
'''
# 

import numpy as np

from vtk import *

import matplotlib
matplotlib.use('Agg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg
import pylab as p

# The vtkImageImporter will treat a python string as a void pointer
importer = vtkImageImport()
importer.SetDataScalarTypeToUnsignedChar()
importer.SetNumberOfScalarComponents(4)

# It's upside-down when loaded, so add a flip filter
imflip = vtkImageFlip()
imflip.SetInputConnection(importer.GetOutputPort())
imflip.SetFilteredAxis(1)

bbmap = vtkImageMapper()
bbmap.SetColorWindow(255.5)
bbmap.SetColorLevel(127.5)
#bbmap.SetInput(importer.GetOutput())
#bbmap.SetInput(imflip.GetOutput())

#bbact = vtkActor2D()
#bbact.SetMapper(bbmap)

# Map the plot as a texture on a cube
cube = vtkCubeSource()

cubeMapper = vtkPolyDataMapper()
cubeMapper.SetInputConnection(cube.GetOutputPort())
#cubeMapper.SetInput(bbmap.GetOutput())

cubeActor = vtkActor()
cubeActor.SetMapper(cubeMapper)

# Create a texture based off of the image
cubeTexture = vtkTexture()
cubeTexture.InterpolateOn()
cubeTexture.SetInputConnection(imflip.GetOutputPort())
cubeActor.SetTexture(cubeTexture)

ren = vtkRenderer()
ren.AddActor(cubeActor)
#ren.SetBackground(1, 0, 1)

renWin = vtkRenderWindow()
renWin.AddRenderer(ren)

iren = vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

# Now create our plot
fig = Figure()
canvas = FigureCanvasAgg(fig)
ax = fig.add_subplot(111)
ax.grid(True)
ax.set_xlabel('Hello from VTK!', size=16)
ax.bar(np.arange(10), p.rand(10))
#ax.scatter(p.rand(10))
#def randrange(n, vmin, vmax):
#    """
#    Helper function to make an array of random numbers having shape (n, )
#    with each number distributed Uniform(vmin, vmax).
#    """
#    return (vmax - vmin)*np.random.rand(n) + vmin
#n = 100

#for m, zlow, zhigh in [('o', -50, -25), ('^', -30, -5)]:
#    xs = randrange(n, 23, 32)
#    ys = randrange(n, 0, 100)
#    zs = randrange(n, zlow, zhigh)
#    ax.scatter(xs, ys, zs, marker=m)




# Powers of 2 image to be clean
w,h = 1024, 1024
dpi = canvas.figure.get_dpi()
print(dpi)
fig.set_size_inches(w / dpi, h / dpi)
canvas.draw() # force a draw

# This is where we tell the image importer about the mpl image
extent = (0, w - 1, 0, h - 1, 0, 0)
#print canvas.tostring_rgb()
importer.SetWholeExtent(extent)
importer.SetDataExtent(extent)
importer.SetImportVoidPointer(canvas.buffer_rgba(), 1)
importer.Update()

iren.Initialize()
iren.Start()