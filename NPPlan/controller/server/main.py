# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 5
@date: 12.07.2012
@summary:
'''

# @todo: parallel|threading
# @todo: starting database if not started
# @todo: обернуть в функцию
# @todo: сервера: управления (pb), передачи, защищённой передачи

import NPPlan
from twisted.internet import task
from twisted.spread import pb
from twisted.internet import reactor
import NPPlan.model.networking.pbWrapper as pbWrapper
from NPPlan.model.networking.informer.factory import informerServerFactory
import NPPlan.model.mongo.connector as db

import NPPlan.controller.logger as log


def checkStopFlag():
    import NPPlan
    if NPPlan.config.runningConfig.exiting:
        reactor.callFromThread(reactor.stop)

def start():
    lc = task.LoopingCall(checkStopFlag)
    lc.start(15)

    #db.connection()
    #print db.getConnector(), db.getDb()#, db.getConnector().server_info()

    pbPort, informerPort = int(NPPlan.config.appConfig.server.pbport), int(NPPlan.config.appConfig.server.informerport)
    log.log('server.main', 'pb on %s' %pbPort)
    reactor.listenTCP(pbPort, pb.PBServerFactory(pbWrapper.PbWrapper()))
    log.log('server.main', 'informer on %s' %informerPort)
    reactor.listenTCP(informerPort, informerServerFactory())
    reactor.run()
    log.log('server.main', 'Stopping listen server')

    import sys
    sys.exit()

