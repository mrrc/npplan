# -*- coding: utf8 -*-
"""
@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 28.06.2012
@summary: контроллер запуска
"""

import NPPlan

def start():
    if 'client' == NPPlan.config.runningConfig.args.mode:
        startClient()
    elif 'server' == NPPlan.config.runningConfig.args.mode:
        startServer()

def startClient():
#    pass
    #import NPPlan.model.multi as fork
    from NPPlan.controller.client.main import start
    #fork.addTask(start, 'func')
    #fork.start()
    app = start()

def startServer():
    from NPPlan.controller.server.main import start
    app = start()
    pass

