# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 31.07.13
@summary: 
'''

import NPPlan
import vtk


class volumeMakerBase(object):
    def __init__(self, output='screen', **kwargs):
        print NPPlan.currentPlan
        self._outputMode = output
        print NPPlan.currentPlan.planGetDicomLocalFolder()

        aRenderer = vtk.vtkRenderer()
        renWin = vtk.vtkRenderWindow()
        renWin.AddRenderer(aRenderer)
        iren = vtk.vtkRenderWindowInteractor()
        iren.SetRenderWindow(renWin)

        v16 = vtk.vtkDICOMImageReader()
        v16.SetDirectoryName(NPPlan.currentPlan.planGetDicomLocalFolder())

        pass


if __name__ == '__main__':
    NPPlan.init(programPath="c:/NPPlan3/", test=True, log=True)
    plan = NPPlan.planAbstract()
    plan.planLoadDicomSeriesFromFile("C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\")
    plan.planSetThumbsDir("C:\\Users\\Chernukha\\Pictures\\DICOM\\thumb\\")
    NPPlan.currentPlan = plan
    volumes = volumeMakerBase()
