# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 02.09.13
@summary: 
'''


import vtk
import time


pluginName = 'sliceCutter'
pluginInfo = 'sliceCutter any'
__version__ = 1


def version():
    return __version__


def name():
    return pluginName


def info():
    return pluginInfo


class sliceCutter(object):
    def __init__(self, cParent=None, inputMethod=0, outputMethod=0, **kwargs):
        self._outputMethod = outputMethod  # 0 - screen, 1 - png file, 2 - numpy array
        self._outputParams = kwargs.get('outputParams', {})
        pass

    def setVtkVolumePath(self, path):
        reader = vtk.vtkDICOMImageReader()
        #reader.SetFilePrefix()
        reader.SetDirectoryName("D:\\old.Users2\\_mrxak\\NGPlan\\Dicom\\01111140a\\")
        #reader.SetDataExtent(0, 63, 0, 63, 1, 93)
        #reader.SetDataSpacing(3.2, 3.2, 1.5)
        #reader.SetDataOrigin(0.0, 0.0, 0.0)
        reader.SetDataScalarTypeToInt()
        reader.SetDataByteOrderToLittleEndian()
        reader.UpdateWholeExtent()

        reader.Update()
        self._volume = reader
        self._makeVolumeData()

    def setVtkVolume(self, volume):
        self._volume = volume
        self._makeVolumeData()

    def _makeVolumeData(self):
        extent = self._volume.GetOutput().GetExtent()
        spacing = self._volume.GetOutput().GetSpacing()
        origin = self._volume.GetOutput().GetOrigin()
        center = [0, 0, 0]
        center[0] = origin[0] + spacing[0] * 0.5 * (extent[0] + extent[1])
        center[1] = origin[1] + spacing[1] * 0.5 * (extent[2] + extent[3])
        center[2] = origin[2] + spacing[2] * 0.5 * (extent[4] + extent[5])
        self._volumeData = {
            'extent': extent,
            'spacing': spacing,
            'origin': origin,
            'center': center,
        }
        pass

    def getVolumeData(self):
        if hasattr(self, '_volumeData'):
            return self._volumeData
        return None

    def setBeamOrigin(self, origin, *args, **kwargs):
        # set cut origin
        pass

    def setBeamNormal(self, normal, *args, **kwargs):
        # set cut normal
        pass

    def setBeam(self, origin, normal, *args, **kwargs):
        # set cut origin and normal
        pass

    def setResliceAxesMatrix(self, *args, **kwargs):
        """
        @keyword matrix: матрица 4х4
        @type matrix: list
        @keyword invert: вычислить обратную матрицу, default True
        @type invert: bool
        @return:
        """
        self._matrix = kwargs.get('matrix')
        self._resliceAxes = vtk.vtkMatrix4x4()
        #resliceAxes.DeepCopy(sagittalElements)
        self._resliceAxes.DeepCopy(self._matrix)
        self._resliceAxes2 = vtk.vtkMatrix4x4()
        self._resliceAxes2.DeepCopy(self._matrix)
        if kwargs.get('invert', True):
            self._inverted = True
            self._resliceAxes.Invert()
        else:
            self._inverted = False

        self._resliceAxes.SetElement(0, 3, self._volumeData['center'][0])
        self._resliceAxes.SetElement(1, 3, self._volumeData['center'][1])
        self._resliceAxes.SetElement(2, 3, self._volumeData['center'][2])
        self._resliceAxes2.SetElement(0, 3, self._volumeData['center'][0])
        self._resliceAxes2.SetElement(1, 3, self._volumeData['center'][1])
        self._resliceAxes2.SetElement(2, 3, self._volumeData['center'][2])

    def setSlice(self, slice, dslice=None):
        # @todo: handle while inverted (!!)
        if self._inverted:
            self._resliceAxes.Invert()
        self._resliceAxes.SetElement(1, 3, slice)
        if self._inverted:
            self._resliceAxes.Invert()
        self._reslice.Update()
        self._castFunction.Update()
        if hasattr(self, '_reslice3d') and dslice is not None:
            #self._resliceAxes2.SetElement(1, 3, float(slice) - float(slice)/10)
            #self._resliceAxes2.SetElement(1, 3, -220 + float(slice)/20)
            print 'Set 3d slice to', dslice
            self._resliceAxes2.SetElement(1, 3, float(dslice))
            self._reslice3d.Update()


    def setOutputPrefix(self, prefix):
        pass

    def setRenderer(self, renderer):
        self._renderer = renderer
        pass

    def _resliceFunction(self):
        reslice = vtk.vtkImageReslice()

        # @todo: to params: background level, interpolation mode, slab number of slices,
        #reslice.SetBackgroundColor(1,1,1,1)
        reslice.SetInputConnection(self._volume.GetOutputPort())
        reslice.SetBackgroundLevel(-1000)   # needed to cut edges
        reslice.SetOutputDimensionality(2)
        reslice.SetResliceAxes(self._resliceAxes)
        reslice.SetInterpolationModeToLinear()
        #reslice.SetSlabNumberOfSlices(3)

        reslice.Update()   # causes app crash
        self._reslice = reslice

    def _useCast(self):
        if not hasattr(self, '_reslice'):
            self._resliceFunction()

        self._castFunction = vtk.vtkImageCast()
        self._castFunction.SetInput(self._reslice.GetOutput())
        self._castFunction.SetOutputScalarTypeToInt()
        self._castFunction.Update()

    def _useLookupTable(self):
        if not hasattr(self, '_reslice'):
            self._resliceFunction()

        table = vtk.vtkLookupTable()
        table.SetRange(0, 2000) #// image intensity range
        table.SetValueRange(0.0, 1.0) #// from black to white
        table.SetSaturationRange(0.0, 0.0) #// no color saturation
        table.SetRampToLinear()
        table.Build()

        self._castFunction = vtk.vtkImageMapToColors()
        self._castFunction.SetLookupTable(table)
        self._castFunction.SetInputConnection(self._reslice.GetOutputPort())

    def _useThresholdSummary(self, threshParams=[]):
        if not hasattr(self, '_reslice'):
            self._resliceFunction()

        threshParams = [
            {'name': 'soft', 'between': (-300, 300), 'target': 128},
            {'name': 'bone', 'upper': 301, 'target': 255}
        ]
        threshFilters = []
        for threshItem in threshParams:
            cFilter = vtk.vtkImageThreshold()
            cFilter.SetInput(self._reslice.GetOutput())
            if 'between' in threshItem:
                cFilter.ThresholdBetween(*threshItem['between'])
            elif 'upper' in threshItem:
                cFilter.ThresholdByUpper(threshItem['upper'])
            cFilter.ReplaceInOn()
            cFilter.SetInValue(threshItem['target'])
            cFilter.ReplaceOutOn()
            cFilter.SetOutValue(0)
            threshFilters.append(cFilter)

        # @todo: handle many
        aFilter = vtk.vtkImageMathematics()
        aFilter.SetInput1(threshFilters[0].GetOutput())
        aFilter.SetInput2(threshFilters[1].GetOutput())
        aFilter.SetOperationToAdd()

        self._castFunction = vtk.vtkImageCast()
        self._castFunction.SetInput(aFilter.GetOutput())
        self._castFunction.SetOutputScalarTypeToUnsignedChar()

        #for i in threshFilters:
        #
        #    pass
        pass

    def _make3d(self):

        reslice3d = vtk.vtkImageReslice()

        # @todo: to params: background level, interpolation mode, slab number of slices,
        #reslice.SetBackgroundColor(1,1,1,1)
        reslice3d.SetInputConnection(self._volume.GetOutputPort())
        reslice3d.SetBackgroundLevel(-1000)   # needed to cut edges
        reslice3d.SetOutputDimensionality(3)
        reslice3d.SetResliceAxes(self._resliceAxes2)
        reslice3d.SetInterpolationModeToLinear()

        self._reslice3d = reslice3d
        print self._resliceAxes2

        boneExtractor = vtk.vtkContourFilter()
        boneExtractor.SetInput(reslice3d.GetOutput())
        boneExtractor.SetValue(0, 1150)
        boneNormals = vtk.vtkPolyDataNormals()
        boneNormals.SetInput(boneExtractor.GetOutput())
        boneNormals.SetFeatureAngle(60.0)

        plane = vtk.vtkPlane()
        #plane.SetOrigin(150, 0, 30)
        x = self._resliceAxes.GetElement(0, 3)
        y = self._resliceAxes.GetElement(1, 3)
        z = self._resliceAxes.GetElement(2, 3)
        plane.SetOrigin(x, y, z)
        plane.SetNormal(0, -1, 0)
        #boneStripper = vtk.vtkStripper()
        #boneStripper.SetInput(boneNormals.GetOutput())
        #boneMapper = vtk.vtkPolyDataMapper()
        #boneMapper.SetInput(boneStripper.GetOutput())
        #boneMapper.ScalarVisibilityOff()
        #bone = vtk.vtkActor()
        #bone.SetMapper(boneMapper)
        #bone.GetProperty().SetDiffuseColor(1, 1, .9412)
        clipper = vtk.vtkClipPolyData()
        clipper.SetInputConnection(boneNormals.GetOutputPort())
        clipper.SetClipFunction(plane)
        clipper.GenerateClipScalarsOn()
        clipper.GenerateClippedOutputOn()
        clipper.SetValue(0.5)
        clipMapper = vtk.vtkPolyDataMapper()
        clipMapper.SetInputConnection(clipper.GetOutputPort())
        clipMapper.ScalarVisibilityOff()
        from vtk.util.colors import peacock, tomato
        backProp = vtk.vtkProperty()
        backProp.SetDiffuseColor(tomato)
        clipActor = vtk.vtkActor()
        clipActor.SetMapper(clipMapper)
        clipActor.GetProperty().SetColor(peacock)
        clipActor.SetBackfaceProperty(backProp)

        # Here we are cutting the cow. Cutting creates lines where the cut
        # function intersects the model. (Clipping removes a portion of the
        # model but the dimension of the data does not change.)
        #
        # The reason we are cutting is to generate a closed polygon at the
        # boundary of the clipping process. The cutter generates line
        # segments, the stripper then puts them together into polylines. We
        # then pull a trick and define polygons using the closed line
        # segements that the stripper created.
        cutEdges = vtk.vtkCutter()
        cutEdges.SetInputConnection(boneNormals.GetOutputPort())
        cutEdges.SetCutFunction(plane)
        cutEdges.GenerateCutScalarsOn()
        cutEdges.SetValue(0, 0.5)
        cutStrips = vtk.vtkStripper()
        cutStrips.SetInputConnection(cutEdges.GetOutputPort())
        cutStrips.Update()
        cutPoly = vtk.vtkPolyData()
        cutPoly.SetPoints(cutStrips.GetOutput().GetPoints())
        cutPoly.SetPolys(cutStrips.GetOutput().GetLines())

        # Triangle filter is robust enough to ignore the duplicate point at
        # the beginning and end of the polygons and triangulate them.
        cutTriangles = vtk.vtkTriangleFilter()
        cutTriangles.SetInput(cutPoly)
        cutMapper = vtk.vtkPolyDataMapper()
        cutMapper.SetInput(cutPoly)
        cutMapper.SetInputConnection(cutTriangles.GetOutputPort())
        cutActor = vtk.vtkActor()
        cutActor.SetMapper(cutMapper)
        cutActor.GetProperty().SetColor(peacock)

        # The clipped part of the cow is rendered wireframe.
        restMapper = vtk.vtkPolyDataMapper()
        restMapper.SetInputConnection(clipper.GetClippedOutputPort())
        restMapper.ScalarVisibilityOff()
        restActor = vtk.vtkActor()
        restActor.SetMapper(restMapper)
        restActor.GetProperty().SetRepresentationToWireframe()
        #self._boneActor = bone
        self._boneActor = (clipActor, cutActor, restActor)

    def _showSliceOn3D(self):
        self._make3d()
        hueLut = vtk.vtkLookupTable()
        hueLut.SetTableRange(0, 2000)
        hueLut.SetHueRange(0, 1)
        hueLut.SetSaturationRange(1, 1)
        hueLut.SetValueRange(1, 1)
        hueLut.Build()

        axialColors = vtk.vtkImageMapToColors()
        axialColors.SetInput(self._reslice3d.GetOutput())
        axialColors.SetLookupTable(hueLut)
        axial = vtk.vtkImageActor()
        axial.SetInput(axialColors.GetOutput())
        #axial.SetDisplayExtent(54, 54, -32, 137, -106, 35)
        axial.SetDisplayExtent(-300, 300, -300, 300, 40, 40)

        aCamera = vtk.vtkCamera()
        aCamera.SetViewUp(0, 0, -1)
        aCamera.SetPosition(0, 1, 0)
        aCamera.SetFocalPoint(0, 0, 0)
        #aCamera.Yaw(21)
        aCamera.Pitch(90)
        #aCamera.Azimuth(90)
        #aCamera.Elevation(-90)
        aCamera.ComputeViewPlaneNormal()

        print self._boneActor

        self._renderer3D = vtk.vtkRenderer()
        for i in self._boneActor:
            self._renderer3D.AddActor(i)
        #self._renderer3D.AddActor(self._boneActor)
        #self._renderer3D.AddActor(axial)
        self._renderer3D.SetActiveCamera(aCamera)
        self._renderer3D.ResetCamera()

        pass

    def work(self):
        if not hasattr(self, '_reslice'):
            self._resliceFunction()

        if 'castMethod' in self._outputParams:
            if 'cast' == self._outputParams['castMethod']:
                self._useCast()
            elif 'table' == self._outputParams['castMethod']:
                self._useLookupTable()
            elif 'thresh' == self._outputParams['castMethod']:
                self._useThresholdSummary()
        else:
            self._useCast()

        if 0 == self._outputMethod:
            #self.setSlice(10)
            #self._makeInteractor()
            self.setSlice(200)
            self._showSliceOn3D()
            self._makeInteractor()
        elif 1 == self._outputMethod:
            self._makeImage()
        elif 2 == self._outputMethod:
            self._makeNumpyArray()

        # @todo: make*Series

    def _makeInteractor(self):
        self._actor = actor = vtk.vtkImageActor()
        actor.GetMapper().SetInputConnection(self._castFunction.GetOutputPort())
        self._renderer = renderer = vtk.vtkRenderer()
        renderer.AddActor(actor)
        renderer.SetViewport(0.0, 0.5, 1.0, 1.0)
        #renderer3D = vtk.vtkRenderer()
        #renderer3D.AddActor(self._skinActor)
        #renderer3D.SetViewport(0.0, 0.0, 1.0, 0.5)
        self._renderer3D.SetViewport(0.0, 0.0, 1.0, 0.5)
        #renderer3D.SetBackground(1, 1, 1)
        #renderer3D.SetActiveCamera(self._spaceCamera)
        #renderer3D.ResetCameraClippingRange()
        #renderer3D.ResetCamera()

        self._window = window = vtk.vtkRenderWindow()
        window.AddRenderer(renderer)
        window.AddRenderer(self._renderer3D)
        window.SetSize(1024, 1024)

        renderer.ResetCameraClippingRange()   # may cause wrong images while inverted matrix
        #renderer.ResetCamera()

        imageStyle = vtk.vtkInteractorStyleImage()
        self._interactor = interactor = vtk.vtkRenderWindowInteractor()
        interactor.SetInteractorStyle(imageStyle)
        window.SetInteractor(interactor)
        window.Render()
        self.goAnimation()
        interactor.Start()
        pass

    def _makeImage(self, target=r'C:\Temp\1.png'):
        writer = vtk.vtkPNGWriter()
        writer.SetInput(self._castFunction.GetOutput())
        writer.SetFileName(target)
        writer.Write()
        pass

    def _makeNumpyArray(self):
        from vtk.util.numpy_support import vtk_to_numpy

        # only works with self._useCast()
        print self._castFunction.GetOutput().GetDimensions()
        vtk_array = self._castFunction.GetOutput().GetPointData().GetScalars()
        arr = vtk_to_numpy(vtk_array)
        print len(arr)
        print arr
        for i in arr:
            print i
        pass
        self._outputData = {
            'dimensions': self._castFunction.GetOutput().GetDimensions(),
            'nparray': arr,
        }
    pass

    def goAnimation(self):
        ar = (-220, 20)
        br = (-220, 340)
        cst = float(ar[1] - ar[0]) / (br[1] - br[0])
        print 'cst', cst, cst*20
        j = 0
        for i in range(-220, 340, 40):
            print 'i', i
            j += 1
            self.setSlice(i, -250 + cst*40*j-15)
            #self._renderer.ResetCameraClippingRange()
            self._renderer.ResetCameraClippingRange()
            self._renderer.ResetCamera()
            self._renderer3D.ResetCameraClippingRange()
            self._renderer3D.ResetCamera()
            #self._renderer.Update()
            #self._renderer3D.Update()
            self._window.Render()
            #print self._resliceAxes
            #print self._resliceAxes2
            time.sleep(0.5)
        pass

    def _makeInteractorSeries(self):
        pass

    def _makeImageSeries(self):
        pass

    def _makeNumpySeries(self):
        pass

    def _makeImageGroup(self):
        # @todo: work with this
        pass


if __name__ == '__main__':
    import NPPlan
    NPPlan.init(programPath=r"D:\dev\p\npplan", test=True, utest=True, log=False)
    plan = NPPlan.planAbstract()
    plan.planLoadDicomSeriesFromFile("D:\\old.Users2\\_mrxak\\NGPlan\\Dicom\\01111140a\\")

    sc = sliceCutter(outputMethod=0, outputParams={'castMethod': 'cast'})
    #sc.setVtkVolume(plan.planGetVtkVolumeReader())
    sc.setVtkVolumePath(plan.planGetDicomLocalFolder())
    sc.setResliceAxesMatrix(matrix = [
        0.6965, 0.5943, -0.4021, 0,
        -0.5147, 0.8042, 0.2972, 0,
        0.5000, -0.0000, 0.8660, 0,
        0, 0, 0, 1
    ], invert=False)

    sc.work()
    #sc.goAnimation()
    print sc.getVolumeData()