# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 04.09.13
@summary: 
'''

'''
// CPP Code:
/*=========================================================================

  Program:   Visualization Toolkit
  Module:    ImageSlicing.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
//
// This example shows how to load a 3D image into VTK and then reformat
// that image into a different orientation for viewing.  It uses
// vtkImageReslice for reformatting the image, and uses vtkImageActor
// and vtkInteractorStyleImage to display the image.  This InteractorStyle
// forces the camera to stay perpendicular to the XY plane.
//
// Thanks to David Gobbi of Atamai Inc. for contributing this example.
//

#include "vtkSmartPointer.h"
#include "vtkImageReader2.h"
#include "vtkMatrix4x4.h"
#include "vtkImageReslice.h"
#include "vtkLookupTable.h"
#include "vtkImageMapToColors.h"
#include "vtkImageActor.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkInteractorStyleImage.h"
#include "vtkCommand.h"
#include "vtkImageData.h"
#include "vtkImageMapper3D.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkInformation.h"

// The mouse motion callback, to turn "Slicing" on and off
class vtkImageInteractionCallback : public vtkCommand
{
public:

  static vtkImageInteractionCallback *New() {
    return new vtkImageInteractionCallback; };

  vtkImageInteractionCallback() {
    this->Slicing = 0;
    this->ImageReslice = 0;
    this->Interactor = 0; };

  void SetImageReslice(vtkImageReslice *reslice) {
    this->ImageReslice = reslice; };

  vtkImageReslice *GetImageReslice() {
    return this->ImageReslice; };

  void SetInteractor(vtkRenderWindowInteractor *interactor) {
    this->Interactor = interactor; };

  vtkRenderWindowInteractor *GetInteractor() {
    return this->Interactor; };

  virtual void Execute(vtkObject *, unsigned long event, void *)
    {
    vtkRenderWindowInteractor *interactor = this->GetInteractor();

    int lastPos[2];
    interactor->GetLastEventPosition(lastPos);
    int currPos[2];
    interactor->GetEventPosition(currPos);

    if (event == vtkCommand::LeftButtonPressEvent)
      {
      this->Slicing = 1;
      }
    else if (event == vtkCommand::LeftButtonReleaseEvent)
      {
      this->Slicing = 0;
      }
    else if (event == vtkCommand::MouseMoveEvent)
      {
      if (this->Slicing)
        {
        vtkImageReslice *reslice = this->ImageReslice;

        // Increment slice position by deltaY of mouse
        int deltaY = lastPos[1] - currPos[1];

        reslice->Update();
        double sliceSpacing = reslice->GetOutput()->GetSpacing()[2];
        vtkMatrix4x4 *matrix = reslice->GetResliceAxes();
        // move the center point that we are slicing through
        double point[4];
        double center[4];
        point[0] = 0.0;
        point[1] = 0.0;
        point[2] = sliceSpacing * deltaY;
        point[3] = 1.0;
        matrix->MultiplyPoint(point, center);
        matrix->SetElement(0, 3, center[0]);
        matrix->SetElement(1, 3, center[1]);
        matrix->SetElement(2, 3, center[2]);
        interactor->Render();
        }
      else
        {
        vtkInteractorStyle *style = vtkInteractorStyle::SafeDownCast(
          interactor->GetInteractorStyle());
        if (style)
          {
          style->OnMouseMove();
          }
        }
      }
    };

private:

  // Actions (slicing only, for now)
  int Slicing;

  // Pointer to vtkImageReslice
  vtkImageReslice *ImageReslice;

  // Pointer to the interactor
  vtkRenderWindowInteractor *Interactor;
};

// The program entry point
int main (int argc, char *argv[])
{
  if (argc < 2)
    {
    cout << "Usage: " << argv[0] << " DATADIR/headsq/quarter" << endl;
    return 1;
    }

  // Start by loading some data.
  vtkSmartPointer<vtkImageReader2> reader =
    vtkSmartPointer<vtkImageReader2>::New();
  reader->SetFilePrefix(argv[1]);
  reader->SetDataExtent(0, 63, 0, 63, 1, 93);
  reader->SetDataSpacing(3.2, 3.2, 1.5);
  reader->SetDataOrigin(0.0, 0.0, 0.0);
  reader->SetDataScalarTypeToUnsignedShort();
  reader->SetDataByteOrderToLittleEndian();
  reader->UpdateWholeExtent();

  // Calculate the center of the volume
  reader->Update();
  int extent[6];
  double spacing[3];
  double origin[3];


  reader->GetOutputInformation(0)->Get(vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT(), extent);
  reader->GetOutput()->GetSpacing(spacing);
  reader->GetOutput()->GetOrigin(origin);

  double center[3];
  center[0] = origin[0] + spacing[0] * 0.5 * (extent[0] + extent[1]);
  center[1] = origin[1] + spacing[1] * 0.5 * (extent[2] + extent[3]);
  center[2] = origin[2] + spacing[2] * 0.5 * (extent[4] + extent[5]);

  // Matrices for axial, coronal, sagittal, oblique view orientations
  //static double axialElements[16] = {
  //         1, 0, 0, 0,
  //         0, 1, 0, 0,
  //         0, 0, 1, 0,
  //         0, 0, 0, 1 };

  //static double coronalElements[16] = {
  //         1, 0, 0, 0,
  //         0, 0, 1, 0,
  //         0,-1, 0, 0,
  //         0, 0, 0, 1 };

  static double sagittalElements[16] = {
           0, 0,-1, 0,
           1, 0, 0, 0,
           0,-1, 0, 0,
           0, 0, 0, 1 };

  //static double obliqueElements[16] = {
  //         1, 0, 0, 0,
  //         0, 0.866025, -0.5, 0,
  //         0, 0.5, 0.866025, 0,
  //         0, 0, 0, 1 };

  // Set the slice orientation
  vtkSmartPointer<vtkMatrix4x4> resliceAxes =
    vtkSmartPointer<vtkMatrix4x4>::New();
  resliceAxes->DeepCopy(sagittalElements);
  // Set the point through which to slice
  resliceAxes->SetElement(0, 3, center[0]);
  resliceAxes->SetElement(1, 3, center[1]);
  resliceAxes->SetElement(2, 3, center[2]);

  // Extract a slice in the desired orientation
  vtkSmartPointer<vtkImageReslice> reslice =
    vtkSmartPointer<vtkImageReslice>::New();
  reslice->SetInputConnection(reader->GetOutputPort());
  reslice->SetOutputDimensionality(2);
  reslice->SetResliceAxes(resliceAxes);
  reslice->SetInterpolationModeToLinear();

  // Create a greyscale lookup table
  vtkSmartPointer<vtkLookupTable> table =
    vtkSmartPointer<vtkLookupTable>::New();
  table->SetRange(0, 2000); // image intensity range
  table->SetValueRange(0.0, 1.0); // from black to white
  table->SetSaturationRange(0.0, 0.0); // no color saturation
  table->SetRampToLinear();
  table->Build();

  // Map the image through the lookup table
  vtkSmartPointer<vtkImageMapToColors> color =
    vtkSmartPointer<vtkImageMapToColors>::New();
  color->SetLookupTable(table);
  color->SetInputConnection(reslice->GetOutputPort());

  // Display the image
  vtkSmartPointer<vtkImageActor> actor =
    vtkSmartPointer<vtkImageActor>::New();
  actor->GetMapper()->SetInputConnection(color->GetOutputPort());

  vtkSmartPointer<vtkRenderer> renderer =
    vtkSmartPointer<vtkRenderer>::New();
  renderer->AddActor(actor);

  vtkSmartPointer<vtkRenderWindow> window =
    vtkSmartPointer<vtkRenderWindow>::New();
  window->AddRenderer(renderer);

  // Set up the interaction
  vtkSmartPointer<vtkInteractorStyleImage> imageStyle =
    vtkSmartPointer<vtkInteractorStyleImage>::New();
  vtkSmartPointer<vtkRenderWindowInteractor> interactor =
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  interactor->SetInteractorStyle(imageStyle);
  window->SetInteractor(interactor);
  window->Render();

  vtkSmartPointer<vtkImageInteractionCallback> callback =
    vtkSmartPointer<vtkImageInteractionCallback>::New();
  callback->SetImageReslice(reslice);
  callback->SetInteractor(interactor);

  imageStyle->AddObserver(vtkCommand::MouseMoveEvent, callback);
  imageStyle->AddObserver(vtkCommand::LeftButtonPressEvent, callback);
  imageStyle->AddObserver(vtkCommand::LeftButtonReleaseEvent, callback);

  // Start interaction
  // The Start() method doesn't return until the window is closed by the user
  interactor->Start();

  return EXIT_SUCCESS;
}

'''

# python code

import vtk
from vtk.util.numpy_support import vtk_to_numpy

VTK_DATA_ROOT = 'C:\\Users\\Public\\VTKData'
#reader = vtk.vtkImageReader2()
#reader.SetFilePrefix(VTK_DATA_ROOT + "/Data/headsq/quarter")
reader = vtk.vtkDICOMImageReader()
#reader.SetFilePrefix()
reader.SetDirectoryName("C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\")
#reader.SetDataExtent(0, 63, 0, 63, 1, 93)
#reader.SetDataSpacing(3.2, 3.2, 1.5)
#reader.SetDataOrigin(0.0, 0.0, 0.0)
reader.SetDataScalarTypeToInt()
reader.SetDataByteOrderToLittleEndian()
reader.UpdateWholeExtent()

reader.Update()

#extent = reader.GetOutputInformation(0).Get(vtk.vtkStreamingDemandDrivenPipeline.WHOLE_EXTENT())
extent = reader.GetOutput().GetExtent()
spacing = reader.GetOutput().GetSpacing()
origin = reader.GetOutput().GetOrigin()

print extent, spacing, origin

center = [0, 0, 0]
center[0] = origin[0] + spacing[0] * 0.5 * (extent[0] + extent[1])
center[1] = origin[1] + spacing[1] * 0.5 * (extent[2] + extent[3])
center[2] = origin[2] + spacing[2] * 0.5 * (extent[4] + extent[5])
print center


sagittalElements = [0, 0,-1, 0,
1, 0, 0, 0,
0,-1, 0, 0,
0, 0, 0, 1 ]

obliqueElements = [
         1, 0, 0, 0,
         0, 0.866025, -0.5, 0,
         0, 0.5, 0.866025, 0,
         0, 0, 0, 1 ]

obliqueElements2 = [
    0.6965, 0.5943, -0.4021, 0,
    -0.5147, 0.8042, 0.2972, 0,
    0.5000, -0.0000, 0.8660, 0,
    0, 0, 0, 1
]

obliqueElements3 = [
    -0.18893742443639305, -0.9352972578934333, -0.29920175304359226, 0,
    0.9379375565914191, -0.26211588383052414, 0.22708677499813312, 0,
    -0.29081916990228146, -0.23772737078586345, 0.9267739247500336, 0,
    0, 0, 0, 1
]

# @todo: вычислять начальное и конечное положение для нарезки

resliceAxes = vtk.vtkMatrix4x4()
#resliceAxes.DeepCopy(sagittalElements)
resliceAxes.DeepCopy(obliqueElements3)
resliceAxes.Invert()  # ?

resliceAxes.SetElement(0, 3, center[0])
#resliceAxes.SetElement(1, 3, center[1])
resliceAxes.SetElement(1, 3, 150)
resliceAxes.SetElement(2, 3, center[2])

reslice = vtk.vtkImageReslice()

#reslice.SetBackgroundColor(1,1,1,1)
reslice.SetInputConnection(reader.GetOutputPort())
reslice.AutoCropOutputOn()
reslice.SetBackgroundLevel(-1000)   # needed to cut edges
reslice.SetOutputDimensionality(2)
reslice.SetResliceAxes(resliceAxes)
reslice.SetInterpolationModeToLinear()
reslice.SetSlabNumberOfSlices(3)
reslice.Update()

print 'reslice params'
print reslice.GetBackgroundColor()
print reslice.GetBackgroundLevel()
print reslice.GetResliceAxesDirectionCosines()
print reslice.GetResliceAxesOrigin()
print reslice.GetResliceTransform()
print reslice.GetInformationInput()
print reslice.GetTransformInputSampling()
print reslice.GetInterpolationModeAsString()
print reslice.GetSlabNumberOfSlices()
print reslice.GetOutputSpacing()
print reslice.GetOutputOrigin()
print reslice.GetOutputExtent()
print reslice.GetStencil()
print 'reslice params end'
print reslice

# // Create a greyscale lookup table
table = vtk.vtkLookupTable()
table.SetRange(0, 2000) #// image intensity range
table.SetValueRange(0.0, 1.0) #// from black to white
table.SetSaturationRange(0.0, 0.0) #// no color saturation
table.SetRampToLinear()
table.Build()


#// Map the image through the lookup table
color = vtk.vtkImageMapToColors()
color.SetLookupTable(table)
color.SetInputConnection(reslice.GetOutputPort())

imSoft=vtk.vtkImageThreshold()
imSoft.SetInput(reslice.GetOutput())
imSoft.ThresholdBetween(-600,300)
imSoft.ReplaceInOn()
imSoft.SetInValue(128)
imSoft.ReplaceOutOn()
imSoft.SetOutValue(0)

addImages=vtk.vtkImageMathematics()
addImages.SetInput1(imSoft.GetOutput())
addImages.SetInput2(color.GetOutput())
addImages.SetOperationToAdd()

#extract = vtk.vtkExtractEdges()
#extract.SetInputConnection(reslice.GetOutputPort())

toChar = vtk.vtkImageCast()
toChar.SetInput(reslice.GetOutput())
toChar.SetOutputScalarTypeToInt()
toChar.Update()


imSoft=vtk.vtkImageThreshold()
imSoft.SetInput(reslice.GetOutput())
imSoft.ThresholdBetween(-300,300)
imSoft.ReplaceInOn()
imSoft.SetInValue(128)
imSoft.ReplaceOutOn()
imSoft.SetOutValue(0)

imBone=vtk.vtkImageThreshold()
imBone.SetInput(reslice.GetOutput())
imBone.ThresholdByUpper(301)
imBone.ReplaceInOn()
imBone.SetInValue(255)
imBone.ReplaceOutOn()
imBone.SetOutValue(0)

addImages=vtk.vtkImageMathematics()
addImages.SetInput1(imSoft.GetOutput())
addImages.SetInput2(imBone.GetOutput())
addImages.SetOperationToAdd()

summary=vtk.vtkImageCast()
summary.SetInput(addImages.GetOutput())
summary.SetOutputScalarTypeToUnsignedChar()



#// Display the image
actor = vtk.vtkImageActor()
#actor.GetMapper().SetInputConnection(reslice.GetOutputPort())
actor.GetMapper().SetInputConnection(summary.GetOutputPort())
print actor.GetBounds()

renderer = vtk.vtkRenderer()
renderer.AddActor(actor)
#renderer.SetBackground(1, 1, 1)

window = vtk.vtkRenderWindow()
window.AddRenderer(renderer)
window.SetSize(512, 512)

vtk_win_im = vtk.vtkWindowToImageFilter()
vtk_win_im.SetInput(window)
vtk_win_im.Update()


cam1 = renderer.GetActiveCamera()
print cam1
#cam1.ParallelProjectionOn()
#renderer.ResetCameraClippingRange()
renderer.ResetCamera()

imageStyle = vtk.vtkInteractorStyleImage()
interactor = vtk.vtkRenderWindowInteractor()
interactor.SetInteractorStyle(imageStyle)
window.SetInteractor(interactor)
window.Render()

vtk_image = vtk_win_im.GetOutput()
print vtk_win_im.GetOutput()
print 'point data'
print vtk_image.GetPointData()
print vtk_image.GetPointData().GetScalars()
print 'vrange'
print vtk_image.GetPointData().GetScalars().GetValueRange()

height, width, _ = vtk_image.GetDimensions()
vtk_array = toChar.GetOutput().GetPointData().GetScalars()
#components = vtk_array.GetNumberOfComponents()

#for i in range(0, 786431):
#    print vtk_image.GetPointData().GetScalars().GetValue(i)

arr = vtk_to_numpy(vtk_array)#.reshape(height, width, components)
print 'numpy array'
print arr
print len(arr)
print arr[arr>0], len(arr[arr>0]) # @todo: loop both sides until this becomes zero

interactor.Start()

print actor
'''
for i in range(-256, 320):

    resliceAxes.SetElement(1, 3, i)
    reslice.Update()
    #resliceAxes.Update()
    renderer.ResetCameraClippingRange()
    renderer.ResetCamera()
    actor.Update()
    window.Render()


    w2if = vtk.vtkWindowToImageFilter()
    w2if.SetInput(window)
    wr = vtk.vtkPNGWriter()
    wr.SetInput(w2if.GetOutput())
    outfile = r"C:\Temp\s1\screen%d.png" % (256+i)
    wr.SetFileName(outfile)
    wr.Write()'''