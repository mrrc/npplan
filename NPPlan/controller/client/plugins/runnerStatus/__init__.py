# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 23.09.13
@summary: 
'''

import wx
import NPPlan

from NPPlan.view.client.main.abstracts.list import listWithControls
#from NPPlan.view.client.main.abstracts.DVListWithControls import listWithControls


class statusPanel1 ( wx.Panel ):
    
    def __init__( self, parent ):
        wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.TAB_TRAVERSAL )
        
        bSizer1 = wx.BoxSizer( wx.VERTICAL )
        
        sbSizer1 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Currently running" ), wx.VERTICAL )
        
        self.currentRunningList = listWithControls(
            self,
            columns={
                '1': {'text': 'Task', 'size': 100, 'order': 0},
                '2': {'text': 'Assigned plan', 'size': 100, 'order': 1},
                '4': {'text': 'MPI', 'size': 80, 'order': 2},
                '3': {'text': 'Started at', 'size': 200, 'order': 3},
                '5': {'text': 'Machine', 'size': 100, 'order': 4}
            },
            controls={
                'add': {'text': 'Refresh', 'backend': self.backRefreshCurrent, 'icon': 'iconRefresh32.png'},
                'cleanup': {'text': 'Cleanup', 'backend': self.backCleanupCurrent, 'icon': 'iconRefresh32.png'},
            },
            controlsPosition='down',
            parser=self.curRunSelected,
        )
        sbSizer1.Add( self.currentRunningList, 13, wx.ALL|wx.EXPAND, 5 )
        
        
        bSizer1.Add( sbSizer1, 1, wx.EXPAND, 5 )
        
        sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Completed" ), wx.VERTICAL )
        
        bSizer3 = wx.BoxSizer( wx.HORIZONTAL )
        
        self.m_staticText1 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText1.Wrap( -1 )
        bSizer3.Add( self.m_staticText1, 0, wx.ALL, 5 )
        
        self.m_searchCtrl1 = wx.SearchCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_searchCtrl1.ShowSearchButton( True )
        self.m_searchCtrl1.ShowCancelButton( False )
        bSizer3.Add( self.m_searchCtrl1, 0, wx.ALL, 5 )
        
        
        sbSizer2.Add( bSizer3, 0, wx.EXPAND, 5 )
        
        self.completedList = listWithControls(
            self,
            columns={
                '1': {'text': 'Task', 'size': 100, 'order': 0},
                '2': {'text': 'Assigned plan', 'size': 100, 'order': 1},
                '6': {'text': 'Input path', 'size': 100, 'order': 2},
                '7': {'text': 'Output path', 'size': 100, 'order': 3},
                '8': {'text': 'mFile path', 'size': 100, 'order': 4},
                '3': {'text': 'Started at', 'size': 200, 'order': 5},
                '4': {'text': 'Completed at', 'size': 200, 'order': 6},
                '5': {'text': 'Machine', 'size': 100, 'order': 7},
                '9': {'text': 'MCNP type', 'size': 80, 'order': 8},
            },
            controls={
                'add': {'text': 'Refresh', 'backend': self.backRefreshCompleted, 'icon': 'iconRefresh32.png'},
            },
            controlsPosition='down',
            parser=self.curRunSelected,
        )
        #sbSizer1.Add( self.currentRunningList, 13, wx.ALL|wx.EXPAND, 5 )
        sbSizer2.Add( self.completedList, 10, wx.EXPAND, 5 )
        
        
        bSizer1.Add( sbSizer2, 1, wx.EXPAND, 5 )
        
        
        self.SetSizer( bSizer1 )
        self.Layout()

        self.Bind(wx.EVT_PAINT, self.onPaint)
        self.backRefreshCurrent()
        self.backRefreshCompleted()

    def curRunSelected(self, event=None, data=None, aType='selection'):
        pass

    def backCleanupCurrent(self, event=None, data=None):
        # ??
        pass

    def backRefreshCurrent(self, event=None, data=None):
        print event, data
        cRu = NPPlan.mongoConnection.mcnpqueue.find({'status.running': True})
        #self.currentRunningList.clear()
        for i in cRu:
            fName = i.name
            aPlan = i.assignedPlan or ''
            lStart = str(i.times.started.strftime('%Y/%m/%d - %H:%M:%S'))
            if not i.useMpi:
                aMpi = 'No'
            else:
                aMpi = '%d' % i.mpiProc
            #self.currentRunningList.addLine([fName, aPlan, lStart, aMpi])
            #self.currentRunningList.setItemTextAtColumn(self, 0, 0, fName)
            mch = i.machineStr or ''
            self.currentRunningList.addItem({'1': fName, '2': aPlan, '3': lStart, '4': aMpi, '5': mch})
        print 'bla'
        #alT = NPPlan.mongoConnection.mcnpqueue.find()
        #for i in alT:
        #    print i
        pass
    def backRefreshCompleted(self, event=None, data=None):
        print event, data
        cRu = NPPlan.mongoConnection.mcnpqueue.find({'status.running': False, 'status.completed': True})
        #self.completedList.clear()
        for i in cRu:
            fName = i.name
            aPlan = i.assignedPlan or ''
            lStart = str(i.times.started.strftime('%Y/%m/%d - %H:%M:%S'))
            lFinish = str(i.times.completed.strftime('%Y/%m/%d - %H:%M:%S'))
            #self.currentRunningList.addLine([fName, aPlan, lStart, aMpi])
            #self.currentRunningList.setItemTextAtColumn(self, 0, 0, fName)
            mch = i.machineStr or ''
            iPath = i.paths.source or ''
            oPath = i.paths.output or ''
            mPath = i.paths.mfile or ''
            mver = i.version or ''
            self.completedList.addItem({'1': fName, '2': aPlan, '3': lStart, '4': lFinish, '5': mch,
                                        '6': iPath, '7': oPath, '8': mPath, '9': mver})
        print 'bla'
        #alT = NPPlan.mongoConnection.mcnpqueue.find()
        #for i in alT:
        #    print i
        pass

    def onPaint(self, event):
        """

        @param event:
        @type event: wx._core.PaintEvent
        @return:
        """
        #print 'aaa', type(event)
        event.Skip()
    
    def __del__( self ):
        pass


class statusFrame1 ( wx.Frame ):
    
    def __init__( self, parent ):
        wx.Frame.__init__ ( self, None, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
        
        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(statusPanel1(self), 1, wx.ALL | wx.EXPAND)
        self.SetSizer(sizer)
        
        self.Centre( wx.BOTH )
    
    def __del__( self ):
        pass
    
if __name__ == '__main__':
    NPPlan.init(programPath="c:/NPPlan3/", test=True, log=False, utest=True)
    app = wx.PySimpleApp()
    rtw = statusFrame1(app)
    rtw.Show()
    app.MainLoop()
    pass
