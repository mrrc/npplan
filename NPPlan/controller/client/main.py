# -*- coding: utf8 -*-
'''
Модуль главного контроллера. Создаёт вступительный экран и главное окно клиента. Используется как синглетон в рамках одного запуска
@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 4
@date: 10.10.2012
@summary: Модуль главного контроллера
'''

import NPPlan

# @todo: refactor below
import os
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship
NPPlan.engine = create_engine('sqlite:///data.db', echo=False)
NPPlan.coreBase = declarative_base(NPPlan.engine)
if not os.path.exists('/data.db'):
    NPPlan.coreBase.metadata.create_all()
from NPPlan.model.alch import *
NPPlan.coreSession = sessionmaker()()
# EOF refactor

import NPPlan.controller.logger as log
import NPPlan.controller.client.mainC.centerPanel as centerPanel
import NPPlan.controller.client.mainC.ribbon as ribbon
from NPPlan.controller.client.timedEvents.main import notifyTimer
from NPPlan.controller.client.mainC.abstracts.handler import handler
from NPPlan.controller.client.mainC.abstracts import baseController
from NPPlan.controller.client.mainC.dynStatusBar import dynStatusBarController

import wx
from twisted.internet import wxreactor
wxreactor.install()
import wx.lib.inspection
from NPPlan.view.client.splash import splashFrame as splashScreen
import NPPlan.view.client.main.main as mainWin
import NPPlan.view.client.main.dynStatusBar as statusBar
import NPPlan.view.client.taskBarIcon as tbIcon
import wx.lib.agw.pybusyinfo as PBI

#from twisted.spread import pb
from NPPlan.model.client.pbHandler import npplanPbFactory
from twisted.internet import reactor, protocol
from twisted.protocols import basic
from twisted.internet.error import ConnectionRefusedError

from NPPlan.model.client.pbHandler.callbacker import callbacker

# @todo: delimiter
splashUri = NPPlan.config.appPath+'/images/splash.jpg'
#iconUri = NPPlan.config.appPath+'/images/smallIcon.png'
_handler = None


class myApp(wx.App):
    """
    Приложение wx.App
    """
    def OnInit(self):
        self.SetAppName('NPPlan')
        #self.SetTopWindow()
        #main()
        #s = splashScreen()
        #s.Show()
        main()
        return True


class mainController(baseController):
    """
    Класс главного контроллера, вызывается после обработки splashScreen'а.
    """
    def __init__(self):
        """
        Конструктор класса. Вызывает главное окно (инстанс NPPlan.view.client.main.main)

        Забирает параметры главного окна, вызывает контроллеры верхнего меню и центральной панели для полученных элементов.
        """
        log.log('main', 'mainController starting')

        # вступительная стадия
        changeCurrentStageType('welcome')

        # вызываем класс отображения окна
        self.frame = mainWin.main(None, _('NPPlan window title'))
        wx.PyApp.SetTopWindow(wx.GetApp(), self.frame)

        self._stageConditions = {}
        self._stageSwitchFunctions = {}

        # загружаем иконку приложения
        iconUri = NPPlan.pathJoin(NPPlan.config.appPath, 'images', 'smallIcon.png')
        icon = wx.Icon(iconUri, wx.BITMAP_TYPE_PNG, 16, 16)

        # устанавливаем иконку окна и вызываем трей
        # @todo: формирование меню трея
        self.frame.mSetIcon(icon, tbIcon.taskBar(self.frame, icon, _('NPPlan tray tooltip')))

        # формируем dynStatusBar и устанавливаем его главным статус-баром окна
        sbView = statusBar.dynStatusBar(self.frame)
        self.frame.SetStatusBar(sbView)
        sbController = dynStatusBarController(sbView, cParent=self)

        # верхнее меню
        self.rb = ribbon.ribbon(self.frame)
        self.rb.createRibbon()

        self.cp = centerPanel.centerPanel(self.frame.centerPanel, topController=self)
        self.cp.createCenter()

        #import NPPlan.model.file.configReader.stages as st
        #stages = st.getStagesForType('welcome')
        #print stages[0]['name']

        # выполняем преобразование размеров
        self.frame.changeLayout()

        # выводим окно на экран
        self.frame.Show()

        log.log('main', 'mainController started')

        #test
        #self.getCenter().getCurInfo().deleteAllPages()
        #self.getCenter().getCurInfo().addManyPages(['test1', 'test2'])
        #self.getCenter().getCurInfo().setCurrentPage(0)

        # timed events
        # wx.CallAfter(self.startTimer, 200)

        # global handler
        globals()['_handler'] = handler()
        globals()['_handler'].register('main', self, self.frame)
        globals()['_handler'].register('ribbon', self.rb, self.frame.ribbon)
        globals()['_handler'].register('statusBar', sbController, sbView)
        NPPlan.cHandler = globals()['_handler']  # check if needed

        self.cp.registerInformer()

        #testPlugin = NPPlan.pluginManager.loadPlugin('test')
        #testPlugin.ptest()
        #self.setDbStatusBar()
        isDebug = 'ON' if 1 == int(NPPlan.config.appConfig.logger.debug) else 'OFF'
        NPPlan.inform("NPPlan %s run. Debug mode: %s" % (NPPlan.VERSION, isDebug), timer=5000)

    def createBusy(self, text='Please wait...', title='Working'):
        if hasattr(self, 'busy'):
            return
        self.busy = PBI.PyBusyInfo(text, parent=None, title=title, )
        wx.Yield()

    def deleteBusy(self):
        del self.busy

    def startTimer(self, event):
        self.frame.mainTimer = notifyTimer()
        self.frame.mainTimer.Start(5000)

    def getTopWindow(self):
        """
        Возвращает главное окно
        @return: окно
        @rtype: wx.Frame
        """
        return self.frame

    def getRibbon(self):
        """
        Возвращает инстанс контроллера верхнего окна
        @return: ribbon
        @rtype: NPPlan.controller.client.mainC.ribbon.ribbon
        """
        return self.rb

    def getCenter(self):
        """
        Возврашает инстанс контроллера верхнего окна
        @return: centralCpanel
        @rtype: NPPlan.controller.client.mainC.centerPanel.centerPanel
        """
        return self.cp

    def runWelcomeWindow(self, **kwargs):
        """
        Вызывает окно приветствия, изменяет текущую стадию
        """
        self.frame.Freeze()
        changeCurrentStageType('welcome')
        self.getCenter().createCenter()
        self.frame.Thaw()

    def createPlan(self, *args, **kwargs):
        log.log('client.main', 'Create new empty plan')
        self.frame.Freeze()
        changeCurrentStageType('main')
        NPPlan.config.runningConfig.curPlanMode = 'emptyPlan'
        self.getCenter().createCenter()
        self.frame.Thaw()
        pass

    def registerStageExitCondition(self, exitCondition, forceStage=None):
        pass

    def registerStageExitFunction(self, exitFunction, forceStage=None):
        pass

    def checkStageCondition(self, forceStage=None):
        # @todo: do this
        return True
        #return False
        pass

    def stageHasExitCondition(self, forceStage=None):
        if forceStage is not None:
            stage = forceStage
        else:
            try:
                stage = (getCurrentStageType(), NPPlan.config.runningConfig.curSubStage)
            except KeyError:
                stage = (getCurrentStageType(), )
        print 'stage condition for', stage
        # @todo: do this
        return True
        pass

    def runPatientsWindow(self, **kwargs):
        """
        Вызывает окно работы с пациентом.
        @keyword type: тип вызова (добавление/список)
        @type type: string in list ['addPatient', 'listPatient']
        """
        print 'adding new patient from mainController'
        self.frame.Freeze()

        # setting left buttons
        # @todo: doctor-dependent stages
        changeCurrentStageType('main')
        self.getCenter().createCenter()

        print self.cp.getCurrentPage()

        # @todo: another inheritance (to center panel)
        from NPPlan.view.client.main.stages.addPatient import addPatientPanel
        print self.getCenter().getScreens().list()
        panel = self.getCenter().getScreens().get('main.patData')
        panel.GetParent().Freeze()
        sz = wx.BoxSizer(wx.VERTICAL)
        pat = addPatientPanel(panel)
        sz.Add(pat, 1, wx.EXPAND|wx.ALL)
        panel.SetSizer(sz)
        panel.Layout()
        panel.GetParent().Layout()
        panel.GetParent().Thaw()

        #NPPlan.setRunningConfig({
        #    'patientStage' : {
        #        'stage' : 'edit',
        #        'type' : kwargs.get('type', 'addPatient'),
        #    }
        #})
        NPPlan.config.runningConfig.patientStage = {'stage' : 'edit', 'type' : kwargs.get('type', 'addPatient')}

        import NPPlan.controller.client.mainC.stages.patData as patData
        patData.init(pat)

        self.frame.Thaw()
        pass

    def showMessage(self, title, message, flags=wx.OK|wx.ICON_ERROR):
        dlg = wx.MessageDialog(self.frame,
                               message,
                               title,
                               flags
                               )
        dlg.ShowModal()
        dlg.Destroy()


def getHandler():
    global _handler
    return _handler


def changeCurrentStageType(type):
    """
    Изменение текущей стадии в рабочем конфиге приложения
    @todo: accurate check
    @param type: стадия
    @type type: string
    """
    log.log('main', 'StageType changed to \"%s\"' %(type, ))
    #NPPlan._programData['curStageType'] = type
    #NPPlan.setRunningConfig('curStageType', type)
    NPPlan.config.runningConfig.curStageType = type


def getCurrentStageType():
    """
    Возвращает текущую стадию
    @return: стадия
    @rtype: string
    """
    return NPPlan.config.runningConfig.curStageType


program = None


def main():
    """
    Загрузчик главного окна
    """
    global program
    NPPlan.program = program = mainController()
    # inspection tool
    #if 1 == int(NPPlan.getProgramData()['appConfig']['client']['inspect']):
    if 1 == int(NPPlan.config.appConfig.client.inspect):
        wx.FutureCall(1000, wx.lib.inspection.InspectionTool().Show)
        #wx.lib.inspection.InspectionTool().Show()


def getApp():
    """
    Возвращает инстанс главного контроллера
    """
    global program
    return program


def start(planInfo=None):
    """
    Запускает главное приложение
    """
    NPPlan.app = app = myApp(False)
    #globals()['_handler'].main.view.Show()
    reactor.registerWxApp(app)
    try:
        factory = npplanPbFactory()
        reactor.connectTCP("localhost", 8800, factory)
        def1 = factory.getRootObject()
        NPPlan.config.pbFactory = factory
        NPPlan.config.pbRoot = def1
        pbCallbacker = callbacker()
        def1.addCallbacks(pbCallbacker.handle)
        if 1 == factory._status:
            reactor.callLater(2, setPbToCHandler, pbCallbacker)
        #globals()['_handler'].register('pbCallbacker', pbCallbacker, None)
        reactor.run()
    except ConnectionRefusedError:
        log.log('core', 'No connection to NPPlan server')
        app.MainLoop()
    return app


def setPbToCHandler(pbCallbacker):
    NPPlan.cHandler.register('pbCallbacker', pbCallbacker, None)


def getCurrentStageInfo(flag=0):
    if 0 == flag:
        return NPPlan.config.runningConfig.curStageType, NPPlan.config.runningConfig.curSubStageType
    if 1 == flag:
        return {'stage': NPPlan.config.runningConfig.curStageType,
                'subStage': NPPlan.config.runningConfig.curSubStageType}

NPPlan.getCurrentStageInfo = getCurrentStageInfo
