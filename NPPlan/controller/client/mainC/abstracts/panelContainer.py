# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 18.03.13
@summary: 
'''

import NPPlan
import wx


class panelContainer(object):
    def __init__(self, parent=None):
        self.__dict__['className'] = wx.Panel
        self.__dict__['parent'] = parent
        self.__dict__['panels'] = {}
        pass

    def add(self, name, panel, rewrite=False):
        #if (name in self or panel in self) and not rewrite:
        #    return
        self.__dict__['panels'][name] = panel

    def new(self, parent, name):
        if parent is None and self.__dict__['parent'] is not None:
            self.__dict__['panels'][name] = self.__dict__['className'](self.__dict__['parent'])
        elif parent is not None:
            self.__dict__['panels'][name] = self.__dict__['className'](parent)
        else:
            return

    def replace(self, sizer, oldName, newName):
        sz = self.__dict__['panels'][oldName].GetSize()
        print 'oldName', oldName, 'size', sz
        self.__dict__['panels'][oldName].Hide()
        sizer.Replace(self.__dict__['panels'][oldName], self.__dict__['panels'][newName], True)
        self.__dict__['panels'][newName].Show()
        self.__dict__['panels'][newName].SetSize(sz)
        return self.__dict__['panels'][newName]

    def hasPanel(self, name):
        return name in self.__dict__['panels']

    def __contains__(self, item):
        if isinstance(item, str):
            return self.hasPanel(item)
        else:
            return item in self.__dict__['panels'].values()

    def __getattr__(self, item):
        return self.__dict__['panels'][item]

    def __setattr__(self, key, value):
        self.add(key, value, True)