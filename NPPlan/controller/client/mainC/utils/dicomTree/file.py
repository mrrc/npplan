# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 05.10.12
@summary:
'''

import NPPlan.model.file.dicom.rawReader as dr

import os

def openFolder(event):
    # @todo: заготовка
    from NPPlan.controller.client.mainC.utils.dicomTree.dicomTree import getPanel
    _panel = getPanel()
    _panel.runDialog(openedFolder)
    pass

def openedFolder(path):
    import NPPlan.controller.client.mainC.utils.dicomTree.storer as storage
    storage.init(path)
    for fileName in os.listdir(path):
        storage.add(fileName, dr.readRawInfo(path+'/'+fileName))
    from NPPlan.controller.client.mainC.utils.dicomTree.dicomTree import getPanel
    _panel = getPanel()
    _panel.addChoices(storage.retChoices())
    pass