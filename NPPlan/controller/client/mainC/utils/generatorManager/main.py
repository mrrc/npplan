# -*- coding: utf8 -*-
'''
Программа, работающая с генератором. Посылает команды на микроконтроллер управления.
@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 25.07.13
@summary: Полнофункциональный набор классов-окон и панелей для работы с генератором
'''

import wx
import wx.lib.agw.flatnotebook as fnb
import os
import cv, cv2
if os.name == 'nt': #sys.platform == 'win32':
    from serial.tools.list_ports_windows import *
elif os.name == 'posix':
    from serial.tools.list_ports_posix import *
import serial
import thread
from wx.lib.wordwrap import wordwrap
import subprocess
import ConfigParser


class comCommunicationThread:
    """
    Класс для описания отдельного потока приложения для работы с COM-портом
    """
    def __init__(self, win):
        """
        Инициализирует класс, создаёт уникальный хендлер для порта
        @param win: Окно
        @type win: generatorPanel
        """
        self._serialPort = serial.Serial()
        self._port = None
        self.win = win
        self.curWriteCommand = ''
        self.curReaderCommand = False

    def setPort(self, port):
        """
        Устанавливает номер порта
        @param port: номер порта
        @type port: str
        @return:
        """
        self._port = port

    def sendCommand(self, command, ctype='w'):
        """
        Записывает следующую команду для исполнения в поток
        @param command: команда
        @type command: string
        @param ctype: одно из ['w', 'r']
        @type ctype: str
        @return:
        """
        if 'w' == ctype:
            self.curWriteCommand = command

    def Start(self):
        """
        Функция открытия порта
        """
        self._serialPort.port = self._port
        print 'Got port at thread', self._port
        self._serialPort.open()
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())

    def Stop(self):
        """
        Функция, закрывающая порт и вызывающая прерывания тела потока
        """
        self._serialPort.close()
        self.keepGoing = False

    def IsRunning(self):
        """
        Запущен ли поток
        @return: статус потока
        @rtype: bool
        """
        return self.running

    def Run(self):
        """
        Тело потока, бесконечный цикл, следящий за очередью команд и посылающий их в контроллер
        """
        while self.keepGoing:
            if '' != self.curWriteCommand:
                self._serialPort.write(self.curWriteCommand)
                self.curWriteCommand = ''
            wx.MilliSleep(200)
            pass

        self.running = False


class generatorPanel(wx.Panel):
    def __init__(self, parent, **kwargs):
        """
        Описание экранной формы
        @param parent: родительское окно
        @type parent: wx.Window
        @return: generatorPanel
        """
        wx.Panel.__init__ (self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, style=wx.TAB_TRAVERSAL)

        self._port = None
        self._ngRemPath = kwargs.get('ngRemPath', '')

        self._commands = {
            'lightOn': '1',   # подсветка
            'lightOff': '2',
            'backend': 'b',
            'enabled': '3',   # индикация коннекта
            'disabled': '4',
            'off': '0',
        }

        bSizer1 = wx.BoxSizer(wx.VERTICAL)

        bSizer2 = wx.BoxSizer(wx.HORIZONTAL)

        self.m_staticText1 = wx.StaticText(self, wx.ID_ANY, u"Порт", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText1.Wrap(-1)
        bSizer2.Add(self.m_staticText1, 0, wx.ALL, 5)

        m_choice1Choices = []
        self.m_choice1 = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice1Choices, 0)
        self.m_choice1.SetSelection(0)
        bSizer2.Add(self.m_choice1, 0, wx.ALL, 5)

        self.m_checkBox1 = wx.CheckBox(self, wx.ID_ANY, u"Автовыбор порта", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer2.Add(self.m_checkBox1, 0, wx.ALL, 5)
        self.m_checkBox1.Disable()

        #self.m_toggleBtn1=wx.ToggleButton(self, wx.ID_ANY, u"Toggle me!", wx.DefaultPosition, wx.DefaultSize, 0)
        #bSizer2.Add(self.m_toggleBtn1, 1, wx.ALL, 5)

        bSizer1.Add(bSizer2, 0, wx.ALL|wx.TOP, 5)

        bSizer3 = wx.BoxSizer(wx.VERTICAL)

        bSizer4 = wx.BoxSizer(wx.HORIZONTAL)

        self.m_staticText3 = wx.StaticText(self, wx.ID_ANY, u"Консоль \nустройства", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText3.Wrap(-1)
        bSizer4.Add(self.m_staticText3, 1, wx.ALIGN_CENTER|wx.ALL, 5)

        self.m_textCtrl1 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       wx.TE_MULTILINE | wx.TE_READONLY)
        bSizer4.Add(self.m_textCtrl1, 5, wx.ALL | wx.EXPAND, 5)

        bSizer3.Add(bSizer4, 1, wx.EXPAND, 5)

        self.m_button1 = wx.Button(self, wx.ID_ANY, u"Включить визуальную индикацию", wx.DefaultPosition,
                                   wx.DefaultSize, 0)
        bSizer3.Add(self.m_button1, 1, wx.ALL | wx.EXPAND, 5)

        self.m_button2 = wx.Button(self, wx.ID_ANY, u"Выключить визуальную индикацию", wx.DefaultPosition,
                                   wx.DefaultSize, 0)
        bSizer3.Add(self.m_button2, 1, wx.ALL | wx.EXPAND, 5)

        self.m_button3 = wx.Button(self, wx.ID_ANY, u"Запустить программу управления генератором", wx.DefaultPosition,
                                   wx.DefaultSize, 0)
        bSizer3.Add(self.m_button3, 1, wx.ALL | wx.EXPAND, 5)

        bSizer1.Add(bSizer3, 1, wx.EXPAND, 5)

        self.SetSizer(bSizer1)
        self.Layout()
        self.doBindings()
        self.log(u'Программа инициализирована')

    def doBindings(self):
        """
        Биндит события по нажатию соответствующих элементов
        """
        self.Bind(wx.EVT_CHOICE, self.setPort, self.m_choice1)
        self.Bind(wx.EVT_BUTTON, self.lightOn, self.m_button1)
        self.Bind(wx.EVT_BUTTON, self.lightOff, self.m_button2)
        self.Bind(wx.EVT_BUTTON, self.startNgRem, self.m_button3)

    def startNgRem(self, event):
        """
        Запускает программу непосредственно управления генератором
        @param event: событие
        @type event: wx._core.CommandEvent
        """
        # @todo: handle path from registry
        os.system(self._ngRemPath)

    def log(self, t):
        """
        Добавляет строку t в конец лога
        @param t: строка
        @type t: unicode
        """
        oldText = self.m_textCtrl1.GetValue()
        self.m_textCtrl1.SetValue(u'%s\n%s' %(oldText, t))
        self.m_textCtrl1.ScrollLines(oldText.count("\n") + 3)

    def lightOn(self, event):
        """

        @param event: событие
        @type event: wx._core.CommandEvent
        """
        if hasattr(self, '_portThread') and self._portThread.IsRunning():
            #self._portThread.sendCommand('1')
            self._portThread.sendCommand(self._commands['lightOn'])
            self.m_button1.Disable()
            self.m_button2.Enable()
            self.log(u'Команда на включение подсветки послана')
        pass

    def lightOff(self, event):
        """

        @param event: событие
        @type event: wx._core.CommandEvent
        """
        if hasattr(self, '_portThread') and self._portThread.IsRunning():
            #self._portThread.sendCommand('3')
            self._portThread.sendCommand(self._commands['lightOff'])
            self.m_button1.Enable()
            self.m_button2.Disable()
            self.log(u'Команда на выключение подсветки послана')
            #wx.MilliSleep(100)
            #self._portThread.sendCommand(self._commands['enabled'])
        pass

    def setPort(self, event=None, **kwargs):
        """

        @param event: событие
        @type event: wx._core.CommandEvent
        """
        if event is not None:
            print 'Got port %s' % (self.m_choice1.GetClientData(self.m_choice1.GetSelection()))
            self._port = (self.m_choice1.GetClientData(self.m_choice1.GetSelection()))
            self.closePort()
            self.createPortConnection()
        pass

    def createPortConnection(self):
        if not hasattr(self, '_portThread'):
            self._portThread = comCommunicationThread(self)
            self.log(u'Создан хендлер порта')
        self._portThread.sendCommand(self._commands['enabled'])
        self._portThread._port = self._port
        self._portThread.Start()
        self.log(u'Порт %s открыт' % self._port)
        pass

    def closePort(self):
        if hasattr(self, '_portThread') and self._portThread.IsRunning():
            self.log(u'Порт %s закрывается' % self._portThread._port)
            #self._portThread.sendCommand('3')
            self._portThread.Stop()
            running = 1

            while running:
                running = 0
                running = running + self._portThread.IsRunning()
                wx.MilliSleep(0.1)
            self.log(u'Порт %s закрыт' % self._portThread._port)

    def preClose(self):
        # @todo: handle if ngrem is started -> block closing
        try:
            self._portThread.sendCommand(self._commands['off'])
            self.log(u'Выключаемся')
            wx.MilliSleep(200)
        except AttributeError:
            pass
        return True

    def __del__(self):
        pass

    def initManagerPanel(self):
        self.m_button2.Disable()

        for port, desc, hwid in sorted(comports()):
            print port, desc, hwid
            try:
                i = self.m_choice1.Append(u'%s %s %s' % (port, '', hwid))
            except :
                i =  self.m_choice1.Append(port)
            self.m_choice1.SetClientData(i, port)
        #self.manager.m_choice1.Append()
        pass


class cameraPanel(wx.Panel):
    def __init__(self, parent, capture, fps=15):
        wx.Panel.__init__(self, parent)

        self.capture = capture
        ret, frame = self.capture.read()

        try:
            height, width = frame.shape[:2]
        except AttributeError:
            parent.cameraError(0)
            return
        parent.SetSize((width, height))
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        self.bmp = wx.BitmapFromBuffer(width, height, frame)

        self.timer = wx.Timer(self)
        self.timer.Start(1000./fps)

        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_TIMER, self.NextFrame)

    def OnPaint(self, evt):
        dc = wx.BufferedPaintDC(self)
        dc.DrawBitmap(self.bmp, 0, 0)

    def NextFrame(self, event):
        ret, frame = self.capture.read()
        if ret:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            self.bmp.CopyFromBuffer(frame)
            self.Refresh()


class mainWin(wx.Frame):
    def __init__(
            self, parent, ID, title, pos=wx.DefaultPosition,
            size=wx.DefaultSize, style=wx.DEFAULT_FRAME_STYLE
           ):

        self._config = {}
        self.readConfig()

        wx.Frame.__init__(self, parent, ID, title, pos, size, style)
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(mainSizer)

        bookStyle = fnb.FNB_NODRAG | fnb.FNB_NO_NAV_BUTTONS | fnb.FNB_NO_X_BUTTON | fnb.FNB_VC8

        self.book = fnb.FlatNotebook(self, wx.ID_ANY, agwStyle=bookStyle)
        self.Freeze()
        self.manager = generatorPanel(self.book, ngRemPath=self._config['ngRemPath'])
        self.book.AddPage(self.manager, u"Управление")
        try:
            self.capture = cv2.VideoCapture(1)
            self.capture.set(cv.CV_CAP_PROP_FRAME_WIDTH, 1280)
            self.capture.set(cv.CV_CAP_PROP_FRAME_HEIGHT, 960)
            self.camera = cameraPanel(self, self.capture)
        except:
            self.camera = wx.TextCtrl(self.book, -1, "No camera found!")
        self.book.AddPage(self.camera, u"Камера")
        self.Thaw()

        self.renderMenu()
        self.initManagerPanel()

        self.book.SetMinSize((800, 600))
        mainSizer.Add(self.book, wx.EXPAND, 1)
        self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)

    def cameraError(self, e=-1):
        print 'Probably application already ran'

    def readConfig(self):
        config = ConfigParser.ConfigParser()
        config.read('generatorManager.ini')
        sections = config.sections()
        try:
            self._config['cameraPort'] = int(config.get('Camera', 'port', False))
        except ConfigParser.NoSectionError:
            self._config['cameraPort'] = 0
        except ConfigParser.NoOptionError:
            self._config['cameraPort'] = 0
        try:
            self._config['ngRemPath'] = config.get('NGRem', 'path')
        except ConfigParser.NoOptionError:
            self._config['ngRemPath'] = ''
        pass

    def writeConfig(self, cameraPort=None, ngRemPath=None):
        config = ConfigParser.ConfigParser()
        config.read('generatorManager.ini')
        if cameraPort is not None:
            print 'setting camera port'
            config.set('Camera', 'port', cameraPort)
        else:
            config.set('Camera', 'port', self._config['cameraPort'])
        if ngRemPath is not None:
            config.set('NGRem', 'path', ngRemPath)
        else:
            config.set('NGRem', 'path', self._config['ngRemPath'])
        config.write(file('generatorManager.ini', 'w'))

    def initManagerPanel(self):
        self.manager.initManagerPanel()

    def onQuit(self, event):
        self.Close(True)

    def OnCloseWindow(self, event):
        if not self.manager.preClose():
            return
        busy = wx.BusyInfo(u"Пожалуйста, подождите...")
        self.camera.timer.Stop()
        cv2.VideoCapture(self._config['cameraPort']).release()
        wx.Yield()
        self.manager.closePort()
        self.Destroy()

    def onAbout(self, event):
        info = wx.AboutDialogInfo()
        info.Name = "MRRC generator manager"
        info.Version = "0.1b"
        info.Copyright = "(C) 2013 MRRC"
        info.SetIcon(wx.Icon(r'C:\NPPlan2\images\mrnc.png', wx.BITMAP_TYPE_PNG))
        info.Description = wordwrap(
            u"Программа управления терапевтическим комплексом с нейтронной головкой.",
            350, wx.ClientDC(self))

        wx.AboutBox(info)

    def onCameraSettings(self, event):
        cv2.VideoCapture(self._config['cameraPort']).release()
        dlg = wx.SingleChoiceDialog(
                self, u'Номер порта камеры?', u'Выбор камеры',
                [str(i) for i in range(0, 10)],
                wx.CHOICEDLG_STYLE
                )

        dlg.SetSelection(self._config['cameraPort'])

        if dlg.ShowModal() == wx.ID_OK:
            v = dlg.GetStringSelection()
            self.writeConfig(cameraPort=v)

        dlg.Destroy()
        wx.CallLater(500, self.restartNeeded)

    def onNgRemPath(self, event):
        wildcard = "ng14rem.exe|ng14rem.exe|Executables (*.exe)|*.exe"
        dlg = wx.FileDialog(
            self, message=u"Путь к NG14Rem",
            defaultDir=os.path.dirname(self._config['ngRemPath']),
            defaultFile=os.path.basename(self._config['ngRemPath']),
            wildcard=wildcard,
            style=wx.OPEN
            )

        if dlg.ShowModal() == wx.ID_OK:
            paths = dlg.GetPaths()
            self.writeConfig(ngRemPath=paths[0])

        dlg.Destroy()
        wx.CallLater(500, self.restartNeeded)

    def restartNeeded(self, event=None):
        dlg2 = wx.MessageDialog(self, u'Перезапустите приложение, чтобы изменения вступили в силу',
                               u'Необходим перезапуск',
                               wx.OK | wx.ICON_INFORMATION
                               )
        dlg2.ShowModal()
        dlg2.Destroy()

    def renderMenu(self):
        self._menuBar = wx.MenuBar()

        _fileMenu = wx.Menu()
        item = wx.MenuItem(_fileMenu, wx.ID_ANY, "Camera settings", "Camera settings")
        self.Bind(wx.EVT_MENU, self.onCameraSettings, item)
        _fileMenu.AppendItem(item)
        item = wx.MenuItem(_fileMenu, wx.ID_ANY, "NGRem path", "Neutron generator manager path")
        self.Bind(wx.EVT_MENU, self.onNgRemPath, item)
        _fileMenu.AppendItem(item)
        item = wx.MenuItem(_fileMenu, wx.ID_ANY, "&Exit\tCtrl-Q", "Exit program")
        self.Bind(wx.EVT_MENU, self.onQuit, item)
        _fileMenu.AppendItem(item)

        _helpMenu = wx.Menu()
        item = wx.MenuItem(_helpMenu, wx.ID_ANY, "&Help\tCtrl-H", "Help")
        #self.Bind(wx.EVT_MENU, self.onHelp, item)
        _helpMenu.AppendItem(item)
        _helpMenu.AppendSeparator()
        item=wx.MenuItem(_helpMenu, wx.ID_ANY, "&About", "Help")
        self.Bind(wx.EVT_MENU, self.onAbout, item)
        _helpMenu.AppendItem(item)

        self._menuBar.Append(_fileMenu, "&File")
        self._menuBar.Append(_helpMenu, "&Help")

        self.SetMenuBar(self._menuBar)


class testApp(wx.App):
    def OnInit(self):
        self.SetAppName("generatorManager")
        return True


def main():
    app = testApp(False)
    topForm = mainWin(None, wx.NewId(), 'Generator Manager by MRRC', size=(800, 600),
                      style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER ^ wx.MAXIMIZE_BOX,
                      pos=(10, 10))
    topForm.Show()
    app.MainLoop()

if __name__ == '__main__':
    main()