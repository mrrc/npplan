# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 30.07.13
@summary: 
'''

import NPPlan
import wx
from NPPlan.controller.client.mainC.abstracts import baseController
import NPPlan.view.client.main.stages.configuration as configViews
from NPPlan.view.client.main.stages.welcomeScreen import welcomeScreen as welcomeScreenForm


class welcome(baseController):
    def __init__(self, parent):
        self.__dict__['parent'] = parent

    def _addPanelToPanel(self, panel, newWxClass):
        panel.GetParent().Freeze()
        panel.Freeze()
        pane = newWxClass(panel)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(pane, 1, wx.ALIGN_CENTER | wx.EXPAND)
        panel.SetSizer(sizer)
        panel.Layout()
        panel.Thaw()
        panel.GetParent().Thaw()
        panel.setCreated()
        return pane

    def welcomeScreen(self, panel):
        NPPlan.config.runningConfig.curSubStageType = 'intro'
        print 'welcomeScreen called', panel
        if not panel.isCreated():
            print 'curStage at welcome', NPPlan.config.runningConfig.curStageType
            # 1. Load wx panel
            self._addPanelToPanel(panel, welcomeScreenForm)
            # 2. Load controller with topController == self, wxParent == panel
            # 3. Append handler if needed
        else:
            # 1. Call work function for handled controller
            pass

    def patwork(self, panel):
        # @todo: add patient working
        # 0. Если текущего плана нет в разработке, то показываем заголовок планов
        if not hasattr(NPPlan, 'currentPlan'):
        #NPPlan.config.runningConfig.curSubStageType = 'patient'
            self.handler.main.view.Freeze()
            NPPlan.config.runningConfig.curStageType = 'patientAndPlans'
            NPPlan.config.runningConfig.curSubStageType = 'patientList'
            self.handler.main.controller.getCenter().createCenter()
            self.handler.main.view.Thaw()
        else:
            # 1. иначе переходим к текущему
            self.handler.main.view.Freeze()
            NPPlan.config.runningConfig.curStageType = 'main'
            #NPPlan.config.runningConfig.curSubStageType = 'patientList'
            self.handler.main.controller.getCenter().createCenter()
            self.handler.main.view.Thaw()
            pass
        pass

    def _fullHome(self, event=None):
        self.handler.main.controller.getCenter().homeHere(event, True)
        pass

    def facility(self, panel):
        self.handler.main.view.Freeze()
        NPPlan.config.runningConfig.curStageType = 'facility'
        self.handler.main.controller.getCenter().createCenter()
        self.handler.main.view.Thaw()
        pass

    def config(self, panel):
        self.handler.main.view.Freeze()
        NPPlan.config.runningConfig.curStageType = 'configuration'
        NPPlan.config.runningConfig.curSubStageType = 'servers'
        self.handler.main.controller.getCenter().createCenter()
        self.handler.main.view.Thaw()

    def serverManager(self, panel):
        self.handler.main.view.Freeze()
        NPPlan.config.runningConfig.curSubStageType = 'simManager'
        p = NPPlan.pluginManager.loadPlugin('runnerStatus')
        #pan = p.statusPanel1()
        self._addPanelToPanel(panel, p.statusPanel1)
        self.handler.main.view.Thaw()
        pass

    def utils(self, panel):
        self.handler.main.view.Freeze()
        #NPPlan.config.runningConfig.curStageType = 'utils'
        #NPPlan.config.runningConfig.curSubStageType = 'main'
        NPPlan.config.runningConfig.curSubStageType = 'utils'
        from NPPlan.view.client.main.utils.main import utilsView
        #from NPPlan.stuff.wxEbtViewer import mainPanel
        p = self._addPanelToPanel(panel, utilsView)
        p.Bind(wx.EVT_BUTTON, self._callWxEbt, p._wxEbtViewer)
        p.Bind(wx.EVT_BUTTON, self._callDicom, p._dicomViewer)
        self.handler.main.view.Thaw()

    def _callWxEbt(self, event):
        self.handler.main.view.Freeze()
        NPPlan.config.runningConfig.curStageType = 'utils'
        NPPlan.config.runningConfig.curSubStageType = 'wxEvtViewer'
        self.handler.main.controller.getCenter().createCenter()
        self.handler.main.view.Thaw()

    def _callDicom(self, event):
        self.handler.main.view.Freeze()
        NPPlan.config.runningConfig.curStageType = 'utils'
        NPPlan.config.runningConfig.curSubStageType = 'dicom'
        self.handler.main.controller.getCenter().createCenter()
        self.handler.main.controller.cp.frame.book.SetSelection(1)
        self.handler.main.view.Thaw()


