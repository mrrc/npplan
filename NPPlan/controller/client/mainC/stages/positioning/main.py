# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 15.11.12
@summary: 
'''

import NPPlan
from NPPlan.controller.client.mainC.abstracts import baseController, abstractTwoPanels
#from NPPlan.controller.client.mainC.stages.positioning.rightPanel import rightPanel as rightPanelController
from NPPlan.controller.client.mainC.stages.positioning.threeDimensional import threeDimensionalController
from NPPlan.controller.client.mainC.stages.positioning.bev import bevController

import wx
from NPPlan.view.client.main.stages.positioning import positioningTopView as posView
from NPPlan.view.client.main.abstracts.rightPanelContainer import rightPanelContainer as rightPanelView

try:
    from agw import flatnotebook as fnb
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.flatnotebook as fnb


# @todo: check rotating non active while rotating beam (!!! important)

class positioningController(baseController, abstractTwoPanels):
    def __init__(self, panel, **kwargs):
        """
        @name: positioning
        @param panel: панель
        @type panel: NPPlan.view.client.main.emptyPanel
        """
        abstractTwoPanels.__init__(self, panel)
        self._rightPanelContainer = NPPlan.panelContainer()
        self._rightPanelState = 'initial'
        pass

    @staticmethod
    def checkRequirements():
        return True
        pass

    def work(self):
        print 'working positioning'
        self.updateRibbon()
        pass

    def updateRibbon(self):
        self.handler.ribbon.controller.update()
        #self.handler.ribbon.controller.setActivePageByPageName('c_positioning')
        self.handler.ribbon.controller.setSaveFunction(self.save)

    def save(self, event=None):
        print 'saving new data!!' \
              ''
        print self.handler.positioningBEV.controller.getData4Simulation()
        #print self.handler.positioningBEV.controller._beamContainer.getData4Simulation()
        pass

    def doMainPanelBindings(self):
        self.mainPanel.book.Bind(fnb.EVT_FLATNOTEBOOK_PAGE_CHANGING, self.onPageChanging)
        self.mainPanel.book.Bind(fnb.EVT_FLATNOTEBOOK_PAGE_CHANGED, self.onPageChanged)
        pass

    def onPageChanging(self, event):
        """
        @param event: event
        @type event: wx.Event
        """
        print 'add', event.GetId()
        print 'oldPage', self.mainPanel.getCurrentPage()
        curPage = self.mainPanel.getCurrentPage()
        #if self.mainPanel.getCurrentPage() is not None:
        if curPage is not None:
            print 'curPage OLD', curPage.cid
            if '3d' == curPage.cid:
                self.handler.positioningThreeDim.controller.deactivate()
            elif 'bev' == curPage.cid:
                self.handler.positioningBEV.controller.deactivate()
        event.StopPropagation()
        event.Skip()

    def onPageChanged(self, event):
        """
        @param event: event
        @type event: wx.Event
        """
        curPage = self.mainPanel.getCurrentPage()
        print 'curPage new', curPage.cid
        try:
            if '3d' == curPage.cid:
                self.handler.positioningThreeDim.controller.activate()
            elif 'bev' == curPage.cid:
                self.handler.positioningBEV.controller.activate()
        except AttributeError:
            print 'No active'
            pass
        event.StopPropagation()
        event.Skip()

    def doRightPanelBindings(self):
        pass

    def getRightPanelSizer(self):
        return self.__dict__['rightPanel'].GetSizer()

    def start(self):
        print 'POSITIONING STAGE'
        self.mainPanel = posView(self.__dict__['mainPanel'])
        self.rightPanel = rightPanelView(self.__dict__['rightPanel'])

        #rpC = rightPanelController(self.rightPanel)

        self.doMainPanelBindings()
        #self.handler.register('positioningRightPanel', rpC, self.rightPanel)
        #self.doRightPanelBindings()
        self._rightPanelContainer.initial = self.rightPanel

        from NPPlan.view.client.main.abstracts.mainPanelContainer import mainPanelContainer

        bev = mainPanelContainer(self.mainPanel, 'bev')
        self.mainPanel.addPage(bev, 'BEV')
        bevC = bevController(bev, parentController=self)
        self.handler.register('positioningBEV', bevC, bev)
        #_viewerObject = viewerObject(mp, self.handler.positioningRightPanel.view)
        #_viewerObject.start()

        mp = mainPanelContainer(self.mainPanel, '3d')
        self.mainPanel.addPage(mp, '3D')
        mpC = threeDimensionalController(mp, parentController=self)
        self.handler.register('positioningThreeDim', mpC, mp)


        #self.handler.positioningThreeDim.controller.activate()
        self.handler.positioningBEV.controller.activate()

        self.__dict__['child'].doLayout()
        self.updateRibbon()
        self.layout()
        pass
