# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 13.12.12
@summary: 
'''

import NPPlan

from NPPlan.model.file.configReader.volume import getData

from NPPlan.controller.client.mainC.abstracts import baseController

import wx
from NPPlan.view.client.main.stages.positioning import viewerVtkBase
import vtk


class threeDimensionalController(baseController):
    def __init__(self, panel, parentController):
        """
        @param panel: панель
        @type panel: NPPlan.view.client.main.abstracts.mainPanelContainer.mainPanelContainer
        """
        self._panel = panel
        self._panel.item = viewerVtkBase(self._panel)
        self._panel.doLayout()
        self._parentController = parentController
        #self.handler.positioningRightPanel.controller.setForView('3d')

    def start(self):
        config = getData()
        print config
        self._volumesConfig = config
        path = "C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\"
        self.__dict__['vtkHandler'] = vtkViewerHandler(self._panel, self._panel.item.vtkWindow, path=path, volumesConfig=self._volumesConfig)

        from NPPlan.view.client.main.stages.positioning.rightPanel import rightPanelThreeDim as rpV
        from NPPlan.controller.client.mainC.stages.positioning.rightPanel import rightPanelThreeDimController as rpC
        self._parentController._rightPanelContainer.rpThreeDim = rpV(self._parentController.__dict__['rightPanel'])
        self._parentController._rightPanelContainer.rpThreeDim.Hide()
        _rpC = rpC(self, self._parentController._rightPanelContainer.rpThreeDim)
        self.handler.register('rightPanelThreeDim', _rpC, self._parentController._rightPanelContainer.rpThreeDim)


    def getVtkController(self):
        """
        @return: объект vtkViewerHandler
        @rtype: vtkViewerHandler
        """
        return self.__dict__['vtkHandler']
    vtkHandler = property(getVtkController, doc="@see: getVtkController")

    def activate(self, **kwargs):
        print 'threedim subview active'
        #self.handler.positioningRightPanel.controller.setForView('3d')
        self._parentController._rightPanelContainer.replace(
            self._parentController.getRightPanelSizer(),
            self._parentController._rightPanelState,
            'rpThreeDim'
        )
        self._parentController._rightPanelState = 'rpThreeDim'
        #print 'active state', self._parentController._rightPanelState
        self.handler.rightPanelThreeDim.controller.activate()
        self.handler.positioning.view.Unbind(wx.EVT_MOUSEWHEEL)

    def deactivate(self, **kwargs):
        self.handler.rightPanelThreeDim.controller.deactivate()
        pass


class volumeMapperItem(object):
    def __init__(self, reader, shift=0):
        self.volumeMapper = vtk.vtkOpenGLGPUVolumeRayCastMapper()
        self.volumeMapper.SetInputConnection(reader.GetOutputPort())
        self._shift = shift

    def setGradient(self, gradientsList):
        self.volumeGradientOpacity = vtk.vtkPiecewiseFunction()
        for grad in gradientsList:
            self.volumeGradientOpacity.AddPoint(self._shift+grad['point'],   grad['opacity'])
    def setColorTransfer(self, colortTansferList):
        self.volumeColor = vtk.vtkColorTransferFunction()
        for col in colortTansferList:
            self.volumeColor.AddRGBPoint(self._shift+col['point'], col['red'], col['green'], col['blue'])
    def setScalar(self, scalarsList):
        self.volumeScalarOpacity = vtk.vtkPiecewiseFunction()
        for scal in scalarsList:
            self.volumeScalarOpacity.AddPoint(self._shift+scal['point'], scal['opacity'])
    def getGradient(self):
        return self.volumeGradientOpacity
    def getColorTransfer(self):
        return self.volumeColor
    def getScalar(self):
        return self.volumeScalarOpacity
    gradient = property(getGradient, setGradient)
    color = property(getColorTransfer, setColorTransfer)
    scalar = property(getScalar, setScalar)

    def createVolume(self):
        volumeProperty = vtk.vtkVolumeProperty()
        volumeProperty.SetColor(self.volumeColor)
        volumeProperty.SetScalarOpacity(self.volumeScalarOpacity)
        volumeProperty.SetGradientOpacity(self.volumeGradientOpacity)
        volumeProperty.SetInterpolationTypeToLinear()
        volumeProperty.ShadeOn()
        volumeProperty.SetAmbient(0.4)
        volumeProperty.SetDiffuse(0.6)
        volumeProperty.SetSpecular(0.2)

        volume = vtk.vtkVolume()
        volume.SetMapper(self.volumeMapper)
        volume.SetProperty(volumeProperty)
        self.volume = volume


class volumeMapperContainer(object):
    def __init__(self, parent):
        # @todo: add active/hidden volumes state
        """
        @param parent: parent
        @type parent: vtkViewerHandler
        """
        self._parent = parent
        self._volumes = {}

    def addVolumeWithParams(self, params):
        print 'GOT PARAMS', params
        self._volumes[params['name']] = {
            'params' : params,
            'volumeItem' : volumeMapperItem(self._parent._scale, 0 if 0 == params['shift'] else self._parent._shift)
        }
        self._volumes[params['name']]['volumeItem'].gradient = params['gradient']
        self._volumes[params['name']]['volumeItem'].color = params['colortransfer']
        self._volumes[params['name']]['volumeItem'].scalar = params['scalar']
        self._volumes[params['name']]['volumeItem'].createVolume()
        self._volumes[params['name']]['volume'] = self._volumes[params['name']]['volumeItem'].volume

    def __getitem__(self, item):
        return self._volumes[item]['volume']
        pass


class vtkViewerHandler(object):
    def __init__(self, wxParent, vtkInteractor, **kwargs):
        self._wxParent = wxParent
        self._vtkInteractor = vtkInteractor
        self._vtkInteractor.SetInteractorStyle(None)
        self._vtkInteractor.Enable(1)
        self.__dict__['dicomDir'] = kwargs.get('path')
        self.__dict__['volumesConfig'] = kwargs.get('volumesConfig')
        self.mouseLeftEvent = False
        self.mouseRightEvent = False
        self.rotateCoefX = 0.2
        self.rotateCoefY = 0.2

        self._mouseFunctions = {
            'leftX' : None,
            'leftY' : None,
            'rightX' : None,
            'rightY' : None,
        }
        self._mouseFunctionsBackends = {
            'leftX' : None,
            'leftY' : None,
            'rightX' : None,
            'rightY' : None,
        }
        self._volumes = {}
        self.process()
        self.setMouseMoveFunction('leftX', 'Azimuth')

    def process(self):
        self.readDicom()
        self.scaleDicom()
        self.createVolumes()
        self.createActors()
        self.setCamera()
        self.setRenderer()
        self.setInteractor()
        self.postStage()

    def readDicom(self):
        if hasattr(NPPlan, 'currentPlan'):
            self._reader = NPPlan.currentPlan.planGetVtkVolumeReader()
            self._reader.SetDataByteOrderToLittleEndian()
            self._reader.Update()
            print 'readDicom', self._reader
        else:
            self._reader = vtk.vtkDICOMImageReader()
            self._reader.SetDirectoryName(self.__dict__['dicomDir'])
            self._reader.SetDataByteOrderToLittleEndian()
            self._reader.Update()

    def scaleDicom(self):
        range = self._reader.GetOutput().GetPointData().GetScalars().GetRange()
        print range
        scale = vtk.vtkImageShiftScale()
        scale.SetInput(self._reader.GetOutput())
        #scale.SetOutputScalarTypeToUnsignedShort()

        scale.SetShift(-range[0])
        #scale.SetScale(vtk.VTK_UNSIGNED_SHORT_MAX/(range[1]-range[0]))
        scale.SetOutputScalarTypeToUnsignedShort()
        print 'Current scale', scale.GetScale()
        print vtk.VTK_UNSIGNED_SHORT_MAX/(range[1]-range[0])
        self._scale = scale
        self._shift = -range[0]

    def createVolumes(self):
        self.volumes = volumeMapperContainer(self)
        for vols in self.__dict__['volumesConfig'].itervalues():
            self.volumes.addVolumeWithParams(vols)
            pass

    def createActors(self):
        outlineData = vtk.vtkOutlineFilter()
        outlineData.SetInput(self._scale.GetOutput())
        mapOutline = vtk.vtkPolyDataMapper()
        mapOutline.SetInput(outlineData.GetOutput())
        outline = vtk.vtkActor()
        outline.SetMapper(mapOutline)
        outline.GetProperty().SetColor(0, 0, 0)
        self._outline = outline

    def setCamera(self):
        self._aCamera = vtk.vtkCamera()
        #self._aCamera.SetPosition(100, 100, 100)
        #self._aCamera.SetFocalPoint(0, 0, 0)
        self._aCamera.SetPosition(0, 1, 0)
        self._aCamera.SetViewUp(0, 0, 1)
        self._aCamera.ComputeViewPlaneNormal()

    def setRenderer(self):
        self._renderer = vtk.vtkRenderer()
        #self._renderer.AddViewProp(self._volume)
        #self._renderer.AddViewProp(volume)
        #for i in self._volumes:
        #    self._renderer.AddVolume(self._volumes[i])
        for vols in self.__dict__['volumesConfig'].keys():
            self._renderer.AddVolume(self.volumes[vols])
        self._renderer.AddActor(self._outline)
        #self._renderer.AddActor(bone)
        self._renderer.SetActiveCamera(self._aCamera)
        self._renderer.InteractiveOn()
        self._renderer.ResetCamera()

    def setInteractor(self):
        self._vtkInteractor.GetRenderWindow().AddRenderer(self._renderer)
        self._vtkInteractor.AddObserver("LeftButtonPressEvent", self.clickLeftPress)
        self._vtkInteractor.AddObserver("LeftButtonReleaseEvent", self.clickLeftRelease)
        self._vtkInteractor.AddObserver("RightButtonPressEvent", self.clickRightPress)
        self._vtkInteractor.AddObserver("RightButtonReleaseEvent", self.clickRightRelease)
        self._vtkInteractor.AddObserver("MouseMoveEvent", self.mouseMove)
        self._vtkInteractor.StartPickCallback()
        self._vtkInteractor.Start()

    def hideVolume(self, name):
        # @todo: re-render all active volumes
        self._renderer.RemoveVolume(self.volumes[name])
        self.update()

    def showVolume(self, name):
        self._renderer.AddVolume(self.volumes[name])
        self.update()

    def postStage(self):
        cam1 = self._renderer.GetActiveCamera()
        print 'CAM POSITION'
        print cam1.GetPosition()
        print cam1.GetFocalPoint()
        print cam1.GetViewUp()

        #self._renderer.GetActiveCamera().Zoom(5)
        #self._renderer.GetActiveCamera().Azimuth(180)
        #self._renderer.GetActiveCamera().Elevation(-90)
        #self._renderer.GetActiveCamera().SetPosition(0, 0, 1)
        #self._renderer.GetActiveCamera().SetFocalPoint(0, 0, 0)
        #self._renderer.GetActiveCamera().SetViewUp(0, 0, -1)
        #self._renderer.GetActiveCamera().SetDolly(1.5)
        self.update()

    def setMouseMoveFunction(self, type, func):
        print 'Setting mouse function %s to %s' %(type, func)
        self._mouseFunctions[type] = func
        if func is not None and 'Rotate' not in func:
            self._mouseFunctionsBackends[type] = getattr(self._renderer.GetActiveCamera(), func)
        elif func is not None and 'Rotate' in func:
            #@todo: rotate all volumes
            #self._mouseFunctionsBackends[type] = getattr(self._volume, func)
            pass

    def update(self):
        self._vtkInteractor.Update()
        self._vtkInteractor.Render()

    def clickLeftPress(self, obj, event):
        #print self.__dict__['vtkInteractor'].GetEventPosition()
        self.mouseLeftEvent = True
        pass
    def clickLeftRelease(self, obj, event):
        #print self.__dict__['vtkInteractor'].GetEventPosition()
        self.mouseLeftEvent = False
        pass

    def clickRightPress(self, obj, event):
        #print self.__dict__['vtkInteractor'].GetEventPosition()
        self.mouseRightEvent = True
        pass
    def clickRightRelease(self, obj, event):
        #print self.__dict__['vtkInteractor'].GetEventPosition()
        self.mouseRightEvent = False
        pass

    def proceedMouseMove(self, type, val):
        func = self._mouseFunctionsBackends[type]
        if func is not None:
            func(val)

    def mouseMove(self, obj, event):
        oldXY = self._vtkInteractor.GetLastEventPosition()
        newXY = self._vtkInteractor.GetEventPosition()
        diffX = oldXY[0] - newXY[0]
        diffY = oldXY[1] - newXY[1]
        if self.mouseLeftEvent:# and 'rotate' == self.mode:
            self.proceedMouseMove('leftX', self.rotateCoefX*diffX)
            self.proceedMouseMove('leftY', self.rotateCoefY*diffY)
        elif self.mouseRightEvent:
            self.proceedMouseMove('rightX', self.rotateCoefX*diffX)
            self.proceedMouseMove('rightY', self.rotateCoefY*diffY)
        self.update()
        pass

    def getBackground(self):
        return self._renderer.GetBackground()
    def setBackground(self, bgTuple):
        print 'setBackground'
        print bgTuple
        self._renderer.SetBackground(bgTuple)
        self.update()
    background = property(getBackground, setBackground)

    def hideOutline(self):
        self._renderer.RemoveActor(self.__dict__['_outline'])
        self.update()
    def showOutline(self):
        self._renderer.AddActor(self.__dict__['_outline'])
        self.update()

class vtkViewerHandlerOld(object):
    def __init__(self, wxParent, vtkInteractor, **kwargs):
        self.__dict__['wxParent'] = wxParent
        self.__dict__['vtkInteractor'] = vtkInteractor

        self.__dict__['vtkInteractor'].SetInteractorStyle(None)
        self.__dict__['vtkInteractor'].Enable(1)

        self.__dict__['dicomDir'] = kwargs.get('path')

        self.__dict__['reader']=vtk.vtkDICOMImageReader()
        print "DICOMDIR", self.__dict__['dicomDir']
        self.__dict__['reader'].SetDirectoryName(self.__dict__['dicomDir'])
        self.__dict__['reader'].Update()

        shrinkFactor = kwargs.get('shrinkFactor', 4)
        shrink = vtk.vtkImageShrink3D()
        shrink.SetShrinkFactors(shrinkFactor, shrinkFactor, 1)
        shrink.SetInput(self.__dict__['reader'].GetOutput())

        self.sk1 = 1
        self.skL = -300
        self.skR = 300

        skinExtractor = vtk.vtkContourFilter()
        skinExtractor.SetInput(shrink.GetOutput())
        #skinExtractor.GenerateValues(self.sk1, self.skL, self.skR)
        skinExtractor.SetValue(self.sk1, self.skL)
        skinExtractor.ComputeGradientsOn()
        skinNormals = vtk.vtkPolyDataNormals()
        skinNormals.SetInput(skinExtractor.GetOutput())
        skinNormals.SetFeatureAngle(60.0)
        skinMapper = vtk.vtkPolyDataMapper()
        #skinMapper = vtk.vtkSmartVolumeMapper()
        skinMapper.SetInput(skinNormals.GetOutput())
        skinMapper.ScalarVisibilityOff()
        skin = vtk.vtkActor()
        skin.SetMapper(skinMapper)
        self.__dict__['_skinActor'] = skin
        self.__dict__['_skinExtractor'] = skinExtractor
        self.__dict__['_skinNormals'] = skinNormals

        outlineData = vtk.vtkOutlineFilter()
        outlineData.SetInput(self.__dict__['reader'].GetOutput())
        mapOutline = vtk.vtkPolyDataMapper()
        mapOutline.SetInput(outlineData.GetOutput())
        outline = vtk.vtkActor()
        outline.SetMapper(mapOutline)
        outline.GetProperty().SetColor(0, 0, 0)
        self.__dict__['_outline'] = outline

        self.__dict__['_aCamera'] = vtk.vtkCamera()
        self.__dict__['_aCamera'].SetViewUp(0, 0, -1)
        self.__dict__['_aCamera'].SetPosition(0, 1, 0)
        self.__dict__['_aCamera'].SetFocalPoint(0, 0, 0)
        self.__dict__['_aCamera'].ComputeViewPlaneNormal()
        self.__dict__['_aCamera'].Dolly(1.5)

        self.__dict__['_ren'] = vtk.vtkRenderer()

        self.__dict__['_ren'].AddActor(outline)
        self.__dict__['_ren'].AddActor(skin)
        #self.__dict__['_ren'].SetViewport(0.0, 0.5, 0.5, 1.0)
        self.__dict__['_ren'].SetActiveCamera(self.__dict__['_aCamera'])
        self.__dict__['_ren'].InteractiveOn()
        self.__dict__['_ren'].SetBackground(0, 0, 0)
        self.__dict__['_ren'].ResetCamera()

        self.__dict__['vtkInteractor'].GetRenderWindow().AddRenderer(self.__dict__['_ren'])
        self.__dict__['vtkInteractor'].Start()
        self.__dict__['vtkInteractor'].AddObserver("LeftButtonPressEvent", self.clickLeftPress)
        self.__dict__['vtkInteractor'].AddObserver("LeftButtonReleaseEvent", self.clickLeftRelease)
        self.__dict__['vtkInteractor'].AddObserver("MouseMoveEvent", self.mouseMove)
        self.__dict__['vtkInteractor'].AddObserver("MiddleButtonPressEvent", self.clickMiddlePress)
        self.__dict__['vtkInteractor'].AddObserver("MiddleButtonReleaseEvent", self.clickMiddlePress)
        self.__dict__['vtkInteractor'].AddObserver("MouseWheelForwardEvent", self.mWheelUp)
        self.__dict__['vtkInteractor'].AddObserver("MouseWheelBackwardEvent", self.mWheelDown)

        #self.__dict__['_ren'].ResetCamera()
        cam1 = self.__dict__['_ren'].GetActiveCamera()
        #cam1.Zoom(2)
        #cam1.Elevation(-30)
        #cam1.Roll(-20)
        print cam1.GetPosition()
        #cam1.SetPosition()
        self.__dict__['_ren'].ResetCameraClippingRange()

        print 'SKIN CENTER', skin.GetCenter()
        x,y,z = skin.GetCenter()
        self.__dict__['reader'].SetDataOrigin(-x,-y,-z)
        self.__dict__['reader'].Update()
        #cam1.SetPosition(-x,-y,-z)
        #cam1.SetFocalPoint(x,y,z)
        cam1.SetViewUp(0, 0, 1)
        #cam1.SetPosition(-100, 0, 0)
        cam1.SetFocalPoint(0, 0, 0)
        cam1.ParallelProjectionOn()
        self.update()
        self.mode = 'rotate'
        self.mouseEvent = False
        self.rotateCoefX = 0.2
        self.rotateCoefY = 0.2
        self.zoom = 1

    def updateExtractorValue(self):
        self.__dict__['_skinExtractor'].SetValue(self.sk1, self.skR)
        #self.__dict__['_skinExtractor'].GenerateValues(self.sk1, self.skL, self.skR)
        self.update(True)

    def setFirstExtractor(self, value):
        self.sk1 = value
        print 'sk0', value
        self.updateExtractorValue()
    def setLeftExtractor(self, value):
        self.skL = value
        self.updateExtractorValue()
    def setRightExtractor(self, value):
        self.skR = value
        self.updateExtractorValue()

    def setNormalsAngle(self, value):
        self.__dict__['_skinNormals'].SetFeatureAngle(value)
        self.update(True)

    def setMode(self, mode):
        self.__dict__['_mode'] = mode
    def getMode(self):
        return self.__dict__['_mode']
    mode = property(getMode, setMode)

    def clickLeftPress(self, obj, event):
        #print self.__dict__['vtkInteractor'].GetEventPosition()
        self.mouseEvent = True
        pass
    def clickLeftRelease(self, obj, event):
        #print self.__dict__['vtkInteractor'].GetEventPosition()
        self.mouseEvent = False
        pass
    def mouseMove(self, obj, event):
        if self.mouseEvent and 'rotate' == self.mode:
            oldXY = self.__dict__['vtkInteractor'].GetLastEventPosition()
            newXY = self.__dict__['vtkInteractor'].GetEventPosition()
            diffX = oldXY[0] - newXY[0]
            diffY = oldXY[1] - newXY[1]
            self.__dict__['_ren'].GetActiveCamera().Azimuth(self.rotateCoefX*diffX)
            self.__dict__['_ren'].GetActiveCamera().Elevation(self.rotateCoefY*diffY)
            #self.__dict__['_skinActor'].RotateZ(self.rotateCoefX*diffX)
            #self.__dict__['_skinActor'].RotateX(self.rotateCoefY*diffY)
            self.update()
        pass
    def mWheelUp(self, obj, event):
        self.zoom = 1.1
        self.updateCameraZoom()
    def mWheelDown(self, obj, event):
        self.zoom = 0.9
        self.updateCameraZoom()
    def clickMiddlePress(self, obj, event):
        self.__dict__['_ren'].ResetCamera()
        self.update()
    def updateCameraZoom(self):
        self.__dict__['_ren'].GetActiveCamera().Zoom(self.zoom)
        self.update()

    def update(self, isForce=False):
        """
        Обновляет окно
        """
        if isForce:
            self.__dict__['_ren'].Render()
        self.__dict__['vtkInteractor'].Update()
        self.__dict__['vtkInteractor'].Render()
        pass

    def setCamera(self):
        cam = self.__dict__['_aCamera']
        print cam.GetPosition(), cam.GetViewUp()
        #self.__dict__['_aCamera'].SetPosition(0, 1, 0)
        #cam.SetViewUp(0, 1, 1)
        print cam.GetPosition(), cam.GetViewUp()
        #self.__dict__['_skinActor'].RotateZ(75)
        self.__dict__['_skinActor'].RotateZ(30)
        #cam.Zoom(5)
        self.update()

    def getBackground(self):
        return self.__dict__['_ren'].GetBackground()
    def setBackground(self, bgTuple):
        print 'setBackground'
        print bgTuple
        self.__dict__['_ren'].SetBackground(bgTuple)
        self.update()
    background = property(getBackground, setBackground)

    def hideOutline(self):
        self.__dict__['_ren'].RemoveActor(self.__dict__['_outline'])
        self.update(True)
    def showOutline(self):
        self.__dict__['_ren'].AddActor(self.__dict__['_outline'])
        self.update(True)

    def work(self):
        pass