# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 13.12.12
@summary: 
'''

import NPPlan
from NPPlan.controller.client.mainC.abstracts import baseController
from NPPlan.model.file.configReader.bev import readConfig, getWindows, getPopups, getHelper
import numpy as np
import wx
from NPPlan.view.client.main.stages.positioning import viewerVtkBase
import vtk
from NPPlan.view.client.main.stages.positioning import fourWindowsView
from NPPlan.controller.client.mainC.abstracts import popupHelperController
from bevBeam import bevBeamController, beamContainer


class wxListController(baseController):
    def __init__(self, bevController, wxWindow):
        """

        @param bevController:
        @type bevController: bevController
        @param wxWindow:
        @type wxWindow: NPPlan.view.client.main.stages.positioning.rightPanel.bevInformer
        @return:
        """
        self._view = wxWindow
        self._listView = wxWindow.listWC
        self._topController = bevController
        print 'wxListController registered'
        print self._view, self._topController
        self._view.ch.SetItems(['o1', 'p', 'rs'])

    def activate(self):
        print 'wxListController activated'
        print self._view, self._topController
        pass

    def selectionParser(self, event=None, data=None, aType='selection'):
        print event, data, aType
        pass

    def addBeamClicked(self, event, data):
        print event, data
        self._listView.addLine(['1', '11'])
        self._topController.appendBeam()
        pass

    def delBeamClicked(self, event, data):
        print event, data
        pass


class bevController(baseController):
    def __init__(self, panel, mode='plan', modeVtk=None, parentController=None):
        """
        @param panel: панель
        @type panel: NPPlan.view.client.main.abstracts.mainPanelContainer.mainPanelContainer
        """
        self._panel = panel
        self._parentController = parentController
        self.__dict__['vtkHandlers'] = {}
        #self._panel.item = viewerVtkBase(self._panel)
        q = readConfig('bev')
        print getWindows()
        self.windowsConfig = windowsConfig = getWindows()
        self.popupsConfig = getPopups()
        #print getPopups()
        if 'plan' == mode:
            self._panel.Freeze()
            self._panel.item = fourWindowsView(self, windowsConfig)
            #self._panel.item.assertController(self)
            self._panel.doLayout()
            self._panel.Thaw()
            self.registerVtks()
        else:
            self.__dict__['vtkHandlers']['threeDim'] = threeDimHandler(self, modeVtk)
            self.__dict__['vtkHandlers']['threeDim'].start()
        self.binded = []
        self._ids = {}
        self._popupBackend = popupHandler(self)
        self._popupBackendsDict = {}
        self._curExpand = -1
        self.leftPressed = False
        self.rotationConfig = {
            'rotateX' : True,
            'rotateZ' : True,
        }
        self._spacingData = None
        self._currentFocus = -1
        self.helpers = {}
        self._beamContainer = beamContainer(self)
        #self.doMwheelBindings()
        #self.doBindings()
        #self.handler.positioningRightPanel.controller.setForView('bev')

    # @todo: refactor
    def colorFromVtkToWx(self, color):
        newColor = (int(color[0]*255), int(color[1]*255), int(color[2]*255))
        return newColor

    # @todo: refactor
    def colorFromWxToVtk(self, color):
        newColor = (float(color[0])/255, float(color[1])/255, float(color[2])/255)
        return newColor

    def addBeam(self, data={}, id=-1):
        print 'ADDING BEAM AT MAIN BEV CONTROLLER, ', data, id
        vtkColor = self.colorFromWxToVtk(data['bevColor'])
        print vtkColor
        b = bevBeamController(parent=self, color=vtkColor)
        self._beamContainer.appendBeam(beamClass=b, beamParams=data, id=id, active=True)
        self.threeDim._spaceRenderer.ResetCamera()
        self.threeDim.update()
        print 'SOURCe COORDINATES'
        print b.getSourceCoordinates()
        self._beamToRight.setBeamSourceData(b.getSourceCoordinates())

    def getData4Simulation(self, **kwargs):
        print 'Getting simulation data'
        if 0 == self._beamContainer.getLength():
            print 'no beam'
            return None
        #print self._beamContainer.getData4Simulation()
        #import pprint
        #pprint.pprint(self._beamContainer.getData4Simulation())
        beamData = self._beamContainer.getData4Simulation()
        return beamData
        '''
        {0: {'active': True, 'beam': <NPPlan.controller.client.mainC.stages.positioning.bevBeam.bevBeamController object at 0x0000000008765B00>, 'params': {'bevColor': wx.Colour(0, 0, 0, 255), 'fX': u'92.77', 'fZ': u'68.00', 'bevName': u'', 'fY': u'88.42'}, 'tracer': <NPPlan.controller.client.mainC.stages.positioning.bevBeam.beamSliceTracer object at 0x000000000E2FF940>, 'source': {'srcMatrix': [(2.220446049250313e-16, -1.0, 0.0), (1.0, 2.220446049250313e-16, 0.0), (0.0, 0.0, 1.0)], 'positionXYZ': (array([ 160.56760672]), array([ 78.31405446]), array([  1.09390247e-14])), 'sst': array([ 178.64783081]), 'angles': (0.45378560551852543, 1.5707963267948966), 'ssd': 100.0}}}
        '''
        #print self._beamContainer.getData4Simulation()[0]['beam'].getSourceCoordinates()

    def activateBeam(self, beamNumber):
        #self._beamContainer.deactivate()
        self._beamContainer.activateById(beamNumber)

    def registerVtks(self):
        for win in self.windowsConfig:
            vtkContainer = self._panel.item.getByPosition(self.windowsConfig[win]['position']).vtkWindow
            self.__dict__['vtkHandlers'][win] = globals()[self.windowsConfig[win]['handler']](self, vtkContainer)
            pass

    def showPopup(self, winNumber, position, isExpanded=False):
        popup = self.popupsConfig[winNumber]
        if winNumber not in self.binded:
            print 'Creating popup for %d at %s' % (winNumber, position)
            for item in popup:
                item['wxId'] = wx.NewId()
                if 'type' in item and 'separator' == item['type']:
                    continue
                self.assertPopupBackend(item['wxId'], item['backend'])
                self._ids[item['wxId']] = winNumber
            self.binded.append(winNumber)
            self.doPopupBindings(winNumber)

        print 'Showing popup for %d at %s' %(winNumber, position)
        menu = wx.Menu()
        #menu.Check()
        print popup
        print self.popupsConfig
        for item in popup:
            print item
            if item.has_key('type') and 'separator' == item['type']:
                menu.AppendSeparator()
                continue
            if (not item.has_key('onexpand')) \
               or (self._curExpand == -1 and item['onexpand'] == '0') \
               or (self._curExpand != -1 and item['onexpand'] == '1'):
                    if item.has_key('type') and 'check' == item['type']:
                        print 'Has check item', item['wxId'], item['caption']
                        menu.Append(item['wxId'], item['caption'], '', wx.ITEM_CHECK)
                        if self.rotationConfig[item['name']]:
                            menu.Check(item['wxId'], self.rotationConfig[item['name']])
                    else:
                        menu.Append(item['wxId'], item['caption'])
        #menu.Check(191, True)
        self._panel.item.PopupMenu(menu, position)

    def assertPopupBackend(self, wxId, backendString):
        backend = self._popupBackend.__getattribute__(backendString)
        self._popupBackendsDict[wxId] = backend
        pass

    def setPopupmenuCheckedState(self, wxId, state):
        for i in self.popupsConfig[self.getWinByWxId(wxId)]:
            if i['wxId'] == wxId:
                self.rotationConfig[i['name']] = state
                break

    def getPopupmenuCheckedState(self, wxId):
        for i in self.popupsConfig[self.getWinByWxId(wxId)]:
            if i['wxId'] == wxId:
                return self.rotationConfig[i['name']]

    def doPopupBindings(self, winNumber):
        popup = self.popupsConfig[winNumber]
        for item in popup:
            self._panel.item.Bind(wx.EVT_MENU, self.menuBackend, id=item['wxId'])

    def menuBackend(self, event=0):
        print event.GetId()
        self._popupBackendsDict[event.GetId()](self._ids[event.GetId()], event)

    def getWinByWxId(self, id):
        return self._ids[id]

    def getControllerById(self, id):
        if 0 == id:
            return self.axial
        elif 1 == id:
            return self.sagittal
        elif 2 == id:
            return self.coronal
        else:
            return self.threeDim

    def onContextMenu(self, event):
        """
        @param event: событие
        @type event: wx.Event
        """
        self.threeDim.stopRotation()
        self.leftPressed = False
        print self.popupsConfig
        winId = self.getWinByWxId(event.GetId())
        cnf = self.popupsConfig[winId]
        if event.GetId() not in self.binded:
            print 'making popup', winId, cnf
            self.binded.append(event.GetId())
        print 'showing popup', winId, cnf

        pass

    def __getattr__(self, item):
        # print item
        return self.__dict__['vtkHandlers'][item]

    def start(self):
        #self._panel.item.appendHelpers({})
        if hasattr(NPPlan, 'currentPlan'):
            plan = NPPlan.currentPlan
        else:
            plan = NPPlan.planAbstract()
            plan.planLoadDicomSeriesFromFile("C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\")
            plan.planSetThumbsDir("C:\\Users\\Chernukha\\Pictures\\DICOM\\thumb\\")
            NPPlan.currentPlan = plan
        self.path = NPPlan.currentPlan.__dict__['data'].__dict__['dir']

        self.helpers = {}
        self.helperConf = getHelper()
        print 'helpers !'
        print self.helperConf
        for i in range(4):
            if i < 3:
                self.helpers[i] = popupHelperController(self._panel.item.wins[i], win=i)
                self._sliceHelper = sliceHelperHandler(self, self.helpers[i])
                self.helpers[i].registerBackendClass(self._sliceHelper)
            else:
                self.helpers[3] = popupHelperController(self._panel.item.wins[3], color='WHITE', win=i)
                self._bevHelper = bevHelperHandler(self, self.helpers[i])
                self.helpers[3].registerBackendClass(self._bevHelper)
            self.helpers[i].addManyItems(self.helperConf[i])
            self.helpers[i].resize()

        self._beam = {}
        # @todo: implement from global dicom reader
        self.getSpacing()
        import os
        self._sliceQuantity = len(os.listdir(self.path))

        #from NPPlan.view.client.main.stages.test.rightPanel import rightPanelOne as rp
        from NPPlan.view.client.main.stages.positioning.rightPanel import rightPanelBev as rpV
        from NPPlan.controller.client.mainC.stages.positioning.rightPanel import rightPanelBevController as rpC
        self._parentController._rightPanelContainer.rpBev = rpV(self._parentController.__dict__['rightPanel'])
        self._parentController._rightPanelContainer.rpBev.Hide()
        _rpC = rpC(self, self._parentController._rightPanelContainer.rpBev)
        self.handler.register('rightPanelBev', _rpC, self._parentController._rightPanelContainer.rpBev)

        #self.__dict__['vtkHandler'] = vtkBevHandler(bevController=self)

    def getSpacing(self):
        if self._spacingData is None:
            path = self.path+'\\'
            print path, 'PPPATH'
            from NPPlan.model.file.dicom.shortStructure import readSpacingData
            self._spacingData = readSpacingData(self.path)
        return self._spacingData

    def getPixelSpacing(self):
        return self._spacingData['PixelSpacing']

    def getSliceSpacing(self):
        return self._spacingData['SliceSpacing']

    def getSlicesPosition(self):
        return {
            'axial': self.axial.origin,
            'coronal': self.coronal.origin,
            'sagittal': self.sagittal.origin,
        }

    def deactivate(self, **kwargs):
        self.handler.rightPanelBev.controller.deactivate()
        self._panel.item.splitter.Unbind(wx.EVT_RIGHT_DOWN)
        self._panel.item.splitter.Unbind(wx.EVT_RIGHT_UP)
        self._panel.item.splitter.Unbind(wx.EVT_LEFT_DOWN)
        self._panel.item.splitter.Unbind(wx.EVT_LEFT_DOWN)
        self._panel.item.splitter.Unbind(wx.EVT_MOTION)
        wx.GetApp().GetTopWindow().Unbind(wx.EVT_MOUSEWHEEL)
        pass

    def getSlicesOrigins(self):
        return {
            'axial': self.axial._plane.GetOrigin(),
            'coronal': self.coronal._plane.GetOrigin(),
            'sagittal': self.sagittal._plane.GetOrigin(),
        }

    def activate(self, **kwargs):
        print 'bev subview active'
        #self.handler.positioningRightPanel.controller.setForView('bev')
        print 'OLD STATE', self._parentController._rightPanelState

        self._parentController._rightPanelContainer.replace(
            self._parentController.getRightPanelSizer(),
            self._parentController._rightPanelState,
            'rpBev'
        )
        self._parentController._rightPanelState = 'rpBev'

        self.threeDim.start()
        self.axial.start(center=self.threeDim.getTarget(), cParent=self)
        self.coronal.start(center=self.threeDim.getTarget(), cParent=self)
        self.sagittal.start(center=self.threeDim.getTarget(), cParent=self)

        self.bindMouseEvents()

        self._bevToRight = bevToRight(self, self.handler.rightPanelBev.controller.bevSlicesView)
        self._bevToRight.bevToRight()
        self._beamToRight = beamToRight(self, self.handler.rightPanelBev.controller.bevBeamView)
        self._beamToRight.beamToRight()

        self.handler.rightPanelBev.controller.activate()

    def appendBeam(self):
        print self._beam
        self._beam[0] = bevBeamController(uId=0, parent=self,
                                          axialController=self.axial,
                                          sagittalController=self.sagittal,
                                          coronalController=self.coronal,
                                          threeDimController=self.threeDim,
                                          )
        print 'beamCreated!!!'
        print self._beam
        pass

    def bindMouseEvents(self):
        self._panel.item.splitter.Bind(wx.EVT_RIGHT_DOWN, self.wxRightDown)
        self._panel.item.splitter.Bind(wx.EVT_RIGHT_UP, self.wxRightUp)
        self._panel.item.splitter.Bind(wx.EVT_LEFT_DOWN, self.wxLeftDown)
        self._panel.item.splitter.Bind(wx.EVT_LEFT_UP, self.wxLeftUp)
        self._panel.item.splitter.Bind(wx.EVT_MOTION, self.wxMouseMotion)
        self.bindMouseWheel()

    def bindMouseWheel(self):
        print 'doing mwheel bindings'
        print self.axial, type(self.axial)
        wx.GetApp().GetTopWindow().Bind(wx.EVT_MOUSEWHEEL, self.mouseWheel, id=wx.GetApp().GetTopWindow().GetId())
        #self._panel.GetGrandParent().Bind(wx.EVT_MOUSEWHEEL, self.mouseWheel)
        #print type(self.handler.positioningBEV.view)
        #self.handler.positioningBEV.view.Bind(wx.EVT_MOUSEWHEEL, self.mouseWheel)
        pass

    def mouseWheel(self, event):
        """
        @param event: событие
        @type event: wx.MouseEvent
        """
        print 'MOUSEWHEEL', event
        event.Skip()
        win = self._panel.item.getWindowNumber(event.GetPosition())
        if win == -1:
            return
        if event.GetWheelRotation() > 0:
            self.mWheel('up', win, event)
        else:
            self.mWheel('down', win, event)
        event.Skip()

    def handleLeft(self, type, window, event):
        """
        @todo: refactor this
        @param type: тип, один из ['down', 'up', 'motion']
        @type type: string
        @param window: окно
        @type window: NPPlan.view.client.main.abstracts.vtkBaseWindow
        @param event: событие
        @type event: wx.MouseEvent
        """
        self._panel.SetFocus()
        if 'motion' != type:
            print type, window, event
            #if type in ['up', 'down']:
            #    self.mWheel(type, window, event)
            #    return
            o = event.GetEventObject()
            print o.GetPositionTuple()
            print event.GetPosition()
            print wx.GetMousePosition()
            if 3 == window and 'down' == type:
                self.threeDim.leftPress(event)
                self.leftPressed = True
            elif self.leftPressed and 'up' == type and (self.threeDim._RotatingX == 1 or self.threeDim._RotatingZ == 1):
                self.threeDim.leftRelease(event)
                self.leftPressed = False
        else:
            if self.leftPressed and 3 == window:
                self.threeDim.motion(event)
        pass

    def wxLeftDown(self, event):
        """
        @param event: событие
        @type event: wx.MouseEvent
        """
        event.Skip()
        print 'leftDown at', event.GetPosition()
        wN = self._panel.item.getWindowNumber(event.GetPosition(), 'self')
        if 3 == wN or 3 == self._curExpand:
            self.threeDim.setState(leftClicked=True, active=True, newPosition=event.GetPosition())
        event.Skip()

    def wxLeftUp(self, event):
        """
        @param event: событие
        @type event: wx.MouseEvent
        """
        event.Skip()
        print 'leftUp at', event.GetPosition()
        wN = self._panel.item.getWindowNumber(event.GetPosition(), 'self')
        if 3 == wN or 3 == self._curExpand:
            # @todo: reset all active handlers
            self.threeDim.setState(leftClicked=False, active=False)
        event.Skip()

    def wxRightDown(self, event):
        """
        @param event: событие
        @type event: wx.MouseEvent
        """
        event.Skip()
        print 'rightDown at', event.GetPosition()
        wN = self._panel.item.getWindowNumber(event.GetPosition(), 'self')
        if 3 == wN or 3 == self._curExpand:
            self.threeDim.setState(rightClicked=True, active=True, newPosition=event.GetPosition())
        event.Skip()

    def wxRightUp(self, event):
        """
        @param event: событие
        @type event: wx.MouseEvent
        """
        event.Skip()
        print 'rightUp at', event.GetPosition()
        wN = self._panel.item.getWindowNumber(event.GetPosition(), 'self')
        if 3 == wN or 3 == self._curExpand:
            # @todo: reset all active handlers
            self.threeDim.setState(rightClicked=False, active=False)
        event.Skip()

    def wxMouseMotion(self, event):
        """
        @param event: событие
        @type event: wx.MouseEvent
        """
        event.Skip()
        #print 'mouseMotion at', event.GetPosition()
        wN = self._panel.item.getWindowNumber(event.GetPosition(), 'self')
        if -1 == wN:
            return
        #self._controller.handleLeft('motion', wN, event)
        self.handleHelper(wN, self._panel.item.wins[wN], event)
        if (3 == wN or 3 == self._curExpand) and self.threeDim.isActive():
            self.threeDim.handleActiveMotion(event)
            #self.threeDim.zoomOnMotion(event)
        event.Skip()

    def looseAllHelpersFocus(self):
        for i in self.helpers:
            self.helpers[i].onFocusLost(None)

    def handleHelper(self, windowNumber, windowView, event):
        """
        @param windowNumber: номер окна
        @type windowNumber: int
        @param windowView: экземпляр класса окна
        @type windowView: NPPlan.view.client.main.abstracts.vtkEmptyPanel.vtkBaseWindow
        @param event: событие
        @type event: wx.Event
        """
        #print 'Old win: %d, new win: %d' %(self._currentFocus, windowNumber)
        if self._curExpand != -1 and self._currentFocus != windowNumber:
            return
        if self._currentFocus != -1 and self._currentFocus != windowNumber:
            #self._panel.item.lostFocus(event, self._currentFocus)
            self.helpers[self._currentFocus].onFocusLost(None)
        self._currentFocus = windowNumber
        self.helpers[self._currentFocus].onFocus(None)
        #helperObj = windowView.showHelper(windowNumber, event)
        #if helperObj is not None:
        #    self.makeHelper(windowNumber, helperObj)
        pass

    def makeHelper(self, windowNumber, helperObject):
        """
        @param windowNumber: номер окна
        @type windowNumber: int
        @param helperObject: класс отображения всплывающего окна
        @type helperObject: NPPlan.view.client.main.abstracts.helper.popupWindowHelper
        """
        print windowNumber, helperObject, helperObject.firstTime
        #helperObject.addButton('w %d 123' %(windowNumber), None, None)
        #helperObject.addButton('456', None, None)
        helperObject.firstTime = False
        pass

    def updateAllLines(self):
        self.handler.positioningBEV.controller.axial.updateLines()
        self.handler.positioningBEV.controller.coronal.updateLines()
        self.handler.positioningBEV.controller.sagittal.updateLines()

    def mWheel(self, rotation, window, event):
        """
        @param rotation: один из ['up', 'down']
        @type rotation: string
        @param window: окно
        @type window: NPPlan.view.client.main.abstracts.vtkBaseWindow
        @param event: событие
        @type event: wx.MouseEvent
        """
        if 'up' == rotation:
            func = 'mWheelUp'
            print 'mWheelUp from window', window
        else:
            func = 'mWheelDown'
            print 'mWheelDown from window', window
        if 0 == self._curExpand or 0 == window:
            getattr(self.axial, func)(event)
            self._bevToRight.bevToRight(caller='Axial')
            #self.handler.positioningRightPanel.controller.bevSetSliceDefaults(self.getSlicesPosition())
            #self._panel.item.getByPosition(window).doLayout(True)
            #self._panel.item.getByPosition(window).text = 'Current position: %2.2f' %(self.axial.origin)
        elif 1 == self._curExpand or 1 == window:
            getattr(self.sagittal, func)(event)
            self._bevToRight.bevToRight(caller='Sagittal')
            #self.handler.positioningRightPanel.controller.bevSetSliceDefaults(self.getSlicesPosition())
        elif 2 == self._curExpand or 2 == window:
            getattr(self.coronal, func)(event)
            self._bevToRight.bevToRight(caller='Coronal')
            #self.handler.positioningRightPanel.controller.bevSetSliceDefaults(self.getSlicesPosition())
            pass

    pixelSpacing = property(getPixelSpacing)
    sliceSpacing = property(getSliceSpacing)


class bevToRight(baseController):
    def __init__(self, parentController, wxView):
        """

        @param parentController:
        @type parentController: NPPlan.controller.client.mainC.stages.positioning.bev.bevController
        @param wxView:
        @type wxView: NPPlan.view.client.main.stages.positioning.rightPanel.bevPosition
        @return:
        """
        print 'bevToRight', type(parentController), type(wxView)
        self._parent = parentController
        self._view = wxView
        pass

    def bevToRight(self, caller=None):
        pos = self._parent.getSlicesPosition()
        if caller is None:
            self._view.axial = pos['axial']
            self._view.coronal = pos['coronal']
            self._view.sagittal = pos['sagittal']
        else:
            getattr(self._view, 'set' + caller)(pos[caller.lower()])


class beamToRight(baseController):
    def __init__(self, parentController, wxView):
        """

        @param parentController:
        @type parentController: NPPlan.controller.client.mainC.stages.positioning.bev.bevController
        @param wxView:
        @type wxView: NPPlan.view.client.main.stages.positioning.rightPanel.bevInformer
        @return:
        """
        print 'bevToRight', type(parentController), type(wxView)
        self._parent = parentController
        self._view = wxView

    def setBeamSourceData(self, data):
        print data
        self._view.cX.SetValue('%1.3f' % data['positionXYZ'][0][0])
        self._view.cY.SetValue('%1.3f' % data['positionXYZ'][1][0])
        self._view.cZ.SetValue('%1.3f' % data['positionXYZ'][2][0])
        self._view.std.SetValue('%1.3f' % data['sst'][0])
        self._view.cA1.SetValue('%1.3f' % np.degrees(data['angles'][0]))
        self._view.cA2.SetValue('%1.3f' % np.degrees(data['angles'][1]))
        pass

    def beamToRight(self):
        xyz = self._parent.threeDim.getTarget()
        print 'TARGET XYZ', xyz
        self._view.fX.SetValue('%2.2f' % xyz[0])
        self._view.fY.SetValue('%2.2f' % xyz[1])
        self._view.fZ.SetValue('%2.2f' % xyz[2])
        x, y, z, = xyz
        r = np.sqrt(x ** 2 + y ** 2 + z ** 2)
        try:
            fi = np.arctan(y / x)
            theta = np.arctan(np.sqrt(x ** 2 + y ** 2) / z)
        except ZeroDivisionError:
            fi = 0
            theta = 0
        self._view.fR.SetValue('%2.2f' % r)
        self._view.fFi.SetValue('%2.2f' % np.degrees(fi))
        self._view.fTheta.SetValue('%2.2f' % np.degrees(theta))


class rightToBev(baseController):
    def __init__(self, rightController=None, bevController=None, wxView=None):
        self._right = rightController
        #self._bev = bevController
        self._view = wxView

    def rightToBev(self, caller=None):
        if caller is None:
            axial = self._view.axial
            coronal = self._view.coronal
            sagittal = self._view.sagittal
            if self.handler.positioningBEV.controller.axial.origin != axial:
                self.handler.positioningBEV.controller.axial.origin = axial
                self.handler.positioningBEV.controller.axial.setCaption(str(axial))
                self.handler.positioningBEV.controller.axial.update()
            if self.handler.positioningBEV.controller.coronal.origin != coronal:
                self.handler.positioningBEV.controller.coronal.origin = coronal
                self.handler.positioningBEV.controller.coronal.setCaption(str(coronal))
                self.handler.positioningBEV.controller.coronal.update()
            if self.handler.positioningBEV.controller.sagittal.origin != sagittal:
                self.handler.positioningBEV.controller.sagittal.origin = sagittal
                self.handler.positioningBEV.controller.sagittal.setCaption(str(sagittal))
                self.handler.positioningBEV.controller.sagittal.update()
        else:
            origin = getattr(self._view, 'get' + caller)()
            getattr(self.handler.positioningBEV.controller, caller.lower()).origin = origin
            getattr(self.handler.positioningBEV.controller, caller.lower()).setCaption(str(origin))
            getattr(self.handler.positioningBEV.controller, caller.lower()).update()
        self.handler.positioningBEV.controller.axial.updateLines()
        self.handler.positioningBEV.controller.coronal.updateLines()
        self.handler.positioningBEV.controller.sagittal.updateLines()
        #self._bev.


class helperHandlerBase(object):
    def __init__(self, bParent, helper):
        """
        @param bParent: родительский класс-контроллер
        @type bParent: bevController
        @param helper: соответствующий хелпер
        @type helper: NPPlan.controller.client.mainC.abstracts.popupHelperController
        """
        self._bevParent = bParent
        self._helper = helper

    def expandCurrent(self, btnObj, event):
        cE = self._bevParent._curExpand
        win = btnObj._controller.associatedWindow
        print 'call expand win %d' % win
        if -1 == cE:
            self._bevParent._panel.item.splitter.SetExpanded(win)
            self._bevParent._curExpand = win
        else:
            self._bevParent._panel.item.splitter.SetExpanded(-1)
            self._bevParent._panel.item.doLayout()
            self._bevParent._curExpand = -1


class bevHelperHandler(helperHandlerBase):
    def __init__(self, bParent, helper):
        """
        @param bParent: родительский класс-контроллер
        @type bParent: bevController
        @param helper: соответствующий хелпер
        @type helper: NPPlan.controller.client.mainC.abstracts.popupHelperController
        """
        helperHandlerBase.__init__(self, bParent, helper)

    def rotateX(self, btnObj, event):
        """
        @param btnObj: кнопка
        @type btnObj: NPPlan.controller.client.mainC.abstracts.popupHelperButton
        @param event: событие
        @type event: wx.Event
        """
        pass

    def toggleLeftMouse(self, btnObj, event):
        print 'Button toggle', btnObj.state
        print self._bevParent, self._bevParent.threeDim
        self._bevParent.threeDim.setParams(leftMouse=btnObj.state)
        pass

    def toggleRightMouse(self, btnObj, event):
        self._bevParent.threeDim.setParams(rightMouse=btnObj.state)

    def rotateBody(self, btnObj, event):
        self._bevParent.threeDim.setParams(rotateBody=btnObj.state, rotateField=False, zoomState=False)

    def rotateField(self, btnObj, event):
        self._bevParent.threeDim.setParams(rotateField=btnObj.state, rotateBody=False, zoomState=False)

    def zoomState(self, btnObj, event):
        self._bevParent.threeDim.setParams(zoomState=btnObj.state, rotateBody=False, rotateField=False)


class sliceHelperHandler(helperHandlerBase):
    def __init__(self, bParent, helper):
        helperHandlerBase.__init__(self, bParent, helper)


class popupHandler(object):
    def __init__(self, parent):
        """
        @param parent: родитель
        @type parent: bevController
        """
        self._parent = parent

    def expand(self, callerWin, event=0):
        print 'Called method expand from win', callerWin
        curExpand = self._parent._panel.item.splitter._expanded
        if -1 == curExpand:
            self._parent._panel.item.splitter.SetExpanded(callerWin)
            #self._parent._panel.item.getByPosition(callerWin).doLayout(True)
        else:
            self._parent._panel.item.splitter.SetExpanded(-1)
            self._parent._panel.item.doLayout()
        self._parent._curExpand = self._parent._panel.item.splitter._expanded
        pass

    def test(self, callerWin, event=0):
        pass

    def teste(self, callerWin, event=0):
        pass

    def rotateX(self, callerWin, event=0):
        print 'rotateX check clicked', event.GetId()
        curState = self._parent.getPopupmenuCheckedState(event.GetId())
        self._parent.setPopupmenuCheckedState(event.GetId(), not curState)
        event.Skip()
        pass

    def rotateZ(self, callerWin, event=0):
        print 'rotateX check clicked', event.GetId()
        print wx.FindWindowById(event.GetId())
        print self._parent._ids[event.GetId()]
        curState = self._parent.getPopupmenuCheckedState(event.GetId())
        self._parent.setPopupmenuCheckedState(event.GetId(), not curState)
        #self._parent._ids[event.GetId()].Check()
        event.Skip()
        pass


class commonHandler(baseController):
    def readSourceImages(self):
        self._rawReader = vtk.vtkDICOMImageReader()
        #self._rawReader = NPPlan.currentPlan.planGetVtkVolumeReader()
        try:
            self._rawReader.SetDirectoryName(self._parent.path)
        except KeyError:
            self._rawReader.SetDirectoryName("C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\")
        self._rawReader.Update()

        self._reader = vtk.vtkDICOMImageReader()
        #self._reader = NPPlan.currentPlan.planGetVtkVolumeReader()
        try:
            self._reader.SetDirectoryName(self._parent.path)
        except KeyError:
            self._reader.SetDirectoryName("C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\")
        #self._reader.SetDataExtent(0, 511, 0, 511, 0, self._parent._sliceQuantity)
        #print self._parent.pixelSpacing, self._parent.pixelSpacing[0], self._parent.sliceSpacing
        #self._reader2.SetDataSpacing(self._parent.pixelSpacing[0], self._parent.pixelSpacing[1], self._parent.sliceSpacing)
        self._reader.Update()
        #self._reader.SetDataOrigin(255, 255, 255)
        #self._reader.SetDataOrigin(-self._dX, -self._dY, -self._dZ)

        self._shrinkFactor = 4
        self._shrink = vtk.vtkImageShrink3D()
        self._shrink.SetShrinkFactors(self._shrinkFactor, self._shrinkFactor, 1)
        self._shrink.SetInput(self._reader.GetOutput())

        try:
            im = vtk.vtkImageMapToWindowLevelColors()
            im.SetWindow(float(NPPlan.config.appConfig.client.defaultwindow))
            im.SetLevel(float(NPPlan.config.appConfig.client.defaultlevel))
            im.SetInput(self._reader.GetOutput())
        except KeyError:
            im = vtk.vtkImageMapToWindowLevelColors()
            im.SetWindow(float(600))
            im.SetLevel(float(700))
            im.SetInput(self._reader.GetOutput())
        except AttributeError:
            im = vtk.vtkImageMapToWindowLevelColors()
            im.SetWindow(float(600))
            im.SetLevel(float(700))
            im.SetInput(self._reader.GetOutput())

        self._wlReader = im
        self._body = vtk.vtkImageReslice()
        #self._body.SetInput(im.GetOutput())
        self._body.SetInput(self._wlReader.GetOutput())
        #self._body.SetOutputSpacing(self._parent.pixelSpacing[0], self._parent.pixelSpacing[1], self._parent.sliceSpacing)
        #self._body.SetOutputExtent(0, 511, 0, 511, 0, self._parent._sliceQuantity*self._parent.sliceSpacing)
        self._body.AutoCropOutputOn()


class planeHandler(commonHandler):
    def __init__(self, parent, vtkWindow, caption, captionColor, **kwargs):
        self._parent = parent
        self._vtkInteractor = vtkWindow
        self._caption = caption
        self._captionColor = captionColor

    def createProjection(self, origin=(0,0,0), normal=(0,0,1)):
        self._plane = vtk.vtkPlane()
        self._plane.SetOrigin(*origin)
        self._plane.SetNormal(*normal)
        #self._plane.SetOrigin(92, 88, 68)
        #self._plane.SetNormal(1, 0, 0)

        #self._transform = vtk.vtkTransform()
        #self._transform.Translate(-self._center[0], -self._center[1], -self._center[2])
        #self._transformF = vtk.vtkTransformPolyDataFilter()
        #self._transformF.SetInputConnection(self._body.GetOutputPort())
        #self._transformF.SetTransform(self._transform)

        self._imgCutter = vtk.vtkCutter()
        self._imgCutter.SetInput(self._body.GetOutput())
        #self._imgCutter.SetInput(self._transformF.GetOutput())
        self._imgCutter.SetCutFunction(self._plane)
        self._imgCutter.GenerateCutScalarsOff()

        self._imgMapper = vtk.vtkPolyDataMapper()
        self._imgMapper.SetInput(self._imgCutter.GetOutput())
        self._imgMapper.Update()

        print 'bounds', self._imgMapper.GetBounds()

        self._imgActor = vtk.vtkActor()
        self._imgActor.SetMapper(self._imgMapper)

    def setRenderer(self):

        self._renderer = vtk.vtkRenderer()
        self._renderer.AddActor(self._imgActor)
        self._renderer.AddActor(self.sliceText)
        self._renderer.AddActor(self.infoText)
        for i in self.linesActor:
            self._renderer.AddActor(i)
        self._renderer.SetActiveCamera(self._camera)
        self._renderer.ResetCamera(self._imgMapper.GetBounds())

    def addBeamTrace(self, beamTraceActor):
        print 'beam tracer actor adding'
        self._renderer.AddActor(beamTraceActor)
        self.update()
        #self._renderer.SetActiveCamera(self._camera)
        #self._renderer.ResetCamera(self._imgMapper.GetBounds())

    def delBeamTrace(self, beamTraceActor):
        self._renderer.RemoveActor(beamTraceActor)
        self.update()

    def setInteractor(self):
        self._vtkInteractor.GetRenderWindow().AddRenderer(self._renderer)
        self._renderer.Render()
        self._vtkInteractor.StartPickCallback()
        self._vtkInteractor.Start()

    def createText(self):
        pass
        txt = vtk.vtkTextActor()
        txt.SetInput(self._caption)
        txtprop=txt.GetTextProperty()
        txtprop.SetFontFamilyToArial()
        txtprop.SetFontSize(18)
        txtprop.BoldOn()
        txtprop.SetColor(*self._captionColor)
        txt.SetDisplayPosition(3,3)
        self.sliceText = txt

        txt2 = vtk.vtkTextActor()
        txt2.SetInput('123')
        txtprop = txt2.GetTextProperty()
        txtprop.SetFontFamilyToArial()
        txtprop.SetFontSize(14)
        txtprop.SetColor(*self._captionColor)
        txt2.SetDisplayPosition(120,3)
        self.infoText = txt2

    def getSliceLinesInfo(self, axis):
        base = self._plane.GetOrigin()
        bounds = self._imgActor.GetBounds()
        try:
            origins = self._parent.getSlicesOrigins()
        except KeyError:
            origins = None
        except AttributeError:
            origins = None

        print base
        print bounds
        if axis == 2:
            origin = [bounds[0], base[1], base[2]]
            p0 = [bounds[1], base[1], base[2]]
            origin2 = [base[0], bounds[2], base[2]]
            p1 = [base[0], bounds[3], base[2]]
        elif axis == 1:
            origin = [bounds[0], base[1], base[2]]
            p0 = [bounds[1], base[1], base[2]]
            origin2 = [base[0], base[1], bounds[4]]
            p1 = [base[0], base[1], bounds[5]]
        elif axis == 0:
            origin = [base[0], bounds[2], base[2]]
            p0 = [base[0], bounds[3], base[2]]
            origin2 = [base[0], base[1], bounds[4]]
            p1 = [base[0], base[1], bounds[5]]
        return p0, origin, p1, origin2

    def drawSliceLine(self, axis=-1, color0=(1, 0, 0), color1=(0, 1, 0)):
        # Create a vtkPoints object and store the points in it
        # @todo: update slice line while plane changed
        #self._plane.GetOrigin(), p0, p1,
        self._axis = axis
        print 'reader get bounds and slice line for axis %d' % axis
        #print self._parent.getSlicesOrigins()
        #self._p0 = p0
        #self._p1 = p1
        #pts = vtk.vtkPoints()
        #pts.InsertNextPoint(origin)
        #pts.InsertNextPoint(p0)
        #pts.InsertNextPoint(origin2)
        #pts.InsertNextPoint(p1)

        p0, origin, p1, origin2 = self.getSliceLinesInfo(axis)
        # Create the first line (between Origin and P0)
        #line0 = vtk.vtkLine()
        self.line0 = line0 = vtk.vtkLineSource()
        line0.SetPoint1(*origin)
        line0.SetPoint2(*p0)
        #print 'pointIds', line0.GetPointIds()
        #line0.GetPointIds().SetId(0,0) # the second 0 is the index of the Origin in the vtkPoints
        #line0.GetPointIds().SetId(1,1) # the second 1 is the index of P0 in the vtkPoints

        # Create the second line (between Origin and P1)
        #line0 = vtk.vtkLine()
        self.line1 = line1 = vtk.vtkLineSource()
        line1.SetPoint1(*origin2)
        line1.SetPoint2(*p1)

        mapper0 = vtk.vtkPolyDataMapper()
        mapper0.SetInput(line0.GetOutput())
        mapper1 = vtk.vtkPolyDataMapper()
        mapper1.SetInput(line1.GetOutput())

        actor0 = vtk.vtkActor()
        actor0.SetMapper(mapper0)
        actor0.GetProperty().SetColor(*color0)
        actor1 = vtk.vtkActor()
        actor1.SetMapper(mapper1)
        actor1.GetProperty().SetColor(*color1)
        self.linesActor = actor0, actor1
        pass

    def setCamera(self, viewUp=(0,0,-1), position=(0, 0, 100), focalPoint=(0, 0, 0)):
        self._camera = vtk.vtkCamera()
        self._camera.SetViewUp(*viewUp)
        self._camera.SetPosition(*position)
        self._camera.SetFocalPoint(*focalPoint)
        self._camera.ParallelProjectionOn()

    def start(self, center='', **kwargs):
        self._center = center
        self.readSourceImages()
        self.createProjection()
        self.createText()
        self.drawSliceLine()
        self.setCamera()
        self.setRenderer()
        self.setInteractor()

    def updateLines(self, p0, origin, p1, origin2):
        print 'new lines with', p0, origin, p1, origin2
        self.line0.SetPoint1(*origin)
        self.line0.SetPoint2(*p0)
        self.line1.SetPoint1(*origin2)
        self.line1.SetPoint2(*p1)
        self.line0.Update()
        self.line1.Update()
        self.update()

    def updatePlane(self):
        self._imgCutter.Update()
        self._imgMapper.Update()
        self._renderer.ResetCamera(self._imgMapper.GetBounds())

    def update(self):
        #self._renderer.Render()
        self._vtkInteractor.Update()
        self._vtkInteractor.Render()

    def mWheelUp(self, event=0):
        # @todo: handle mWheelUp/mWheelDown after scroll at rightBar affected
        print 'mwheelup final'
        print type(self._parent)
        step = self._parent.handler.rightPanelBev.controller.bevSlicesView.getStep(caller=self._name)
        #step = 1.5
        self.origin = self.origin + step
        self.setCaption(str(self.origin))
        #self.update()
        self._parent.updateAllLines()
        if 0 != event:
            event.Skip()
        pass
    def mWheelDown(self, event=0):
        print 'mwheeldown final'
        print self.origin
        step = self._parent.handler.rightPanelBev.controller.bevSlicesView.getStep(caller=self._name)
        #step = 1.5
        self.origin = self.origin - step
        self.setCaption(str(self.origin))
        #self.update()
        self._parent.updateAllLines()
        if 0 != event:
            event.Skip()
        pass


class axialHandler(planeHandler):
    def __init__(self, parent, vtkWindow, **kwargs):
        planeHandler.__init__(self, parent, vtkWindow, 'Axial', (1, 0, 0), **kwargs)
        print 'axial got vtkWindow', vtkWindow
        self._parent = parent
        self._vtkInteractor = vtkWindow
        self._name = 'Axial'
        #vtkWindow.controller = self
        pass

    def createProjection(self, origin=0, normal=0):
        #planeHandler.createProjection(self, (0, 0, 20), (0, 0, 1))
        #planeHandler.createProjection(self, (0.1, 0.1, 0.1), (0, 0, 1))
        planeHandler.createProjection(self, self._center, (0, 0, 1))
        #planeHandler.createProjection(self, (92, 88, 68), (0, 0, 1))

    def setCaption(self, text):
        self.infoText.SetInput('z = %s'%text)
        pass

    def drawSliceLine(self, *args, **kwargs):
        planeHandler.drawSliceLine(self, 2, (0, 1, 0), (0, 0, 1))

    def updateLines(self):
        origins = self._parent.getSlicesOrigins()
        bounds = self._imgActor.GetBounds()
        p0 = [bounds[0], origins['coronal'][1], bounds[4]]
        origin = [bounds[1], origins['coronal'][1], bounds[5]]
        p1 = [origins['sagittal'][0], bounds[2], bounds[4]]
        origin2 = [origins['sagittal'][0], bounds[3], bounds[5]]
        planeHandler.updateLines(self, p0, origin, p1, origin2)
        pass

    def setCamera(self, viewUp=0, position=0, focalPoint=0):
        planeHandler.setCamera(self, (0, 0, -1), (0, 0, 100), (0, 0, 0))

    def setPlaneOrigin(self, val):
        oldO = self._plane.GetOrigin()
        #p = self._parent.getSlicesOrigins()
        self._plane.SetOrigin(oldO[0], oldO[1], val)
        self.updatePlane()
    def getPlaneOrigin(self):
        return self._plane.GetOrigin()[2]
    origin = property(getPlaneOrigin, setPlaneOrigin)

    def start(self, **kwargs):
        planeHandler.start(self, **kwargs)
        self.setCaption(self.origin)


class coronalHandler(planeHandler):
    def __init__(self, parent, vtkWindow, **kwargs):
        planeHandler.__init__(self, parent, vtkWindow, 'Coronal', (0, 1, 0), **kwargs)
        print 'coronal got vtkWindow', vtkWindow
        self._parent = parent
        self._vtkInteractor = vtkWindow
        self._name = 'Coronal'

    def updateLines(self):
        print 'updateLines called'
        origins = self._parent.getSlicesOrigins()
        bounds = self._imgActor.GetBounds()
        p0 = [bounds[0], bounds[2], origins['axial'][2]]
        origin = [bounds[1], bounds[3], origins['axial'][2]]
        p1 = [origins['sagittal'][0], bounds[2], bounds[4]]
        origin2 = [origins['sagittal'][0], bounds[3], bounds[5]]
        planeHandler.updateLines(self, p0, origin, p1, origin2)
        #planeHandler.updateLines(self, p0, origin, p1, origin2)
        pass

    def drawSliceLine(self, *args, **kwargs):
        planeHandler.drawSliceLine(self, 1, (1, 0, 0), (0, 0, 1))

    def createProjection(self, origin=0, normal=0):
        #planeHandler.createProjection(self, (92, 88, 68), (0, 1, 0))
        #planeHandler.createProjection(self, (0.1, 0.1, 0.1), (0, 1, 0))
        planeHandler.createProjection(self, self._center, (0, 1, 0))
        #planeHandler.createProjection(self, (20, 20, 20), (0, 1, 0))

    def setCamera(self, viewUp=0, position=0, focalPoint=0):
        planeHandler.setCamera(self, (0, 1, 0), (0, 100, 0), (0, 0, 0))

    def setCaption(self, text):
        self.infoText.SetInput('y = %s' %text)
        pass

    def setPlaneOrigin(self, val):
        #p = self._parent.getSlicesOrigins()
        oldO = self._plane.GetOrigin()
        self._plane.SetOrigin(oldO[0], val, oldO[2])
        self.updatePlane()
    def getPlaneOrigin(self):
        return self._plane.GetOrigin()[1]
    origin = property(getPlaneOrigin, setPlaneOrigin)

    def start(self, **kwargs):
        planeHandler.start(self, **kwargs)
        self.setCaption(self.origin)


class sagittalHandler(planeHandler):
    def __init__(self, parent, vtkWindow, **kwargs):
        planeHandler.__init__(self, parent, vtkWindow, 'Sagittal', (0, 0, 1), **kwargs)
        print 'sagittal got vtkWindow', vtkWindow
        self._parent = parent
        self._vtkInteractor = vtkWindow
        self._name = 'Sagittal'
        #vtkWindow.controller = self

    def updateLines(self):
        print 'updateLines called'
        origins = self._parent.getSlicesOrigins()
        bounds = self._imgActor.GetBounds()
        p0 = [bounds[0], bounds[2], origins['axial'][2]]
        origin = [bounds[1], bounds[3], origins['axial'][2]]
        p1 = [bounds[0], origins['coronal'][1], bounds[4]]
        origin2 = [bounds[1], origins['coronal'][1], bounds[5]]
        planeHandler.updateLines(self, p0, origin, p1, origin2)
        pass

    def drawSliceLine(self, *args, **kwargs):
        planeHandler.drawSliceLine(self, 0, (1, 0, 0), (0, 1, 0))

    def createProjection(self, origin=0, normal=0):
        #planeHandler.createProjection(self, (20, 20, 20), (1, 0, 0))
        #planeHandler.createProjection(self, (0.1, 0.1, 0.1), (1, 0, 0))
        planeHandler.createProjection(self, self._center, (1, 0, 0))
        #planeHandler.createProjection(self, (92, 88, 68), (1, 0, 0))

    def setCamera(self, viewUp=0, position=0, focalPoint=0):
        planeHandler.setCamera(self, (0, 0, 1), (100, 0, 0), (0, 0, 0))

    def setCaption(self, text):
        self.infoText.SetInput('x = %s'%text)
        pass

    def setPlaneOrigin(self, val):
        oldO = self._plane.GetOrigin()
        #p = self._parent.getSlicesOrigins()
        self._plane.SetOrigin(val, oldO[1], oldO[2])
        self.updatePlane()
    def getPlaneOrigin(self):
        return self._plane.GetOrigin()[0]
    origin = property(getPlaneOrigin, setPlaneOrigin)

    def start(self, **kwargs):
        planeHandler.start(self, **kwargs)
        self.setCaption(self.origin)


class threeDimHandler(commonHandler):
    def __init__(self, parent, vtkWindow):
        """
        @param parent: родитель
        @type parent: bevController
        @param vtkWindow: окно
        @type vtkWindow: NPPlan.view.client.main.stages.positioning.threeDimWindow.threeDimWindow
        """
        print 'threeDim got vtkWindow', vtkWindow
        self._parent = parent
        self._vtkInteractor = vtkWindow
        self._tumorActor = None
        self._params = {
            'leftMouse': True,
            'rightMouse': True,
            'rotateField': False,
            'rotateBody': True,
            'zoomState': False,
        }
        self._state = {
            'leftClicked': False,
            'rightClicked': False,
            'active': False,
            'oldPosition': (-1, -1,),
            'newPosition': (-1, -1,),
        }
        self._center = None
        self.coeffX = 0.5
        self.coeffY = 0.5
        self._bodyRotation = {'anglesDegrees': [0, 0], 'anglesRadians': [0, 0]}
        #vtkWindow.controller = self

    def setParams(self, **kwargs):
        print 'before'
        print self._params
        self._params.update(kwargs)
        print 'after'
        print self._params

    def setState(self, **kwargs):
        oldState = self._state.copy()
        self._state.update(kwargs)

        if self.getParam('zoomState') and self._state['rightClicked']:
            self._spaceRenderer.ResetCamera()
            self.update()

        #if (self.getParam('rotateField') and not self._state['leftClicked'] and oldState['leftClicked']) or \
        #    (self.getParam('rotateBody') and not self._state['leftClicked'] and oldState['leftClicked']) or \
        #    (self.getParam('rotateBody') and not self._state['rightClicked'] and oldState['rightClicked']):
        #    self.calculateBeam()
        if not self.getParam('zoomState') and (not self._state['leftClicked'] and oldState['leftClicked']) or \
                (not self._state['rightClicked'] and oldState['rightClicked']):
            self.calculateBeam()
            pass

    def calculateBeam(self):
        if 0 != self._parent._beamContainer.getLength():
            aid, abeam = self._parent._beamContainer.getActive()
            print abeam.getSourceCoordinates()
            self._parent._beamToRight.setBeamSourceData(abeam.getSourceCoordinates())
        pass

    def getParam(self, name):
        if name in self._params:
            return self._params[name]
        else:
            # @todo: throw
            return -1

    def isActive(self):
        return self._state['active']

    def handleActiveMotion(self, event, skip=False):
        #print event.GetPosition()
        if not skip:
            self.setState(oldPosition=self._state['newPosition'], newPosition=event.GetPosition())
        if self.getParam('zoomState') and self.getParam('leftMouse'):
            self.zoomOnMotion(event=event)
        if self.getParam('rotateBody'):
            angle, axis = self.rotateBody(event)
            self._parent._beamContainer.rotateNonActive(angle, axis)
            self.update()
        if self.getParam('rotateField'):
            self.rotateField(event)
            self.update()

        #if 0 != self._parent._beamContainer.getLength():
        #    aid, abeam = self._parent._beamContainer.getActive()
        #    print abeam.getSourceCoordinates()

    def start(self):
        self.readSourceImages()
        self._trans = vtk.vtkTransform()
        self._body.SetResliceTransform(self._trans)
        self.create3D()
        self.setCamera()
        self.setRenderer()
        self.setInteractor()

    def outline(self):
        outlineData = vtk.vtkOutlineFilter()
        outlineData.SetInput(self._skinStripper.GetOutput())
        mapOutline = vtk.vtkPolyDataMapper()
        mapOutline.SetInput(outlineData.GetOutput())
        outline = vtk.vtkActor()
        outline.SetMapper(mapOutline)
        outline.GetProperty().SetColor(0, 0, 0)
        self._outlineActor = outline

    def create3D(self):
        self._shrink = vtk.vtkImageShrink3D()
        self._shrink.SetShrinkFactors(4, 4, 1)
        self._shrink.SetInput(self._reader.GetOutput())

        self._skinExtractor = vtk.vtkContourFilter()
        self._skinExtractor.SetInput(self._shrink.GetOutput())
        #skinExtractor.GenerateValues(self.sk1, self.skL, self.skR)
        self._skinExtractor.SetValue(1, -300)
        self._skinExtractor.ComputeGradientsOn()
        #self._skinExtractor = skinExtractor

        self._transform = vtk.vtkTransform()
        #self._transform.Translate(20, 20, 20)
        self._transformF = vtk.vtkTransformPolyDataFilter()
        self._transformF.SetInputConnection(self._skinExtractor.GetOutputPort())
        self._transformF.SetTransform(self._transform)

        self._skinNormals = vtk.vtkPolyDataNormals()
        self._skinNormals.SetInputConnection(self._transformF.GetOutputPort())
        self._skinNormals.SetFeatureAngle(60.0)
        #self._skinNormals = skinNormals
        self._skinStripper = vtk.vtkStripper()
        self._skinStripper.SetInputConnection(self._skinNormals.GetOutputPort())
        self._skinMapper = vtk.vtkPolyDataMapper()
        self._skinMapper.SetInputConnection(self._skinStripper.GetOutputPort())
        self._skinMapper.ScalarVisibilityOff()
        #self._skinMapper = skinMapper
        self._skinActor = vtk.vtkActor()
        self._skinActor.SetMapper(self._skinMapper)

        center = self.getTarget()
        self._transform.Translate(-center[0], -center[1], -center[2])
        #self._transform.Translate(100, 100, 100)

        #self._skinActor = skin
        return

        boneExtractor = vtk.vtkMarchingCubes()
        boneExtractor.SetInputConnection(shrink.GetOutputPort())
        boneExtractor.SetValue(0, 1150)

        self._boneExtractor = boneExtractor

        boneNormals = vtk.vtkPolyDataNormals()
        boneNormals.SetInputConnection(boneExtractor.GetOutputPort())
        boneNormals.SetFeatureAngle(60.0)

        boneStripper = vtk.vtkStripper()
        boneStripper.SetInputConnection(boneNormals.GetOutputPort())

        #boneLocator = vtk.vtkCellLocator()
        #boneLocator.SetDataSet(boneExtractor.GetOutput())
        #boneLocator.LazyEvaluationOn()

        #self._boneLocator = boneLocator
        self._boneStripper = boneStripper

        boneMapper = vtk.vtkPolyDataMapper()
        boneMapper.SetInputConnection(boneStripper.GetOutputPort())
        #boneMapper.SetInput(self._boneStripper.GetOutput())
        boneMapper.ScalarVisibilityOff()

        boneProperty = vtk.vtkProperty()
        boneProperty.SetColor(1.0, 1.0, 0.9)

        bone = vtk.vtkActor()
        bone.SetMapper(boneMapper)
        bone.SetProperty(boneProperty)

        self._boneActor = bone

    def rotateBody(self, event=0):
        diffX = self._state['newPosition'][0] - self._state['oldPosition'][0]
        diffY = self._state['newPosition'][1] - self._state['oldPosition'][1]
        center = self.getTarget()
        if self.getParam('leftMouse') and self._state['leftClicked']:
            #self._transform.Translate(-center[0], -center[1], -center[2])
            self._skinActor.RotateZ(0.5 * diffX)
            self._bodyRotation['anglesDegrees'][1] += 0.5 * diffX
            self._bodyRotation['anglesRadians'][1] = np.radians(self._bodyRotation['anglesDegrees'][1])
            if hasattr(self, '_outlineActor'):
                self._outlineActor.RotateZ(0.5 * diffX)
            #self._transform.Translate(+center[0], +center[1], +center[2])
            self._parent._beamContainer.rotateNonActive(0.5 * diffX, 2)
            self._parent._beamContainer.rotateTracers(0.5 * diffX, 2)
        if self.getParam('rightMouse') and self._state['rightClicked']:
            # @todo: diffY ??
            self._skinActor.RotateY(0.5 * diffX)
            if hasattr(self, '_outlineActor'):
                self._outlineActor.RotateY(0.5 * diffX)
            self._bodyRotation['anglesDegrees'][0] += 0.5 * diffX
            self._bodyRotation['anglesRadians'][0] = np.radians(self._bodyRotation['anglesDegrees'][0])
            self._parent._beamContainer.rotateNonActive(0.5 * diffX, 0)
            self._parent._beamContainer.rotateTracers(0.5 * diffX, 0)
        #self.update()
        #{'anglesDegrees': [0, 0], 'anglesRadians': [0, 0]}

        self._parent.axial.update()
        self._parent.sagittal.update()
        self._parent.coronal.update()
        return (
            0.5 * diffX,
            0 if self.getParam('rightMouse') and self._state['rightClicked'] else 2
        )
        pass

    def getBodyRotation(self):
        return self._bodyRotation

    def rotateField(self, event=0):
        if self.getParam('leftMouse') and self._state['leftClicked']:
            diffX = self._state['newPosition'][0] - self._state['oldPosition'][0]
            diffY = self._state['newPosition'][1] - self._state['oldPosition'][1]
            angleX = 0.5 * diffX
            angleY = 0.5 * diffY
            self._parent._beamContainer.rotateActive(angleX, 2)
            self._parent._beamContainer.rotateActive(angleY, 0)
        if self.getParam('rightMouse') and self._state['rightClicked']:
            diffX = self._state['newPosition'][0] - self._state['oldPosition'][0]
            diffY = self._state['newPosition'][1] - self._state['oldPosition'][1]
            angleX = 0.04 * diffX
            angleY = 0.04 * diffY
            self._parent._beamContainer.rotateActive(angleX, 2)
            self._parent._beamContainer.rotateActive(angleY, 0)
        self._parent.axial.update()
        self._parent.sagittal.update()
        self._parent.coronal.update()

    def setCamera(self):
        self._spaceCamera = vtk.vtkCamera()
        self._spaceCamera.SetViewUp(0, 0, 1)
        self._spaceCamera.SetPosition(1, 0, 0)
        self._spaceCamera.SetDistance(700)
        self._spaceCamera.SetFocalPoint(0, 0, 0)
        self._spaceCamera.ParallelProjectionOff()

    def showSkin(self):
        self._spaceRenderer.AddActor(self._skinActor)

    def hidSkin(self):
        self._spaceRenderer.RemoveActor(self._skinActor)

    def showPoint(self, coords, _id=-1, color=(0.0, 1.0, 0.0)):
        points = vtk.vtkPoints()
        p = coords

        # Create the topology of the point (a vertex)
        vertices = vtk.vtkCellArray()

        id = points.InsertNextPoint(p)
        vertices.InsertNextCell(1)
        vertices.InsertCellPoint(id)

        # Create a polydata object
        point = vtk.vtkPolyData()

        # Set the points and vertices we created as the geometry and topology of the polydata
        point.SetPoints(points)
        point.SetVerts(vertices)

        # Visualize
        mapper = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            mapper.SetInput(point)
        else:
            mapper.SetInputData(point)

        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetPointSize(20)
        actor.GetProperty().SetColor(*color)

        #self._points[_id] = {'actor': actor, 'coords': coords}

        self._spaceRenderer.AddActor(actor)

    def setRenderer(self):
        self._spaceRenderer = vtk.vtkRenderer()
        self._spaceRenderer.AddActor(self._skinActor)
        self.outline()
        self._spaceRenderer.AddActor(self._outlineActor)
        #self._spaceRenderer.AddActor(self._boneActor)
        self._spaceRenderer.SetBackground(1, 1, 1)
        self._spaceRenderer.SetActiveCamera(self._spaceCamera)
        self._spaceRenderer.ResetCamera()

    def getTarget(self):
        if self._center is None:
            if self._tumorActor is not None:
                self._center = self._tumorActor.GetCenter()
            else:
                self._center = self._skinActor.GetCenter()
        return self._center

    def addBeamActor(self, actor):
        print 'Adding actor'
        self._spaceRenderer.AddActor(actor)
        print 'Actor added'
        self.update()
        print 'Rendered updated'
        #print self._skinActor.

    def zoomOnMotion(self, event):
        if self._state['newPosition'][0] - self._state['oldPosition'][0] > 0:
            self._spaceCamera.Zoom(1.03)
        else:
            self._spaceCamera.Zoom(0.97)
        self.update()

    def testFunc(self):
        print 'testFunc'
        print self._skinActor.GetOrientation()

    def update(self):
        #self._spaceRenderer.Update()
        self._spaceRenderer.Render()
        self._vtkInteractor.Render()

    def setInteractor(self):
        # @todo: handle buttons click
        #self._rendererWin = vtk.vtkRenderWindow()
        #self._rendererWin.AddRenderer(self._spaceRenderer)
        #self.showPoint(self.getTarget())
        self.showPoint((0, 0, 0))
        #self._vtkInteractor.GetRenderWindow().AddRenderer(self._rendererWin)
        self._vtkInteractor.GetRenderWindow().AddRenderer(self._spaceRenderer)
        self._spaceRenderer.Render()
        self._vtkInteractor.StartPickCallback()
        self._vtkInteractor.Start()
        print 'Skin actor center', self._skinActor.GetCenter()

    def showPopup(self, obj=0, event=0):
        print 'test'

    def leftPress(self, event):
        self._RotatingZ = 1 if self._parent.rotationConfig['rotateZ'] else 0
        self._RotatingX = 1 if self._parent.rotationConfig['rotateX'] else 0
        self._oldPos = self._curPos = wx.GetMousePosition()
        #self._oldPos = self._curPos = event.GetPosition()
        print 'Start rotating from', self._oldPos
        pass

    def leftRelease(self, event):
        self._RotatingZ = 0
        self._RotatingX = 0
        self._curPos = wx.GetMousePosition()
        #self._curPos = event.GetPosition()
        print 'Stop rotating at', self._curPos
        pass

    def stopRotation(self):
        self._RotatingX = 0
        self._RotatingZ = 0

    def motion(self, event):
        #print self._vtkInteractor.GetLastEventPosition()
        #print self._vtkInteractor.GetEventPosition()
        if self._RotatingX == 0 and self._RotatingZ == 0:
            return
        self._curPos = wx.GetMousePosition()
        print 'Rotating from %s to %s' %(self._oldPos, self._curPos)
        xo, yo = self._oldPos
        xn, yn = self._curPos
        diffX = xn - xo
        diffY = yn - yo
        if self._RotatingX:
            self._skinActor.RotateZ(self.coeffX*diffX)
        if self._RotatingZ:
            self._skinActor.RotateY(-self.coeffY*diffY)
        self._spaceRenderer.Render()
        self._vtkInteractor.Update()
        self._vtkInteractor.Render()
        self._oldPos = self._curPos
        pass


class vtkBevHandler(object):
    def __init__(self, bevController, **kwargs):
        """
        @param bevController: контроллер
        @type bevController: bevController
        """
        self._parent = bevController
        self._vtkInteractor = bevController._panel.item.vtkWindow
        self._vtkInteractor.SetInteractorStyle(None)
        self._vtkInteractor.Enable(1)

        self.readSourceImages()
        self.setOrigin()
        self.create3D()
        self.createProjections()
        self.setCamera()
        self.setRenderer()
        self.setInteractor()

    def readSourceImages(self):
        self._reader = vtk.vtkDICOMImageReader()
        self._reader.SetDirectoryName(self._parent.path)
        self._reader.Update()

        im = vtk.vtkImageMapToWindowLevelColors()
        im.SetWindow(float(NPPlan.config.appConfig.client.defaultwindow))
        im.SetLevel(float(NPPlan.config.appConfig.client.defaultlevel))
        im.SetInput(self._reader.GetOutput())
        self._wlReader = im
        self._body = vtk.vtkImageReslice()
        self._body.SetInput(im.GetOutput())

    def setExtent(self):
        pass
    def setOrigin(self):
        # @todo: set to tumour
        pass

    def create3D(self):
        shrink = vtk.vtkImageShrink3D()
        shrink.SetShrinkFactors(4, 4, 1)
        shrink.SetInput(self._reader.GetOutput())

        skinExtractor = vtk.vtkContourFilter()
        skinExtractor.SetInput(shrink.GetOutput())
        #skinExtractor.GenerateValues(self.sk1, self.skL, self.skR)
        skinExtractor.SetValue(1, -300)
        skinExtractor.ComputeGradientsOn()
        skinNormals = vtk.vtkPolyDataNormals()
        skinNormals.SetInput(skinExtractor.GetOutput())
        skinNormals.SetFeatureAngle(60.0)
        skinMapper = vtk.vtkPolyDataMapper()
        skinMapper.SetInput(skinNormals.GetOutput())
        skinMapper.ScalarVisibilityOff()
        skin = vtk.vtkActor()
        skin.SetMapper(skinMapper)

        self._skinActor = skin

    def createProjection(self, id, normalTuple, ):

        pass

    def createProjections(self):
        self._planes = vtkPlaneContainer(self)

    def setCamera(self):
        self._spaceCamera = vtk.vtkCamera()
        self._spaceCamera.SetViewUp(0, 0, 1)
        self._spaceCamera.SetPosition(-500, 0, 0)
        self._spaceCamera.SetFocalPoint(0, 0, 0)
        self._spaceCamera.ParallelProjectionOn()

        self._axialCamera = vtk.vtkCamera()
        self._axialCamera.SetViewUp(0, 0, -1)
        self._axialCamera.SetPosition(0, 0, 100)
        self._axialCamera.SetFocalPoint(0, 0, 0)
        self._axialCamera.ParallelProjectionOn()


    def setRenderer(self):

        outlineData = vtk.vtkOutlineFilter()
        outlineData.SetInput(self._body.GetOutput())
        mapOutline = vtk.vtkPolyDataMapper()
        mapOutline.SetInput(outlineData.GetOutput())
        outline = vtk.vtkActor()
        outline.SetMapper(mapOutline)
        outline.GetProperty().SetColor(0, 0, 0)

        self._spaceRenderer = vtk.vtkRenderer()
        self._spaceRenderer.AddActor(self._skinActor)
        self._spaceRenderer.AddActor(outline)
        self._spaceRenderer.SetBackground(1, 1, 1)
        self._spaceRenderer.SetViewport(0.0, 0.5, 0.5, 1.0)
        self._spaceRenderer.SetActiveCamera(self._spaceCamera)
        self._spaceRenderer.ResetCamera()

        self._planes.axial.setRenderer()
        self._planes.coronal.setRenderer()
        self._planes.saggital.setRenderer()

    def setInteractor(self):
        #self._rendererWin = vtk.vtkRenderWindow()
        #self._rendererWin.AddRenderer(self._spaceRenderer)
        #self._vtkInteractor.GetRenderWindow().AddRenderer(self._rendererWin)
        self._vtkInteractor.GetRenderWindow().AddRenderer(self._spaceRenderer)
        self._vtkInteractor.GetRenderWindow().AddRenderer(self._planes.axial.renderer)
        self._vtkInteractor.GetRenderWindow().AddRenderer(self._planes.coronal.renderer)
        self._vtkInteractor.GetRenderWindow().AddRenderer(self._planes.saggital.renderer)
        self._spaceRenderer.Render()
        self._vtkInteractor.StartPickCallback()
        self._vtkInteractor.Start()


    def work(self):
        print 'BEV WORKING'
        pass

    def test(self):
        pass
        pass


class vtkPlaneObject(object):
    def __init__(self, parent, id, originTuple, normalsTuple, **kwargs):
        """
        @param parent: родитель
        @type parent: vtkBevHandler
        """
        self._parent = parent
        self._id = id

        _plane = vtk.vtkPlane()
        _plane.SetOrigin(*originTuple)
        _plane.SetNormal(*normalsTuple)

        _imgCutter = vtk.vtkCutter()
        _imgCutter.SetInput(self._parent._body.GetOutput())
        _imgCutter.SetCutFunction(_plane)
        _imgCutter.GenerateCutScalarsOff()

        self._imgMapper = vtk.vtkPolyDataMapper()
        self._imgMapper.SetInput(_imgCutter.GetOutput())
        self._imgMapper.Update()

        self.actor = vtk.vtkActor()
        self.actor.SetMapper(self._imgMapper)

    def getBounds(self):
        return self._imgMapper.GetBounds()
    bounds = property(getBounds)

    def cameraViewUp(self, viewUp, position):
        self.camera = vtk.vtkCamera()
        self.camera.SetViewUp(*viewUp)
        self.camera.SetPosition(*position)
        self.camera.SetFocalPoint(0, 0, 0)
        self.camera.ParallelProjectionOn()

    def setViewport(self, viewport):
        self.__dict__['viewport'] = viewport
    def getViewport(self):
        return self.__dict__['viewport']
    viewport = property(getViewport, setViewport)

    def setRenderer(self):
        self._renderer = vtk.vtkRenderer()
        self._renderer.AddActor(self.actor)
        #self._renderer.SetBackground(1, 1, 1)
        self._renderer.SetViewport(*self.viewport)
        self._renderer.SetActiveCamera(self.camera)
        self._renderer.ResetCamera(self.bounds)
        print self.actor, self.viewport, self.camera, self.bounds, self._renderer
    def getRenderer(self):
        return self._renderer
    renderer = property(getRenderer)

class vtkPlaneContainer(object):
    def __init__(self, parent):
        """
        @param parent: родитель
        @type parent: vtkBevHandler
        """
        self._parent = parent
        self.__dict__['axial'] = vtkPlaneObject(self._parent, 'axial', (20, 20, 20), (0, 0, 1))
        self.__dict__['coronal'] = vtkPlaneObject(self._parent, 'axial', (20, 20, 20), (0, 1, 0))
        self.__dict__['saggital'] = vtkPlaneObject(self._parent, 'axial', (20, 20, 20), (1, 0, 0))
        self.__dict__['axial'].cameraViewUp((0, 0, -1), (0, 0, 100))
        self.__dict__['axial'].viewport = (0.5, 0.5, 1.0, 1.0)
        self.__dict__['coronal'].cameraViewUp((0, 0, 1), (0, 100, 0))
        self.__dict__['coronal'].viewport = (0.5, 0.0, 1.0, 0.5)
        self.__dict__['saggital'].cameraViewUp((0, 1, 0), (-100, 0, 0))
        self.__dict__['saggital'].viewport = (0.0, 0.0, 0.5, 0.5)

    def getAxial(self):
        return self.__dict__['axial']
    axial = property(getAxial)

    def getCoronal(self):
        return self.__dict__['coronal']
    coronal = property(getCoronal)

    def getSaggital(self):
        return self.__dict__['saggital']
    saggital = property(getSaggital)


class Test(wx.Frame):
    def __init__(self, parent, wind):
        wx.Frame.__init__(self, None, 1, 'BEV TEST', size=(520, 611))
        sizer = wx.BoxSizer(wx.VERTICAL)

        self.vtkWindow = wxVTKRenderWindowInteractor(self, wx.NewId(), name='vtkWindow')
        sizer.Add(self.vtkWindow, 1, wx.EXPAND)
        self.SetSizer(sizer)

        self.start()

    def start(self):
        self.bv = bevController(self, mode='single', modeVtk=self.vtkWindow)
        self.bm = bevBeamController(uId=0, threeDimController=self.bv.threeDim)#, target=(92, 88, 68))
        #self.bm.center((10, 25, 30))
        self.bm.rotateSingle(10)
        pass


if __name__ == '__main__':
    from NPPlan.view.client.main.stages.contouring.renderWindow import renderWindow
    from vtk.wx.wxVTKRenderWindowInteractor import *
    NPPlan.init(programPath=r"c:\NPPlan3", test=True, utest=True, log=False)
    app = wx.PySimpleApp()
    #rw = renderWindow(app)
    #rw.Show()
    rtw = Test(app, wind=renderWindow)
    rtw.Show()
    app.MainLoop()
    pass


