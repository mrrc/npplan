# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 2
@date: 01.08.13
@summary: 
'''

import NPPlan
from NPPlan.controller.client.mainC.abstracts import baseController
import wx


class clientServerPostController(baseController):
    def __init__(self, parent, wxParent):
        """

        @param parent:
        @type parent: configurationControllerMain
        @param wxParent:
        @type wxParent: NPPlan.view.client.main.stages.configuration.cs.clientsAndServersView
        @return:
        """
        self._cParent = parent
        self._wxParent = wxParent

        self.__dict__['params'] = {}

    def checkCurrentState(self):
        # 1. test main server connection
        if not NPPlan.config.runningConfig.serverConnection:
            self._wxParent.addConsoleLine('Server connection failed')
        # 2. test database connection
        #self._wxParent.
        if NPPlan.mongoConnection is None:
            self.__dict__['params']['dbStateOn'] = False
        else:
            self.__dict__['params']['dbStateOn'] = True
        pass

    def sendDataToWxView(self):
        for i in self.__dict__['params']:
            val = self.__dict__['params'][i]
            if 'dbStateOn' == i:
                if val:
                    iconPath = NPPlan.getIconPath() + 'iconDbStateOn.png'
                    self.setDbInfo()
                else:
                    iconPath = NPPlan.getIconPath() + 'iconDbStateOff.png'
                self._wxParent.mongoStateBitmap.SetBitmap(wx.Bitmap(iconPath))

    def setDbInfo(self):
        self._wxParent.mongoHost.SetValue(NPPlan.config.appConfig.database.host)
        self._wxParent.mongoPort.SetValue(NPPlan.config.appConfig.database.port)
        self._wxParent.mongoDatabase.SetValue(NPPlan.config.appConfig.database.dbname)

        print NPPlan.mongoConnection._registered_documents
        for i in NPPlan.mongoConnection._registered_documents.keys():
            self._wxParent.mongoCollectionsList.Append(i)

    def doBindings(self):
        self._wxParent.Bind(wx.EVT_BUTTON, self.testConnection, self._wxParent.mongoTestConnection)

    def testConnection(self, event):
        # @todo: handle new params -> test connection
        print 'testing connection'
        NPPlan.log('configuration.cs', 'Mongo connection test initiated', 2)

        print NPPlan.mongoConnection.alive()
        print NPPlan.mongoConnection.server_info()
        if not NPPlan.mongoConnection.alive():
            self.handler.main.controller.showMessage(
                _('Connection error'),
                _('No database connection available')
            )
        else:
            info = NPPlan.mongoConnection.server_info()
            serverLine = self._wxParent.mongoHost.GetValue() + ":" + self._wxParent.mongoPort.GetValue()
            msg = "Server connection successful\n" \
                  "Server: %s\n" \
                  "sysInfo: %s\n" \
                  "version: %s\n" \
                  "" % (serverLine, info['sysInfo'], info['version'])
            self.handler.main.controller.showMessage(
                _('Test successful'),
                msg,
                wx.OK | wx.ICON_INFORMATION
            )
        pass

    def start(self, **kwargs):
        self.doBindings()
        self.checkCurrentState()
        self.sendDataToWxView()

    def work(self, **kwargs):
        pass