# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 11.09.13
@summary: 
'''

import NPPlan
import wx
from NPPlan.controller.client.mainC.abstracts import baseController


class facility(baseController):
    def __init__(self, parent):
        self.__dict__['parent'] = parent

    def _addPanelToPanel(self, panel, newWxClass):
        panel.GetParent().Freeze()
        panel.Freeze()
        pane = newWxClass(panel)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(pane, 1, wx.ALIGN_CENTER | wx.EXPAND)
        panel.SetSizer(sizer)
        panel.Layout()
        panel.Thaw()
        panel.GetParent().Thaw()
        panel.setCreated()
        return pane

    def mrrcvc(self, panel):
        print 'stage facility, substage mrrcvc'
        NPPlan.config.runningConfig.curSubStageType = 'mrrcvc'
        from NPPlan.controller.client.mainC.utils.generatorManager.main import generatorPanel
        p = self._addPanelToPanel(panel, generatorPanel)
        p.initManagerPanel()


    def dosimetry(self, panel):
        print 'stage facility, substage dosimetry'
        from NPPlan.stuff.COMCounter import topPanel as MyPanel1
        self._addPanelToPanel(panel, MyPanel1)
        pass

    def tableMotion(self, panel):
        print 'stage facility, substage tableMotion'
        pass

    def cameras(self, panel):
        print 'stage facility, substage cameras'
        pass

    def common(self, panel):
        pass

    def irradiation(self, panel):
        pass