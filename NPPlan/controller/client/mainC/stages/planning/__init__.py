# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 17.05.13
@summary: 
'''

from panel import *
from addPatient import *

__all__ = {
    'panel',
    'addPatient'
}