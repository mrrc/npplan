# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 06.12.12
@summary: 
'''

import NPPlan
from NPPlan.controller.client.mainC.abstracts.baseController import baseController

from NPPlan.view.client.main.stages.calculate import calculateMcnp, calculateGeant
import wx
import os
import NPPlan.model.file.local as npplanLocalFile
from random import random
try:
    from agw import pybusyinfo as PBI
except ImportError:
    import wx.lib.agw.pybusyinfo as PBI


class calculateController(baseController):
    @staticmethod
    def checkRequirements():
        return True

    def __init__(self, panel):
        """
        @param panel: панель
        @type panel: NPPlan.view.client.main.stages.calculate.main.calculateView
        """
        explorerChoices = [
                    _('All tasks (overall)'), _('All tasks (own)'),
                    _('Finished tasks (overall)'), _('Finished tasks (own)'),
                    _('Running tasks (overall)'), _('Running tasks (own)'),
                    _('Queued tasks (overall)'), _('Queued tasks (own)'),
                ]
        self.mcnpPanel = calculateMcnp(panel, choiceStatusExplorerChoices=explorerChoices)
        self.mcnpPanel._choiceStatusExplorer.SetSelection(1)
        panel.addPage(self.mcnpPanel, _('MCNP'))

        self.geantPanel = calculateGeant(panel, choiceStatusExplorerChoices=explorerChoices)
        self.geantPanel._choiceStatusExplorer.SetSelection(1)
        panel.addPage(self.geantPanel, _('Geant4'))
        #NPPlan.getRunningConfig()
        #NPPlan.config.aa = '123'
        pass

    def doBindings(self):
        print 'bindings'
        #self.mcnpPanel.Bind(wx.EVT_BUTTON, self.createMcnpButton, self.mcnpPanel._buttons['createMCNP'])
        #self.mcnpPanel.Bind(wx.EVT_BUTTON, self.runMcnpServerButton, self.mcnpPanel._buttons['runMCNPServer'])
        self.mcnpPanel.Bind(wx.EVT_BUTTON, self.createMcnpTaskLocal, self.mcnpPanel._btnCreateTaskClient)
        self.mcnpPanel.Bind(wx.EVT_BUTTON, self.createMcnpTaskServer, self.mcnpPanel._btnCreateTaskServer)
        self.mcnpPanel.Bind(wx.EVT_BUTTON, self.appendMcnpTask, self.mcnpPanel._btnAppendTask)
        self.mcnpPanel.Bind(wx.EVT_BUTTON, self.doWork, self.mcnpPanel._btnDoWork)
        self.mcnpPanel.Bind(wx.EVT_CHOICE, self.setExploration, self.mcnpPanel._choiceStatusExplorer)
        print 'bindings done'

    def setExploration(self, event):
        event.Skip()
        pass

    def doWork(self, event):
        event.Skip()
        pass

    def createMcnpTaskLocal(self, event):
        print 'Creating McnpTask via', event
        print NPPlan.currentPlan.planGetDicomLocalFolder()
        print self.mcnpPanel._checkCreateTaskClientBackground.GetValue()
        self.mcnpPanel.switchCreateLocalState(not self.mcnpPanel._createLocalState)
        if not self.mcnpPanel._checkCreateTaskClientBackground.GetValue():
            self.handler.main.controller.createBusy()
            self.saveMcnp(inputDicomDir=NPPlan.currentPlan.planGetDicomLocalFolder(),
                          )
            self.handler.main.controller.deleteBusy()
        pass

    def createMcnpTaskServer(self, event):
        pass

    def appendMcnpTask(self, event):
        print event
        event.Skip()
        # 0. busy
        self.handler.main.controller.createBusy()

        # 1. logic append
        # copy to local task dir

        # 2. visual append
        self.mcnpPanel.appendTask({
            'name': 'test1',
            'cid': 'CLIENT0',
            'status': 'Queued',
        })
        # 3. del busy
        self.handler.main.controller.deleteBusy()

    def createMcnpButton(self, event):
        event.Skip()
        self.saveMcnp(inputDicomDir=r'C:\Users\Chernukha\Pictures\DICOM\01111140')
        print 'aaa'
        event.Skip()

    def start(self):
        print 'STARTING CALC CONTROLLER'
        self.mcnpPanel.createListColumns([
            {'text': _('Task name'), 'size': 200},
            {'text': _('Task runner ID'), 'size': 400},
            {'text': _('Task status'), 'size': 50},
        ])
        #print NPPlan.config.aa
        self.doBindings()
        self.setInitialButtonsState()

    def setInitialButtonsState(self):
        self.mcnpPanel._btnAppendTask.Disable()
        self.mcnpPanel._checkCreateTaskClientBackground.SetValue(1)
        self.mcnpPanel.switchCreateLocalState(False)
        self.geantPanel.switchCreateLocalState(False)

    def work(self):
        print 'WORKING CALC CONTROLLER'
        # 1. Check currently running background tasks
        # 2. Switch buttons according to state
        pass

    def saveMcnp(self, inputDicomDir='', outputFile=''):
        from NPPlan.model.calc.base.grid import dicomSeries2MCNP
        from NPPlan.model.abstracts.temporary import getDir
        import os
        print getDir()
        v = dicomSeries2MCNP(inputDicomDir, os.path.join(getDir(), 'mcnplan'), compression=32)
        v.write()
        pass

    def runMcnpServerButton(self, event):
        event.Skip()
        task = self.runMcnpTaskOnServer(localFilePath=r'c:\mcnp5\mcnplan', targetDir='c:\\mcnpx\\executable\\')
        print task
        #self.runMcnpTaskOnServer()
        #from NPPlan.model.abstracts.temporary import getDir
        #import os
        #localFileName = os.path.join(getDir(), 'mcnplan')
        #print localFileName
        event.Skip()

    def runMcnpTaskOnServer(self, localFilePath='',
                            localCopy=True, targetDir='', saveOld=True, localRemoveAuxiliary=True,
                            remoteCopy=False,
                            ):
        print localFilePath
        auxExts = ['m', 'r', 'o']
        localFileName = os.path.basename(localFilePath)
        localFileDir = os.path.dirname(localFilePath)
        print localFileName, localFileDir
        if not os.path.exists(localFilePath):
            # @todo: throw error
            return -1
        if localCopy:
            if not (os.path.exists(targetDir) and os.path.isdir(targetDir)):
                return -2
            if localRemoveAuxiliary:
                for ext in auxExts:
                    npplanLocalFile.remove('%s%s' %(os.path.join(targetDir, localFileName), ext))
            if os.path.exists(os.path.join(targetDir, localFileName)):
                print 'Local file in targetDir already exists.'
                if saveOld:
                    # copy old file
                    # @todo: make new file name
                    targetFrom = os.path.join(targetDir, localFileName)
                    targetTo = os.path.join(targetDir, "%s%f" % (localFileName, random()) )
                    npplanLocalFile.copyFile(targetFrom, targetTo)
                    pass
            pass
            #targetDir =
        pass



