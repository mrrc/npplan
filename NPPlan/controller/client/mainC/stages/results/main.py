# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 04.03.14
@summary: 
'''

import NPPlan
from NPPlan.controller.client.mainC.abstracts import baseController
import wx
import os
try:
    from pubsub import pub
except ImportError:
    from wx.lib.pubsub import pub

from NPPlan.view.client.main.stages.results.dim2dose import *
from NPPlan.view.client.main.stages.results.report import *


class resultsController(baseController):
    @staticmethod
    def checkRequirements():
        return True
        pass

    def __init__(self, wxPanel, cParent, **kwargs):
        """

        @param wxPanel:
        @type wxPanel: NPPlan.view.client.main.stages.results.topPanel.resultsTopPanel
        @param cParent:
        @param kwargs:
        @return:
        """
        self._wxPanel = wxPanel
        self._cParent = cParent

    def start(self, *args, **kwargs):
        #spacer = wx.Panel(self._wxPanel, -1)
        #spacer.SetBackgroundColour(wx.SystemSettings_GetColour(wx.SYS_COLOUR_3DFACE))
        self._reportView = reportView(self._wxPanel)
        self._wxPanel.addPage(self._reportView, _('Report'))

        self._dim2dView = dim2doseView(self._wxPanel, self)
        self._wxPanel.addPage(self._dim2dView, _('2D isodoses'))
        self._cDose = 0
        pass

    def nextDose(self):
        self._cDose += 1
        self.updateDoseWithImage()

    def prevDose(self):
        self._cDose -= 1
        self.updateDoseWithImage()

    def updateDoseWithImage(self):
        isolinesDir = 'C:\\NPPlan2\\Data\\4e2948e45157510aac000000\\Test2\\'
        NPPlan.currentPlan.planSetRawCurrentSelected(self._cDose)
        self.__dict__['reader'].SetFileName(NPPlan.currentPlan.planGetCurrentDicomImagePath())
        self.layerReader.SetFileName(isolinesDir+'_axis0_slice%d.png' %self._cDose)
        self.__dict__['reader'].Update()
        self.layerReader.Update()
        self.__dict__['_ren'].Render()
        self.__dict__['vtkInteractor'].Update()
        self.__dict__['vtkInteractor'].Render()

    def drawIsodoses(self, vtkWindow):
        #fromFile = r'C:\NPPlan2\Data\4e1fccae51575108e8000000\Result\mcnplanm'
        #toDir = r'C:\NPPlan2\Data\4e1fccae51575108e8000000\Isolines2'

        self.__dict__['vtkInteractor'] = vtkWindow
        isolinesDir = 'C:\\NPPlan2\\Data\\4e2948e45157510aac000000\\Test2\\'
        self.__dict__['reader'] = vtk.vtkDICOMImageReader()
        self.__dict__['reader'].SetFileName(NPPlan.currentPlan.planGetFirstDicomImage())
        self.__dict__['reader'].Update()
        self.__dict__['im'] = vtk.vtkImageMapToWindowLevelColors()
        self.__dict__['im'].SetWindow(float(NPPlan.config.appConfig.client.defaultwindow))
        self.__dict__['im'].SetLevel(float(NPPlan.config.appConfig.client.defaultlevel))
        self.__dict__['im'].SetInput(self.__dict__['reader'].GetOutput())

        self.layerReader = vtk.vtkPNGReader()
        self.layerReader.SetFileName(isolinesDir+'_axis0_slice0.png')

        atextLayer = vtk.vtkTexture()
        atextLayer.SetInputConnection(self.layerReader.GetOutputPort())

        planeLayer = vtk.vtkPlaneSource()

        _transform = vtk.vtkTransform()
        _transform.Translate(-0.107, -0.07, 0.05)
        _transform.Scale(2, 2, 1.2)
        _transformF = vtk.vtkTransformPolyDataFilter()
        _transformF.SetInputConnection(planeLayer.GetOutputPort())
        _transformF.SetTransform(_transform)

        planeMapperLayer = vtk.vtkPolyDataMapper()

        _beamNormals = vtk.vtkPolyDataNormals()
        #self._beamNormals.SetInput(transformF.GetOutput())
        _beamNormals.SetInput(_transformF.GetOutput())

        #planeMapperLayer.SetInputConnection(planeLayer.GetOutputPort())
        planeMapperLayer.SetInputConnection(_beamNormals.GetOutputPort())
        planeMapperLayer.SetScalarModeToUsePointData()
        planeMapperLayer.SetColorModeToMapScalars()
        planeActorLayer = vtk.vtkActor()
        planeActorLayer.SetMapper(planeMapperLayer)
        planeActorLayer.SetTexture(atextLayer)

        atext = vtk.vtkTexture()
        atext.SetInputConnection(self.__dict__['im'].GetOutputPort())
        plane = vtk.vtkPlaneSource()
        planeMapper = vtk.vtkPolyDataMapper()
        planeMapper.SetInputConnection(plane.GetOutputPort())
        planeMapper.SetScalarModeToUsePointData()
        planeMapper.SetColorModeToMapScalars()
        planeActor = vtk.vtkActor()
        planeActor.SetMapper(planeMapper)
        planeActor.SetTexture(atext)
        self.__dict__['_ren'] = vtk.vtkRenderer()

        txt = vtk.vtkTextActor()
        txt.SetInput('test')
        txt.SetTextScaleModeToViewport()
        txtprop = txt.GetTextProperty()
        #txtprop.SetFontFamilyToArial()
        #txtprop.SetFontSize(18)
        txtprop.SetVerticalJustification(5)
        print 'just', txtprop.GetVerticalJustificationAsString()
        #txtprop.BoldOn()
        txtprop.SetColor((1, 0, 0))
        #txtMapper = vtk.vtkPolyDataMapper2D()
        #txt.SetMapper(txtMapper)
        txt.SetDisplayPosition(3, 3)

        #self.__dict__['_ren'].AddActor(txt)
        self.__dict__['_ren'].AddActor(planeActor)
        self.__dict__['_ren'].AddActor(planeActorLayer)
        self.__dict__['_ren'].SetBackground(1, 1, 1)

        self.__dict__['vtkInteractor'].AddObserver("LeftButtonPressEvent", self.eventLeftButtonPress)
        self.__dict__['vtkInteractor'].AddObserver("LeftButtonReleaseEvent", self.eventLeftButtonRelease)



        self.__dict__['vtkInteractor'].GetRenderWindow().AddRenderer(self.__dict__['_ren'])
        self.__dict__['vtkInteractor'].Start()

        self.__dict__['_ren'].ResetCamera()

    def eventLeftButtonPress(self, *args, **kwargs):
        pass

    def eventLeftButtonRelease(self, *args, **kwargs):
        pass
    pass