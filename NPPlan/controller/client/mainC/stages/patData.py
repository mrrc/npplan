# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 15.10.12
@summary: 
'''

import NPPlan
from NPPlan.model.file.configReader.addPatientForm import readConfig, getData

import wx

_panel = None
def init(panel):
    global _panel
    _panel = panel
    NPPlan.setRunningConfig('curPlan', {})
    NPPlan.setRunningConfig('listPlans', [])
    readConfig('addPatient.personal')
    #print getData('addPatient.personal')
    fieldData = getData('addPatient.personal')
    _panel.setFields('personal', fieldData)
    for i in fieldData:
        if not 'caption' == i:
            # @todo: buttons backends
            pass
        pass

    readConfig('addPatient.medical')
    fieldData = getData('addPatient.medical')
    _panel.setFields('medical', fieldData)
    #_panel.Bind(wx.EVT_BUTTON, self.OnClick, b))
    initPlanList(_panel.getPlanPanel().plansList)
    doBindings(_panel.getPlanPanel())

    restore()
    print 'patData controller'

def doBindings(panel):
    '''
    Инициализатор для NPPlan.view.client.main.stages.patient.plan
    '''
    #panel.collect()
    panel.Bind(wx.EVT_BUTTON, addPlanButton, panel.addPlanBtn)
    panel.Bind(wx.EVT_LIST_ITEM_SELECTED, planItemSelected, panel.plansList)
    panel.Bind(wx.EVT_LIST_ITEM_DESELECTED, planItemDeselected, panel.plansList)
    panel.Bind(wx.EVT_BUTTON, addStudyButton, panel.addStudyBtn)
    pass

def addStudyButton(event):
    from NPPlan.controller.client.mainC.stages.study import addStudy
    addStudy.addStudy(_panel)




def addPlanData(data):
    #curPlan = NPPlan.getRunningConfig('curPlan')
    #curPlan.update(data)
    listPlans = NPPlan.getRunningConfig('listPlans')
    listPlans.append(data)
    #NPPlan.setRunningConfig('curPlan', curPlan)
    NPPlan.setRunningConfig('listPlans', listPlans)

def addPlanButton(event):
    global _panel
    print _panel.getPlanPanel().collect()
    curItems = _panel.getPlanPanel().collect()
    addPlanData(curItems)
    _panel.getPlanPanel().addListItem([
        curItems['plan.name'],
        'doctor', #curItems[''],
        curItems['plan.type'],
        curItems['plan.status'],
        curItems['plan.startDate'],
        curItems['plan.treatmentStartDate'],
        curItems['plan.treatmentStopDate'],
        curItems['plan.studies'],
        str(curItems['plan.notes'])[0:50]+'...',
    ])
    pass

def initPlanList(list):
    # @todo: read from xml config
    list.InsertColumn(0, _('Plan name'))
    list.InsertColumn(1, _('Plan main doctor'))
    list.InsertColumn(2, _('Plan type'))
    list.InsertColumn(3, _('Plan status'))
    list.InsertColumn(4, _('Plan created date'))
    list.InsertColumn(5, _('Treatment date start'))
    list.InsertColumn(6, _('Treatment date end'))
    list.InsertColumn(7, _('Studies quantity'))
    list.InsertColumn(8, _('Notes'))

def planItemSelected(event):
    print event.m_itemIndex
    _panel.getPlanPanel().addStudyBtn.Enable(1)
    curPlan = NPPlan.getRunningConfig('listPlans')[event.m_itemIndex]
    print NPPlan.getRunningConfig('listPlans')[event.m_itemIndex]
    NPPlan.setRunningConfig('curPlan', curPlan)
    #NPPlan.setRunningConfig('curPlan', NPPlan.getRunningConfig())
    pass

def planItemDeselected(event):
    _panel.getPlanPanel().addStudyBtn.Enable(0)
    pass

def populatePlanList(list):
    pass

def restore():
    # @todo: use db abstract

    # restore plan list
    populatePlanList(_panel.plan.plansList)
    #print NPPlan.getRunningConfig()
    #print NPPlan.getRunningConfig('patientStage')
    #print NPPlan.getRunningConfig('patientStage.type')

def click_testbtn(event):
    pass