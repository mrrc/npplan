# -*- coding: utf8 -*-
'''
Набор классов для оконтуривания

contourPoint - описание единичной точки любого вида контура

pointContainer - описание контейнера точек для поточечного режима оконтуривание

solidContourContainer - описание контейнера для непрерывного режима оконтуривания

vtkHandler - основной хендлер центрального окна оконтуривания,
срез для оконтуривания берётся из текущего плана (self.image = NPPlan.currentPlan.planGetCurrentDicomImagePath()),
window/level управляется из contouring.main (из ribbon или из rightPanel),
управляет рулеткой родительского wx-окна (NPPlan.view.client.stages.contouring.renderWindow.renderWindow)

@see: NPPlan.controller.client.mainC.stages.contouring.main,
 NPPlan.controller.client.mainC.stages.contouring.rightPanel,
 NPPlan.view.client.stages.contouring.*
@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 3
@date: 04.09.2013
@summary: набор классов для оконтуривания (конечное окно)
'''

import NPPlan
import vtk
import numpy as np
from NPPlan.lib.melkmanSort import isLeft as isPointAtLeft
import wx
import math

# contour styles
NPPLAN_CONTOURING_POINT = 0b00000001                # точечное оконтуривание
NPPLAN_CONTOURING_SOLID = 0b00000010                # непрерывное оконтуривание
NPPLAN_CONTOURING_POLYGON = 0b00000100              # полигональное оконтуривание

# Список соответствий action -> cursor
actions = {
    'zoom': wx.CURSOR_SIZEWE,
    'pan': wx.CURSOR_SIZING,
    'work': wx.CURSOR_PENCIL,
    'default': wx.CURSOR_DEFAULT,
    'select': wx.CURSOR_HAND,
    'polygon': wx.CURSOR_PENCIL,
}


class contourPoint(object):
    """
    Базовый класс точек контура
    """
    def __init__(self, parent, eventCoordinates=[], worldCoordinates=[], id='', color=(1.0, 0.0, 0.0), **kwargs):
        """
        @param parent: родительский контроллер
        @type parent: vtkHandler
        @param eventCoordinates: координаты события точки
        @type eventCoordinates: list
        @param worldCoordinates: мировые координаты точки
        @type worldCoordinates: list
        @param id: id точки
        @type id: str
        @param color: цвет точки (R,G,B), где R,G,B от 0 до 1
        @type color: tuple
        @keyword size: размер точки (в масштабе мировых координат)
        @type size: int
        """
        self._parent = parent
        self._eCoord = eventCoordinates
        self._wCoord = worldCoordinates
        self._id = id
        self._vtkId = None
        self._points = None
        self._color = color
        self._vtkActor = None
        self._size = kwargs.get('size', 5)

    def createVtkRepresentation(self):
        """
        Создаёт vtk-представление данной точки
        """
        # @todo: refactor this
        # @todo: убрать нафик vtk-контейнер точки, так как есть контейнер верхнего уровня
        print '\ncontour point currentThumbName: ', self.currentThumbName

        self._points = points = vtk.vtkPoints()
        p = self._wCoord[0], self._wCoord[1], 0

        vertices = vtk.vtkCellArray()

        self._vtkId = id = points.InsertNextPoint(p)
        vertices.InsertNextCell(1)
        vertices.InsertCellPoint(id)

        point = vtk.vtkPolyData()

        point.SetPoints(points)
        point.SetVerts(vertices)

        mapper = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            mapper.SetInput(point)
        else:
            mapper.SetInputData(point)

        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetPointSize(self._size)
        actor.GetProperty().SetColor(*self._color)
        self._vtkActor = actor

    def setPointSize(self, newSize):
        """
        Устанавливает размер уже созданной точки
        @param newSize: новый размер
        @type newSize: int
        """
        self._vtkActor.GetProperty().SetPointSize(newSize)

    def setPointColor(self, newColor, g=None, b=None):
        """
        Устанавливает цвет, передаётся tuple (R,G,B) или 3 параметра R,G,B
        @param newColor: цвет, tuple(R,G,B) или float R - красная компонента
        @type newColor: float or tuple
        @param g: зелёная компонента
        @type g: float
        @param b: синяя компонента
        @type b: float
        """
        if g is not None and b is not None:
            newColor = (newColor, g, b)
        self._color = newColor
        self._vtkActor.GetProperty().SetColor(*newColor)

    def getVtkActor(self):
        """
        Возвращает vtkActor'а точки, создаёт если его нет
        @rtype: vtk.vtkActor()
        """
        if self._vtkActor is None:
            self.createVtkRepresentation()
        return self._vtkActor

    def getId(self):
        return self._id

    def getVtkId(self):
        return self._vtkId

    def getCoords(self):
        return self._points.GetPoint(self.getVtkId())

    def getWCoord(self):
        return self._wCoord

    def __repr__(self):
        """
        Строковое представление точки, неявно вызывается при print объекта
        """
        s = 'Point: %d, world coordinates: %1.4f %1.4f %1.4f' % (self._id, self._wCoord[0], self._wCoord[1],
                                                                 self._wCoord[2])
        return s


class pointContainer(object):
    """
    Контейнер точек для поточечного режима
    """

    sliceDictonary = {}
    currentNumberOfPoints = 0
    currentThumbName = None

    def __init__(self, parent):
        """

        @param parent:
        @type parent: vtkHandler
        @return:
        """
        self._parent = parent
        self._points = {}  # vtkId -> pointData
        self._vtkPoints = vtk.vtkPoints()
        self._vtkPoints.DebugOn()

    def addPoint(self, pointData):
        """
        @todo: добавить параметр с номером среза, хранить точки для каждого среза
        @todo: добавить id органа/контура, хранить точки для каждого из органов
        @type pointData: contourPoint
        """

        #savefile = './NPPlan/controller/client/mainC/stages/contouring/thumbname'
        #f = open(savefile, 'rb')
        #currentThumbName = f.read()
        currentThumbName = str(NPPlan.currentPlan.planGetCurrentDicomImagePath())
        self.currentThumbName = currentThumbName
        contourPoint.currentThumbName = currentThumbName

        print '\n', currentThumbName, '\n'
        #f.close
        # просто вставляем точки по порядку
        self._vtkPoints.InsertNextPoint(pointData._wCoord[0], pointData._wCoord[1], 0)
        #self._points[int(_id)] = pointData
        print '\n Point Data:', pointData, '\n'
        pointDataString = str(pointData)
        pointDataWordList = pointDataString.split()
        pointNumber = int(pointDataWordList[1][:-1])
        pointCoordinates = (float(pointDataWordList[4]), float(pointDataWordList[5]), float(pointDataWordList[6]))
        pointDataList = [pointNumber, pointCoordinates]

        if self.sliceDictonary.has_key(currentThumbName) == True:
            _id = len(self.sliceDictonary[currentThumbName])
            #self._points[int(_id)] = pointData
            #print '\n Point Data:', pointData, '\n'
            self.sliceDictonary[currentThumbName].append(pointDataList)
        else:
            _id = 0
            #self._points[int(_id)] = pointData
            #print '\n Point Data:', pointData, '\n'
            self.sliceDictonary[currentThumbName] = [pointDataList]

        print 'id: ', _id #, '\nraw', self._points

        """if self._vtkPoints.GetNumberOfPoints() < 3:
            # просто вставляем точки по порядку
            _id = self._vtkPoints.InsertNextPoint(pointData._wCoord[0], pointData._wCoord[1], 0)
            self._points[int(_id)] = pointData
            print '\n Point Data:', pointData, '\n'

            if self.sliceDictonary.has_key(currentThumbName) == True:
                self.sliceDictonary[currentThumbName].append(pointData)
            else:
                self.sliceDictonary[currentThumbName] = [pointData]

            print 'id: ', _id, '\nraw', self._points
        else:
            # добавляем точку в конец
            print 'old points', self._points
            _id = len(self._points)
            print 'id: ', _id
            self._points[int(_id)] = pointData
            if self.sliceDictonary.has_key(currentThumbName)== True:
                self.sliceDictonary[currentThumbName].append(pointData)
            else:
                self.sliceDictonary[currentThumbName] = [pointData]
            print 'old points with new', self._points
            # находим правую нижнюю точку
            stack = []
            bottomY = 1.0
            indexBottomY = []
            for i in self._points:
                point = self._points[i]
                if point._wCoord[1] <= bottomY:
                    if point._wCoord[1] < bottomY:
                        bottomY = point._wCoord[1]
                        indexBottomY = [i]
                    else:
                        indexBottomY.append(i)
            print bottomY, indexBottomY
            if len(indexBottomY) != 1:
                rightX = -1.0
                indexRightX = -1
                for i in indexBottomY:
                    if self._points[i]._wCoord[0] > rightX:
                        rightX = self._points[i]._wCoord[0]
                        indexRightX = i
                indexBottomY = indexRightX
            else:
                indexBottomY = indexBottomY[0]
            stack.append(self._points[indexBottomY])
            # сортируем точки по углу от oX
            print 'sorted points'
            for i in sorted(self._points, key=lambda x:
                np.arctan2(self._points[x]._wCoord[1] - self._points[indexBottomY]._wCoord[1],
                           self._points[x]._wCoord[0] - self._points[indexBottomY]._wCoord[0]),
                       ):
                try:
                    print i, self._points[i]
                except ZeroDivisionError:
                    print i, self._points[i], 'start point'
                stack.append(self._points[i])
                pass
            # создаём новый контейнер vtkPoints
            #self._points = stack
            del self._vtkPoints
            del self._points
            self._vtkPoints = vtk.vtkPoints()
            self._points = {}
            for i in stack:
                _id = self._vtkPoints.InsertNextPoint(i._wCoord[0], i._wCoord[1], 0)
                self._points[int(_id)] = i
            print 'new points', self._points"""
        self._id = _id

        print '\n Slice Dict: ', self.sliceDictonary, '\n'
        pass

    def getLength(self):
        """
        @todo: использовать номера срезов/координаты
        Возвращаем число точек
        @return: число точек
        @rtype: int
        """
        currentThumbName = self.currentThumbName #str(NPPlan.currentPlan.planGetCurrentDicomImagePath())
        numberOfPoints = len(self.sliceDictonary[currentThumbName])
        print '\n number of points: ', numberOfPoints, '\n'
        #print self._vtkPoints.GetNumberOfPoints(), '\n'
        #return self._vtkPoints.GetNumberOfPoints()
        self.currentNumberOfPoints = numberOfPoints
        return numberOfPoints

    def getVtkPoints(self):
        """
        Возвращаем набор точек
        @return: набор точек
        @rtype: vtk.vtkPoints()
        """
#        print self._vtkPoints
        currentThumbName = self.currentThumbName #str(NPPlan.currentPlan.planGetCurrentDicomImagePath())
        self._vtkPoints.SetNumberOfPoints(self.currentNumberOfPoints)
        for i in range(self.currentNumberOfPoints):
            coord = self.sliceDictonary[currentThumbName][i][1]
            #print '\n\n\n coord: ', coord
            self._vtkPoints.SetPoint(i, coord)
        print '\n vtk Points: ', self._vtkPoints, '\n'
        return self._vtkPoints


class solidContourContainer(object):
    """
    Класс-контейнер сплошных контуров
    """
    def __init__(self, parent):
        """
        @type parent: vtkHandler
        """
        self._vtkHandler = parent
        self._data = {}

    def startNewContour(self, organ):
        """
        Создаёт новый контур
        @param organ: id контура/органа
        @type organ: str
        @return:
        """
        print 'started new contour for', organ
        self._data[organ] = {
            'raw': [],   # исходные точки, [(x0, y0), (x1, y1), ...] в мировой системе координат
            'simplified': [], # упрощённые точки
            'pd3': None, # третья координата точки [z0, z1, ...] - можно использовать для переключения по срезам
        }

    def addRawPoint(self, pointData, organ=None):
        """
        Добавляет точку
        @param pointData: данные точки (xi, yi, zi)
        @type pointData: tuple
        @param organ: id контура/органа
        @type organ: str
        @return:
        """
        self._data[organ]['raw'].append((pointData[0], pointData[1]))
        self._data[organ]['pd3'] = pointData[2]
        pass

    def endContour(self, organ):
        """
        Когда контур завершён, то вызываем упрощение
        @param organ: id контура/органа
        @type organ: str
        @return:
        """
        print 'ended contour for', organ
        print 'raw data', self._data
        self._data[organ]['simplified'] = self.simplifyPoints(self._data[organ]['raw'], 0.01)
        del self._data[organ]['raw']
        print 'new data', self._data
        pass

    def simplifyPoints(self, pts, tolerance):
        """
        Упрощение полигона
        pure-Python Douglas-Peucker line simplification/generalization
        \n\n
        this code was written by Schuyler Erle <schuyler@nocat.net> and is
        made available in the public domain.
        \n\n
        the code was ported from a freely-licensed example at
        http://www.3dsoftware.com/Cartography/Programming/PolyLineReduction/
        \n\n
        the original page is no longer available, but is mirrored at
        http://www.mappinghacks.com/code/PolyLineReduction/

        """
        anchor  = 0
        floater = len(pts) - 1
        stack   = []
        keep    = set()

        stack.append((anchor, floater))
        while stack:
            anchor, floater = stack.pop()

            # initialize line segment
            if pts[floater] != pts[anchor]:
                anchorX = float(pts[floater][0] - pts[anchor][0])
                anchorY = float(pts[floater][1] - pts[anchor][1])
                seg_len = math.sqrt(anchorX ** 2 + anchorY ** 2)
                # get the unit vector
                anchorX /= seg_len
                anchorY /= seg_len
            else:
                anchorX = anchorY = seg_len = 0.0

            # inner loop:
            max_dist = 0.0
            farthest = anchor + 1
            for i in range(anchor + 1, floater):
                dist_to_seg = 0.0
                # compare to anchor
                vecX = float(pts[i][0] - pts[anchor][0])
                vecY = float(pts[i][1] - pts[anchor][1])
                seg_len = math.sqrt( vecX ** 2 + vecY ** 2 )
                # dot product:
                proj = vecX * anchorX + vecY * anchorY
                if proj < 0.0:
                    dist_to_seg = seg_len
                else:
                    # compare to floater
                    vecX = float(pts[i][0] - pts[floater][0])
                    vecY = float(pts[i][1] - pts[floater][1])
                    seg_len = math.sqrt( vecX ** 2 + vecY ** 2 )
                    # dot product:
                    proj = vecX * (-anchorX) + vecY * (-anchorY)
                    if proj < 0.0:
                        dist_to_seg = seg_len
                    else:  # calculate perpendicular distance to line (pythagorean theorem):
                        dist_to_seg = math.sqrt(abs(seg_len ** 2 - proj ** 2))
                    if max_dist < dist_to_seg:
                        max_dist = dist_to_seg
                        farthest = i

            if max_dist <= tolerance: # use line segment
                keep.add(anchor)
                keep.add(floater)
            else:
                stack.append((anchor, farthest))
                stack.append((farthest, floater))

        keep = list(keep)
        keep.sort()
        return [pts[i] for i in keep]

    def getData(self, organ):
        """
        Возвращает данные по контуру
        @param organ: id органа/контура
        @type organ: str
        @return: {'raw', 'simplified', 'pd3'}
        @rtype: dict
        """
        return self._data[organ]


class vtkHandler(object):
    """
    Контроллер вывода DICOM-изображения.
    """
    def __init__(self, wxParent, vtkInteractor, **kwargs):
        """
        @param wxParent: родительское окно wx
        @type wxParent: renderWindow(wx.Panel)
        @param vtkInteractor: интерактор vtk-wx
        @type vtkInteractor: wxVTKRenderWindowInteractor
        @keyword rulers: полоски масштаба для окна, {top, left}
        @type rulers: dict
        """
        self.__dict__['wxParent'] = wxParent
        self.__dict__['vtkInteractor'] = vtkInteractor
        self.__dict__['rightPressed'] = False
        self.__dict__['leftPressed'] = False
        self.__dict__['rulers'] = kwargs.get('rulersCtrls', None)

        self.__dict__['vtkInteractor'].SetInteractorStyle(None)
        self.__dict__['vtkInteractor'].Enable(1)

        self.__dict__['cstyle'] = kwargs.get('cstyle', NPPLAN_CONTOURING_POINT)
        self.__dict__['selection'] = None
        self.__dict__['currentCursor'] = None
        self.__dict__['solidContours'] = solidContourContainer(self)
        self.__dict__['solidPointsToRemoval'] = []

        self.__dict__['reader'] = vtk.vtkDICOMImageReader()
        self.__dict__['reader'].SetFileName(NPPlan.currentPlan.planGetFirstDicomImage())
        self.__dict__['reader'].Update()
        self.__dict__['im'] = vtk.vtkImageMapToWindowLevelColors()
        self.__dict__['im'].SetWindow(float(NPPlan.config.appConfig.client.defaultwindow))
        self.__dict__['im'].SetLevel(float(NPPlan.config.appConfig.client.defaultlevel))
        self.__dict__['im'].SetInput(self.__dict__['reader'].GetOutput())

        self._cPid = 0
        self.__dict__['cPoints'] = {}
        self.__dict__['pointContainer'] = pointContainer(self)
        self.__dict__['cachePixelSpacing'] = NPPlan.currentPlan.planGetPixelSpacing()

        atext = vtk.vtkTexture()
        atext.SetInputConnection(self.__dict__['im'].GetOutputPort())
        plane = vtk.vtkPlaneSource()
        planeMapper = vtk.vtkPolyDataMapper()
        planeMapper.SetInputConnection(plane.GetOutputPort())
        planeMapper.SetScalarModeToUsePointData()
        planeMapper.SetColorModeToMapScalars()
        planeActor = vtk.vtkActor()
        planeActor.SetMapper(planeMapper)
        planeActor.SetTexture(atext)
        self.__dict__['_ren'] = vtk.vtkRenderer()

        txt = vtk.vtkTextActor()
        txt.SetInput('test')
        txt.SetTextScaleModeToViewport()
        txtprop = txt.GetTextProperty()
        #txtprop.SetFontFamilyToArial()
        #txtprop.SetFontSize(18)
        txtprop.SetVerticalJustification(5)
        print 'just', txtprop.GetVerticalJustificationAsString()
        #txtprop.BoldOn()
        txtprop.SetColor((1, 0, 0))
        #txtMapper = vtk.vtkPolyDataMapper2D()
        #txt.SetMapper(txtMapper)
        txt.SetDisplayPosition(3, 3)

        self.__dict__['vtkInteractor'].AddObserver("LeftButtonPressEvent", self.eventLeftButtonPress)
        self.__dict__['vtkInteractor'].AddObserver("LeftButtonReleaseEvent", self.eventLeftButtonRelease)
        #self.__dict__['vtkInteractor'].AddObserver("RightButtonPressEvent", self.eventRightButtonPress)
        #self.__dict__['vtkInteractor'].AddObserver("RightButtonReleaseEvent", self.eventRightButtonRelease)
        self.__dict__['vtkInteractor'].AddObserver("MiddleButtonReleaseEvent", self.eventMiddleButtonRelease)
        self.__dict__['vtkInteractor'].AddObserver("MouseMoveEvent", self.eventMouseMove)

        #self.__dict__['_ren'].AddActor(txt)
        self.__dict__['_ren'].AddActor(planeActor)
        self.__dict__['_ren'].SetBackground(1, 1, 1)

        self.draw1CmAxis()

        self.__dict__['vtkInteractor'].GetRenderWindow().AddRenderer(self.__dict__['_ren'])
        self.__dict__['vtkInteractor'].Start()

        self.__dict__['_ren'].ResetCamera()
        cam1 = self.__dict__['_ren'].GetActiveCamera()
        #cam1.Zoom(2)
        #cam1.Elevation(-30)
        #cam1.Roll(-20)
        print cam1.GetPosition()
        self.__dict__['cam'] = {
            'original': {
                'position': cam1.GetPosition(),
                'focalPoint': cam1.GetFocalPoint(),
            },
            'current': {
                'position': cam1.GetPosition(),
                'focalPoint': cam1.GetFocalPoint(),
            },
            'moveFactor': 0.001
        }
        print cam1
        print 'cam position'
        #cam1.SetParallelProjection(1)
        #cam1.SetPosition(0.2, 0.02, cam1.GetPosition()[2]) # wrong or set parallel projection
        #cam1.SetFocalPoint(0.2, 0.02, 0)
        #self.moveView((0.2, 0.02))
        self.__dict__['_ren'].ResetCameraClippingRange()

        print 'at init'
        a, b = self.__dict__['vtkInteractor'].GetSize()
        print '0:0', self.getWorldCoordinatesByEventPosition(0, 0)
        print 'a:0', a, self.getWorldCoordinatesByEventPosition(a-1, 0)
        print '0:b', self.getWorldCoordinatesByEventPosition(0, b)
        print 'a:b', self.getWorldCoordinatesByEventPosition(a, b)

    def setCurrentAction(self, mode='work', **kwargs):
        """
        Устанавливаем текущее действие
        @param mode:
        @param kwargs:
        @return:
        """
        self.__dict__['action'] = mode
        pass

    def setSelectionData(self, key='', contourData={}, **kwargs):
        print 'handling contourData', key, contourData
        # @todo:
        # 1. set current data key (handle slice number), create new arrays if needed
        # 2. set current polygon filling options (color, opacity)
        pass

    def moveView(self, newPosition, y=None):
        """
        Перемещает изображение в новую точку
        @param newPosition: новая позиция, (x,y) или x
        @type newPosition: int or tuple
        @param y: новая позиция y
        @return:
        """
        if not isinstance(newPosition, tuple) and y is not None:
            newPosition = (newPosition, y)
        cam1 = self.__dict__['_ren'].GetActiveCamera()
        newX = self.__dict__['cam']['current']['position'][0] + newPosition[0]
        newY = self.__dict__['cam']['current']['position'][1] + newPosition[1]
        cam1.SetPosition(newX, newY, self.__dict__['cam']['original']['position'][2])
        cam1.SetFocalPoint(newX, newY, self.__dict__['cam']['original']['focalPoint'][2])
        self.__dict__['cam']['current']['position'] = cam1.GetPosition()
        self.__dict__['cam']['current']['focalPoint'] = cam1.GetFocalPoint()
        self.__dict__['_ren'].ResetCameraClippingRange()

    def draw1CmAxis(self):
        """
        Вычисляет отрезок 1см на основе данных DICOM, рисует отрезок
        Возможно неправильно
        @return:
        """
        # @todo: check the calculations

        # получаем pixelSpacing открытого плана
        pixelSpacing = NPPlan.currentPlan.planGetPixelSpacing()

        # устанавливаем параметры текста
        textProperty = vtk.vtkTextProperty()
        textProperty.SetBold(1)
        textProperty.SetItalic(0)
        textProperty.SetShadow(0)
        textProperty.SetColor(0, 0, 0)
        textProperty.SetFontSize(2)  # not working

        # Создаём отрезок X
        axisX = vtk.vtkAxisActor2D()
        axisX.SetTitle('1cm')
        axisX.SetTitleTextProperty(textProperty)
        axisX.SetFontFactor(0.4)
        axisX.SetPosition(0, 0.1)
        axisX.SetPosition2(1, 0.1)
        axisX.GetProperty().SetColor(0, 0, 0)
        axisX.GetProperty().SetLineWidth(1.0)
        axisX.SetLabelVisibility(0)
        axisX.SetTickLength(2)
        axisX.SetAdjustLabels(0)
        #axisX.SetNumberOfMinorTicks(15)
        axisX.GetProperty().SetDisplayLocationToForeground()

        # Устанавливаем координаты в мировой системе, точка начала отрезка
        aXc0 = axisX.GetPositionCoordinate()
        aXc0.SetCoordinateSystemToWorld()
        aXc0.SetValue(-0.5, -0.51)

        # Устанавливаем координаты в мировой системе, точка конца отрезка
        aXc1 = axisX.GetPosition2Coordinate()
        aXc1.SetCoordinateSystemToWorld()
        aXc1.SetValue(-0.5 + 10 / (pixelSpacing[0] * 512), -0.51)

        # Добавляем вывод отрезка в renderer окна
        self.__dict__['_ren'].AddActor2D(axisX)

        # Создаём отрезок Y
        axisY = vtk.vtkAxisActor2D()
        axisY.SetPosition(0, 0.1)
        axisY.SetPosition2(1, 0.1)
        axisY.GetProperty().SetColor(0, 0, 0)
        axisY.GetProperty().SetLineWidth(1.0)
        axisY.SetLabelVisibility(0)
        axisY.SetTickLength(2)
        axisY.SetAdjustLabels(0)
        #axisX.SetNumberOfMinorTicks(15)
        axisX.GetProperty().SetDisplayLocationToForeground()

        # Задаём начало и конец отрезка Y в мировой системе
        aYc0 = axisY.GetPositionCoordinate()
        aYc0.SetCoordinateSystemToWorld()
        # @todo: 512 -> dicom[0x0028, 0x0010/0x0011]
        aYc0.SetValue(-0.51, -0.5 + 10 / (pixelSpacing[1] * 512))

        aYc1 = axisY.GetPosition2Coordinate()
        aYc1.SetCoordinateSystemToWorld()
        aYc1.SetValue(-0.51, -0.5)

        # Добавляем вывод отрезка в renderer окна
        self.__dict__['_ren'].AddActor2D(axisY)

    def backendOnPaintEvent(self, *args, **kwargs):
        """
        Функция, вызываемая из события OnPaint wx-окна.
        Отладочный вывод, вызывает установку рулеток верхнего wx-окна,
        заполняет self.__dict__['sizeBounds'] и self.__dict__['screenBounds'] параметрами окна взаимодействия
        (vtkInteractor) и отображаемого рисунка в мировой системе координат
        Вызываем
        @param args:
        @param kwargs:
        @keyword wxSize: размеры окна wx
        @type wxSize: tuple
        @return:
        """
        print 'onPaint at vtkHandler'
        print kwargs.get('wxSize')
        print 'ruler handled at vtkHandler', self.__dict__['rulers']
        print 'interactor size', self.__dict__['vtkInteractor'].GetSize()
        a, b = self.__dict__['vtkInteractor'].GetSize()
        print 'wxParent size', self.__dict__['wxParent'].GetSize(), self.__dict__['wxParent'].GetSizeTuple(), self.__dict__['wxParent'].GetClientSize()
        #self.__dict__['rulers']['top'].SetRange(0, self.__dict__['vtkInteractor'].GetSize()[0])

        print '0:0', self.getWorldCoordinatesByEventPosition(0, 0)
        print 'a:0', a, self.getWorldCoordinatesByEventPosition(a-1, 0)
        print '0:b', self.getWorldCoordinatesByEventPosition(0, b)
        print 'a:b', self.getWorldCoordinatesByEventPosition(a, b)

        self.__dict__['sizeBounds'] = self.__dict__['vtkInteractor'].GetSize()
        print 'initial screenBounds', self.getCurrentScreenBounds()
        self.__dict__['screenBounds'] = self.getCurrentScreenBounds()
        self.setRulersBounds()

    def setRulersBounds(self):
        """
        Устанавливает рулетки в родительском wx-окне
        @return:
        """
        self.__dict__['rulers']['top'].SetRange(
            10 * self.__dict__['screenBounds']['topLeft'][0] / self.__dict__['cachePixelSpacing'][0],
            10 * self.__dict__['screenBounds']['topRight'][0] / self.__dict__['cachePixelSpacing'][0],
        )

        self.__dict__['rulers']['left'].SetRange(
            10 * self.__dict__['screenBounds']['topLeft'][1] / self.__dict__['cachePixelSpacing'][1],
            10 * self.__dict__['screenBounds']['bottomLeft'][1] / self.__dict__['cachePixelSpacing'][1],
        )

        pass

    def getCurrentScreenBounds(self):
        """
        Вычисляет мировые координаты текущего состояния окна снимка на основе self.__dict__['sizeBounds']
        @return: {topLeft, topRight, bottomLeft,bottomRight}
        @rtype: dict
        """
        tl = vtk.vtkCoordinate()
        tr = vtk.vtkCoordinate()
        bl = vtk.vtkCoordinate()
        br = vtk.vtkCoordinate()
        for i in [tl, tr, bl, tr]:
            i.SetCoordinateSystemToDisplay()
        tl.SetValue(0, self.__dict__['sizeBounds'][1], 0)
        tr.SetValue(self.__dict__['sizeBounds'][0], self.__dict__['sizeBounds'][1], 0)
        bl.SetValue(0, 0, 0)
        br.SetValue(self.__dict__['sizeBounds'][0], 0, 0)
        #return tl.GetComputedWorldValue(self.__dict__['_ren']), tr.GetComputedWorldValue(self.__dict__['_ren']), \
        #       bl.GetComputedWorldValue(self.__dict__['_ren']), br.GetComputedWorldValue(self.__dict__['_ren'])
        return {
            'topLeft': tl.GetComputedWorldValue(self.__dict__['_ren']),
            'topRight': tr.GetComputedWorldValue(self.__dict__['_ren']),
            'bottomLeft': bl.GetComputedWorldValue(self.__dict__['_ren']),
            'bottomRight': br.GetComputedWorldValue(self.__dict__['_ren']),
        }
        pass

    def getContourStyle(self):
        """
        Возвращает текущий стиль оконтуривания
        @return: Флаг, соответствующий выбранному стилю
        @rtype: int
        """
        return self.__dict__['cstyle']

    def setContourStyle(self, cstyle=NPPLAN_CONTOURING_POINT):
        """
        Устанавливает флаг
        @param cstyle: флаг
         NPPLAN_CONTOURING_POINT - поточечное оконтуривание с дополнением
         NPPLAN_CONTOURING_SOLID - непрерывный режим
         NPPLAN_CONTOURING_POLYGON - полигональный режим (как в фотошопе, not implemented yet)
        @type cstyle: int
        @return:
        """
        self.__dict__['cstyle'] = cstyle

    cstyle = property(getContourStyle, setContourStyle, doc="Setting contour style")

    def showPoint(self, coords, _id, color=(0.0, 1.0, 0.0)):
        """
        @deprecated
        @param coords:
        @param _id:
        @param color:
        @return:
        """
        points = vtk.vtkPoints()
        p = coords[0], coords[1], 0

        # Create the topology of the point (a vertex)
        vertices = vtk.vtkCellArray()

        id = points.InsertNextPoint(p)
        vertices.InsertNextCell(1)
        vertices.InsertCellPoint(id)

        # Create a polydata object
        point = vtk.vtkPolyData()

        # Set the points and vertices we created as the geometry and topology of the polydata
        point.SetPoints(points)
        point.SetVerts(vertices)

        # Visualize
        mapper = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            mapper.SetInput(point)
        else:
            mapper.SetInputData(point)

        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetPointSize(20)
        actor.GetProperty().SetColor(*color)

        #self._points[_id] = {'actor': actor, 'coords': coords}

        print self.__dict__['_ren'], self.__dict__['_ren'].GetActors()
        self.__dict__['_ren'].AddActor(actor)
        print self.__dict__['_ren'], self.__dict__['_ren'].GetActors()
        self.update()

    def getWorldCoordinatesByEventPosition(self, x, y=0):
        """
        Получает мировые координаты в точке события на экране
        @param x: x или (x,y) события
        @type x: int or tuple
        @param y: y события
        @return: (x, y, z), где z по идее должно быть всегда нулём, так как actor плоский
        @rtype: tuple
        """
        if isinstance(x, tuple) or isinstance(x, list):
            x, y = x
        coord = vtk.vtkCoordinate()
        coord.SetCoordinateSystemToDisplay()
        coord.SetValue(x, y, 0)

        return coord.GetComputedWorldValue(self.__dict__['_ren'])

    def eventLeftButtonPress(self, obj, event):
        print 'lpress', self.__dict__['vtkInteractor'].GetEventPosition()
        self.__dict__['leftPressed'] = True
        if 'work' == self.__dict__['action'] and 's' == self.getContourMode():
            self.__dict__['solidContours'].startNewContour('123')
        if self.__dict__['selection'] is None:
            return
        pass

    def eventRightButtonPress(self, obj, event):
        # @deprecated
        # @todo: make context menu with options for current action: contour, move, scale
        print 'rpress', self.__dict__['vtkInteractor'].GetEventPosition()
        self.__dict__['rightPressed'] = True
        #cursor = wx.StockCursor(wx.CURSOR_SIZEWE)
        cursor = wx.StockCursor(wx.CURSOR_SIZING)
        self.__dict__['wxParent'].SetCursor(cursor)
        # wx.CURSOR_SIZEWE
        pass

    def eventRightButtonRelease(self, obj, event):
        # @deprecated
        print 'rrelease', self.__dict__['vtkInteractor'].GetEventPosition()
        self.__dict__['rightPressed'] = False
        cursor = wx.StockCursor(wx.CURSOR_ARROW)
        self.__dict__['wxParent'].SetCursor(cursor)
        print 'screen bounds', self.getCurrentScreenBounds()
        # wx.CURSOR_ARROW
        pass

    def changeCursor(self):
        """
        Изменяет курсор на тот, который соответствует данному действию
        """
        if self.__dict__['currentCursor'] != actions[self.__dict__['action']]:
            cursor = wx.StockCursor(actions[self.__dict__['action']])
            self.__dict__['wxParent'].getVtkWindow().SetCursor(cursor)
            self.__dict__['currentCursor'] = actions[self.__dict__['action']]

    def eventMouseMove(self, obj, event):
        """
        Хендлер перемещения мышки
        @param obj: объект события
        @type obj: vtkobject
        @param event: название события
        @type event: str
        @return:
        """
        #print 'moveEvent', self.__dict__['vtkInteractor'].GetEventPosition()
        self.changeCursor()
        if self.__dict__['leftPressed']:
            # если нажата левая клавиша
            if 'zoom' == self.__dict__['action']:
                # если выбранное действие - zoom, то
                # получаем предущую и текущуюю координату, вычитаем
                # Zoom в долях от текущего
                lastXYpos = self.__dict__['vtkInteractor'].GetLastEventPosition()
                lastX = lastXYpos[0]
                xypos = self.__dict__['vtkInteractor'].GetEventPosition()
                x = xypos[0]
                if lastX - x < 0:
                    self.__dict__['_ren'].GetActiveCamera().Zoom(1.02)
                else:
                    self.__dict__['_ren'].GetActiveCamera().Zoom(0.98)
            elif 'pan' == self.__dict__['action']:
                # если выбранное действие - pan
                # то сдвигаем рисунок, вызываем self.moveView
                # от изменения положения по по x и y с коэффициентом
                lastXYpos = self.__dict__['vtkInteractor'].GetLastEventPosition()
                lastX = lastXYpos[0]
                lastY = lastXYpos[1]
                xypos = self.__dict__['vtkInteractor'].GetEventPosition()
                x = xypos[0]
                y = xypos[1]
                self.moveView(
                    (self.__dict__['cam']['moveFactor'] * (lastX - x), self.__dict__['cam']['moveFactor'] * (lastY - y))
                )
                pass
            elif 'work' == self.__dict__['action'] and 's' == self.getContourMode():
                # если текущее действие - work и режим оконтуривания s (сплошной)
                # то сохраняем координаты точки в контуре
                # и рисуем её на экране
                xypos = obj.GetEventPosition()
                xyrealpos = self.getWorldCoordinatesByEventPosition(*xypos)
                print 'xypos', xypos, xyrealpos
                self.__dict__['solidContours'].addRawPoint(xyrealpos, '123')

                cPoint = contourPoint(self, xypos, self.getWorldCoordinatesByEventPosition(*xypos), self.getNextPointId(),
                                      size=2)
                self.__dict__['solidPointsToRemoval'].append(cPoint)
                self.__dict__['_ren'].AddActor(cPoint.getVtkActor())
                pass
            pass
            self.update()
            self.__dict__['screenBounds'] = self.getCurrentScreenBounds()
            self.setRulersBounds()
        if self.__dict__['rightPressed']:
            # @deprecated
            # этот кусок не выполняется, так как нажатие правой клавиши перехватывается уровнем выше
            # и вызывает контекстное меню выбора действия
            print 'moveWithRight pressed'
            lastXYpos = self.__dict__['vtkInteractor'].GetLastEventPosition()
            lastX = lastXYpos[0]
            lastY = lastXYpos[1]
            xypos = self.__dict__['vtkInteractor'].GetEventPosition()
            x = xypos[0]
            y = xypos[1]
            # @todo: handle moving/sizing
            '''
            if lastX - x < 0:
                self.__dict__['_ren'].GetActiveCamera().Zoom(1.02)
            else:
                self.__dict__['_ren'].GetActiveCamera().Zoom(0.98)
                '''
            self.moveView(
                (self.__dict__['cam']['moveFactor'] * (lastX - x), self.__dict__['cam']['moveFactor'] * (lastY - y))
            )
            self.update()
            self.__dict__['screenBounds'] = self.getCurrentScreenBounds()
            self.setRulersBounds()
        pass

    def eventLeftButtonRelease(self, obj, event):
        self.__dict__['leftPressed'] = False
        if 'work' != self.__dict__['action']:
            return
        #if self.__dict__['selection'] is None:
        #    return
        print 'lrelease', self.__dict__['vtkInteractor'].GetEventPosition()
        if self.__dict__['rightPressed']:
            return
        xypos = obj.GetEventPosition()
        #if self.cstyle & NPPLAN_CONTOURING_POINT:
        if 'd' == self.getContourMode():
            cPoint = contourPoint(self, xypos, self.getWorldCoordinatesByEventPosition(*xypos), self.getNextPointId())
            self.appendPoint(cPoint)
            self.createPolygon()
        elif 'p' == self.getContourMode():
            pass
        elif 's' == self.getContourMode():
            self.__dict__['solidContours'].endContour('123')
            print self.__dict__['solidContours'].getData('123')
            for i in self.__dict__['solidPointsToRemoval']:
                self.__dict__['_ren'].RemoveActor(i.getVtkActor())
            self.__dict__['solidPointsToRemoval'] = []
            self.createSolidSimplifiedPolygon()
            pass
        self.update()
        pass

    def getNextPointId(self):
        # @todo: use self.__dict__
        self._cPid += 1
        return self._cPid

    def appendPoint(self, cPoint):
        # @todo: delete last inserted point
        #self.__dict__['cPoints'][cPoint.getId()] = cPoint
        self.__dict__['pointContainer'].addPoint(cPoint)
        self.__dict__['_ren'].AddActor(cPoint.getVtkActor())
        #if not hasattr(self, '_polyPoints'):
        #    self._polyPoints = vtk.vtkPoints()
        #self._polyPoints.InsertNextPoint(cPoint.getWCoord()[0], cPoint.getWCoord()[1], 0)
        # @todo: implement cPoint state

    def createSolidSimplifiedPolygon(self, ):
        """
        Создаём упрощённый полигон для сплошного режима оконтуривания
        @return:
        """
        # @todo: дописать id точки/контура в зависимости от органа
        print 'creating solid polygon'
        points = vtk.vtkPoints()
        polygon = vtk.vtkPolygon()
        pd3 = self.__dict__['solidContours'].getData('123')['pd3']
        lo = self.__dict__['solidContours'].getData('123')['simplified']
        lp = len(self.__dict__['solidContours'].getData('123')['simplified'])
        for i in range(lp):
            points.InsertNextPoint(lo[i][0], lo[i][1], 0)

        print points

        polygon.GetPointIds().SetNumberOfIds(lp)
        for i in range(lp):
            polygon.GetPointIds().SetId(i, i)

        print polygon

        polygons = vtk.vtkCellArray()
        polygons.InsertNextCell(polygon)
        print polygons

        polygonPolyData = vtk.vtkPolyData()
        polygonPolyData.SetPoints(points)
        polygonPolyData.SetPolys(polygons)
        print polygonPolyData

        mapper = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            mapper.SetInput(polygonPolyData)
        else:
            mapper.SetInputData(polygonPolyData)

        actor = vtk.vtkActor()
        actor.GetProperty().SetColor(1.0, 1.0, 0.0)
        actor.SetMapper(mapper)

        self.__dict__['_ren'].AddActor(actor)
        pass

    def createPolygon(self):
        # @deprecated ?
        print 'Creating poly check', len(self.__dict__['cPoints'])
        if self.__dict__['pointContainer'].getLength() < 3:
            return
        print 'Creating poly'
        try:
            self.__dict__['_ren'].RemoveActor(self.__dict__['polyActor'])
        except :
            pass
        '''
        vtkSmartPointer<vtkPoints> points =
            vtkSmartPointer<vtkPoints>::New();
          points->InsertNextPoint(0.0, 0.0, 0.0);
          points->InsertNextPoint(1.0, 0.0, 0.0);
          points->InsertNextPoint(1.0, 1.0, 0.0);
          points->InsertNextPoint(0.0, 1.0, 0.0);'''
        polyPoints = vtk.vtkPoints()
        #self._polyPoints = vtk.vtkPoints()
        polygon = vtk.vtkPolygon()
        polygon.GetPointIds().SetNumberOfIds(self.__dict__['pointContainer'].getLength())
        #j = 0
        for i in range(self.__dict__['pointContainer'].getLength()):
            #v = self.__dict__['cPoints'][i]
            #polyPoints.InsertNextPoint(v.getCoords())
            polygon.GetPointIds().SetId(i, i)

        poly = vtk.vtkCellArray()
        poly.InsertNextCell(polygon)

        polygonPolyData = vtk.vtkPolyData()
        polygonPolyData.SetPoints(self.__dict__['pointContainer'].getVtkPoints())
        polygonPolyData.SetPolys(poly)

        filter = vtk.vtkLinearExtrusionFilter()
        filter.SetInput(polygonPolyData)
        filter.SetScaleFactor(1)
        filter.SetExtrusionTypeToNormalExtrusion()
        filter.SetVector(1, 0, 0)

        mapper = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            #mapper.SetInput(filter)
            mapper.SetInput(polygonPolyData)
            #mapper.SetLookupTable(lut)
        else:
            #mapper.SetInputData(filter)
            mapper.SetInputData(polygonPolyData)
        mapper.SetScalarModeToUseCellData()
        mapper.UseLookupTableScalarRangeOn()

        actor = vtk.vtkActor()
        actor.SetMapper(mapper)

        self.__dict__['polyActor'] = actor
        self.__dict__['polyActor'].GetProperty().SetColor(1.0, 1.0, 0.0)
        self.__dict__['_ren'].AddActor(self.__dict__['polyActor'])

    def eventMiddleButtonRelease(self, obj, event):
        """
        Возвращаем исходное состояние камеры и рулеток
        @param obj: объект события
        @type obj: vtkobject
        @param event: название события
        @type event: str
        """
        self.__dict__['_ren'].ResetCamera()
        self.update()
        self.__dict__['screenBounds'] = self.getCurrentScreenBounds()
        self.setRulersBounds()

    def getReader(self):
        """
        @return: reader
        @rtype: vtk.vtkDICOMImageReader
        """
        return self.__dict__['reader']

    def setWindowLevel(self, windowlevel):
        """
        @param windowlevel: (window, level)
        @type windowlevel: tuple
        """
        try:
            self.__dict__['im'].SetWindow(windowlevel[0])
        except TypeError:
            self.__dict__['im'].SetWindow(float(NPPlan.config.appConfig.client.defaultwindow))
        try:
            self.__dict__['im'].SetLevel(windowlevel[1])
        except TypeError:
            self.__dict__['im'].SetLevel(float(NPPlan.config.appConfig.client.defaultwindow))

    def setWindow(self, window):
        """
        @param window: window
        @type window: int
        """
        self.__dict__['im'].SetWindow(window)

    def setLevel(self, level):
        """
        @param level: level
        @type level: int
        """
        self.__dict__['im'].SetLevel(level)

    windowlevel = property(fset=setWindowLevel)
    window = property(fset=setWindow)
    level = property(fset=setLevel)

    def setImage(self, fileName):
        """
        Загружает рисунок по пути fileName
        @param fileName: имя файла
        @type fileName: string
        """
        self.__dict__['reader'].SetFileName(fileName)

    image = property(fset=setImage)

    def updateImage(self):
        """
        Обновляет рисунок текущим, установленным в плане
        """
        #print 'curFile loading', self.__dict__['_informer'].currentFile.path
        #self.image = self.__dict__['_informer'].currentFile.path
        self.image = NPPlan.currentPlan.planGetCurrentDicomImagePath()
        #self.windowlevel = self.__dict__['_informer'].currentFile.windowlevel
        self.update()

    def updateWindowLevel(self, value=(0, 0)):
        """
        @param value: tuple (new window, new level)
        @type value: tuple
        """
        self.windowlevel = value
        self.update()

    def update(self):
        """
        Обновляет окно
        """
        self.__dict__['_ren'].Render()
        self.__dict__['vtkInteractor'].Update()
        self.__dict__['vtkInteractor'].Render()
        pass

    def getContourMode(self):
        try:
            return self.__dict__['contourMode']
        except KeyError:
            return 'd'

    def setContourMode(self, newMode):
        print 'contourMode in vtk class'
        self.__dict__['contourMode'] = newMode
