# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 23.11.12
@summary: 
'''

import NPPlan
from NPPlan.controller.client.mainC.abstracts import baseController
from histogram import histogramController

import wx

# @todo: from NPPlan.controller.client.mainC.stages.contouring.main import WL_CHANGE_MAIN, WL_CHANGE_SLIDER, WL_UPDATE_MAIN, WL_CHANGE_TEXTS

class sliderChanger(object):
    def __init__(self, parent):
        """
        @param parent: родитель
        @type parent: rightPanelController
        """
        self.__dict__['_parent'] = parent

    def getLevel(self):
        """
        @return: значение слайдера Level
        @rtype: float
        """
        return float(self.__dict__['_parent'].wlPanel.slider[1].GetValue())

    def setLevel(self, value):
        self.__dict__['_parent'].wlPanel.slider[1].SetValue(int(value))

    def getWindow(self):
        return float(self.__dict__['_parent'].wlPanel.slider[0].GetValue())

    def setWindow(self, value):
        self.__dict__['_parent'].wlPanel.slider[0].SetValue(int(value))

    level = property(getLevel, setLevel)
    window = property(getWindow, setWindow)

class textsChanger(object):
    def __init__(self, parent):
        """
        @param parent: родитель
        @type parent: rightPanelController
        """
        self.__dict__['_parent'] = parent

    def getLevel(self):
        """
        @return: значение текстового поля level
        @rtype: float
        """
        return float(self.__dict__['_parent'].wlPanel.texts[1].GetValue())

    def setLevel(self, value):
        self.__dict__['_parent'].wlPanel.texts[1].SetValue(str(value))

    def getWindow(self):
        return float(self.__dict__['_parent'].wlPanel.texts[0].GetValue())

    def setWindow(self, value):
        self.__dict__['_parent'].wlPanel.texts[0].SetValue(str(value))

    level = property(getLevel, setLevel)
    window = property(getWindow, setWindow)


class rightPanelController(baseController):
    def __init__(self, panel):
        self.__dict__['panel'] = panel
        self.slider = sliderChanger(self)
        self.texts = textsChanger(self)
        self.__dict__['contourMode'] = 'd'

    def getWlPanel(self):
        """
        @return: window/level панель
        @rtype: NPPlan.view.client.main.stages.contouring.rightPanel.wlPanel
        """
        return self.__dict__['panel'].wlPanel
    wlPanel = property(getWlPanel)

    def doPanelBindings(self):
        self.doWLPanelBindings()
        self.doPointsModeBindings()
        self.populateOrgans()

    def doWLPanelBindings(self):
        wlPanel = self.__dict__['panel'].wlPanel
        print 'wlPanel slider', wlPanel.slider
        wlPanel.Bind(wx.EVT_SLIDER, self.changeWindowBySlider, wlPanel.slider[0])
        wlPanel.Bind(wx.EVT_SLIDER, self.changeLevelBySlider, wlPanel.slider[1])
        wlPanel.texts[0].Bind(wx.EVT_KEY_UP, self.onWindowChar)
        wlPanel.texts[1].Bind(wx.EVT_KEY_UP, self.onLevelChar)

        wlPanel.Bind(wx.EVT_CHAR, self.onWindowChar, wlPanel.__dict__['wText'])

    def doPointsModeBindings(self):
        pointsPanel = self.__dict__['panel'].pointsPane
        pointsPanel.Bind(wx.EVT_TOGGLEBUTTON, self.onDotsButton, pointsPanel._dotsButton)
        pointsPanel.Bind(wx.EVT_TOGGLEBUTTON, self.onSolidButton, pointsPanel._solidButton)
        pointsPanel.Bind(wx.EVT_TOGGLEBUTTON, self.onPolygonButton, pointsPanel._polygonButton)
        self.__dict__['panel'].pointsPane.unPressAll()
        self.__dict__['panel'].pointsPane.setState(self.__dict__['contourMode'])
        print pointsPanel

    def populateOrgans(self):
        organsPanel = self.__dict__['panel'].pointsPane

        _list = organsPanel.list
        organsPanel.Bind(wx.EVT_TREE_SEL_CHANGED, self.selectOrganBackend, organsPanel.list)
        print 'populating Organs'
        for i in NPPlan.mongoConnection.organs.find({'pid': '1', 'active': True}):
            print i
            child = _list.AppendItem(organsPanel.root, i.name.originalName)
            _list.SetItemText(child, str(i.uid), 1)
            _list.SetItemPyData(child, i)
        _list.Expand(organsPanel.root)
        pass

    def selectOrganBackend(self, event):
        print self.__dict__['panel'].pointsPane.list.GetItemPyData(event.GetItem())
        self.handler.contouring.controller.setOrganSelection(
            data=self.__dict__['panel'].pointsPane.list.GetItemPyData(event.GetItem())
        )


    def onDotsButton(self, event):
        event.Skip()
        self.contourMode = 'd'
        self.__dict__['panel'].pointsPane.unPressAll()
        self.__dict__['panel'].pointsPane.setState(self.__dict__['contourMode'])
        event.Skip()
        pass

    def onSolidButton(self, event):
        event.Skip()
        self.contourMode = 's'
        self.__dict__['panel'].pointsPane.unPressAll()
        self.__dict__['panel'].pointsPane.setState(self.__dict__['contourMode'])
        event.Skip()
        pass

    def onPolygonButton(self, event):
        event.Skip()
        self.contourMode = 'p'
        self.__dict__['panel'].pointsPane.unPressAll()
        self.__dict__['panel'].pointsPane.setState(self.__dict__['contourMode'])
        event.Skip()
        pass

    def onWindowChar(self, event):
        """
        Бэкенд для EVT_KEY_UP у поля ввода window, @see: setWindow
        @param event: событие
        @type event: wx.Event
        """
        from NPPlan.controller.client.mainC.stages.contouring.main import WL_CHANGE_MAIN, WL_CHANGE_SLIDER, WL_UPDATE_MAIN, WL_CHANGE_TEXTS
        window = self.__dict__['panel'].wlPanel.__dict__['wText'].GetValue()
        self.handler.contouringWindowLevelPresets.controller.setWindow(window, WL_CHANGE_MAIN | WL_CHANGE_SLIDER | WL_UPDATE_MAIN)
        event.Skip()

    def onLevelChar(self, event):
        '''
        Бэкенд для EVT_KEY_UP у поля ввода level, @see: setLevel
        @param event: событие
        @type event: wx.Event
        '''
        from NPPlan.controller.client.mainC.stages.contouring.main import WL_CHANGE_MAIN, WL_CHANGE_SLIDER, WL_UPDATE_MAIN, WL_CHANGE_TEXTS
        level = self.__dict__['panel'].wlPanel.__dict__['lText'].GetValue()
        self.handler.contouringWindowLevelPresets.controller.setWindow(level, WL_CHANGE_MAIN | WL_CHANGE_SLIDER | WL_UPDATE_MAIN)
        event.Skip()

    def changeWindowBySlider(self, event=0):
        '''
        Вызывает изменение значения window у текущего среза
        @param event: событие
        @type event: wx.Event
        '''
        from NPPlan.controller.client.mainC.stages.contouring.main import WL_CHANGE_MAIN, WL_CHANGE_SLIDER, WL_UPDATE_MAIN, WL_CHANGE_TEXTS
        window = self.wlPanel.slider[0].GetValue()
        self.handler.contouringWindowLevelPresets.controller.setWindow(window, WL_CHANGE_MAIN | WL_CHANGE_TEXTS | WL_UPDATE_MAIN)

    def changeLevelBySlider(self, event=0):
        '''
        Вызывает изменение значения level у текущего среза
        @param event: событие
        @type event: wx.Event
        '''
        from NPPlan.controller.client.mainC.stages.contouring.main import WL_CHANGE_MAIN, WL_CHANGE_SLIDER, WL_UPDATE_MAIN, WL_CHANGE_TEXTS
        level = self.wlPanel.slider[1].GetValue()
        self.handler.contouringWindowLevelPresets.controller.setLevel(level, WL_CHANGE_MAIN | WL_CHANGE_TEXTS | WL_UPDATE_MAIN)

    def start(self):
        self.__dict__['panel'].start()

        #vtkWindow = self.__dict__['panel'].histogram.getVtkWindow()
        vtkWindow = None
        wxParent = self.__dict__['panel'].histogram
        self.__dict__['histogram'] = histogramController(wxParent, vtkWindow)
        self.handler.register('contouringHistogram', self.__dict__['histogram'], wxParent)

    def setContourMode(self, newMode):
        print 'Setting contour mode from %s to %s' % (self.__dict__['contourMode'], newMode)
        self.__dict__['contourMode'] = newMode
        self.handler.contouring.controller.__dict__['vtkHandler'].setContourMode(newMode)

    def getContourMode(self):
        return self.__dict__['contourMode']

    contourMode = property(getContourMode, setContourMode)