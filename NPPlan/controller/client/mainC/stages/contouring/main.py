# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 2
@date: 16.11.12
@summary: 
'''

import NPPlan
import wx
import vtk
import os
import NPPlan.model.file.configReader.windowLevelPresets as wlPresets
from NPPlan.controller.client.mainC.abstracts import baseController, abstractTwoPanels
from NPPlan.controller.client.main import getApp
from NPPlan.controller.client.mainC.stages.contouring.vtkHandler import vtkHandler
from NPPlan.controller.client.mainC.stages.contouring.contourInfo import contourInfo
from NPPlan.controller.client.mainC.stages.contouring.rightPanel import rightPanelController

try:
    from agw import thumbnailctrl as TC
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.thumbnailctrl as TC
from NPPlan.view.client.main.stages.contouring import contourWin
from NPPlan.view.client.main.stages.contouring import rightPanelView

WL_CHANGE_MAIN = 0b001
"""@var WL_CHANGE_MAIN: флаг смены главного окна"""
WL_CHANGE_SLIDER = 0b010
"""@var WL_CHANGE_SLIDER: флаг смены слайдера"""
WL_CHANGE_TEXTS = 0b100
"""@var WL_CHANGE_TEXTS: флаг смены текстовых полей"""
WL_UPDATE_MAIN = 0b1000
"""@var WL_UPDATE_MAIN: флаг обновления после смены"""

_panel = None
_vtkHandler = None
_informer = None


class contouringController(baseController, abstractTwoPanels):
    def __init__(self, panel):
        NPPlan.config.runningConfig.curSubStage = 'contouring'
        abstractTwoPanels.__init__(self, panel)
        wlPresets.readConfig()
        self.__dict__['window'] = float(NPPlan.config.appConfig.client.defaultwindow)
        self.__dict__['level'] = float(NPPlan.config.appConfig.client.defaultlevel)
        self.__dict__['contextMenuItems'] = {}

        self.informer()
        self.windowLevelPresetsHandler()

    def informer(self):
        self.__dict__['informer'] = self.handler.contouringContours.controller

    def windowLevelPresetsHandler(self):
        self.__dict__['wlPresetsHandler'] = windowLevelPresets()
        self.handler.register('contouringWindowLevelPresets', self.__dict__['wlPresetsHandler'], None)

    @staticmethod
    def checkRequirements():
        # @todo: implement

        # loading info for slices
        _informer = contourInfo()
        _informer.studyName = 'text'
        _informer.dicomDir = 'C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\'
        _informer.thumbDir = 'C:\\Users\\Chernukha\\Pictures\\DICOM\\thumb\\'
        _informer.check()
        _informer.currentFile = -1
        from NPPlan.controller.client.main import getHandler
        getHandler().register('contouringContours', _informer, None)

        return True

    def setOrganSelection(self, *args, **kwargs):
        print 'organs selected', kwargs.get('data')
        self._currentOrgan = kwargs.get('data', None)
        if self._currentOrgan is None:
            return
        self.__dict__['vtkHandler'].setSelectionData(key=self._currentOrgan['uid'],
                                                     contourData=self._currentOrgan['contourData']
        )
        pass

    def start(self):
        print 'CONTOURING STAGE'
        #print self.__dict__['windowLevelData']
        if hasattr(NPPlan, 'currentPlan'):
            plan = NPPlan.currentPlan
        else:
            plan = NPPlan.planAbstract()
            plan.planLoadDicomSeriesFromFile("C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\")
            plan.planSetThumbsDir("C:\\Users\\Chernukha\\Pictures\\DICOM\\thumb\\")
            NPPlan.currentPlan = plan
        controls, data = plan.planToThumbnailData()
        self.mainPanel = contourWin(self.__dict__['mainPanel'],
                                    thumbnailBackend=self.selectionBackend,
                                    thumbnailControls=controls,
                                    thumbnailData=data,
                                    parentController=self,
                                    )
        self.rightPanel = rightPanelView(self.__dict__['rightPanel'])

        self.__dict__['vtkHandler'] = vtkHandler(self.mainPanel, self.mainPanel.getVtkWindow(),
                                                 informer=self.__dict__['informer'],
                                                 rulersCtrls=self.mainPanel.renderWindow.rulers,
                                                 parentController=self)
        self.__dict__['vtkHandler'].setCurrentAction('work')
        rpC = rightPanelController(self.rightPanel)

        self.doMainPanelBindings()
        self.handler.register('contouringRightPanel', rpC, self.rightPanel)
        self.doRightPanelBindings()

        self.__dict__['child'].doLayout()
        self.layout()
        self.updateRibbon()
        wx.CallLater(500, self.setDefaults)
        pass

    def selectionBackend(self, index, data, event=None):

        pass

    def setDefaults(self, event=0):
        window = NPPlan.config.appConfig.client.defaultwindow
        level = NPPlan.config.appConfig.client.defaultlevel
        self.handler.contouringWindowLevelPresets.controller.setWindow(window, WL_CHANGE_MAIN | WL_CHANGE_TEXTS | WL_CHANGE_SLIDER)
        self.handler.contouringWindowLevelPresets.controller.setLevel(level, WL_CHANGE_MAIN | WL_CHANGE_TEXTS | WL_CHANGE_SLIDER | WL_UPDATE_MAIN)

        self.updateInfoText()

    def updateInfoText(self):
        if not hasattr(self, '_curSelectedData'):
            self._curSelectedData = self.mainPanel.thumbnailer.getCurrentSelected()
        self.mainPanel.setInfoText("Patient: %s\nSlice: %d/%d\nLocation: %2.2f\n" % (
            NPPlan.currentPlan.planGetPatientName(),
            self._curSelectedData['instance'],
            NPPlan.currentPlan.planGetSeriesLength(),
            self._curSelectedData['loc'],
        ))

    def getVtkHandler(self):
        return self.__dict__['vtkHandler']
    vtkHandler = property(getVtkHandler)

    def updateRibbon(self):
        self.handler.ribbon.controller.update()
        #self.handler.ribbon.controller.setActivePageByPageName('b_contour')

    def doMainPanelBindings(self):
        self.mainPanel.Bind(TC.EVT_THUMBNAILS_SEL_CHANGED, self.mainPanelClickThumbnailer)
        self.mainPanel.getVtkWindow().Bind(wx.EVT_RIGHT_UP, self.showContextMenu)

    def showContextMenu(self, event):
        print 'mouseRightUp', event.GetId()
        if 'btnSize' not in self.__dict__['contextMenuItems']:
            self.__dict__['contextMenuItems']['curSelected'] = 'btnContour'
            self.__dict__['contextMenuItems']['btnSize'] = wx.NewId()
            self.__dict__['contextMenuItems']['btnMove'] = wx.NewId()
            self.__dict__['contextMenuItems']['btnContour'] = wx.NewId()
            self.mainPanel.getVtkWindow().Bind(wx.EVT_MENU,
                                               self.setZoomMode,
                                               id=self.__dict__['contextMenuItems']['btnSize'])
            self.mainPanel.getVtkWindow().Bind(wx.EVT_MENU,
                                               self.setPanMode,
                                               id=self.__dict__['contextMenuItems']['btnMove'])
            self.mainPanel.getVtkWindow().Bind(wx.EVT_MENU,
                                               self.setContourMode,
                                               id=self.__dict__['contextMenuItems']['btnContour'])
        menu = wx.Menu()
        menu.Append(self.__dict__['contextMenuItems']['btnContour'], _('Contouring'),
                    _('Use contouring'), wx.ITEM_RADIO)
        menu.Append(self.__dict__['contextMenuItems']['btnSize'], _('Zoom'), _('Zooming'), wx.ITEM_RADIO)
        menu.Append(self.__dict__['contextMenuItems']['btnMove'], _('Pan'), _('Move image'), wx.ITEM_RADIO)
        menu.FindItemById(self.__dict__['contextMenuItems'][self.__dict__['contextMenuItems']['curSelected']]).Check()
        self.mainPanel.getVtkWindow().PopupMenu(menu)
        menu.Destroy()
        pass

    def setZoomMode(self, event):
        self.__dict__['contextMenuItems']['curSelected'] = 'btnSize'
        self.__dict__['vtkHandler'].setCurrentAction('zoom')

    def setPanMode(self, event):
        self.__dict__['contextMenuItems']['curSelected'] = 'btnMove'
        self.__dict__['vtkHandler'].setCurrentAction('pan')

    def setContourMode(self, event):
        self.__dict__['contextMenuItems']['curSelected'] = 'btnContour'
        self.__dict__['vtkHandler'].setCurrentAction('work')

    def selectContextMenu(self, event):
        pass

    def doRightPanelBindings(self):
        self.handler.contouringRightPanel.controller.doPanelBindings()

    def mainPanelClickThumbnailer(self, event):
        #print self.mainPanel.thumb.GetItem(self.mainPanel.thumb.GetSelection()).GetFileName()
        #thumbName = self.mainPanel.thumb.GetItem(self.mainPanel.thumb.GetSelection()).GetFileName()
        thumbData = self.mainPanel.thumbnailer.getCurrentSelected()
        print 'thumbdata', thumbData
        #self.__dict__['informer'].currentFile = thumbData['name']+'.png'
        self._curSelectedData = thumbData
        NPPlan.currentPlan.planSetCurrentSelectedByFileName(thumbData['name'])
        self.__dict__['vtkHandler'].updateImage()
        self.handler.contouringHistogram.controller.update()
        self.updateInfoText()
        pass

    def work(self):
        print 'work contour'
        self.updateRibbon()
        pass


class windowLevelPresets(baseController):
    def __init__(self):
        data = wlPresets.parse()
        for i in data:
            data[i]['menuId'] = wx.NewId()
        self.__dict__['windowLevelData'] = data
        self.__dict__['bindings'] = {}
        self.__dict__['menu'] = None
        pass

    def showPopup(self, target):
        """
        @param target: событие для показа
        @type target: wx.lib.agw.ribbon.buttonbar.RibbonButtonBarEvent
        """
        if self.__dict__['menu'] is None:
            menu = wx.Menu()
            for i in self.__dict__['windowLevelData']:
                menu.Append(self.__dict__['windowLevelData'][i]['menuId'], self.__dict__['windowLevelData'][i]['name'])
                self.popupBinding(menu, self.__dict__['windowLevelData'][i]['menuId'], i)
            self.__dict__['menu'] = menu

        target.PopupMenu(self.__dict__['menu'])
        pass

    def popupBinding(self, target, itemId, data):
        target.Bind(wx.EVT_MENU, self.clickWindowLevelPresets, id=itemId)
        self.__dict__['bindings'][itemId] = data

    def setWindow(self, value, flag=WL_CHANGE_MAIN):
        """
        @param value: значение
        @type value: str or int or float
        @param flag: флаг,
        WL_CHANGE_MAIN -- изменять главное окно
        WL_CHANGE_SLIDER -- изменять слайдер
        WL_CHANGE_TEXTS -- изменять текст
        @type flag: int
        """
        self.__dict__['window'] = value
        if flag & WL_CHANGE_MAIN:
            self.handler.contouringContours.controller.currentFile.window = float(value)
        if flag & WL_CHANGE_SLIDER:
            self.handler.contouringRightPanel.controller.slider.window = value
        if flag & WL_CHANGE_TEXTS:
            self.handler.contouringRightPanel.controller.texts.window = value
        if flag & WL_UPDATE_MAIN:
            self.handler.contouring.controller.vtkHandler.updateWindowLevel((self.__dict__['window'], self.__dict__['level']))

    def setLevel(self, value, flag=WL_CHANGE_MAIN):
        """
        @param value: значение
        @type value: str or int or float
        @param flag: флаг,
        WL_CHANGE_MAIN -- изменять главное окно
        WL_CHANGE_SLIDER -- изменять слайдер
        WL_CHANGE_TEXTS -- изменять текст
        @type flag: int
        """
        self.__dict__['level'] = value
        if flag & WL_CHANGE_MAIN:
            self.handler.contouringContours.controller.currentFile.level = float(value)
        if flag & WL_CHANGE_SLIDER:
            self.handler.contouringRightPanel.controller.slider.level = value
        if flag & WL_CHANGE_TEXTS:
            self.handler.contouringRightPanel.controller.texts.level = value
        if flag & WL_UPDATE_MAIN:
            self.handler.contouring.controller.vtkHandler.updateWindowLevel((self.__dict__['window'], self.__dict__['level']))

    def clickWindowLevelPresets(self, event):
        #print event.GetId()
        #print self.__dict__['windowLevelData'][self.__dict__['bindings'][event.GetId()]]
        # update w/l in main window
        #self.handler.contouringContours.controller.currentFile.window = float(self.__dict__['windowLevelData'][self.__dict__['bindings'][event.GetId()]]['window'])
        #self.handler.contouringContours.controller.currentFile.level = float(self.__dict__['windowLevelData'][self.__dict__['bindings'][event.GetId()]]['level'])
        #self.handler.contouring.controller.vtkHandler.updateWindowLevel()
        self.setWindow(float(self.__dict__['windowLevelData'][self.__dict__['bindings'][event.GetId()]]['window']), WL_CHANGE_MAIN | WL_CHANGE_TEXTS | WL_CHANGE_SLIDER)
        self.setLevel(float(self.__dict__['windowLevelData'][self.__dict__['bindings'][event.GetId()]]['level']), WL_CHANGE_MAIN | WL_CHANGE_TEXTS | WL_CHANGE_SLIDER | WL_UPDATE_MAIN)

        # update w/l in right panel
        #self.handler.contouringRightPanel.controller.window = float(self.__dict__['windowLevelData'][self.__dict__['bindings'][event.GetId()]]['window'])
        #self.handler.contouringRightPanel.controller.level = float(self.__dict__['windowLevelData'][self.__dict__['bindings'][event.GetId()]]['level'])

def preInit():
    """
    Условие перехода на стадию
    @todo: implement
    @return: True если условия выполнены, иначе False
    @rtype: boolean
    """
    global _informer
    _informer = contourInfo()
    _informer.studyName = 'text'
    _informer.dicomDir = 'C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\'
    _informer.thumbDir = 'C:\\Users\\Chernukha\\Pictures\\DICOM\\thumb\\'
    _informer.check()
    _informer.currentFile = -1
    print _informer.currentFile.path

    loadWindowLevelPresets()

    NPPlan.dumpRunningConfig()
    return True
    study = NPPlan.getRunningConfig('curPlan.studies')
    if study is None:
        return False
    if 1 == len(study):
        curPlan = NPPlan.getRunningConfig('curPlan')
        curPlan['activeStudy'] = study.keys()[0]
        NPPlan.setRunningConfig(curPlan)
        print curPlan
    else:
        return False
    return True

def loadWindowLevelPresets():
    data = NPPlan.getRunningConfig('windowLevelPresets')
    if data is None:
        wlPresets.readConfig()
        data = wlPresets.parse()
        for i in data:
            data[i]['menuId'] = wx.NewId()
        NPPlan.setRunningConfig('windowLevelPresets', data)
    bindWindowLevelPresets()
    print data

_bindings = {}

def bindWindowLevelPresets():
    global _bindings
    data = NPPlan.getRunningConfig('windowLevelPresets')
    frame = getApp().getTopWindow()
    for i in data:
        frame.Bind(wx.EVT_MENU, clickWindowLevelPresets, id=data[i]['menuId'])
        _bindings[data[i]['menuId']] = i

def clickWindowLevelPresets(event=0):
    print event.GetId()
    global _vtkHandler, _bindings

    data = NPPlan.getRunningConfig('windowLevelPresets')
    #data[event.GetId()]
    _vtkHandler.window = int(data[_bindings[event.GetId()]]['window'])
    _vtkHandler.level = int(data[_bindings[event.GetId()]]['level'])
    _vtkHandler.update()

def showWindowLevelPresetsPopup(event=None):
    global _panel
    data = NPPlan.getRunningConfig('windowLevelPresets')
    print data
    menu = wx.Menu()
    for i in data:
        menu.Append(data[i]['menuId'], data[i]['name'])

    event.PopupMenu(menu)
    pass


def init(panel):
    global _panel
    _panel = panel
    global _vtkHandler
    global _informer
    _vtkHandler = vtkHandler(_panel, _panel.getVtkWindow(), informer=_informer)
    _panel.vtkHandler = _vtkHandler
    doBindings(panel)
    updateRibbon()
    pass

def doBindings(panel):
    panel.thumb.Bind(TC.EVT_THUMBNAILS_SEL_CHANGED, switcher)

def switcher(event):
    # @todo: GetFileName() own implementation, can be changed to slice location/etc
    global _panel, _informer, _vtkHandler
    thumbName = _panel.thumb.GetItem(_panel.thumb.GetSelection()).GetFileName()
    _informer.currentFile = thumbName
    _vtkHandler.updateImage()
    pass

def work():
    global _informer
    print _informer.currentFile.path
    updateRibbon()
    pass

def updateRibbon():
    getApp().getRibbon().update()
    getApp().getRibbon().setActivePageByPageName('b_contour')




