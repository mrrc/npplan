# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 18.03.13
@summary: 
'''

import NPPlan
import wx
from .base import baseProgress
import wx.animate


class waitingProgress(baseProgress):
    def __init__(self, caller=None, callback=None, autostart=True, style=None):
        baseProgress.__init__(self, caller, callback, autostart=False)
        from NPPlan.controller.client.main import getHandler
        getHandler().register('progress', self, None)

    def run(self):
        dlg = waitingFrame(wx.GetApp().GetTopWindow(), -1, 'Wait please')
        dlg.Show()

        while self._keepGoing:
            wx.MilliSleep(100)


class waitingFrame(wx.Frame):
    def __init__(
            self, parent, ID, title, pos=wx.DefaultPosition,
            size=wx.DefaultSize, style=wx.DEFAULT_FRAME_STYLE | wx.CENTER_FRAME | wx.SPLASH_CENTRE_ON_SCREEN
                                       | wx.CENTER_ON_SCREEN
            ):
        pos = (600, 600)
        wx.Frame.__init__(self, parent, ID, title, pos, size, style)

        path = r'C:\Users\Chernukha\Pictures\loading-gif-animation.gif'

        ani = wx.animate.Animation(path)
        ctrl = wx.animate.AnimationCtrl(self, -1, ani)
        ctrl.SetUseWindowBackgroundColour()
        ctrl.Play()

        border = wx.BoxSizer()
        border.Add(ctrl, 0, wx.EXPAND)
        self.SetSizer(border)




