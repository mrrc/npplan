# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 13.09.12
@summary: 
'''

import NPPlan
import wx
from NPPlan.model.file.configReader.ribbon import readConfig, parse
import NPPlan.controller.logger as log

from NPPlan.controller.client.mainC.ribbonHandler import ribbonHandler
try:
    from agw import ribbon as RB
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.ribbon as RB


ID_EXPAND_COLLAPSE_STAGES = wx.NewId()
ID_NEW_PLAN = wx.NewId()
ID_SAVE_PLAN = wx.NewId()

# plan
ID_PLAN_TEST = wx.NewId()
ID_PLAN2_TEST = wx.NewId()
ID_PLAN3_TEST = wx.NewId()
ID_PLAN_COPY_TO = wx.NewId()
ID_PLAN_COPY_FROM = wx.NewId()

# contouring
ID_DICOM_WL = wx.NewId()
ID_DICOM_WL_MANUAL = wx.NewId()
ID_DICOM_WL_LIST = wx.NewId()
ID_DICOM_WL_SAVE = wx.NewId()

ID_CONTOUR_DOTS = wx.NewId()
ID_CONTOUR_INF = wx.NewId()
ID_CONTOUR_POLY = wx.NewId()

# 3D
ID_3D_TEST = wx.NewId()

ID_SHOW_USER_DOC = wx.NewId()
ID_SHOW_DEV_DOC = wx.NewId()

ID_SHOW_DIALOG_TEST = wx.NewId()


class ribbon:
    def __init__(self, frame):
        self.frame = frame

        readConfig()
        self.ribbonDefaultConfig = parse()
        self.ribbonRunningConfig = self.ribbonDefaultConfig.copy()

        self.clearInternalStuff()

        self.ribbonHandler = ribbonHandler()
        #print parse()

    def redrawRibbon(self):
        self.frame.Freeze()
        self.frame.ribbon.Destroy()
        self.frame.ribbon = RB.RibbonBar(self.frame.mainPanel, wx.NewId())
        self.frame.Thaw()

    def getButton(self, name):
        return self.buttons[name]

    def disableButtonByName(self, name):
        pPanel = self.panels[self.ribbonRunningConfig['items'][name]['parent']]['panel']
        pPanel.EnableButton(self.buttonsIds[name], False)

    def enableButtonByName(self, name):
        pPanel = self.panels[self.ribbonRunningConfig['items'][name]['parent']]['panel']
        pPanel.EnableButton(self.buttonsIds[name], True)

    def clearInternalStuff(self):
        self.hiddenPages = []
        self.pages = {}
        self.panels = {}
        self.items = {}
        self.buttons = {}
        self.buttonsIds = {}
        self.backendProvider = {}
        self.disabledButtons = []

    def createRibbon(self):
        #print 'rbrunning', self.ribbonRunningConfig
        curStage = NPPlan.config.runningConfig.curStageType
        if hasattr(NPPlan.config.runningConfig, 'curSubStageType'):
            curSubStage = NPPlan.config.runningConfig.curSubStageType
        else:
            curSubStage = None
        #print curStage, curSubStage

        # clear old
        self.clearInternalStuff()

        # add pages
        for i in sorted(self.ribbonRunningConfig['pages'].iterkeys()):
            page = self.ribbonRunningConfig['pages'][i]
            if 'all' == page['onstage'] or (curStage == page['onstage']):
                self.pages[page['name']] = RB.RibbonPage(self.frame.ribbon, wx.ID_ANY, page['rusname'])
            else:
                self.hiddenPages.append(page['name'])

        #self.printCurrentState()

        # add panels
        for i in sorted(self.ribbonRunningConfig['panels'], key=lambda z: self.ribbonRunningConfig['panels'][z]['order']):
            if self.ribbonRunningConfig['panels'][i]['parent'] not in self.hiddenPages:
                self.panels[i] = {}
                page = i
                panel = self.ribbonRunningConfig['panels'][i]
                self.panels[i]['container'] = RB.RibbonPanel(self.pages[panel['parent']], wx.ID_ANY, panel['rusname'])
                self.panels[i]['id'] = wx.NewId()
                if 'buttonbar' == panel['type']:
                    self.panels[i]['panel'] = RB.RibbonButtonBar(self.panels[i]['container'], self.panels[i]['id'])
                elif 'toolbar' == panel['type']:
                    self.panels[i]['panel'] = RB.RibbonToolBar(self.panels[i]['container'], self.panels[i]['id'])
                    if panel.has_key('rows'):
                        self.panels[i]['panel'].SetRows(panel['rows'][0], panel['rows'][1])

        #self.printCurrentState()

        # add items
        # @todo: sorted by <order> value
        for i in sorted(self.ribbonRunningConfig['items'], key=lambda z: self.ribbonRunningConfig['items'][z]['order']):
            item = self.ribbonRunningConfig['items'][i]
            if self.ribbonRunningConfig['panels'][item['parent']]['parent'] in self.hiddenPages:
                continue
            pPanel = self.panels[item['parent']]
            iId = eval(item['id'])
            if 'standart' == item['iconType']:
                #print item['iconSize']
                image = wx.ArtProvider.GetBitmap(eval(item['icon']), wx.ART_OTHER, item['iconSize'])
                imageSmall = wx.ArtProvider.GetBitmap(eval(item['icon']), wx.ART_OTHER, (16,16))
                pass
            else:
                image = wx.Bitmap(NPPlan.getIconPath()+item['icon'], wx.BITMAP_TYPE_PNG)
                try:
                    imageSmall = wx.Bitmap(NPPlan.getIconPath()+item['iconSmall'], wx.BITMAP_TYPE_PNG)
                except TypeError:
                    imageSmall = ""
            try:
                imageDisabled = wx.Bitmap(NPPlan.getIconPath()+item['iconDisabled'], wx.BITMAP_TYPE_PNG)
            except TypeError:
                imageDisabled = wx.NullBitmap
            pass
            if 'simple' == item['type']:
                btn = pPanel['panel'].AddSimpleButton(iId, item['rusname'], image, '')#, imageSmall)
            elif  'simpledropdown' == item['type']:
                btn = pPanel['panel'].AddDropdownButton(iId, item['rusname'], image)#, imageSmall)
            elif 'simplehybrid' == item['type']:
                btn = pPanel['panel'].AddHybridButton(iId, item['rusname'], image)#, imageSmall)
            elif 'dropdown' == item['type']:
                #btn = pPanel['panel'].AddDropdownTool(iId, image, imageDisabled, help_string=item['rusname'])
                btn = pPanel['panel'].AddTool(iId, image, imageDisabled, help_string=item['rusname'], kind=RB.RIBBON_BUTTON_DROPDOWN)
            elif 'hybrid' == item['type']:
                #btn = pPanel['panel'].AddHybridTool(iId, image, imageDisabled, help_string=item['rusname'])
                btn = pPanel['panel'].AddTool(iId, image, imageDisabled, help_string=item['rusname'], kind=RB.RIBBON_BUTTON_HYBRID)
            elif 'tool' == item['type']:
                btn = pPanel['panel'].AddTool(iId, image, imageDisabled, help_string=item['rusname'])
            elif 'toggle' == item['type']:
                #RB.RIBBON_BUTTON_TOGGLE
                btn = pPanel['panel'].AddButton(iId, item['rusname'], image, kind=RB.RIBBON_BUTTON_TOGGLE)
            #elif 'toggle' == item['type']:
            #    btn = pPanel['panel'].AddToggleButton(iId, label=item['rusname'], bitmap=image)
            self.buttons[item['name']] = btn
            self.buttonsIds[item['name']] = iId
            self.registerBackend(iId, item['click'], item['rightClick'], item['ddClick'])
            #print item['name']
            if item.has_key('disabled') and item['disabled']:
                self.disabledButtons.append(item['name'])
            if item.has_key('isLast') and item['isLast']:
                pPanel['panel'].AddSeparator()

        for i in self.disabledButtons:
            self.disableButtonByName(i)
        #self.enableButtonByName('c_dicompresetslist')
        #self.enableButtonByName('d_dicompresetsave')

        self.backendBinder()

        #self.printCurrentState()

        self.frame.ribbon.Realize()
        pass

    def setActivePageByPageName(self, name):
        page = self.pages[name]
        self.frame.ribbon.SetActivePage(page)

    def printCurrentState(self):
        print 'Pages', self.pages
        print 'hiddenPages', self.hiddenPages
        print 'Panels', self.panels
        print 'items', self.items

    def registerBackend(self, id, click, rightClick, ddClick):
        self.backendProvider[id] = {
            'backend' : click,
            'rightClick' : rightClick,
            'ddClick' : ddClick
        }

    def backendBinder(self):
        #self.frame.ribbon.Bind()
        for i in self.ribbonRunningConfig['items']:
            item = self.ribbonRunningConfig['items'][i]
            if self.ribbonRunningConfig['panels'][item['parent']]['parent'] in self.hiddenPages:
                continue
            parentPanel = self.panels[item['parent']]['panel']
            if 'simple' == item['type']:
                parentPanel.Bind(RB.EVT_RIBBONBUTTONBAR_CLICKED, self.handleBackend, id=eval(item['id']))
            elif 'toggle' == item['type']:
                parentPanel.Bind(RB.EVT_RIBBONBUTTONBAR_CLICKED, self.handleBackend, id=eval(item['id']))
            elif 'simpledropdown' == item['type']:
                parentPanel.Bind(RB.EVT_RIBBONBUTTONBAR_DROPDOWN_CLICKED, self.handleBackend, id=eval(item['id']))
            elif 'hybrid' == item['type']:
                parentPanel.Bind(RB.EVT_RIBBONTOOLBAR_CLICKED, self.handleBackend, id=eval(item['id']))
                parentPanel.Bind(RB.EVT_RIBBONTOOLBAR_DROPDOWN_CLICKED, self.handleDdClick, id=eval(item['id']))
            elif 'tool' == item['type']:
                parentPanel.Bind(RB.EVT_RIBBONTOOLBAR_CLICKED, self.handleBackend, id=eval(item['id']))
            elif 'dropdown' == item['type']:
                parentPanel.Bind(RB.EVT_RIBBONTOOLBAR_DROPDOWN_CLICKED, self.handleBackend, id=eval(item['id']))
            elif 'simplehybrid' == item['type']:
                parentPanel.Bind(RB.EVT_RIBBONBUTTONBAR_CLICKED, self.handleBackend, id=eval(item['id']))
                parentPanel.Bind(RB.EVT_RIBBONBUTTONBAR_DROPDOWN_CLICKED, self.handleDdClick, id=eval(item['id']))
            pass
            #parentPanel =
        pass

    def handleBackend(self, event):
        log.log('client.ribbon', "Item (id: %id) called backend %s" %(event.GetId(), self.backendProvider[event.GetId()]['backend']))
        getattr(self.ribbonHandler, self.backendProvider[event.GetId()]['backend'])(event)

    def handleRightClick(self, event):
        log.log('client.ribbon', "Item (id: %id) called rightClick %s" %(event.GetId(), self.backendProvider[event.GetId()]['rightClick']))
        getattr(self.ribbonHandler, self.backendProvider[event.GetId()]['rightClick'])(event)

    def handleDdClick(self, event):
        log.log('client.ribbon', "Item (id: %id) called ddClick %s" %(event.GetId(), self.backendProvider[event.GetId()]['ddClick']))
        getattr(self.ribbonHandler, self.backendProvider[event.GetId()]['ddClick'])(event)

    def bindEvents(self, bars):
        collapse, b = bars
        collapse.Bind(RB.EVT_RIBBONBUTTONBAR_CLICKED, self.onExpandCollapseClicked, id=ID_EXPAND_COLLAPSE_STAGES)
        pass

    def onExpandCollapseClicked(self, event):
        wx.GetApp().GetTopWindow().centerPanel.toggleStage()
        pass

    def update(self):
        from NPPlan.controller.client.main import getApp
        getApp().getTopWindow().Freeze()
        self.redrawRibbon()
        self.createRibbon()
        getApp().getTopWindow().changeLayout()
        getApp().getTopWindow().Thaw()
        pass

    def setSaveFunction(self, saveFunction):
        self._saveFunction = saveFunction



