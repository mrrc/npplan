# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 18.10.13
@summary: 
'''

import NPPlan
from NPPlan.controller.client.mainC.abstracts import baseController


class dynStatusBarController(baseController):
    def __init__(self, sbView, **kwargs):
        """

        @param sbView:
        @type sbView: NPPlan.view.client.main.dynStatusBar.dynStatusBar
        @return:
        """
        self._sbView = sbView
        self._cParent = kwargs.get('cParent')

    def start(self, **kwargs):
        self.setDbStatusBar()
        pass

    def setDbStatusBar(self):
        print 'Setting status bar'
        if hasattr(NPPlan, 'mongoConnection') and NPPlan.mongoConnection is not None:
            NPPlan.log('core.client.main', 'Accessing status bar', 10)
            #self.handler.statusBar.controller.setStatus(db='on')
            self._sbView.setStatus(db='on')
            try:
                tipText = _('Connected')
                for i in NPPlan.mongoConnection.__dict__['_MongoClient__nodes']:
                    tipText = '%s\nNode: %s:%d' % (tipText, i[0], i[1])
                if NPPlan.mongoConnection.__dict__['_MongoClient__use_ssl'] is not None:
                    tipText = '%s\nUse SSL: Yes' % tipText
                else:
                    tipText = '%s\nUse SSL: No' % tipText
                self._sbView.setDbTipText(tipText)
            except BaseException:
                self._sbView.setDbTipText(_('Connected, but parsing errors'))
        else:
            self._sbView.setStatus(db='off')
            self._sbView.setDbTipText(_('Disconnected'))
        pass

    def setStatus(self, db=None, inf=None):
        self._sbView.setStatus(db, inf)