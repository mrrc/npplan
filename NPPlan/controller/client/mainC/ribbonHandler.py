# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 02.11.12
@summary: 
'''

import NPPlan
import wx
from NPPlan.controller.client.mainC.abstracts import baseController
try:
    from agw import pybusyinfo as PBI
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.pybusyinfo as PBI
try:
    from pubsub import pub
except ImportError:
    from wx.lib.pubsub import pub


class ribbonHandler(baseController):
    def __init__(self):
        self.__dict__['menus'] = {}
        pass

    def clickLeftPanelCollapser(self, event=None):
        wx.GetApp().GetTopWindow().centerPanel.toggleStage()
        #from NPPlan.controller.client.main import getApp
        #getApp().getCenter().toggleStage()

    def clickDicomPresets(self, event=None):
        #from NPPlan.controller.client.mainC.stages.contouring.main import showWindowLevelPresetsPopup
        #showWindowLevelPresetsPopup(event)
        from NPPlan.controller.client.main import getHandler
        getHandler().contouringWindowLevelPresets.controller.showPopup(event)
        pass

    def click3dTest(self, event=None):
        from NPPlan.controller.client.main import getHandler
        #getHandler().threeDimViewer.controller.setCamera()
        print type(getHandler().positioningBEV.controller)
        getHandler().positioningBEV.controller.threeDim.testFunc()
        print getHandler().positioningBEV.controller._beam
        print type(getHandler().positioningBEV.controller._beam[0])
        print type(getHandler().positioningBEV.controller._beam[0]._beam)
        #getHandler().positioningBEV.controller._beam[0]._beamActor.RotateX(-20)
        #getHandler().positioningBEV.controller._beam[0]._beamActor.RotateY(-20)
        getHandler().positioningBEV.controller.threeDim._skinActor.RotateX(-30)
        getHandler().positioningBEV.controller.threeDim._skinActor.RotateY(-30)
        getHandler().positioningBEV.controller.threeDim.update()
        pass

    def clickNew(self, event):
        from NPPlan.controller.client.main import getHandler
        #getHandler().pbCallbacker.controller.doCall('test', '123', self.testBackend, 'tester')
        #getHandler().pbCallbacker.controller.doCall('getMainDbAddress', '123', self.testBackend, 'tester')
        self.handler.pbCallbacker.controller.doCall('getMainDbAddress', '123', self.testBackend, 'tester')
        print getHandler().pbCallbacker.controller.fetchByCallId('tester')

    def clickSave(self, event=None):
        #print NPPlan.config.pbFactory
        #print NPPlan.config.pbRoot
        #factory = NPPlan.config.pbFactory
        #def1 = factory.getRootObject()
        #def1.addCallbacks(got_obj1, err_obj1)
        from NPPlan.controller.client.main import getHandler
        if hasattr(getHandler().ribbon.controller, '_saveFunction'):
            busy = PBI.PyBusyInfo(_('Saving in progress'), parent=None, title=_('Please wait...'),)
            wx.Yield()
            saveFunction = getHandler().ribbon.controller._saveFunction
            saveFunction(event)
            del busy

        if event is not None:
            event.Skip()
        #def1 = NPPlan.config.pbFactory
        #def1.addCallbacks(test)

    def testBackend(self, data):
        print 'backended data'
        print data
        self.handler.pbCallbacker.controller.fetchByCallId('tester')

    def clickShowDialog(self, event=None):
        '''

        @param event:
        @type event: wx.lib.agw.ribbon.buttonbar.RibbonButtonBarEvent
        @return:
        '''
        #try:
        #    from agw import pybusyinfo as PBI
        #except ImportError:
        #    import wx.lib.agw.pybusyinfo as PBI
        #from NPPlan.model.calc.mcnp import singleImageToGridParallel

        #message = "test PBI"
        #busy = PBI.PyBusyInfo(message, parent=None, title="Really Busy",)

        #wx.Yield()

        #singleImageToGridParallel(r'C:\NPPlan2\Data\4e1ad18d5157510c64000000\Segmented\slice.1', 2)

        #del busy
        #self.handler.main.controller.getCenter().getWindow().setMessage("test", timer=500)
        NPPlan.inform('Test2')
        pass

    def stopGauge(self):
        pass

    def clickPlanExportData(self, event=None):
        """
        @param event: событие
        @type event: wx.lib.agw.ribbon.buttonbar.RibbonButtonBarEvent
        """
        menu = wx.Menu()
        menu.Append(wx.NewId(), '123')
        event.PopupMenu(menu)


def err_obj1(reason):
    #print "error getting first object", reason
    from twisted.internet import reactor
    reactor.stop()


def got_obj1(obj1):
    def2 = obj1.callRemote('test', 'tst05q')
    def2.addCallbacks(got_obj2)


def got_obj2(obj):
    print obj