# -*- coding: utf8 -*-
'''
Модуль логирования, используется единый файл лога и стандартный модуль логирования logging

Использование:

import NPPlan.controller.logger as log

log.log('module.submodule', 'Test string')
@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 3
@date: 28.06.2012
@summary: модуль логирования, обёртка над стандартным модулeм logging
'''

import NPPlan
import pprint
import StringIO
from time import strftime, localtime
import logging

verbose = 0


def objToStr(obj):
    """
    Переводит объект в строковое представление, используя встроенную библиотеку pprint
    @summary: переводит объект в строковое представление
    @param obj: объект
    @type obj: object
    @return: строка
    @rtype: string || unicode
    """
    s = StringIO.StringIO()
    pprint.pprint(obj, s)
    return s.getvalue()


def log(moduleName, data, level=0, **kwargs):
    """
    @todo: add verbosity level
    Главная функция-обёртка для логгирования. Принимает параметры имя модуля, строку для записи
    @param moduleName: имя модуля, уровни разделяются точками
    @type moduleName: string
    @param data: строка или объект для записи в лог
    @type data: string || object
    @param level: verbosity-уровень
     0 - выводить всегда
     1 и выше - выводить при соответствующем уровне
    @type level: int
    @keyword mode: вид записи
    @type mode: [con, ]
    """
    import NPPlan
    import logging
    if level > verbose:
        return
    logger = logging.getLogger('npplan.'+moduleName)
    if not isinstance(data, basestring):
        data = "\n%s" %objToStr(data)
    #_data = "%s > %s > %s\n" %(strftime("%H:%M:%S> ", localtime()), moduleName, data)
    logger.info(data)
    try:
        if (int(NPPlan.getProgramData()['appConfig']['logger']['debug']) == 1 or 'con' == kwargs.get('mode')):
            print localtime(), data
    except:
        print "%s %s: %s" % (strftime("%H:%M:%S> ", localtime()), moduleName, data)
    pass


def init(verbosity=0):
    """
    @summary: функция инициализации базового модуля логгирования 
    """
    global verbose
    verbose = verbosity
    #fileName = NPPlan.getProgramData()['appConfig']['logger']['directory']+NPPlan.getProgramData()['uuid']+'.txt'
    print NPPlan.getProgramData()
    logger = logging.getLogger('npplan')
    logger.setLevel(logging.DEBUG)

    #fh = logging.FileHandler(NPPlan.getProgramData()['appConfig']['logger']['directory']+NPPlan.getProgramData()['args'].mode+'Log.txt')
    fh = logging.FileHandler(NPPlan.config.appConfig.logger.directory+NPPlan.config.runningConfig.args.mode+'Log.txt')
    fh.setLevel(logging.DEBUG)

    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    logger.addHandler(fh)
    logger.addHandler(ch)