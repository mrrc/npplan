# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 14.05.13
@summary: 
'''


def parseObjectWithDotName(object, name, mode=0):
    """
    @param object:
    @type object: hashable
    @param name:
    @type name: str
    @return:
    """
    if 0 == mode:
        for ln in name.split('.'):
            object = object.get(ln)
        return object
    elif 1 == mode:
        for ln in name.split('.'):
            object = getattr(object, ln)
        return object


#    def test_pdots(self):
#        from NPPlan.lib import parseObjectWithDotName as pd
#        self.assertEqual('1', pd({'n' : {'m' : '1'}}, 'n.m'))
