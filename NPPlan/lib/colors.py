# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 10.06.13
@summary: 
'''


def colorFromVtkToWx(color, rtype='tuple'):
    if 'tuple' == rtype:
        newColor = (int(color[0]*255), int(color[1]*255), int(color[2]*255))
    elif 'list' == rtype:
        newColor = [int(color[0]*255), int(color[1]*255), int(color[2]*255)]
    return newColor


def colorFromWxToVtk(color, rtype='tuple'):
    if 'tuple' == rtype:
        newColor = (float(color[0])/255, float(color[1])/255, float(color[2])/255)
    elif 'list' == rtype:
        newColor = [float(color[0])/255, float(color[1])/255, float(color[2])/255]
    return newColor