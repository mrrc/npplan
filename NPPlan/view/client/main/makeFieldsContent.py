# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 19.10.12
@summary: 
'''
import NPPlan
import wx

def makeFieldsContent(panel, sizer, fields, fieldContainer):
    for field in fields:
        caption = wx.StaticText(panel, wx.ID_ANY, field['caption'])
        if field.has_key('captionFontSize'):
            captionFontSize = field['captionFontSize']
            caption.SetFont(wx.Font(captionFontSize, wx.SWISS, wx.NORMAL, wx.NORMAL))
        else:
            captionFontSize = field['captionFontSize'] = int(NPPlan.config.appConfig.client.defaultfontsize)
        caption.SetFont(wx.Font(captionFontSize, wx.SWISS, wx.NORMAL, wx.NORMAL))
        if 'text' == field['type']:
            fieldContainer[field['name']] = wx.TextCtrl(panel, wx.ID_ANY, "", size=(350,-1), name=field['name'])
        elif 'select' == field['type']:
            fieldContainer[field['name']] = wx.Choice(panel, wx.ID_ANY, choices = field['values'].values(), size=(350,-1), name=field['name'])
            fieldContainer[field['name']].SetSelection(0)
        elif 'date' == field['type']:
            fieldContainer[field['name']] = wx.DatePickerCtrl(panel, size=(350,-1),
                                            style = wx.DP_DROPDOWN
                                                  | wx.DP_SHOWCENTURY
                                                  #| wx.DP_ALLOWNONE
                                                  ,
                                            name = field['name'])
        elif 'staticText' == field['type']:
            if field.has_key('fontSize'):
                font = wx.Font(field['fontSize'], wx.SWISS, wx.NORMAL, wx.NORMAL)
            else:
                font = wx.Font(int(NPPlan.config.appConfig.client.defaultfontsize), wx.SWISS, wx.NORMAL, wx.NORMAL)
            fieldContainer[field['name']] = wx.StaticText(panel, wx.ID_ANY, field['text'])
            fieldContainer[field['name']].SetFont(font)
        elif 'button' == field['type']:
            fieldContainer[field['name']] = wx.Button(panel, wx.ID_ANY, field['caption'])
        sizer.AddMany([caption, fieldContainer[field['name']]])
    return fieldContainer