# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 19.10.12
@summary: 
'''

import NPPlan
import wx
import NPPlan.controller.logger as log

class fileDropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window

    def OnDropFiles(self, x, y, filenames):
        self.window.SetInsertionPointEnd()
        log.log('main.client', "\n%d file(s) dropped at %d,%d:\n" %
                                      (len(filenames), x, y))

        for file in filenames:
            self.window.WriteText(file + '\n')

class fileDropPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.NewId(), style=wx.BORDER_SUNKEN)
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.text = wx.TextCtrl(
                        self, -1, "",
                        style = wx.TE_MULTILINE|wx.HSCROLL|wx.TE_READONLY,
                        size = (350, 200)
                        )

        dt = fileDropTarget(self)
        self.text.SetDropTarget(dt)
        sizer.Add(self.text, 1, wx.EXPAND)
        self.SetSizer(sizer)

    def SetInsertionPointEnd(self):
        self.text.SetInsertionPointEnd()

    def WriteText(self, text):
        # @todo: text -> list
        self.text.WriteText(text)