# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 23.04.13
@summary: 
'''

import wx
from wx.lib.agw.thumbnailctrl import PILImageHandler
import wx.lib.platebtn as platebtn
import  wx.lib.scrolledpanel as scrolled
import thread
import os
import time
try:
    from agw import thumbnailctrl as TC
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.thumbnailctrl as TC


class thumbnailerBase(wx.Panel):
    def __init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition,
                 size=wx.DefaultSize,):
        wx.Panel.__init__(self, parent, id, pos, size)

        pass


class thumbnailElement(object):
    def __init__(self, parent, data):
        self._parent = parent
        self._folder = os.path.dirname(data['thumbnailPath'])
        self._filename = os.path.basename(data['thumbnailPath'])
        #print self._folder, self._filename
        self._image = wx.EmptyImage(1,1)
        self._originalSize = (-1, -1)
        self._alpha = 0
        self._highlighted = wx.EmptyImage(1,1)
        self._keys = data.copy()
        self._keys.pop('thumbnailPath')
        self._block = None
        self._textItem = None
        self._panel = None
        self._btnWxId = -1
        self._color = wx.WHITE
        #for i in data:
        #    print 'DDI', i

    def getImage(self):
        return self._image

    def setImage(self, image):
        self._image = image

    def getFullPath(self):
        return os.path.join(self._folder, self._filename)

    def dump(self):
        print self._image, self._originalSize

    def getFirstKey(self):
        return self._keys.keys()[0]

    # @todo: implement path to dicom path, thumbnail path ?
    path = property(getFullPath)

    def __getitem__(self, item):
        return self._keys[item]


class dicomScrolledThumbnail(TC.ScrolledThumbnail):
#class dicomScrolledThumbnail(TC.ThumbnailCtrl):
    def __init__(self, *args, **kwargs):
        self._backend = None
        self._dataObject = None
        TC.ScrolledThumbnail.__init__(self, *args, **kwargs)
        #TC.ThumbnailCtrl.__init__(self, *args, **kwargs)

    def setBackend(self, backend):
        self._backend = backend

    def setDataObject(self, dataObject):
        self._dataObject = dataObject

    def showDataObject(self, key):
        thumbs = []
        print 'sort by key:', key
        filenames = [self._dataObject[i].path for i in sorted(self._dataObject, key=lambda el: self._dataObject[el][key])]
        caption = [str(self._dataObject[i][key]) for i in sorted(self._dataObject, key=lambda el: self._dataObject[el][key])]
        print filenames, caption
        #print filenames
        #for fileData in filenames:
        #    caption =
        for i in range(len(filenames)):
            thumbs.append(TC.Thumb(self, os.path.dirname(filenames[i]), os.path.basename(filenames[i]), caption[i]))
        self.ShowThumbs(thumbs, caption=caption)
        pass

    def ShowThumbs(self, thumbs, caption):
        self.SetCaption(caption)

        self._isrunning = False

        self._items = thumbs
        myfiles = [thumb.GetFullFileName() for thumb in thumbs]

        items = self._items[:]

        newfiles = TC.SortFiles(items, self._items, myfiles)
        self._isrunning = True

        import sys
        if sys.platform.startswith('win'):
            thread.start_new_thread(self.ThreadImage, (newfiles, ))
        else:
            for i in range(len(newfiles)):
                self.LoadImages(newfiles[i], i)
        wx.MilliSleep(20)

        self._selectedarray = []
        self.UpdateProp()
        self.Refresh()

#class dicomScrolledThumbnail(wx.ScrolledWindow):
class dicomScrolledThumbnailOld(scrolled.ScrolledPanel):
    def __init__(self, parent, id, data, backend=None):
        # @todo: rewrite with DC
        #wx.ScrolledWindow.__init__(self, parent, id)
        scrolled.ScrolledPanel.__init__(self, parent, -1)
        self._imageHandler = PILImageHandler()
        self._data = data
        self._tSize = (128, 128)
        self._runned = False
        self._backend = backend

        self._selectedColor = wx.BLUE
        self._unSelectedColor = wx.WHITE

        self._buttonToPanel = {}

        self.readImageList()
        self.calculateBounds()
        self.setScroller()
        #self.SetAutoLayout(1)
        self.SetupScrolling()
        print 'THUMB COLS/ROWS', self._cols, self._rows
        self.fgSizer = wx.FlexGridSizer(cols=self._cols, rows=self._rows, hgap=8, vgap=8)
        self.SetSizer(self.fgSizer)
        #self.Bind(wx.EVT_PAINT, self.onPaint)
        #self.showImageList(key=-1)
        #newfiles = SortFiles(items, self._items, myfiles)

    def setSelectedColor(self, color):
        self._selectedColor = color

    def setUnSelectedColor(self, color):
        self._unSelectedColor = color

    def readImageList(self):
        #myfiles = [thumb.path for thumb in self._data]
        filesList = []
        for i in sorted(self._data, key=lambda el: self._data[el]._filename):
            filesList.append((self._data[i].path, i))
        self._isrunning = True
        thread.start_new_thread(self.threadImage, (filesList,))
        wx.MilliSleep(20)

    def threadImage(self, filesList):
        count = 0
        while count < len(filesList):
            if not self._isrunning:
                self._isrunning = False
                thread.exit()
                return

            self.loadThumbnailImage(filesList[count])
            if count < 4:
                self.Refresh()
            elif count % 4 == 0:
                self.Refresh()

            count += 1

        self._isrunning = False
        thread.exit()
        pass

    def loadThumbnailImage(self, data):
        if not self._isrunning:
            thread.exit()
            return
        path = data[0]
        key = data[1]
        img, originalsize, alpha = self._imageHandler.LoadThumbnail(path, self._tSize)

        #self._items[imagecount]
        self._data[key]._image = img
        self._data[key]._originalSize = originalsize
        self._data[key]._alpha = alpha
        self._data[key]._highlighted = self._imageHandler.HighlightImage(img, 0.5)
        pass

    def calculateBounds(self):
        # @todo: implement new calculations
        width = self.GetGrandParent().GetClientSize().GetWidth()
        #width = self.GetClientSize().GetWidth()
        print 'THUMBNAIL', width
        _tBorder = 8
        self._cols = (width - _tBorder) / (self._tSize[0] + _tBorder)
        if self._cols == 0:
            self._cols = 1

        tmpvar = (len(self._data) % self._cols and [1] or [0])[0]
        self._rows = len(self._data) / self._cols + tmpvar
        return self._cols, self._rows
        pass

    def getCaptionHeight(self, *args, **kwargs):
        # @todo: implement
        return 16

    def setScroller(self):
        self.SetVirtualSize((self._cols*(self._tSize[0] + 8) + 8,
                            self._rows*(self._tSize[1] + 8) + \
                            self.getCaptionHeight(0, self._rows) + 8))

        #self.SetSizeHints(self._cols*self._tSize[0] + self._cols*2*8 + 16,
        #                  self._rows*self._tSize[1] + self._rows*2*8 + self._rows*8)
        self.SetSizeHints(self._cols*self._tSize[0] + 16,
                          self._rows*self._tSize[1] + 16)
        #self.SetScrollRate((self._tSize[0] + 8)/4,
        #                   (self._tSize[1] + 8)/4)
        #self.SetScrollbar(orientation=wx.SB_VERTICAL, position=16, thumbSize=16, range=50)

    def showImageList(self, key=-1, reSort=True):
        if -1 == key:
            key = self._data[self._data.keys()[0]].getFirstKey()
        if reSort or not self._runned:
            self.DestroyChildren()
            self._buttonToPanel = {}
            if self._runned:
                for i in range(len(self._data)):
                    self.fgSizer.Remove(i)
            c = 0
            for i in sorted(self._data, key=lambda el: self._data[el][key]):
                self.drawSingleThumbnail(c, self._data[i], key)
                c += 1
        else:
            for i in sorted(self._data, key=lambda el: self._data[el][key]):
                self._data[i]._textItem.SetLabel(str(self._data[i][key]))
        self.setScroller()
        self.SetupScrolling()
        #self.fgSizer.Layout()
        #self.Fit()
        self.Update()
        self.Refresh()
        self._runned = True
        #self.Bind(wx.EVT_LEFT_DOWN, self.thumbnailMouseEvent)
        pass

    def drawSingleThumbnail(self, pos, data, key):
        png = data.getImage()
        bId = wx.NewId()
        pan = wx.Panel(self, -1, style=wx.BORDER_SIMPLE)
        bit = wx.StaticBitmap(pan, bId, png.ConvertToBitmap(), size=(png.GetWidth(), png.GetHeight()))
        txt = wx.StaticText(pan, -1, str(data[key]))
        font = wx.Font(14, wx.SWISS, wx.NORMAL, wx.FONTWEIGHT_BOLD)
        txt.SetFont(font)
        blockSizer = wx.BoxSizer(wx.VERTICAL)
        blockSizer.Add(bit)
        blockSizer.Add(txt, flag=wx.ALIGN_CENTER | wx.CENTER)
        pan.SetBackgroundColour(self._unSelectedColor)
        #self.Bind(wx.EVT_MOUSE_EVENTS, self.OnMouse)
        #pan.Bind(wx.EVT_LEFT_DOWN, self.thumbnailMouseEvent)
        bit.Bind(wx.EVT_LEFT_DOWN, self.thumbnailMouseClickEvent)
        #bit.Bind(wx.EVT_MOTION, self.thumbnailMouseMoveEvent)
        pan.SetSizer(blockSizer)
        #self.fgSizer.Add(blockSizer)
        self.fgSizer.Add(pan)
        data._block = blockSizer
        data._panel = pan
        data._btnWxId = bId
        data._textItem = txt
        self._buttonToPanel[bId] = pan
        pass

    def thumbnailMouseClickEvent(self, event):
        event.Skip()
        for i in self._data:
            self._data[i]._panel.SetBackgroundColour(self._data[i]._color)
            if self._data[i]._btnWxId == event.GetId():
                self._data[i]._panel.SetBackgroundColour(self._selectedColor)
                if self._backend is not None:
                    self._backend(index=i)
            self._data[i]._panel.Refresh()
        event.Skip()

    def thumbnailMouseMoveEvent(self, event):
        """
        @param event:
        @type event: wx.MouseEvent
        @return:
        """
        self._buttonToPanel[event.GetId()].SetBackgroundColour(wx.BLUE)
        self._buttonToPanel[event.GetId()].SetOwnBackgroundColour(wx.BLUE)

        self._buttonToPanel[event.GetId()].Update()
        self._buttonToPanel[event.GetId()].Refresh()
        self.Update()
        self.Refresh()
        pass


class thumbnailerWithControls(wx.Panel):
    def __init__(self, wxParent, buttons={}, data={}, selBackend=None):
        wx.Panel.__init__(self, wxParent, )

        self._buttons = buttons
        self._toggled = -1
        self._data = data
        self._selBackend = selBackend

        self.topSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.topSizer)
        k = self.makeButtons()
        self._curKey = k
        self.makeList()
        self.fillList(key=k)
        self._thumbnailer.SetMinSize((0, 500))

    def getCurrentSelected(self):
        index = self._thumbnailer.GetSelection()
        if -1 == index:
            index = 0
        key = [i for i in sorted(self._dataObject, key=lambda el: self._dataObject[el][self._curKey])][index]
        return self._data[key]
        pass

    def makeButtons(self):
        self.btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        bstyle = platebtn.PB_STYLE_DEFAULT | platebtn.PB_STYLE_SQUARE | platebtn.PB_STYLE_TOGGLE
        k = -1
        for i in sorted(self._buttons, key=lambda el: self._buttons[el]['order']):
            el = self._buttons[i]
            #print el
            el['wxId'] = wx.NewId()
            el['btnObj'] = platebtn.PlateButton(self, el['wxId'], el['text'], None, style=bstyle)
            el['btnObj'].SetPressColor(wx.BLACK)
            self.btnSizer.Add(el['btnObj'])
            if 'default' in el and 1 == el['default']:
                #el['btnObj'].SetValue(1)
                el['btnObj']._pressed = True
                el['btnObj']._SetState(platebtn.PLATE_PRESSED)
                self._toggled = el['wxId']
                k = el['key']
            self.Bind(wx.EVT_TOGGLEBUTTON, self.buttonBackend, id=el['wxId'])
            #print i
        self.topSizer.Add(self.btnSizer, 0, wx.EXPAND)
        return k
        pass

    def setBackend(self, backend):
        pass

    def setData(self, data):
        #self._thumbnailer.setDataObject()
        self._dataObject = {}
        for i in self._data:
            self._dataObject[i] = thumbnailElement(self, data=self._data[i])
        self._thumbnailer.setDataObject(self._dataObject)
        pass

    def unToggleAll(self):
        for i in self._buttons:
            if 'wxId' in self._buttons[i]:
                self._buttons[i]['btnObj']._pressed = False
                self._buttons[i]['btnObj']._SetState(platebtn.PLATE_NORMAL)

    def buttonBackend(self, event):
        event.Skip()
        bId = event.GetId()
        self.unToggleAll()
        for i in self._buttons:
            if bId == self._buttons[i]['wxId']:
                self._buttons[i]['btnObj']._pressed = True
                self._buttons[i]['btnObj']._SetState(platebtn.PLATE_PRESSED)
                if self._toggled != bId:
                    self.fillList(key=self._buttons[i]['key'])
                self._toggled = bId
                break
        event.Skip()

    def fillList(self, key=-1):
        #self._thumbnailer.showImageList(key=key, reSort=True)
        self._curKey = key
        oldSelected = self._thumbnailer.GetSelection()
        self._thumbnailer.showDataObject(key=key)
        if -1 != oldSelected:
            self._thumbnailer.SetSelection(oldSelected)
        pass

    def makeList(self):
        self._dataObject = {}
        for i in self._data:
            self._dataObject[i] = thumbnailElement(self, data=self._data[i])
            self._dataObject[i].dump()
        #self._thumbnailer = dicomScrolledThumbnail(self, wx.NewId(), data=self._dataObject, backend=self.callBackend)
        self._thumbnailer = dicomScrolledThumbnail(self, wx.NewId())
        self._thumbnailer.setDataObject(self._dataObject)
        #self._thumbnailer.setBackend(self.callBackend)
        #self._thumbnailer.ShowDir(r'C:\Users\Chernukha\Pictures\DICOM\thumb')
        self.topSizer.Add(self._thumbnailer, 0, flag=wx.EXPAND)
        #self._thumbnailer.showImageList(key=-1)
        #for i in self._dataObject:
        #    self._dataObject[i].dump()
        pass

    def callBackend(self, index=-1):
        # @deprecated
        if index != -1 and self._selBackend is not None:
            self._selBackend(index=index, data=self._data[index])
        pass


def testSelBackend(index=-1, data=None):
    print 'BACKEND: ', index, data
    pass


class Test(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, None, 1, 'OWN THUMB CONTROL WITH BUTTONS TEST', size=(520, 611))
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetBackgroundColour(wx.Color(255, 255, 255))

        self.ctrl = thumbnailerWithControls\
                (self,
                 buttons={
                     'q': {'order': 0, 'text': 'test1', 'key': 'mode', 'default': 0},
                     'p': {'order': 1, 'text': 'test2', 'key': 'image', 'default': 1},
                 },
                 data={
                     'item1': {
                         'thumbnailPath': r'C:\Users\Chernukha\Pictures\DICOM\thumb\84269306.png',
                         'image': '84269306',
                         'mode': 226,
                         },
                     'item2': {
                         'thumbnailPath': r'C:\Users\Chernukha\Pictures\DICOM\thumb\84269326.png',
                         'image': '84269326',
                         'mode': 54
                         },
                 },
                 selBackend=testSelBackend
        )

        sizer.Add(self.ctrl, 0, wx.TOP|wx.LEFT|wx.EXPAND|wx.ALL, 8)
        self.SetSizer(sizer)
        wx.FutureCall(200, wx.lib.inspection.InspectionTool().Show)


if __name__ == '__main__':
    import wx.lib.inspection
    app = wx.PySimpleApp()
    rtw = Test(app)
    rtw.Show()
    app.MainLoop()
    pass