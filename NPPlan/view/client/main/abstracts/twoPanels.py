# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 16.11.12
@summary: 
'''

import NPPlan
import wx

class panelObject(object):
    def __init__(self, panel, proportion=1, flag=1):
        self.panel = panel
        self.proportion = proportion

    def setPanel(self, panel):
        self.__dict__['panel'] = panel
    def getPanel(self):
        return self.__dict__['panel']

    def setProportion(self, prop=1):
        self.__dict__['proportion'] = int(prop)
    def getProportion(self):
        return self.__dict__['proportion']

    panel = property(getPanel, setPanel, doc='See :setPanel, :getPanel')
    proportion = property(getProportion, setProportion, doc='See :getProportion, :setProportion')

class twoPanels(wx.Panel):
    def __init__(self, parent, id=wx.ID_ANY, **kwargs):
        wx.Panel.__init__(self, parent, id, **kwargs)

    def setPanel(self, panel):
        if isinstance(panel, panelObject):
            return panel
        elif isinstance(panel, dict):
            print panel
            p = panelObject(panel.get('panel'), panel.get('proportion', 1))
            return p
        else:
            p = panelObject(panel, 1)
            return p


    def addMainPanel(self, panel):
        self.__dict__['mainPanel'] = self.setPanel(panel)
    def getMainPanel(self):
        return self.__dict__['mainPanel']
    mainPanel = property(getMainPanel, addMainPanel)

    def addRightPanel(self, panel):
        self.__dict__['rightPanel'] = self.setPanel(panel)
    def getRightPanel(self):
        return self.__dict__['rightPanel']
    rightPanel = property(getRightPanel, addRightPanel)

    def doLayout(self):
        vrSizer = wx.BoxSizer(wx.VERTICAL)
        lrSizer = wx.BoxSizer(wx.HORIZONTAL)
        lrSizer.Add(self.__dict__['mainPanel'].panel, self.__dict__['mainPanel'].proportion, wx.EXPAND)
        lrSizer.Add(self.__dict__['rightPanel'].panel, self.__dict__['rightPanel'].proportion, wx.EXPAND)
        vrSizer.Add(lrSizer, 1, wx.EXPAND)
        self.SetSizer(vrSizer)
        self.Layout()
        self.GetParent().Layout()