# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 17.01.13
@summary: 
'''

import NPPlan
import wx
import wx.lib.platebtn as platebtn
import os

class popupWindowHelper(wx.PopupWindow):
    def __init__(self, parent, style, windowNumber, params):
        wx.PopupWindow.__init__(self, parent, style)
        if windowNumber != 3:
            self.SetBackgroundColour("BLACK")
        else:
            self.SetBackgroundColour("WHITE")
        #self.SetTransparent(2)

        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        #b = wx.ToggleButton(self, -1, '123')
        #self.sizer.Add(wx.ToggleButton(self, -1, '123'), 1)
        #self.sizer.Add(wx.ToggleButton(self, -1, '456'), 1)
        bt1 = wx.Bitmap(os.path.join(NPPlan.getIconPath(), 'iconWindowExpand16.png'), wx.BITMAP_TYPE_PNG)
        singleSize = self.addButton('', bt1, None, (1, 1))
        self.addButton('456', None, None, (singleSize.width+3, 1))
        self.SetSizerAndFit(self.sizer)
        self.firstTime = True
        self.resize()
        #sz = st.GetBestSize()
        self.SetSize( (singleSize.width*2+4*2+1, singleSize.height+2) )

    def resize(self):
        self.SetSizerAndFit(self.sizer)

    def addButton(self, text, icon, type, pos):
        style = platebtn.PB_STYLE_DEFAULT
        if 'toggle' == type:
            style = style | platebtn.PB_STYLE_DEFAULT
        elif 'dd' == type:
            style = style | platebtn.PB_STYLE_DROPARROW
        tbtn = platebtn.PlateButton(self, wx.ID_ANY, text, icon, style=style, pos=pos)
        #tbtn.SetWindowVariant(wx.WINDOW_VARIANT_SMALL)
        return tbtn.GetBestSize()


