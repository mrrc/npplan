# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 18.01.13
@summary: 
'''

import NPPlan
import wx

class popupHelperView(wx.PopupWindow):
    def __init__(self, cParent, wxParent, style=wx.SIMPLE_BORDER, color="BLACK"):
        """
        @param cParent: parent controller
        @type cParent: NPPlan.controller.client.mainC.abstracts.popupHelper.popupHelperController
        @param wxParent: родительское окно wx
        @type wxParent: wx.Window
        @param style: стиль окна
        @type style: int
        @param color: цвет фона окна
        @type color: str
        """
        wx.PopupWindow.__init__(self, wxParent, style)
        self.SetBackgroundColour(color)
        self.controller = cParent

    def resize(self, quantity, singleWidth, singleHeight, hgap=2, vgap=2):
        self.SetSize(((singleWidth+hgap)*quantity, singleHeight+vgap))
        pass


