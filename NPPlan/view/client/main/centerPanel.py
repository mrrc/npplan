# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 11.09.12
@summary: 
'''

import NPPlan
import wx
from NPPlan.view.client.main.curInfo import curInfo

import wx.lib.agw.labelbook as LB
from wx.lib.agw.fmresources import *
import wx.lib.agw.infobar as IB
from NPPlan.view.client.elements import labelBook

class SamplePane(wx.Panel):
    """

    """
    def __init__(self, parent, colour, label):

        wx.Panel.__init__(self, parent, style=0)#wx.BORDER_SUNKEN)
        self.SetBackgroundColour(wx.Colour(255,255,255))
        self.created = False


class centerPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.NewId(), style=0)#wx.BORDER_SUNKEN)
#        self.Freeze()
        #self.curInfo = curInfo(self)
        self.mainStyle = INB_FIT_BUTTON | INB_LEFT | INB_DRAW_SHADOW | INB_BORDER | INB_FIT_LABELTEXT | INB_USE_PIN_BUTTON | INB_NO_RESIZE
        self.userStyle = INB_SHOW_ONLY_IMAGES | INB_SHOW_ONLY_TEXT
        self.book = None
        self.infoBar = IB.InfoBar(self)
        NPPlan.inform = self.setMessage
        self._infoButtons = {}
        #self.imageList = self.createImageList()
        pass

    def getBookStyle(self):
        return self.mainStyle | self.userStyle

    def setBookStyle(self, style):
        self.userStyle = style

    def _unbindOldInfoButtons(self):
        for i in self._infoButtons:
            self.Unbind(wx.EVT_BUTTON, id=i)

    def _deleteOldInfoButtons(self):
        for i in self._infoButtons:
            self.infoBar.RemoveButton(i)
        self._infoButtons.clear()
        pass

    def _addButtonsToMessage(self):
        for bId in self._infoButtons:
            btnText = self._infoButtons[bId]['text']
            btnBackend = self._infoButtons[bId]['backend']
            if 'bitmap' in self._infoButtons:
                btnBitmap = self._infoButtons[bId]['bitmap']
            else:
                btnBitmap = wx.NullBitmap
            self.infoBar.AddButton(bId, btnText, btnBitmap)
            self.Bind(wx.EVT_BUTTON, btnBackend, id=bId)
        pass

    def setMessage(self, msg, style=wx.ICON_INFORMATION, timer=0, buttons=[]):
        '''

        @param msg: строка сообщения
        @type msg: str
        @param style: стиль иконки, wx.ICON_XXX
        @type style: int
        @param timer: время до исчезновения (мс), 0 если не закрывать автоматически
        @type timer: int
        @param buttons: пока не работает из-за внутренней реализации RemoveButton() (???)
        @todo: buttons
        @return:
        '''
        #print 'IB before', self._infoButtons
        #if len(self._infoButtons) > 0:
        #    self._unbindOldInfoButtons()
        #    self._deleteOldInfoButtons()
        #if len(buttons) > 0:
        #    for i, btn in enumerate(buttons):
        #        if not ('text' in btn and 'backend' in btn):
        #            continue
        #        bId = wx.NewId()
        #        self._infoButtons[bId] = btn
        #        pass
        #    self._addButtonsToMessage()
        self.infoBar.ShowMessage(msg, style)
        if timer > 0:
            wx.CallLater(timer, self.closeMessage)
        #print 'IB after', self._infoButtons
        pass

    def closeMessage(self, event=None):
        self.infoBar.Dismiss()

    def startBook(self, **kwargs):
        self.Freeze()
        if self.book is not None:
            self.book.DeleteAllPages()
            #self.book.Destroy()
            #self.GetSizer().Detach(0)
        if (kwargs.get('userStyle')):
            self.setBookStyle(kwargs.get('userStyle'))
        if self.book is None:
            #self.book = LB.LabelBook(self, wx.NewId(), agwStyle=self.getBookStyle())
            self.book = labelBook(self, wx.NewId(), agwStyle=self.getBookStyle(), caller='plan')
        if (kwargs.get('imageList')):
            self.book.AssignImageList(kwargs.get('imageList'))
            pass
        self.Thaw()

    def getCurrentPage(self):
        return self.book.GetSelection()

    def doLayout(self):
        self.Freeze()
        vSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(vSizer)

        #vSizer.Add(self.curInfo, 1, wx.EXPAND|wx.ALL)
        vSizer.Add(self.infoBar, 1, wx.EXPAND|wx.ALL)
        vSizer.Add((1, 1))
        vSizer.Add(self.book, 21, wx.EXPAND)

        vSizer.Layout()
        self.Layout()
        self.Thaw()

    def toggleStage(self, status=-1):
        self.book._pages._nPinButtonStatus = INB_PIN_NONE
        self.book._pages._bCollapsed = not self.book._pages._bCollapsed

        if self.book._pages._bCollapsed:
            # Save the current tab area width
            self.book._pages._tabAreaSize = self.book._pages.GetSize()
            self.book._pages.SetSizeHints(20, self.book._pages._tabAreaSize.y)
        else:
            self.book._pages.SetSizeHints(self.book._pages._tabAreaSize.x, -1)

        self.Freeze()
        self.book._pages.GetParent().GetSizer().Layout()
        self.book.GetParent().GetSizer().Layout()
        self.Thaw()
        self.book._pages.Refresh()
        return