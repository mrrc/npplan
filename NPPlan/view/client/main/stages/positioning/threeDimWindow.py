# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 24.12.12
@summary: 
'''

import NPPlan

from vtk.wx.wxVTKRenderWindowInteractor import *
import wx
import os
try:
    from agw import flatmenu as FM
    from agw.artmanager import ArtManager, RendererBase, DCSaver
    from agw.fmresources import ControlFocus, ControlPressed
    from agw.fmresources import FM_OPT_SHOW_CUSTOMIZE, FM_OPT_SHOW_TOOLBAR, FM_OPT_MINIBAR
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.flatmenu as FM
    from wx.lib.agw.artmanager import ArtManager, RendererBase, DCSaver
    from wx.lib.agw.fmresources import ControlFocus, ControlPressed
    from wx.lib.agw.fmresources import FM_OPT_SHOW_CUSTOMIZE, FM_OPT_SHOW_TOOLBAR, FM_OPT_MINIBAR

from NPPlan.view.client.main.abstracts import vtkBaseWindow

#class threeDimWindow(wx.ScrolledWindow):
class threeDimWindow(vtkBaseWindow):
    def __init__(self, parent):
        vtkBaseWindow.__init__(self, parent)
        #wx.ScrolledWindow.__init__(self, parent, wx.ID_ANY)
        #vtkBaseWindow.__init__(self, parent)
