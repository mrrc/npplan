# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 13.12.12
@summary: 
'''

__all__ = [
    'main',
    'viewer',
    'rightPanel',
    'fourWindows'
]

from main import *
from viewer import *
from rightPanel import *
from fourWindows import *