# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 16.10.12
@summary: 
'''

import NPPlan
import wx
import  wx.lib.scrolledpanel as scrolled
try:
    from agw import pycollapsiblepane as PCP
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.pycollapsiblepane as PCP
from NPPlan.view.client.main.makeFieldsContent import makeFieldsContent

class _abstract(scrolled.ScrolledPanel):
#class _abstract(wx.Panel):
    def __init__(self, parent, id):
        scrolled.ScrolledPanel.__init__(self, parent, id, style=wx.TAB_TRAVERSAL)
        #wx.Panel.__init__(self, parent, id, style=wx.TAB_TRAVERSAL)

        #self.SetAutoLayout(1)
        #self.SetupScrolling()


    def setFields(self, fields):
        self.cp = {}
        self.fields = {}
        self.Freeze()
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        captions = fields['captions']
        fields.pop('captions')
        for i in captions:
            caption = wx.StaticText(self, wx.NewId(), captions[i])
            caption.SetFont(wx.Font(15, wx.SWISS, wx.NORMAL, wx.NORMAL))
            mainSizer.Add(caption)
            #self.cp[i] = PCP.PyCollapsiblePane(self, wx.NewId(), label=captions[i], agwStyle=wx.CP_GTK_EXPANDER)
            #self.cp[i].Bind(wx.EVT_COLLAPSIBLEPANE_CHANGED, self.onPaneChanged)
            #self.cp[i].SetExpanderDimensions(3,6)
            #self.makeFieldsContent(self.cp[i], fields[i])
            self.makeFieldsContent(self, fields[i], mainSizer)
        #mainSizer.Add(self.cp[i], 0, wx.EXPAND)
        self.SetSizer(mainSizer)
        #self.Layout()
        self.Thaw()
        self.Layout()

    def onPaneChanged(self, event=None):
        self.Layout()

    def makeFieldsContent(self, pane, fields, sizer):
        self.fields = makeFieldsContent(pane, sizer, fields, self.fields)

    def collect(self):
        for i in self.fields:
            print self.fields[i].GetText()