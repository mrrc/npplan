# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 10.07.13
@summary: 
'''

import NPPlan
from main import calculateBase


class calculateGeant(calculateBase):
    def __init__(self, parent, choiceStatusExplorerChoices=[]):
        calculateBase.__init__(self, parent, choiceStatusExplorerChoices, headerText=_('Geant calculation'))