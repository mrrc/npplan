# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 17.06.13
@summary: 
'''

import NPPlan
import wx
import wx.aui
import  wx.gizmos   as  gizmos


class materialsView(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self._controller = None

        #self.nb = wx.aui.AuiNotebook(self)
        self.nb = wx.Notebook(self, wx.ID_ANY, style=wx.BK_DEFAULT)
        self.isotopesPage = isotopesList(self.nb)
        self.nb.AddPage(self.isotopesPage, _('Isotopes list'))
        self.materialsPage = materialsList(self.nb)
        self.nb.AddPage(self.materialsPage, _('Materials list'))
        sizer = wx.BoxSizer()
        sizer.Add(self.nb, 1, wx.EXPAND)
        self.SetSizer(sizer)
        self.SetMinSize((900, 700))

    def assignController(self, controller):
        self._controller = controller
        pass


class isotopesList(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self._controller = None
        self.SetMinSize((800, 600))

        mainSizer = wx.BoxSizer(wx.HORIZONTAL)

        self.tree = gizmos.TreeListCtrl(self, -1, style =
                                        wx.TR_DEFAULT_STYLE
                                        #| wx.TR_HAS_BUTTONS
                                        #| wx.TR_TWIST_BUTTONS
                                        | wx.TR_ROW_LINES
                                        | wx.TR_COLUMN_LINES
                                        #| wx.TR_NO_LINES
                                        | wx.TR_FULL_ROW_HIGHLIGHT
                                   )
        mainSizer.Add(self.tree, 1, wx.EXPAND)
        self.tree.AddColumn(_('Isotope'))
        self.tree.AddColumn(_('Info'))
        self.tree.SetMainColumn(0)

        dataSizer = wx.BoxSizer(wx.VERTICAL)

        topbox = wx.StaticBox(self, -1, _('Material data'))
        topboxsizer = wx.StaticBoxSizer(topbox, wx.VERTICAL)
        topboxinnersizer = wx.FlexGridSizer(cols=2, hgap=8, vgap=8)
        nameLabel = wx.StaticText(self, wx.ID_ANY, _('Name'))
        self._name = wx.TextCtrl(self, wx.ID_ANY, size=(250, -1))
        nameLabel2 = wx.StaticText(self, wx.ID_ANY, _('Russian name'))
        self._name2 = wx.TextCtrl(self, wx.ID_ANY, size=(250, -1))
        descriptionLabel = wx.StaticText(self, wx.ID_ANY, _('Description'))
        self._description = wx.TextCtrl(self, wx.ID_ANY, size=(250, 100), style=wx.TE_MULTILINE)
        topboxinnersizer.AddMany([
            nameLabel, self._name,
            nameLabel2, self._name2,
            descriptionLabel, self._description,
        ])
        topboxsizer.Add(topboxinnersizer)
        dataSizer.Add(topboxsizer)

        topbox2 = wx.StaticBox(self, -1, _('Mendeleev info'))
        topbox2sizer = wx.StaticBoxSizer(topbox2, wx.VERTICAL)
        topbox2innersizer = wx.FlexGridSizer(cols=2, hgap=8, vgap=8)
        symbolLabel = wx.StaticText(self, wx.ID_ANY, _('Symbol'))
        self._symbol = wx.TextCtrl(self, wx.ID_ANY, size=(250, -1))
        atomicNumLabel = wx.StaticText(self, wx.ID_ANY, _('Z'))
        self._atomicNum = wx.TextCtrl(self, wx.ID_ANY, size=(250, -1))
        atomicMassLabel = wx.StaticText(self, wx.ID_ANY, _('Atomic mass, g / mole'))
        self._atomicMass = wx.TextCtrl(self, wx.ID_ANY, size=(250, -1))
        topbox2innersizer.AddMany([
            symbolLabel, self._symbol,
            atomicNumLabel, self._atomicNum,
            atomicMassLabel, self._atomicMass,
        ])
        topbox2sizer.Add(topbox2innersizer)
        dataSizer.Add(topbox2sizer)

        topbox3 = wx.StaticBox(self, -1, _('Geant representation'))
        topbox3sizer = wx.StaticBoxSizer(topbox3, wx.VERTICAL)
        topbox3innersizer = wx.FlexGridSizer(cols=2, hgap=8, vgap=8)
        geantStringLabel = wx.StaticText(self, wx.ID_ANY, _('Geant string'))
        self._geantString = wx.TextCtrl(self, wx.ID_ANY, style=wx.TE_MULTILINE, size=(500, 100))
        topbox3innersizer.AddMany([
            geantStringLabel, self._geantString
        ])
        topbox3sizer.Add(topbox3innersizer)
        dataSizer.Add(topbox3sizer)

        topbox4 = wx.StaticBox(self, -1, _('MCNP representation'))
        topbox4sizer = wx.StaticBoxSizer(topbox4, wx.VERTICAL)
        topbox4innersizer = wx.FlexGridSizer(cols=2, hgap=8, vgap=8)
        mcnpLibraryLabel = wx.StaticText(self, wx.ID_ANY, _('MCNP data library'))
        self._mcnpLibrary = wx.TextCtrl(self, wx.ID_ANY, size=(250, -1))
        mcnpStringLabel = wx.StaticText(self, wx.ID_ANY, _('MCNP string'))
        self._mcnpString = wx.TextCtrl(self, wx.ID_ANY, style=wx.TE_MULTILINE, size=(250, 100))
        topbox4innersizer.AddMany([
            mcnpLibraryLabel, self._mcnpLibrary,
            mcnpStringLabel, self._mcnpString,
        ])
        topbox4sizer.Add(topbox4innersizer)
        dataSizer.Add(topbox4sizer)

        self._doButton = wx.Button(self, wx.ID_ANY, _('DO!'))
        dataSizer.Add(self._doButton)

        mainSizer.Add(dataSizer, 3, wx.EXPAND)
        self.SetSizer(mainSizer)

    def assignController(self, controller):
        """

        @param controller:
        @type controller: NPPlan.controller.client.mainC.stages.configuration.controller.isotopesPostController
        @return:
        """
        self._controller = controller
        pass


class materialsList(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, style=wx.TAB_TRAVERSAL)
        self._controller = None
        #wx.ScrolledWindow.__init__(self, parent, )
        #self.SetVirtualSize((900, 600))
        #self.SetScrollRate(20,20)
        self.SetMinSize((800, 600))

        self.list = wx.ListBox(self, wx.ID_ANY, size=(400, -1), choices=[], style=wx.LB_SINGLE)
        listSizer = wx.BoxSizer(wx.VERTICAL)
        listSizer.Add(wx.StaticText(self, wx.ID_ANY, _('List of materials')))
        self._addMatButton = wx.Button(self, wx.ID_ANY, _('Add new materials'))
        listSizer.Add(self.list, 1, wx.EXPAND)
        listSizer.Add(self._addMatButton, 0, wx.EXPAND)

        constructorSizer = wx.BoxSizer(wx.VERTICAL)
        constructorSizer.Add(wx.StaticText(self, wx.ID_ANY, _('Constructor')))

        topbox1 = wx.StaticBox(self, -1, _('Material properties'))
        topbox1sizer = wx.StaticBoxSizer(topbox1, wx.VERTICAL)
        topbox1innersizer = wx.FlexGridSizer(cols=2, hgap=8, vgap=8)
        nameLabel = wx.StaticText(self, wx.ID_ANY, _('Material name'))
        self._name = wx.TextCtrl(self, wx.ID_ANY, size=(250, -1))
        rusNameLabel = wx.StaticText(self, wx.ID_ANY, _('Material russian name'))
        self._rusName = wx.TextCtrl(self, wx.ID_ANY, size=(250, -1))
        densityLabel = wx.StaticText(self, wx.ID_ANY, _('Density'))
        self._density = wx.TextCtrl(self, wx.ID_ANY, size=(200, -1))
        self._densityUnits = wx.Choice(self, -1, choices=[_('g/cm**3'), _('mg/cm**3')])
        self._densityUnits.SetSelection(0)
        denSizer = wx.BoxSizer(wx.HORIZONTAL)
        denSizer.Add(self._density)
        denSizer.Add(self._densityUnits)
        topbox1innersizer.AddMany([
            nameLabel, self._name,
            rusNameLabel, self._rusName,
            densityLabel, denSizer,
        ])
        topbox1sizer.Add(topbox1innersizer)
        constructorSizer.Add(topbox1sizer)

        topbox2 = wx.StaticBox(self, -1, _('Composition map'))
        topbox2sizer = wx.StaticBoxSizer(topbox2, wx.VERTICAL)
        topbox2innersizer = wx.BoxSizer(wx.HORIZONTAL)
        isotopeLabel = wx.StaticText(self, wx.ID_ANY, _('Isotope'))
        self._isotope = wx.Choice(self, wx.ID_ANY)
        proportionLabel = wx.StaticText(self, wx.ID_ANY, _('Proprotion, %'))
        self._proportion = wx.TextCtrl(self, wx.ID_ANY)
        self.addIsotopeButton = wx.Button(self, wx.ID_ANY, _('Add to list'))
        topbox2innersizer.AddMany([
            isotopeLabel, self._isotope,
            proportionLabel, self._proportion,
            self.addIsotopeButton
        ])
        topbox2sizer.Add(topbox2innersizer)
        self._isotopesList = wx.ListCtrl(self, wx.ID_ANY, size=(415, 200),
                                         style=wx.LC_REPORT | wx.LC_HRULES | wx.LC_VRULES)
        self._isotopesList.InsertColumn(0, _('Isotope atomic number / isotope name'))
        self._isotopesList.InsertColumn(1, _('Proportion, %'))
        self._isotopesList.SetColumnWidth(0, 225)
        self._isotopesList.SetColumnWidth(1, 175)
        #self._isotopesList.SetItemCount(1000000)
        topbox2sizer.Add(self._isotopesList)
        constructorSizer.Add(topbox2sizer)

        topbox5 = wx.StaticBox(self, -1, _('Planning data'))
        topbox5sizer = wx.StaticBoxSizer(topbox5, wx.VERTICAL)
        topbox5innersizer = wx.FlexGridSizer(cols=4, hgap=8, vgap=8)
        ctLowValueLabel = wx.StaticText(self, wx.ID_ANY, _('CT low'))
        self._ctLowValue = wx.TextCtrl(self, wx.ID_ANY, size=(100, -1))
        ctHighValueLabel = wx.StaticText(self, wx.ID_ANY, _('CT high'))
        self._ctHighValue = wx.TextCtrl(self, wx.ID_ANY, size=(100, -1))
        contourIntValueLabel = wx.StaticText(self, wx.ID_ANY, _('Plan contour int value'))
        self._contourIntValue = wx.TextCtrl(self, wx.ID_ANY, size=(250, -1))
        topbox5innersizer.AddMany([
            ctLowValueLabel, self._ctLowValue, ctHighValueLabel, self._ctHighValue,
            contourIntValueLabel, self._contourIntValue, (1,1), (1,1),
        ])
        topbox5sizer.Add(topbox5innersizer)
        constructorSizer.Add(topbox5sizer)

        topbox3 = wx.StaticBox(self, -1, _('Geant representation'))
        topbox3sizer = wx.StaticBoxSizer(topbox3, wx.VERTICAL)
        topbox3innersizer = wx.FlexGridSizer(cols=2, hgap=8, vgap=8)
        geantStringLabel = wx.StaticText(self, wx.ID_ANY, _('Geant string'))
        self._geantString = wx.TextCtrl(self, wx.ID_ANY, style=wx.TE_MULTILINE, size=(500, 100))
        topbox3innersizer.AddMany([
            geantStringLabel, self._geantString
        ])
        topbox3sizer.Add(topbox3innersizer)
        constructorSizer.Add(topbox3sizer)

        topbox4 = wx.StaticBox(self, -1, _('MCNP representation'))
        topbox4sizer = wx.StaticBoxSizer(topbox4, wx.VERTICAL)
        topbox4innersizer = wx.FlexGridSizer(cols=2, hgap=8, vgap=8)
        mcnpCellRepresentationLabel = wx.StaticText(self, wx.ID_ANY, _('MCNP cell string'))
        self._mcnpCellRepresentation = wx.TextCtrl(self, wx.ID_ANY, size=(350, -1))
        mcnpMatStringLabel = wx.StaticText(self, wx.ID_ANY, _('MCNP mat card string'))
        self._mcnpMatString = wx.TextCtrl(self, wx.ID_ANY, style=wx.TE_MULTILINE, size=(250, 100))
        topbox4innersizer.AddMany([
            mcnpCellRepresentationLabel, self._mcnpCellRepresentation,
            mcnpMatStringLabel, self._mcnpMatString,
        ])
        topbox4sizer.Add(topbox4innersizer)
        constructorSizer.Add(topbox4sizer)

        self._saveButton = wx.Button(self, wx.ID_ANY, _('Save'))
        self._previewButton = wx.Button(self, wx.ID_ANY, _('Preview'))
        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer.AddMany([self._previewButton, self._saveButton])
        constructorSizer.Add(btnSizer)

        mainSizer = wx.BoxSizer(wx.HORIZONTAL)

        mainSizer.Add(listSizer, 1, wx.EXPAND)
        mainSizer.Add(constructorSizer, 3, wx.EXPAND)
        self.SetSizer(mainSizer)

    def assignController(self, controller):
        self._controller = controller
        pass


