# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 10.06.13
@summary: 
'''

import NPPlan
import wx
import  wx.gizmos   as  gizmos
import  wx.lib.colourselect as  csel


class organsView(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self._controller = None

        sizer = wx.BoxSizer(wx.VERTICAL)
        organsLabel = wx.StaticText(self, wx.NewId(), _('Organs label description'))
        self.tree = gizmos.TreeListCtrl(self, -1, style =
                                        wx.TR_DEFAULT_STYLE
                                        #| wx.TR_HAS_BUTTONS
                                        #| wx.TR_TWIST_BUTTONS
                                        | wx.TR_ROW_LINES
                                        | wx.TR_COLUMN_LINES
                                        #| wx.TR_NO_LINES
                                        | wx.TR_FULL_ROW_HIGHLIGHT,
                                   )

        self.tree.SetMinSize((800, -1))

        self._editButton = wx.Button(self, wx.NewId(), _('Add/save button'))
        self._delButton = wx.Button(self, wx.NewId(), _('Delete button'))

        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer.AddMany([self._editButton, self._delButton])

        box = wx.StaticBox(self, -1, _('Organ data'))
        bsizer = wx.StaticBoxSizer(box, wx.VERTICAL)
        elemsSizer = wx.FlexGridSizer(cols=4, hgap=8, vgap=8)

        nameLabel = wx.StaticText(self, wx.ID_ANY, _('Latin name'))
        self._originalName = wx.TextCtrl(self, wx.NewId(), size=(300, -1))
        rusNameLabel = wx.StaticText(self, wx.ID_ANY, _('Russian name'))
        self._rusName = wx.TextCtrl(self, wx.NewId(), size=(300, -1))
        uidLabel = wx.StaticText(self, wx.ID_ANY, _('Identificator'))
        self._uid = wx.TextCtrl(self, wx.NewId(), size=(300, -1))
        self._uid.Enable(False)
        groupLabel = wx.StaticText(self, wx.ID_ANY, _('Group'))
        self._group = wx.Choice(self, wx.NewId(), choices=[_('Contouring organs'), _('Autosegmentation organs')])
        self._group.SetSelection(0)
        self._group.Enable(False)
        enabledLabel = wx.StaticText(self, wx.ID_ANY, _('Enabled'))
        self._enabled = wx.CheckBox(self, wx.NewId())
        colorLabel = wx.StaticText(self, wx.ID_ANY, _('Color'))
        self._color = csel.ColourSelect(self, -1)
        opacityLabel = wx.StaticText(self, wx.ID_ANY, _('Opacity, %'))
        self._opacity = wx.TextCtrl(self, wx.ID_ANY, '100')
        densityLabel = wx.StaticText(self, wx.ID_ANY, _('Density, g/cm**3'))
        self._density = wx.TextCtrl(self, wx.ID_ANY, '')

        # add elements cmap

        elemsSizer.AddMany([
            groupLabel, self._group,
            uidLabel, self._uid,
            nameLabel, self._originalName,
            enabledLabel, self._enabled,
            rusNameLabel, self._rusName,
            (1, 1), (1, 1),
            colorLabel, self._color,
            (1, 1), (1, 1),
            opacityLabel, self._opacity,
            (1, 1), (1, 1),
            densityLabel, self._density,
            (1, 1), (1, 1),

        ])

        bsizer.Add(elemsSizer, 0, wx.TOP|wx.LEFT,)

        sizer.Add(organsLabel, 0, wx.EXPAND)
        sizer.Add(self.tree, 0, wx.ALL | wx.EXPAND)
        sizer.Add(btnSizer, 0, wx.EXPAND)
        sizer.Add(bsizer, 0, wx.EXPAND | wx.ALL)
        self.SetSizer(sizer)
        self.SetMinSize((800, 600))

    def assignController(self, controller):
        """

        @param controller:
        @type controller: NPPlan.controller.client.mainC.stages.configuration.controller.organsPostController
        @return:
        """
        self._controller = controller

    def clearItemsValues(self):
        self._originalName.SetValue('')
        self._rusName.SetValue('')
        self._density.SetValue('')
        self._opacity.SetValue('100')
        self._color.SetValue(wx.RED)
        self._enabled.SetValue(1)

