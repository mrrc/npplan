# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 28.04.14
@summary: 
'''
import wx
import wx.grid
import wx.richtext


class patientRadView( wx.Panel ):
    def __init__( self, parent ):
        wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, style = wx.TAB_TRAVERSAL )
        
        bSizer20 = wx.BoxSizer( wx.VERTICAL )
        
        sbSizer23 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Врачебная информация" ), wx.VERTICAL )
        
        bSizer22 = wx.BoxSizer( wx.HORIZONTAL )
        
        self.m_staticText38 = wx.StaticText( self, wx.ID_ANY, u"Лечащий персонал", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText38.Wrap( -1 )
        bSizer22.Add( self.m_staticText38, 2, wx.ALIGN_CENTER|wx.ALL, 5 )
        
        self.iDoctorsList = wx.grid.Grid( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
        
        # Grid
        self.iDoctorsList.CreateGrid( 5, 5 )
        self.iDoctorsList.EnableEditing( True )
        self.iDoctorsList.EnableGridLines( True )
        self.iDoctorsList.EnableDragGridSize( False )
        self.iDoctorsList.SetMargins( 0, 0 )
        
        # Columns
        self.iDoctorsList.EnableDragColMove( False )
        self.iDoctorsList.EnableDragColSize( True )
        self.iDoctorsList.SetColLabelSize( 30 )
        self.iDoctorsList.SetColLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
        
        # Rows
        self.iDoctorsList.EnableDragRowSize( True )
        self.iDoctorsList.SetRowLabelSize( 80 )
        self.iDoctorsList.SetRowLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
        
        # Label Appearance
        
        # Cell Defaults
        self.iDoctorsList.SetDefaultCellAlignment( wx.ALIGN_LEFT, wx.ALIGN_TOP )
        bSizer22.Add( self.iDoctorsList, 7, wx.ALL|wx.EXPAND, 5 )
        
        
        sbSizer23.Add( bSizer22, 1, wx.EXPAND, 5 )
        
        bSizer24 = wx.BoxSizer( wx.HORIZONTAL )
        
        self.m_staticText37 = wx.StaticText( self, wx.ID_ANY, u"Отметки", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText37.Wrap( -1 )
        bSizer24.Add( self.m_staticText37, 2, wx.ALIGN_CENTER|wx.ALL, 5 )
        
        self.iNotes = wx.richtext.RichTextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0|wx.VSCROLL|wx.HSCROLL|wx.NO_BORDER|wx.WANTS_CHARS )
        bSizer24.Add( self.iNotes, 7, wx.EXPAND |wx.ALL, 5 )
        
        
        sbSizer23.Add( bSizer24, 1, wx.EXPAND, 5 )
        
        
        bSizer20.Add( sbSizer23, 1, wx.EXPAND, 5 )
        
        sbSizer24 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Планы облучения и планируемые сеансы" ), wx.VERTICAL )
        
        bSizer25 = wx.BoxSizer( wx.HORIZONTAL )
        
        self.iPlansList = wx.grid.Grid( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
        
        # Grid
        self.iPlansList.CreateGrid( 5, 5 )
        self.iPlansList.EnableEditing( True )
        self.iPlansList.EnableGridLines( True )
        self.iPlansList.EnableDragGridSize( False )
        self.iPlansList.SetMargins( 0, 0 )
        
        # Columns
        self.iPlansList.EnableDragColMove( False )
        self.iPlansList.EnableDragColSize( True )
        self.iPlansList.SetColLabelSize( 30 )
        self.iPlansList.SetColLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
        
        # Rows
        self.iPlansList.EnableDragRowSize( True )
        self.iPlansList.SetRowLabelSize( 80 )
        self.iPlansList.SetRowLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
        
        # Label Appearance
        
        # Cell Defaults
        self.iPlansList.SetDefaultCellAlignment( wx.ALIGN_LEFT, wx.ALIGN_TOP )
        bSizer25.Add( self.iPlansList, 7, wx.ALL|wx.EXPAND, 5 )
        
        bSizer26 = wx.BoxSizer( wx.VERTICAL )
        
        self.iBtnAddPlan = wx.Button( self, wx.ID_ANY, u"Создать новый план", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
        bSizer26.Add( self.iBtnAddPlan, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
        
        self.iBtnAssignPlan = wx.Button( self, wx.ID_ANY, u"Привязать план", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
        bSizer26.Add( self.iBtnAssignPlan, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
        
        self.iBtnRecalc = wx.Button( self, wx.ID_ANY, u"Пересчитать план", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
        bSizer26.Add( self.iBtnRecalc, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
        
        self.iBtnTomo = wx.Button( self, wx.ID_ANY, u"Текущая томография", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
        bSizer26.Add( self.iBtnTomo, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
        
        
        bSizer25.Add( bSizer26, 2, wx.EXPAND, 5 )
        
        
        sbSizer24.Add( bSizer25, 1, wx.EXPAND, 5 )
        
        
        bSizer20.Add( sbSizer24, 1, wx.EXPAND, 5 )
        
        sbSizer25 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Проведенное лечение" ), wx.VERTICAL )
        
        bSizer28 = wx.BoxSizer( wx.HORIZONTAL )
        
        self.iTreatmentList = wx.grid.Grid( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
        
        # Grid
        self.iTreatmentList.CreateGrid( 5, 5 )
        self.iTreatmentList.EnableEditing( True )
        self.iTreatmentList.EnableGridLines( True )
        self.iTreatmentList.EnableDragGridSize( False )
        self.iTreatmentList.SetMargins( 0, 0 )
        
        # Columns
        self.iTreatmentList.EnableDragColMove( False )
        self.iTreatmentList.EnableDragColSize( True )
        self.iTreatmentList.SetColLabelSize( 30 )
        self.iTreatmentList.SetColLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
        
        # Rows
        self.iTreatmentList.EnableDragRowSize( True )
        self.iTreatmentList.SetRowLabelSize( 80 )
        self.iTreatmentList.SetRowLabelAlignment( wx.ALIGN_CENTRE, wx.ALIGN_CENTRE )
        
        # Label Appearance
        
        # Cell Defaults
        self.iTreatmentList.SetDefaultCellAlignment( wx.ALIGN_LEFT, wx.ALIGN_TOP )
        bSizer28.Add( self.iTreatmentList, 7, wx.ALL|wx.EXPAND, 5 )
        
        bSizer29 = wx.BoxSizer( wx.VERTICAL )
        
        self.iBtnOpenIrrLog = wx.Button( self, wx.ID_ANY, u"Открыть лог облучения", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
        bSizer29.Add( self.iBtnOpenIrrLog, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
        
        self.irrBtnDoIrradiation = wx.Button( self, wx.ID_ANY, u"Провести сеанс", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
        bSizer29.Add( self.irrBtnDoIrradiation, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
        
        
        bSizer28.Add( bSizer29, 2, wx.EXPAND, 5 )
        
        
        sbSizer25.Add( bSizer28, 1, wx.EXPAND, 5 )
        
        
        bSizer20.Add( sbSizer25, 1, wx.EXPAND, 5 )
        
        
        self.SetSizer( bSizer20 )
        self.Layout()