# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 28.04.14
@summary: 
'''

import wx
import wx.lib.agw.flatnotebook as fnb
import NPPlan


class patientTopPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        self.mainSizer = wx.BoxSizer(wx.VERTICAL)

        bookStyle = fnb.FNB_NODRAG | fnb.FNB_VC8 | fnb.FNB_NO_X_BUTTON | fnb.FNB_SMART_TABS
        self.book = fnb.FlatNotebook(self, wx.ID_ANY, agwStyle=bookStyle)

        self.mainSizer.Add(self.book, 1, wx.EXPAND)

        self.SetSizer(self.mainSizer)
        self.Layout()

        self.Bind(fnb.EVT_FLATNOTEBOOK_PAGE_CHANGING, self.onPageChanging)
        self.Bind(fnb.EVT_FLATNOTEBOOK_PAGE_CHANGED, self.onPageChanged)

    def onPageChanging(self, event):
        """

        @param event:
        @type event: wx.lib.agw.flatnotebook.FlatNotebookEvent
        @return:
        """
        event.StopPropagation()

    def onPageChanged(self, event):
        """

        @param event:
        @type event: wx.lib.agw.flatnotebook.FlatNotebookEvent
        @return:
        """
        event.StopPropagation()

    def addPage(self, pageObject, name):
        self.Freeze()
        self.book.AddPage(pageObject, name)
        self.Thaw()

    def getCurrentPage(self):
        return self.book.GetCurrentPage()