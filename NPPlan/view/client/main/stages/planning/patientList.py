# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 28.04.14
@summary: 
'''

import NPPlan
import wx
import wx.dataview as dv
try:
    import wx.lib.platebtn as platebtn
except ImportError:
    import platebtn


class patientListView(wx.Panel):
    """

    @param parent:
    @type parent: wx.Panel
    """

    def __init__(self, parent):
        """

        @param parent:
        @type parent: wx.Panel
        @return:
        """
        wx.Panel.__init__(self, parent)
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        searchSizer = wx.BoxSizer(wx.HORIZONTAL)
        self._search = wx.SearchCtrl(self, size=(500,-1), style=wx.TE_PROCESS_ENTER)
        self._search.ShowSearchButton(True)
        self._search.ShowCancelButton(True)
        self._search.SetMenu(None)
        searchSizer.Add(self._search, 0, wx.ALL)
        #print NPPlan.getIcon('iconSearch16.png')
        iconSearchURL = u'C:\\Users\\Chernukha\\Pictures\\Search-icon.png'
        self._searchExtraButton = platebtn.PlateButton(self, wx.ID_ANY, "",
                                                       wx.Bitmap(iconSearchURL).ConvertToImage().Rescale(16, 16).ConvertToBitmap(),
                                                       style=platebtn.PB_STYLE_DEFAULT)
        self._searchExtraButton.SetBackgroundColour(wx.Colour(255, 255, 255))
        self._searchExtraButton.SetWindowVariant(wx.WINDOW_VARIANT_SMALL)
        searchSizer.Add(self._searchExtraButton, 0, wx.ALL)

        self.cp = cp = wx.CollapsiblePane(self, label=_("Advanced search"),
                                          style=wx.CP_DEFAULT_STYLE|wx.CP_NO_TLW_RESIZE)
        self.cp.SetBackgroundColour(wx.Colour(255, 255, 255))
        self._makeAdvancedSearch(self.cp.GetPane())
        #mainSizer.Add(self._search, 0, wx.ALL)
        mainSizer.Add(searchSizer, 0, wx.ALL)
        mainSizer.Add(self.cp, 0, wx.ALL)

        self.dvc = dv.DataViewCtrl(self,
                                   style=wx.BORDER_THEME
                                   | dv.DV_ROW_LINES # nice alternating bg colors
                                   #| dv.DV_HORIZ_RULES
                                   | dv.DV_VERT_RULES
                                   | dv.DV_MULTIPLE
                                   )

        self.dvc.AppendTextColumn(_("Surname Name Lastname"), 0, width=300)
        self.dvc.AppendTextColumn(_("Gender"), 1, width=50)
        self.dvc.AppendDateColumn(_("Birth date"), 2, width=200)
        self.dvc.AppendDateColumn(_("Treatment date"), 3, width=200)
        self.dvc.AppendTextColumn(_("Doctors data"), 4, width=300)
        self.dvc.AppendTextColumn(_("Notes"), 5, width=600)

        self.Bind(wx.EVT_COLLAPSIBLEPANE_CHANGED, self.onPaneChanged, cp)

        mainSizer.Add(self.dvc, 1, wx.ALL|wx.EXPAND, 10)
        self.mainSizer = mainSizer
        self.SetSizer(mainSizer)
        pass

    def onPaneChanged(self, event):
        self.Layout()
        #self.SetSizerAndFit(self.mainSizer)
        pass

    def _makeAdvancedSearch(self, pane):
        """

        @param pane:
        @type pane: wx.Window
        @return:
        """
        pane.SetBackgroundColour(wx.Colour(255, 255, 255))
        gSizer1 = wx.GridSizer( 0, 4, 8, 8 )

        self.m_staticText1 = wx.StaticText( pane, wx.ID_ANY, u"Имя", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText1.Wrap( -1 )
        gSizer1.Add( self.m_staticText1, 0, wx.ALL, 5 )

        self.scName = wx.TextCtrl( pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
        gSizer1.Add( self.scName, 0, wx.ALL, 5 )

        self.m_staticText2 = wx.StaticText( pane, wx.ID_ANY, u"Фамилия", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText2.Wrap( -1 )
        gSizer1.Add( self.m_staticText2, 0, wx.ALL, 5 )

        self.scSurname = wx.TextCtrl( pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
        gSizer1.Add( self.scSurname, 0, wx.ALL, 5 )

        self.m_staticText3 = wx.StaticText( pane, wx.ID_ANY, u"Отчество", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText3.Wrap( -1 )
        gSizer1.Add( self.m_staticText3, 0, wx.ALL, 5 )

        self.scLastname = wx.TextCtrl( pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
        gSizer1.Add( self.scLastname, 0, wx.ALL, 5 )

        self.m_staticText4 = wx.StaticText( pane, wx.ID_ANY, u"Дата рождения", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText4.Wrap( -1 )
        gSizer1.Add( self.m_staticText4, 0, wx.ALL, 5 )

        bSizer1 = wx.BoxSizer( wx.HORIZONTAL )

        self.scBirthFrom = wx.TextCtrl( pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.scBirthFrom, 0, wx.ALL, 5 )

        self.m_staticText9 = wx.StaticText( pane, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText9.Wrap( -1 )
        bSizer1.Add( self.m_staticText9, 0, wx.ALL, 5 )

        self.scBirthTo = wx.TextCtrl( pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer1.Add( self.scBirthTo, 0, wx.ALL, 5 )


        gSizer1.Add( bSizer1, 0, wx.EXPAND, 5 )

        self.m_staticText8 = wx.StaticText( pane, wx.ID_ANY, u"Дата лечения", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText8.Wrap( -1 )
        gSizer1.Add( self.m_staticText8, 0, wx.ALL, 5 )

        bSizer2 = wx.BoxSizer( wx.HORIZONTAL )

        self.scTreatFrom = wx.TextCtrl( pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer2.Add( self.scTreatFrom, 0, wx.ALL, 5 )

        self.m_staticText10 = wx.StaticText( pane, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText10.Wrap( -1 )
        bSizer2.Add( self.m_staticText10, 0, wx.ALL, 5 )

        self.scTreatTo = wx.TextCtrl( pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer2.Add( self.scTreatTo, 0, wx.ALL, 5 )


        gSizer1.Add( bSizer2, 0, wx.EXPAND, 5 )

        self.m_staticText11 = wx.StaticText( pane, wx.ID_ANY, u"Врачебный персонал", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText11.Wrap( -1 )
        gSizer1.Add( self.m_staticText11, 0, wx.ALL, 5 )

        self.scDoctors = wx.TextCtrl( pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
        gSizer1.Add( self.scDoctors, 0, wx.ALL, 5 )

        self.m_staticText12 = wx.StaticText( pane, wx.ID_ANY, u"Пол", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText12.Wrap( -1 )
        gSizer1.Add( self.m_staticText12, 0, wx.ALL, 5 )

        m_choice1Choices = []
        #self.m_choice1 = wx.Choice( pane, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice1Choices, 0 )
        #self.m_choice1.SetSelection( 0 )
        self.m_choice1 = wx.TextCtrl( pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
        gSizer1.Add( self.m_choice1, 0, wx.ALL, 5 )

        self.m_staticText13 = wx.StaticText( pane, wx.ID_ANY, u"Служебные отметки", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText13.Wrap( -1 )
        gSizer1.Add( self.m_staticText13, 0, wx.ALL, 5 )

        self.m_textCtrl13 = wx.TextCtrl( pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
        gSizer1.Add( self.m_textCtrl13, 0, wx.ALL, 5 )


        #self.SetSizer( gSizer1 )
        #self.Layout()

        border = wx.BoxSizer(wx.VERTICAL)
        border.Add(gSizer1, 1, wx.EXPAND|wx.ALL, 5)
        self._advGo = wx.Button(pane, -1, _("Go advanced search"))
        border.Add(self._advGo, 0, wx.ALL)
        pane.SetSizer(border)
        pass