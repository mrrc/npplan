# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 30.10.12
@summary: 
'''

import NPPlan

from vtk.wx.wxVTKRenderWindowInteractor import *
import wx

try:
    from agw import rulerctrl as RC
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.rulerctrl as RC


class renderWindow(wx.ScrolledWindow):
    def __init__(self, parent, parentController=None):
        """
        @param parent:
        @type parent: NPPlan.view.client.main.stages.contouring.main.contourWin
        @param parentController:
        @type parentController: NPPlan.controller.client.mainC.stages.contouring.main.contouringController
        @return:
        """
        wx.ScrolledWindow.__init__(self, parent, wx.ID_ANY)
        self._parentController = parentController

        self.vtkWindow = wxVTKRenderWindowInteractor(self, wx.NewId(), name='vtkWindow')
        self.rcTop = RC.RulerCtrl(self, -1, orient=wx.HORIZONTAL, style=wx.SUNKEN_BORDER)
        self.rcTop.LabelMinor(True)
        self.rcTop.SetLabelEdges(True)
        self.rcLeft = RC.RulerCtrl(self, -1, orient=wx.VERTICAL, style=wx.SUNKEN_BORDER)
        self.rcTop.SetFormat(RC.RealFormat)
        self.rcLeft.SetFormat(RC.RealFormat)
        self.rcTop.SetRange(-256, 256+512)
        self.rcTop.SetDrawingParent(self.vtkWindow)
        self.doLayout()

        self.vtkWindow.Bind(wx.EVT_PAINT, self.OnPaint)

    def getRulers(self):
        return {'top': self.rcTop, 'left': self.rcLeft}
    rulers = property(getRulers)

    def OnPaint(self, event):
        event.Skip()
        #print self.GetSizeTuple()
        #print self._parentController.getVtkHandler().backendOnPaintEvent(wxSize=self.GetSizeTuple())
        wx.CallLater(100, self.setInitialScreenBounds)
        event.Skip()

    def setInitialScreenBounds(self, event=None):
        # @todo: check and refactor
        #print self._parentController.getVtkHandler().getCurrentScreenBounds()
        print self._parentController.getVtkHandler().backendOnPaintEvent(wxSize=self.GetSizeTuple())
        pass

    def doLayout(self):
        sizer = wx.BoxSizer(wx.VERTICAL)
        leftsizer = wx.BoxSizer(wx.VERTICAL)
        bottomleftsizer = wx.BoxSizer(wx.HORIZONTAL)
        topsizer = wx.BoxSizer(wx.HORIZONTAL)

        leftsizer.Add((0, 0), 0, wx.ADJUST_MINSIZE, 0)
        topsizer.Add((39, 0), 0, wx.ADJUST_MINSIZE, 0)
        topsizer.Add(self.rcTop, 1, wx.EXPAND, 0)
        leftsizer.Add(topsizer, 0, wx.EXPAND, 0)

        bottomleftsizer.Add((10, 0))
        bottomleftsizer.Add(self.rcLeft, 0, wx.EXPAND, 0)
        bottomleftsizer.Add(self.vtkWindow, 1, wx.EXPAND, 0)
        leftsizer.Add(bottomleftsizer, 1, wx.EXPAND, 0)

        sizer.Add(leftsizer, 3, wx.EXPAND)

        self.SetSizer(sizer)



    def getDC(self):
        return wx.ClientDC(self.vtkWindow)

    def getVtkWindow(self):
        return self.vtkWindow

    def getVtkHandler(self):
        return self.GetParent().vtkHandler


class renderWindowOld(wx.ScrolledWindow):
    def __init__(self, parent):
        wx.ScrolledWindow.__init__(self, parent, wx.ID_ANY)

        sizer = wx.BoxSizer(wx.VERTICAL)

        self.vtkWindow = wxVTKRenderWindowInteractor(self, wx.NewId(), name='vtkWindow')
        sizer.Add(self.vtkWindow, 1, wx.EXPAND)
        self.SetSizer(sizer)