# -*- coding: utf8 -*-
'''
Окно "изодозы"
@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 18.03.14
@summary: 
'''

import wx
from vtk.wx.wxVTKRenderWindowInteractor import *


class dim2doseView(wx.Panel):
    def __init__(self, parent, cParent):
        wx.Panel.__init__(self, parent)
        sizer = wx.BoxSizer(wx.VERTICAL)
        self._cParent = cParent

        self.__dict__['vtkWindow'] = wxVTKRenderWindowInteractor(self, wx.NewId(), name='viewer')
        sizer.Add(self.__dict__['vtkWindow'], 1, wx.EXPAND)

        prevBtn = wx.Button(self, -1, _("prev slice"))
        nextBtn = wx.Button(self, -1, _("Next slice"))

        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer.Add(prevBtn)
        btnSizer.Add(nextBtn)

        sizer.Add(btnSizer)

        self.SetSizer(sizer)
        self.Bind(wx.EVT_PAINT, self.onPaint)
        self.Bind(wx.EVT_BUTTON, self.prevPressed, prevBtn)
        self.Bind(wx.EVT_BUTTON, self.nextPressed, nextBtn)

    def prevPressed(self, event):
        self._cParent.prevDose()
        pass

    def nextPressed(self, event):
        self._cParent.nextDose()
        pass

    def onPaint(self, event):
        """

        @param event:
        @type event: wx._core.PaintEvent
        @return:
        """
        self._cParent.drawIsodoses(self.__dict__['vtkWindow'])
        event.Skip()
        pass
