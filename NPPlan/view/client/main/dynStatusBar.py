# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 10.09.12
@summary: 
'''

import NPPlan

import wx
# STT работает неправильно когда внизу (или исправить позиционирование)
#import wx.lib.agw.supertooltip as STT
import wx.lib.agw.balloontip as BT
import wx.animate
from twisted.internet import reactor


class dynStatusBar(wx.StatusBar):
    # self.handler.main.controller.sb.
    def __init__(self, parent):
        wx.StatusBar.__init__(self, parent, -1)

        self.SetFieldsCount(8)
        # 0 - text
        # 1 - database status
        # 2 - informer status
        # 3 - NPPlan server status
        # 4 - ?
        # 5 - local tasks status
        # 6 - ?
        # 7 - open local system log
        self.SetStatusWidths([-5, -1, -1, -1, -1, -1, -3, -1])
        #self.SetStatusText("A Custom StatusBar...", 0)

        #print NPPlan.getIconPath()
        iconPath = NPPlan.getIconPath()
        self._bmps = {
            'dbOn': NPPlan.getIcon('iconStatusDbOn.png'),
            'dbOff': NPPlan.getIcon('iconStatusDbOff.png'),
            'infOn': NPPlan.getIcon('iconStatusInfoOn.png'),
            'infOff': NPPlan.getIcon('iconStatusInfoOff.png'),
        }

        bmpDb = wx.Bitmap(self._bmps['dbOff'], wx.BITMAP_TYPE_ANY)
        self._imDb = wx.StaticBitmap(self, -1, bmpDb, (80, 50), (bmpDb.GetWidth(), bmpDb.GetHeight()))
        #self._dbTip = STT.SuperToolTip('Test')
        self._dbTip = BT.BalloonTip(message='123')
        #self._dbTip.SetHeader('Database connection status')
        self._dbTip.SetTarget(self._imDb)
        self._dbTip.SetBalloonTitle(_('Database connection status'))
        self._dbTip.SetTitleColour(wx.Colour(0, 0, 0))
        self._dbTip.SetBalloonShape(BT.BT_RECTANGLE)
        self._dbTip.SetTitleFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.FONTWEIGHT_BOLD, False))
        #self._dbTip.SetBalloonMessage('')
        #self._dbTip.ApplyStyle('Office 2007 Blue')

        #self._workImage = throb.Throbber(self, -1, None, size=(20, 20), frameDelay = 0.1)
        ani = wx.animate.Animation(NPPlan.pathJoin(NPPlan.getIconPath(), 'progressSmall.gif'))
        self._workImage = wx.animate.AnimationCtrl(self, -1, ani)
        self._workImage.SetUseWindowBackgroundColour()
        self._workImage.Play()
        #self._workImage.
        #self._workImage.Hide()
        rectWi = self.GetFieldRect(5)
        self._workImage.SetPosition((rectWi.x + 2, rectWi.y + 2))
        self._workImage.SetSize((rectWi.width - 4, rectWi.height - 4))

        bmpInf = wx.Bitmap(self._bmps['infOff'], wx.BITMAP_TYPE_ANY)
        self._imInf = wx.StaticBitmap(self, -1, bmpInf, (80, 50), (bmpInf.GetWidth(), bmpInf.GetHeight()))
        self.reposition()

    def setStatus(self, db=None, inf=None):
        if 'on' == db:
            bmpDb = wx.Bitmap(self._bmps['dbOn'], wx.BITMAP_TYPE_ANY)
            self._imDb.SetBitmap(bmpDb)
        elif 'off' == db:
            bmpDb = wx.Bitmap(self._bmps['dbOff'], wx.BITMAP_TYPE_ANY)
            self._imDb.SetBitmap(bmpDb)
        if 'on' == inf:
            bmpInf = wx.Bitmap(self._bmps['infOn'], wx.BITMAP_TYPE_ANY)
            self._imInf.SetBitmap(bmpInf)
        elif 'off' == inf:
            bmpInf = wx.Bitmap(self._bmps['infOff'], wx.BITMAP_TYPE_ANY)
            self._imInf.SetBitmap(bmpInf)
        self.reposition()

    def setDbTipText(self, text):
        #self._dbTip.SetMessage(text)
        self._dbTip.SetBalloonMessage(text)

    def reposition(self):
        rectDb = self.GetFieldRect(1)
        self._imDb.SetPosition((rectDb.x + 2, rectDb.y + 2))
        self._imDb.SetSize((rectDb.width - 4, rectDb.height - 4))

        rectInf = self.GetFieldRect(2)
        self._imInf.SetPosition((rectInf.x + 2, rectInf.y + 2))
        self._imInf.SetSize((rectInf.width - 4, rectInf.height - 4))
