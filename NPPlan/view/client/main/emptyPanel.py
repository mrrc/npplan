# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 17.09.12
@summary: 
'''

import wx

class emptyPanel(wx.Panel):
    def __init__(self, parent, *args, **kwargs):
        wx.Panel.__init__(self, parent, style=0)#|wx.BORDER_SUNKEN)
        self.SetBackgroundColour(wx.Colour(255,255,255))
        self.created = False

    def isCreated(self):
        return self.created

    def setCreated(self):
        self.created = True

    def unsetCreated(self):
        self.created = False